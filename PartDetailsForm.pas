unit PartDetailsForm;

interface

uses
// Delphi units
  Windows, SysUtils, Variants, Classes, Controls, Forms, StdCtrls, DB, CommCtrl,
  DBCtrls, Dialogs, XPMan, Buttons, Grids, ExtCtrls, ComCtrls, DBGrids, ImgList,
  Graphics,
// Third Party
  sSkinProvider, sBitBtn, sPanel, sDBLookupComboBox, sDBNavigator,
  sGroupBox, sEdit, RXDBCtrl, acAlphaImageList, DBCtrlsEh,
// Own units
  Common, ArsenalDB, ChildForms, sMemo, sSpeedButton;

type

  TPartsForm = class(TCommonChild)
  private
    ID_Part: TID;
  protected
    procedure AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity); override;
    procedure SpecModifying(ADataSet: TDataSet; FldCnt: Integer;
      FieldName: string; IsLookup: Boolean = False); override;
    procedure OperationChange(Action: TButtonType; Value: Boolean); override;
    procedure InsertCmd; override;
    procedure UpdateCmd; override;
    procedure Cmd(IsUpd: Boolean);
    procedure PartChanged(DataSet: TDataSet);
    procedure GridDblClick(Sender: TObject);
  public
    procedure Start; override;
    procedure Stop(NeedsSave: Boolean = False); override;
  end;

  TPartDetailsForm = class(TBaseForm)
  published
    edtCaption: TsEdit;
    cbbVendor: TsDBLookupComboBox;
    edtVisualN: TsEdit;
    edtPrice: TsEdit;
    cbbBill: TsDBLookupComboBox;
    sPanel1: TsPanel;
    ButtonsPanel: TsPanel;
    gbCaptionEditor: TsGroupBox;
    MovingsDBNav: TsDBNavigator;
    DetailsPanel: TsPanel;
    EquipmentTypeList: TsDBLookupComboBox;
    edtSettings: TsMemo;
    MovingsGroupBox: TsGroupBox;
    MovingsGrid: TDBGridUsed;
    ControlPanel: TsPanel;
    btnOK: TsBitBtn;
    sSkinProvider1: TsSkinProvider;
    XPManifest1: TXPManifest;
    sBitBtn2: TsBitBtn;
    procedure MovingsGridColEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    FMode: TJobMode;
    FID: TID;
    DSMovings: TDataSet;
    SrcMovings: TDataSource;
    Btns: array[TSpecAction] of TButtonUsed;
    ID_BuyMovingType: TID;
  protected
    DateEditor: TDaterUsed;
    procedure MovingPost(DataSet: TDataSet);
    procedure DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure MovingsGridColExit(Sender: TObject);
    procedure SetupButtons;
    procedure ApplyAction(Sender: TObject);
    procedure SetBtnEnabled(Btn: TSpecAction; DoEnable: Boolean);
    procedure FormResize(Sender: TObject); override;
    function CanShowDateEditor(WithBrowse: Boolean = True): Boolean;
    procedure ObtainData; override;
    procedure RequeryMovings;
  public
    function Open(AMode: TJobMode; AID: TID): TModalResult;
    property Mode: TJobMode read FMode;
    property ID: TID read FID;
  end;

implementation

{$R *.dfm}

uses
// Delphi
  DateUtils,
//Own
  MainForm;

const
  SDetails = '����������� �� ��������������';


{$REGION ' TPartDetailsForm '}

procedure TPartDetailsForm.MovingPost(DataSet: TDataSet);
var
  BM: TBookmark;
  IsNew: Boolean;
begin
  BM := DSMovings.GetBookmark;
  DSMovings.AfterPost := nil;
  DSMovings.First;
  IsNew := False;
  while not DSMovings.Eof do
  begin
    IsNew := SameID(DSMovings.FieldValues['ID'], ID_BuyMovingType) and
      SameID(VarToStrDef(DSMovings.FieldValues['ID_Accepter'], ID_NULL), ID_NULL);
    if IsNew then
      Break;
    DSMovings.Next;
  end;
//  SetBtnEnabled(saAcceptDevice, IsNew);
  GotoBookmark(DSMovings, BM);
  DSMovings.AfterPost := MovingPost;
end;

function TPartDetailsForm.CanShowDateEditor(WithBrowse: Boolean = True): Boolean;
var
  States: set of TDataSetState;
begin
  States := [dsInsert, dsEdit];
  if WithBrowse then
    States := States + [dsBrowse];
  Result := (DSMovings.State in States) and (MovingsGrid.SelectedField.DataType = ftDateTime);
end;

procedure TPartDetailsForm.MovingsGridColEnter(Sender: TObject);
var
  Rect: TRect;
begin
  if CanShowDateEditor then
  begin
    Rect := TCustomDrawGrid(MovingsGrid).CellRect(MovingsGrid.Col, MovingsGrid.Row);
    DateEditor.Left := Rect.Left + MovingsGrid.Left + 1;
    DateEditor.Top := Rect.Top + MovingsGrid.Top + 1;
    DateEditor.Width := Rect.Right - Rect.Left + 2;
//    DateEditor.Value := MovingsGrid.SelectedField.Value;// TernOP(MovingsGrid.SelectedField.IsNull, MovingsGrid.SelectedField.AsDateTime, Now);
    DateEditor.DataSource := SrcMovings;
    DateEditor.DataField := MovingsGrid.SelectedField.FieldName;
//    MovingsGrid.InplaceEditor := DateEditor;
    DateEditor.Show;
    DateEditor.BringToFront;
  end;
end;

procedure TPartDetailsForm.MovingsGridColExit(Sender: TObject);
begin
  if CanShowDateEditor(True)then
  begin
    DateEditor.DataSource := nil;
    DateEditor.Hide;
//    MovingsGrid.SelectedField.Value := DateEditor.Value;
  end;
end;

procedure TPartDetailsForm.FormCreate(Sender: TObject);
begin
  Caption := SDetails;
  gbCaptionEditor.Caption := Russian('Settings');
  MovingsGroupBox.Caption := Russian('Movings');
  edtVisualN.BoundLabel.Caption := Russian('VisualNumber');
  edtPrice.BoundLabel.Caption := Russian('Price');
  edtCaption.BoundLabel.Caption := Russian('Product');
  EquipmentTypeList.BoundLabel.Caption := Russian('Type');
  cbbVendor.BoundLabel.Caption := Russian('Vendor');
  cbbBill.BoundLabel.Caption := Russian('Bill');
end;

procedure TPartDetailsForm.FormResize(Sender: TObject);
begin
  inherited;
  DateEditor.Hide;
end;

procedure TPartDetailsForm.ObtainData;
begin
  FDataSet := Manager.GetData(xePart);
end;

function TPartDetailsForm.Open(AMode: TJobMode; AID: TID): TModalResult;
var
  CanInsert, CanUpdate: Boolean;
  DSWorkplaces, DSMovingTypes: TDataSet;
  ID_Stock: TID;  
begin
  Result := mrNone;

  DSMovingTypes := Manager.GetData(xeMovingType);
  try
    DSMovingTypes.Locate('Name', 'Buy', [loCaseInsensitive]);
    ID_BuyMovingType := DSMovingTypes.FieldValues['ID'];
    if ID_BuyMovingType = ID_NULL then
      Abort;
  except
    ShowMessage('�� ������� "���������������" �������� !');
    Close;
    Exit;
  end;
  
  DSWorkplaces := Manager.GetData(xeWorkplace);
  try
    DSWorkplaces.Locate('Name', 'Stock', [loCaseInsensitive]);
    ID_Stock := DSWorkplaces.FieldValues['ID'];
    if ID_Stock = ID_NULL then
      Abort;
  except
    ShowMessage('�� ������� "���������" ������� ����� !');
    Close;
    Exit;
  end;

  Start;
  FID := AID;
  FMode := AMode;

  ArsenalInstance.Switch(xePart);
  DataSource := ArsenalInstance.DS;
  FDataSet := DataSource.DataSet;
//  ObtainData;

  Lookup(DataSet, EquipmentTypeList, xeEquipmentType, 'ID_EquipmentType', 'Caption', []);
  Lookup(DataSet, cbbVendor, xeVendor, 'ID_Vendor', 'Caption', []);
  Lookup(DataSet, cbbBill, xeBill, 'ID_Bill', 'Caption', []);

  edtSettings.Text := VarToStrDef(DataSet.FieldValues['Settings'], '');
  edtCaption.Text := VarToStrDef(DataSet.FieldValues['Product'], '');
  edtVisualN.Text := VarToStrDef(DataSet.FieldValues['VisualNumber'], '');
  edtPrice.Text := VarToStrDef(DataSet.FieldValues['Price'], '');

  if Mode = jmAdd then
    Manager.Insert(xeMoving, [ID_Stock, ID_BuyMovingType, ID_NULL, ID,
      Manager.Portfolio[pdID], IncMinute(Now, -1)]);

  DateEditor := TDaterUsed.Create(Self);
  DateEditor.Parent := {MovingsGrid;//}MovingsGroupBox;
  DateEditor.Hide;
  DateEditor.Kind := dtkDateTimeEh;
  DateEditor.EditFormat := DateTimeFormat;
  
  RequeryMovings;

  MovingsGrid.OnColExit := MovingsGridColExit;
  //MovingsGrid.DefaultDrawing := False;
//  MovingsGrid.OnDrawColumnCell := MovingsGridDrawColumnCell;

  MovingsDBNav.DataSource := SrcMovings;

  CanInsert := ArsenalInstance.EntityActionAccessible[xeMoving, daInsert];
  CanUpdate := ArsenalInstance.EntityActionAccessible[xeMoving, daUpdate];
  ShowDBNavButton(MovingsDBNav, nbInsert, CanInsert);
  ShowDBNavButton(MovingsDBNav, nbEdit, CanUpdate);
  ShowDBNavButton(MovingsDBNav, nbPost, CanInsert or CanUpdate);
  ShowDBNavButton(MovingsDBNav, nbDelete, ArsenalInstance.EntityActionAccessible[xeMoving, daDelete]);

  MovingsDBNav.BeforeAction := DBNavBeforeAction;
  MovingsDBNav.Tag := Ord(xeMoving);

  SetupGrid(MovingsGrid);
  SetupButtons;

//  DSMovings.AfterPost := MovingPost;
  Result := ShowModal;
//  DSMovings.AfterPost := nil;
end;

procedure TPartDetailsForm.RequeryMovings;
begin
  if Assigned(DSMovings) then
    DSMovings.DisableControls;
  DSMovings := Manager.GetData(xeMoving, [ID_NULL, ID_NULL, ID_NULL, ID]);
  SrcMovings := ArsenalInstance.DS;
  ModifyDataSet(DSMovings, xeMoving);
  DSMovings.FieldByName('Equipment').Visible := False;
  DSMovings.FieldByName('Part').Visible := False;
  if ArsenalInstance.EntityPersonalized[xePerson] then
    case Mode of
      jmAdd: DSMovings.FieldByName('Person').Visible := True;
      jmEdit: DSMovings.FieldByName('Person').ReadOnly := True;
    end;
  MovingsGrid.DataSource := SrcMovings;
  DSMovings.EnableControls;
  ManageColumns(DSMovings, MovingsGrid);
  Resize;
end;

procedure TPartDetailsForm.ApplyAction(Sender: TObject);
var
  Btn: TButtonUsed absolute Sender;
  BtnAction: TSpecAction;
begin
  if not (Sender is TButtonUsed) then
    Exit;
  BtnAction := TSpecAction(Btn.Tag);
  Manager.SpecAction(BtnAction, [ID, Manager.Portfolio[pdID]], True);
  MovingPost(DSMovings);
  RequeryMovings;
  Resize;
end;

procedure TPartDetailsForm.DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
var
  DBNav: TsDBNavigator absolute Sender;
  Entity: TEntity;
  DS: TDataSet;
  function V(Name: string): Variant;
  begin
    if (Name = 'ID_Person') and ArsenalInstance.EntityPersonalized[xePerson]
      and (Mode = jmAdd) then
      Result := Manager.Portfolio[pdID]
    else
      Result := DS.FieldValues[Name];
  end;
begin
  if not (Sender is TsDBNavigator) then
    Exit;
  if not (Button in [nbPost, nbDelete]) then
    Exit;
  DS := DBNav.DataSource.DataSet;
  Entity := TEntity(DBNav.Tag);
  if Entity = xeMoving then
    if DS.State = dsInsert then
      Manager.Insert(Entity, [V('ID_Workplace'), V('ID_MovingType'),
        ID_NULL, ID, V('ID_Person'), V('Date'), V('Note')])
    else if DS.State = dsEdit then
      if Manager.Update(Entity, [V('ID'), V('ID_Workplace'), V('ID_MovingType'),
        ID_NULL, ID, V('ID_Person'), V('Date'), V('Note')]) <> 0
      then
        ShowMessage('������ ������� �������� ��������������, ��������, ������ � ��������� �����');
  if Entity = xeMoving then
    RequeryMovings;
  Resize;
  Abort; // cancel standard dbnav action
end;

procedure TPartDetailsForm.btnOKClick(Sender: TObject);
begin
  if CondHint(edtCaption, '������� ������') or CondHint(edtPrice, '������� ����')
    or CondHint(edtVisualN, '������� ���������� �����')
    or CondHint('������� ���', VarIsNull(EquipmentTypeList.KeyValue))
  then
    Exit;
  ModalResult := mrOk;
end;

procedure TPartDetailsForm.SetBtnEnabled(Btn: TSpecAction; DoEnable: Boolean);
begin
  if Assigned(Btns[Btn]) then
    Btns[Btn].Enabled := DoEnable; 
end;

procedure TPartDetailsForm.SetupButtons;
const
  BtnCaptions: array[saNone..saNone] of string =
    ('�������');
var
  BtnAction: TSpecAction;
begin
  ButtonsPanel.Visible := False;
  for BtnAction := Low(BtnCaptions) to High(BtnCaptions) do
    if (SpecActionToEntity[BtnAction] = xeMoving) and ArsenalInstance.SpecActionAccessible[BtnAction] then
    begin
      ButtonsPanel.Visible := True;    
      Btns[BtnAction] := TButtonUsed.Create(Self);
      Btns[BtnAction].Parent := ButtonsPanel;
      Btns[BtnAction].AlignWithMargins := True;
      Btns[BtnAction].Margin := 2;
      Btns[BtnAction].Left := Btns[BtnAction].Parent.Width;
      Btns[BtnAction].Align := alLeft;
      Btns[BtnAction].Width := 65;
      Btns[BtnAction].Tag := Ord(BtnAction);
      Btns[BtnAction].Caption := BtnCaptions[BtnAction];
//      Btns[TSpecAction].AllowAllUp := TSpecAction in [btFilter, btInsert, btUpdate];
//      Btns[TSpecAction].GroupIndex := Ord(Btns[TSpecAction].AllowAllUp) * (Ord(TSpecAction) + 1);
      Btns[BtnAction].OnClick := ApplyAction;
//      Btns[TSpecAction].Images := MainFormInstance.ButtonIcons;
//      Btns[TSpecAction].ImageIndex := Ord(TSpecAction);
    end;
end;

{$ENDREGION ' TPartDetailsForm '}

{$REGION ' TPartsForm '}

procedure TPartsForm.Cmd(IsUpd: Boolean);
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  SL.Assign(SG.Rows[DataRowNumber]);
  SL.Delete(SG.Rows[HeaderRowNumber].IndexOf(Russian('Installed')));
  if IsUpd then
    Manager.Update(Entity, [DataSet.FieldByName('ID').Value, ID_NULL], SL)
  else
    Manager.Insert(Entity, [ID_NULL], SL);
  FreeAndNil(SL);
end;

procedure TPartsForm.GridDblClick(Sender: TObject);
begin
  if DBGrid1.ScreenToClient(Mouse.CursorPos).y > TGridUsed(DBGrid1).RowHeights[0] then
    IsUpdating := True;
end;

procedure TPartsForm.InsertCmd;
begin
  Cmd(False);
end;

procedure TPartsForm.OperationChange(Action: TButtonType; Value: Boolean);
var
  Details: TPartDetailsForm;
  Ico: TIcon;
  Mode: TJobMode;
  d: Double;
  n: Integer;
  ID_Equipment: TID;
  V: Variant;
begin
  inherited;
  SetBtnEnabled(btApply, IsUpdating or Inserting);
  SetBtnEnabled(btClear, IsUpdating or Inserting or Filtering);
  if Filtering then
    Exit;
  if (Action = btFilter) and not Value and not IsUpdating then
  begin
    Requery;
    ManageColumns(DataSet, DBGrid1);
    Resize;
    Exit;
  end;
  if not Value then
    Exit;
  Details := TPartDetailsForm.Create(Application);
  begin
    Ico := TIcon.Create;
    MainFormInstance.TreeIcons.GetIcon(Ord(Entity), Ico);
    Details.Icon := Ico;
    FreeAndNil(Ico);
  end;
  if Inserting then
  begin
    ID_Part := Manager.Insert(xePart, []);
    ManageColumns(DataSet, DBGrid1);
  end;
  V := DataSet.FieldByName('ID_Equipment').Value;
  if VarIsNull(V) then
    ID_Equipment := ID_NULL
  else
    ID_Equipment := V;
  Mode := TernOp(Inserting, jmAdd, TernOp(IsUpdating, jmEdit, jmView));
  if Details.Open(Mode, ID_Part) = mrOk then
  begin
    with Details do
      if Manager.Update(xePart, [ID_Part, ID_Equipment, EquipmentTypeList.KeyValue,
        cbbBill.KeyValue, cbbVendor.KeyValue, edtCaption.Text,
        TernOp(edtVisualN.Text <> '', edtVisualN.Text, ID_NULL),
        TernOp(TryStrToFloat(edtPrice.Text, d), edtPrice.Text, ID_NULL),
        edtSettings.Text]) <> 0
      then
        if Inserting then
        begin
          ShowMessage(Format(SFErrorIn, ['�������']) + #13#10 + SRepeatedNumber);
          Manager.Delete(xePart, [ID_Part]);
        end;
  end
  else
    if Inserting then
      Manager.Delete(xePart, [ID_Part]);
  ClearClick(nil);
  Details.Free;
  Requery;
  ManageColumns(DataSet, DBGrid1);
  Resize;
end;

procedure TPartsForm.PartChanged(DataSet: TDataSet);
begin
  ArsenalInstance.Switch(xePart);
  ID_Part := ArsenalInstance.CurrentID;
end;

procedure TPartsForm.UpdateCmd;
begin
  Cmd(True);
end;

procedure TPartsForm.SpecModifying(ADataSet: TDataSet; FldCnt: Integer;
  FieldName: string; IsLookup: Boolean);
begin
  if (FieldName = 'ID_Equipment') or (FieldName = 'Caption') then
    FDataSet.FieldDefs.Find(FieldName).Attributes := FDataSet.FieldDefs.Find(FieldName).Attributes + [faHiddenCol];
  inherited;
end;

procedure TPartsForm.Start;
begin
  inherited;
  DataSet.AfterScroll := PartChanged;
  ManageColumns(DataSet, DBGrid1);
  DBGrid1.DataSource := DataSource;
  DBGrid1.OnDblClick := GridDblClick;
  ClearClick(nil);
  Resize;
end;

procedure TPartsForm.Stop(NeedsSave: Boolean = False);
begin
  inherited;
  DataSet.AfterScroll := nil;
end;

procedure TPartsForm.AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity);
begin
  inherited;
  FDataSet.FieldByName('Equipment').Visible := False;
  FDataSet.FieldByName('Caption').Visible := False;
end;

{$ENDREGION ' TPartsForm '}

end.

