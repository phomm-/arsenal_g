unit SpecialForms;

interface

uses
// Delphi units
  Windows, Classes, SysUtils, Forms, Controls, StdCtrls, Dialogs, DB, Variants,
  Grids, DBGrids, 
// Own units
  ArsenalDB, Common, ChildForms;

type

  TBillsForm = class(TCommonChild)
  protected
    procedure AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity); override;
  end;

  TOrganisationForm = class(TCommonChild)
  protected
    procedure AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity); override;
  end;

  TDepartmentForm = class(TCommonChild)
  protected
    procedure AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity); override;
  end;

  TWorkersForm = class(TCommonChild)
  protected
    procedure AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity); override;
  end;

  TWorkPlacesForm = class(TCommonChild)
  private
    BtnReport: TButtonUsed;
    ID: TID;
  protected
    procedure DoReport(Sender: TObject);
    procedure AfterScroll(ADataset: TDataSet);
  public
    procedure Start; override;
    procedure Stop(NeedsSave: Boolean = False); override;
  end;

implementation

uses
// Own
  Reports, MainForm;

{$REGION ' TBillsForm '}

procedure TBillsForm.AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity);
begin
  inherited;
  HideField(ADataSet, 'Caption');
end;

{$ENDREGION ' TBillsForm '}

{$REGION ' TOrganisationForm '}

procedure TOrganisationForm.AfterModifyDataSet(ADataSet: TDataSet;
  Entity: TEntity);
begin
  inherited;
  HideField(ADataSet, 'PurposeName');
end;

{$ENDREGION ' TOrganisationForm '}

{$REGION ' TDepartmentForm '}

procedure TDepartmentForm.AfterModifyDataSet(ADataSet: TDataSet;
  Entity: TEntity);
begin
  inherited;
  DataSet.FieldByName('Organisation').LookupDataSet.Filter := 'PurposeName = ''Buyer''';
  DataSet.FieldByName('Organisation').LookupDataSet.Filtered := True;
end;

{$ENDREGION ' TDepartmentForm '}

{$REGION ' TWorkersForm '}

procedure TWorkersForm.AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity);
begin
  inherited;
  DataSet.FieldByName('Organisation').LookupDataSet.Filter := 'PurposeName = ''Buyer''';
  DataSet.FieldByName('Organisation').LookupDataSet.Filtered := True;
end;

{$ENDREGION ' TWorkersForm '}

{$ENDREGION ' TWorkPlacesForm '}

procedure TWorkPlacesForm.AfterScroll(ADataset: TDataSet);
begin
  ArsenalInstance.Switch(xeWorkPlace);
  ID := ArsenalInstance.CurrentID;
end;

procedure TWorkPlacesForm.DoReport(Sender: TObject);
begin
  if not SameID(ID, ID_NULL) then
    WPReport(ID);
end;

procedure TWorkPlacesForm.Start;
begin
  inherited;
  DataSet.AfterScroll := AfterScroll;
  Panel1.Height := 130;
  btnReport := TButtonUsed.Create(Self);
  btnReport.Parent := Panel1;
  btnReport.Caption := ReportInfo[Ord(saWPReport) - 1].Name;
  btnReport.Width := 130;
  btnReport.Align := alRight;
  btnReport.Images := MainFormInstance.TreeIcons;
  btnReport.ImageIndex := Ord(xeReport);
  btnReport.OnClick := DoReport;
end;

procedure TWorkPlacesForm.Stop(NeedsSave: Boolean = False);
begin
  inherited;
  DataSet.AfterScroll := nil;
end;

{$ENDREGION ' TWorkPlacesForm '}

end.

