unit ArsenalDB;

interface

uses
// Delphi
  SysUtils, Classes, DB, ADODB, Variants,
// Thirdparty

// Own
  Common;

type

// Common is for basic actions, must be first, not used in UI
  TEntity = (xeCommon, xeEquipmentType, xeMovingType, xeWorker, xeDepartment,
    xeOrganisation, xeWorkPlace, xeVendor, xeOperation, xeJob, xeJobOperation,
    xePerson, xePersonJob, xeEquipment, xePart, xeStructure, xeInventarisation,
    xeResult, xeBill, xeReport, xeMoving, xePurpose);

  TDataAction = (daSelect, daInsert, daUpdate, daDelete);

  TSpecAction = (saNone, saEquipmentReport, saBarcodeReport, saWPReport, saMovingReport,
    saInvReport, saForDisposeReport, saDisposedReport);//, saStartInv, saEndInv); {TODO:}

  TPortfolioData = (pdID, pdLogin, pdJob, pdFirstName, pdLastName, pdMiddleName,
    pdDepartment, pdOrganisation);

  NSearchMode = (smName, smID, smObj);

const
  EntityCount = Ord(High(TEntity)) + 1;
  ActionCount = Ord(High(TDataAction)) + 1;
  DictionaryEntities = [xeEquipmentType, xeMovingType, xeDepartment,
    xeOrganisation, xeVendor, xeWorkPlace, xeWorker];
  SystemEntities = [xeOperation, xeJob, xeJobOperation, xePerson, xePersonJob];
  ManagableEntities = [xeOperation, xePerson, xeJob, xeDepartment, xePersonJob,
    xeJobOperation, xeVendor, xePart, xeWorkPlace, xeOrganisation, 
    xeEquipment, xeMovingType, xeInventarisation, xeEquipmentType, xeBill,
    xeStructure, xeWorker];
  WorkingEntities = [xeOperation, xePerson, xeJob, xeDepartment, xePersonJob,
    xeJobOperation, xeVendor, xePart, xeWorkPlace, xeOrganisation, xeResult,
    xeEquipment, xeMovingType, xeInventarisation, xeEquipmentType, xeBill,
    xeStructure, xeWorker, xePurpose];

  SpecActionToEntity: array[TSpecAction] of TEntity =
    (xeCommon, xeCommon, xeCommon, xeCommon, xeCommon, xeCommon, xeCommon, xeCommon);

  UseDictionary: Boolean = True;
  UsePascalStyleSysObjectNaming: Boolean = False;
  DenyInvAltering: Boolean = False;

  EanFontFName = 'eanbwrp36tt.ttf';

type

  RProcAccess = record
    Name: string;
    Available: Boolean;
    Personal: Boolean;
  end;

  Allies = 0..2;

  TArsenalDB = class(TDataModule)
    ADOConn: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    CurrentEntity: TEntity;
    SPs: array[TEntity, Allies] of TADOStoredProc;
    DSs: array[TEntity, Allies] of TDataSource;
    FQuery: TADOQuery;
    FProcNames: array[Byte] of RProcAccess;
    function GetSP: TADOStoredProc;
    function GetEntityAccessible(Entity: TEntity): Boolean;
    function GetEntityActionAccessible(Entity: TEntity;
      Action: TDataAction): Boolean;
    function GetProcName(Entity: TEntity; Action: TDataAction): RProcAccess;
    function GetSpecProc(Action: TSpecAction): string;
    function GetSpecActionAccessible(Action: TSpecAction): Boolean;
    function GetEntityPersonalized(Entity: TEntity): Boolean;
  protected
    property Query: TADOQuery read FQuery;
    property SP: TADOStoredProc read GetSP;
    function GetID: TID;
    function GetInsertedID(Entity: TEntity): TID;
    function GetCurrentDS: TDataSource;
    procedure SetupAccess;
    procedure Exec(Entity: TEntity; Str: string; ToSet: Boolean = False);
    function ExecSP(Entity: TEntity; Name: string; const Params1: array of const;
      Params2: TStrings = nil; ToSet: Boolean = False): Integer;
    property ProcNames[Entity: TEntity; Action: TDataAction]: RProcAccess read GetProcName;
    procedure FetchImagesToList(Entity: TEntity; const List: TStrings; FieldName: string);
    procedure LoadPictureToBitmap(Bitmap: TPictureUsed; DataSet: TDataSet; FieldName: string);
    procedure UploadPicture(Entity: TEntity; Bmp: TPictureUsed; ParamName: string = ''; Index: Integer = 0);
  public
    Ally: Allies;    
    procedure FetchToList(Entity: TEntity; const List: TStrings; FieldName: string);
    procedure Switch(Entity: TEntity);
    property DS: TDataSource read GetCurrentDS;
    property CurrentID: TID read GetID;
    property SpecActionAccessible[Action: TSpecAction]: Boolean read GetSpecActionAccessible;
    property EntityAccessible[Entity: TEntity]: Boolean read GetEntityAccessible;
    property EntityActionAccessible[Entity: TEntity; Action: TDataAction]: Boolean
      read GetEntityActionAccessible;
    property EntityPersonalized[Entity: TEntity]: Boolean read GetEntityPersonalized;
    property SpecProcName[Action: TSpecAction]: string read GetSpecProc;
  end;

  TConnect = class(TObject) // maybe TPersistent for serialization
  private
    FConnectData: TConnectData;
    FADOConnection : TADOConnection;
    FTries: Integer;
    function GetFirstTry: Boolean;
    function GetSecondTry: Boolean;
  protected
    function GetConnectData(Idx: TConnectInfo): string;
    procedure SetConnectData(Idx: TConnectInfo; const Value: string);
  public
    property ConnectData[Idx: TConnectInfo]: string read GetConnectData
    write SetConnectData; default;
    function LoadConnection(): Boolean;
    function SaveConnection(AConnData: TConnectData): Boolean;
    function ConnectToDB(): Boolean;
    property ConnData : TConnectData read FConnectData;
    constructor Create(ADOConnection: TADOConnection);
    property FirstTry: Boolean read GetFirstTry;
    property SecondTry: Boolean read GetSecondTry;
  end;

  TManager = class(TObject) // maybe some sort of TComponent
  private
    FConnect: TConnect;
    FImages: TOOStringList;
    FPortfolio: array[TPortfolioData] of string;
    function GetPortfolio(Index: TPortfolioData): string;
    function GetImage(Index: Integer): TImageData;
    function GetImagesCount: Integer;
    function GetPersonFIO: string;
  protected
    procedure ClearImages;
  public
    procedure GetImages();
    constructor Create();
    destructor Destroy(); override;
    property Connect: TConnect read FConnect;
    procedure LoadData;
    function GetData(Entity: TEntity; AProcName: string = ''): TDataSet; overLoad;
    function GetData(Entity: TEntity; Params: array of const; AProcName: string = ''): TDataSet; overLoad;

    function Insert(Entity: TEntity; const Params1: array of const;
      Params2: TStrings = nil; WithRefresh: Boolean = True; SelectParam1: string = ''): TID;
    function Update(Entity: TEntity; {ID: TID;} const Params1: array of const;
      Params2: TStrings = nil; WithRefresh: Boolean = True): Integer;
    procedure Delete(Entity: TEntity; Params: array of const; WithRefresh: Boolean = True);
    procedure SpecAction(Action: TSpecAction; const Params1: array of const;
      WithRefresh: Boolean = True);
    property Portfolio[Index: TPortfolioData]: string read GetPortfolio;
    function PesonalID(Entity: TEntity): TID;
    function FindImage(out Idx: Integer; Mode: NSearchMode = smName;
      Name: string = ''; ID: string = ''; Obj: TImageData = nil): Boolean;
    property ImagesCount: Integer read GetImagesCount;
    property Images[Index: Integer]: TImageData read GetImage;
    function AddImage(Name: string; Picture: TPictureUsed; FileSize: Integer): Integer;
    procedure CommitImages;
    property PersonFIO: string read GetPersonFIO;
  end;

procedure Lookup(DS: TDataSet; CB: TComboUsed; Entity: TEntity; Field: string;
  ListField: string; Params: array of const; NeedKV: Boolean = True);
procedure HideField(ADataSet: TDataSet; Field: string);
function IsFieldHidden(ADataSet: TDataSet; Field: string): Boolean;

function EntityName(Entity: TEntity): string;

function EntP: Pointer;
function ActP: Pointer;
function SpActP: Pointer;

function Russian(FieldName: string): string;
procedure GotoBookMark(DataSet: TDataSet; BM: Pointer);
function CheckBarcodePrintReady: Boolean;

// singletone emulation
function Manager: TManager;
function ArsenalInstance: TArsenalDB;    

//------------------------------------------------------------------------------
implementation

uses
  Forms, Dialogs, Windows, Messages;

{$R *.dfm}

function EntityName(Entity: TEntity): string;
begin
  case Entity of
//    seCommon: ;
    xeOperation: Result := '��������';
    xePerson: Result := '������������';
    xeWorker: Result := '����������';
    xeJob: Result := '����';
    xeDepartment: Result := '�������������';
    xeBill: Result := '�����������';
    xeMoving: Result := '�������� �������';
    xeEquipment: Result := '������������';
    xePersonJob: Result := '���� �������������';
    xeJobOperation: Result := '������ �����';
    xeInventarisation: Result := '��������������';
    xePart: Result := '�������������';
    xeResult: Result := '��� ��������������';
    xeEquipmentType: Result := '���� ������������';
    xeMovingType: Result := '���� ��������';
    xeOrganisation: Result := '�����������';
    xeVendor: Result := '�������������';
    xeWorkPlace: Result := '������� �����';
    xeStructure: Result := '���������';
  else
    Result := EnumText(EntP, Ord(Entity));
  end;
end;

procedure GotoBookMark(DataSet: TDataSet; BM: Pointer);
begin
  try
    if DataSet.BookMarkValid(BM) then
      DataSet.GotoBookMark(BM);
    DataSet.FreeBookMark(BM);
  except
  end;
end;

procedure Lookup(DS: TDataSet; CB: TComboUsed; Entity: TEntity; Field: string;
  ListField: string; Params: array of const; NeedKV: Boolean = True);
begin
  Manager.GetData(Entity, Params);
  CB.ListSource := ArsenalInstance.DS;
  if NeedKV then
    CB.KeyValue := DS.FieldValues[Field];
  CB.KeyField := 'ID';
  CB.ListField := ListField;
end;

procedure HideField(ADataSet: TDataSet; Field: string);
begin
  if ADataSet.Active then
    ADataSet.FieldByName(Field).Visible := False;
  if Assigned(ADataSet.FieldDefList.Find(Field)) then
    with ADataSet.FieldDefList.FieldByName(Field) do
      Attributes := Attributes + [faHiddenCol];
end;

function IsFieldHidden(ADataSet: TDataSet; Field: string): Boolean;
begin
  Result := (ADataSet.Active and not ADataSet.FieldByName(Field).Visible) or
    (faHiddenCol in ADataSet.FieldDefList.FieldByName(Field).Attributes);
end;

{$REGION ' TConnect '}

function TConnect.SaveConnection(AConnData: TConnectData): Boolean;
var
  fs: TFileStream;
  i: TConnectInfo;
  Len: Integer;
begin
  FConnectData := AConnData;
  Result := ConnectToDB;
  if not Result then
    Exit;
  fs := TFileStream.Create(ConnectFile, fmCreate);
  try
  for i := Low(TConnectInfo) to High(TConnectInfo) do
    if i <> ciPassword then
    begin
      Len := Length(FConnectData[i]);
      fs.Write(Len, SizeOf(Len));
      fs.Write(FConnectData[i][1], Len);
    end;
    Result := True;
  finally
    FreeAndNil(fs);
  end;
end;

function TConnect.LoadConnection(): Boolean;
var
  fs: TFileStream;
  i: TConnectInfo;
  Len: Integer;
begin
  Result := False;
  if FileExists(ConnectFile) then
  try
    fs := TFileStream.Create(ConnectFile, fmOpenRead);
    for i := Low(TConnectInfo) to High(TConnectInfo) do
    if i <> ciPassword then
    begin
      fs.Read(Len, SizeOf(Len));
      SetLength(FConnectData[i], Len);
      fs.Read(FConnectData[i][1], Len);
    end;
    Result := ConnectToDB;
  finally
    FreeAndNil(fs);
  end;
end;

constructor TConnect.Create(ADOConnection: TADOConnection);
begin
  inherited Create;
  FADOConnection := ADOConnection;
end;

function TConnect.GetConnectData(Idx: TConnectInfo): string;
begin
  Result := FConnectData[Idx];
end;

function TConnect.GetFirstTry: Boolean;
begin
  Result := FTries < 1;
end;

function TConnect.GetSecondTry: Boolean;
begin
  Result := FTries > 1;
end;

procedure TConnect.SetConnectData(Idx: TConnectInfo; const Value: string);
begin
  FConnectData[Idx] := Value;
end;

function TConnect.ConnectToDB: Boolean;
var
  connstr: string;
  SL: TStringList;
  i: TConnectInfo;
begin
  // pasre and fill ConnectionString with proper values
  connstr := FADOConnection.ConnectionString;
  SL := TStringList.Create();
  SL.QuoteChar := ';';
  sl.Delimiter := ';';
  sl.DelimitedText := StringReplace(connstr, ' ', '/', [rfReplaceAll]);
  sl.Text := StringReplace(sl.Text, '/', ' ', [rfReplaceAll]);
  for i :=  Low(TConnectInfo) to High(TConnectInfo) do
    SL.Values[ConnectParams[i]] := ConnectData[i];
  FADOConnection.ConnectionString := sl.DelimitedText;
  sl.Free;

  try
    FADOConnection.Connected := True;
  except
  end;
  Result := FADOConnection.Connected;

  if Result then
    ArsenalInstance.Exec(xeCommon, 'USE ' + ConnectData[ciDBName])
  else
    Inc(FTries);
end;

{$ENDREGION ' TConnect '}

{$REGION ' TArsenalDB '}

procedure TArsenalDB.DataModuleCreate(Sender: TObject);
var
  Ent: TEntity;
  I: Integer;
begin
  FQuery := TADOQuery.Create(Self);
  Query.Connection := ADOConn;
  for Ent := Low(TEntity) to High(TEntity) do
    for I := Low(Allies) to High(Allies) do
    begin
      DSs[Ent, I] := TDataSource.Create(Self);
      SPs[Ent, I] := TADOStoredProc.Create(Self);
      DSs[Ent, I].Tag := Ord(Ent);
      SPs[Ent, I].Tag := Ord(Ent);
      SPs[Ent, I].Connection := ADOConn;
    end;
end;

procedure TArsenalDB.Exec(Entity: TEntity; Str: string; ToSet: Boolean = False);
begin
  Switch(Entity);
  Query.Close();
  Query.SQL.Clear();
  Query.SQL.Add(Str);
  if ToSet then
    Query.Open()
  else
    Query.ExecSQL();
  DS.DataSet := Query;
end;

function TArsenalDB.ExecSP(Entity: TEntity; Name: string; const Params1: array of const;
  Params2: TStrings = nil; ToSet: Boolean = False): Integer;
var
  i, Cnt: Integer;
  procedure SetParam(Number: Integer; Value: Variant);
  begin
    if Number < SP.Parameters.Count then
      if VarIsType(Value, varString) and (Value = ID_NULL) then
        SP.Parameters[Number].Value := Null
      else
        SP.Parameters[Number].Value := Value;
  end;
begin
  Switch(Entity);
  SP.Close;
  SP.ProcedureName := Name;
  SP.Parameters.Refresh;
  for i  := 0 to SP.Parameters.Count - 1 do
    SP.Parameters[I].Value := Null;
  Cnt := 0;
  for i := Low(Params1) to High(Params1) do
  begin
    case TVarRec(Params1[i]).VType of
      vtObject:
        if Assigned(TVarRec(Params1[i]).VObject) then
          UpLoadPicture(Entity, TVarRec(Params1[i]).VObject as TPictureUsed, '', i + 1)
        else
          SP.Parameters[i + 1].Value := Null;
      vtAnsiString: SetParam(i + 1, AnsiString(Params1[i].VAnsiString));
      vtInteger: SetParam(i + 1, Params1[i].VInteger);
      vtExtended: SetParam(i + 1, Params1[i].VExtended^);
      vtVariant: SetParam(i + 1, Params1[i].VVariant^);
      vtBoolean: SetParam(i + 1, Params1[i].VBoolean);
    else
      ShowMessage('Stored Procedure parameter is not supported');
    end;
    Inc(Cnt);
  end;
  if Assigned(Params2) then
    for i := 0 to Params2.Count - 1 do
      SetParam(i + Cnt + 1, Params2[i]);
  try
    if ToSet then
      SP.Open
    else
      SP.ExecProc;
  finally
    Result := SP.Parameters[0].Value;
  end;                               
  DS.DataSet := SP;
end;

procedure TArsenalDB.FetchToList(Entity: TEntity; const List: TStrings; FieldName: string);
//var
//  i: Integer;
begin
  Switch(Entity);
  List.Clear();
  DS.DataSet.First();
  while not DS.DataSet.Eof do
  begin
    List.Add(DS.DataSet.FieldByName(FieldName).AsString);
    DS.DataSet.Next();
  end;
end;

function TArsenalDB.GetCurrentDS: TDataSource;
begin
  Result := DSs[CurrentEntity, Ally];
end;

function TArsenalDB.GetEntityAccessible(Entity: TEntity): Boolean;
//var
//  Act: TDataAction;
begin
//  Result := False;
//  for Act := Low(TDataAction) to High(TDataAction) do
//    Result := Result or (ProcNames[Entity, Act] <> '');
  with ProcNames[Entity, daSelect] do
    Result := (Name <> '') and Available;
end;

function TArsenalDB.GetEntityActionAccessible(Entity: TEntity;
  Action: TDataAction): Boolean;
begin
  Result := ProcNames[Entity, Action].Name <> '';
end;

function TArsenalDB.GetEntityPersonalized(Entity: TEntity): Boolean;
begin
  Result := ProcNames[Entity, daSelect].Personal;
end;

function TArsenalDB.GetID: TID;
begin
  Result := WrongGUID;
  try
    Result := DS.DataSet.FieldByName('ID').Value;
  except
    ShowMessage(SDataSetError);
  end;
end;

function TArsenalDB.GetInsertedID(Entity: TEntity): TID;
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  FetchToList(Entity, SL, 'ID');
  Result := SL[0];
  SL.Free;
end;

function TArsenalDB.GetProcName(Entity: TEntity; Action: TDataAction): RProcAccess;
begin
  Result := FProcNames[Ord(Entity) * ActionCount + Ord(Action)];
end;

function TArsenalDB.GetSpecActionAccessible(Action: TSpecAction): Boolean;
begin
  Result := SpecProcName[Action] <> '';
end;

function TArsenalDB.GetSpecProc(Action: TSpecAction): string;
begin
  Result := FProcNames[EntityCount * ActionCount + Ord(Action)].Name;
end;

function TArsenalDB.GetSP: TADOStoredProc;
begin
  Result := SPs[CurrentEntity, Ally];
end;

procedure TArsenalDB.SetupAccess;
var
  Ent: TEntity;
  Act: TDataAction;
  ProcName: string;
  Data: TDataSet;
  Available, Personal: Boolean;
  sa: TSpecAction;
label
  found;
  function SetProc(AName: string; Action: TSpecAction; IsEntity: Boolean = False;
    AAvailable: Boolean = True; APersonal: Boolean = False): Boolean;
  begin
    Result := AnsiSameText(Copy(ProcName, Pos('.', ProcName) + 1, 64), AName);
    if Result then
      if isEntity then
      begin
        FProcNames[Ord(Ent) * ActionCount + Ord(Act)].Name := ProcName;
        FProcNames[Ord(Ent) * ActionCount + Ord(Act)].Available := AAvailable;
        FProcNames[Ord(Ent) * ActionCount + Ord(Act)].Personal:= APersonal;
      end
      else
        FProcNames[EntityCount * ActionCount + Ord(Action)].Name := ProcName;
  end;
begin
  ExecSP(xeCommon, 'dbo.SelectAvailableOperation', [Manager.Connect.ConnectData[ciLogin]], nil, True);
  Data := ds.DataSet;
  Data.First;
  while not Data.Eof do
  begin
    ProcName := Data.FieldByName('ProcedureName').AsString;
    Available := Data.FieldByName('Managable').AsBoolean;
    Personal := False;//Data.FieldByName('Personal').AsBoolean;
    for Ent := Succ(Low(TEntity)) to High(TEntity) do
    begin
      for Act := Low(TDataAction) to High(TDataAction) do
        if SetProc(EnumText(ActP, Ord(Act)) + EnumText(EntP, Ord(Ent)), saNone,
          True, Available, Personal)
        then
          goto found;
    end;
    for sa := Succ(Low(TSpecAction)) to High(TSpecAction) do
      if SetProc(EnumText(SpActP, Ord(sa)), sa) then
        goto found;
//    raise Exception.Create('Combination of code and database procedure failed');
    found:
    Data.Next;
  end;
end;

procedure TArsenalDB.Switch(Entity: TEntity);
begin
  CurrentEntity := Entity;
end;

procedure TArsenalDB.FetchImagesToList(Entity: TEntity; const List: TStrings; FieldName: string);
var
  MS: TStream;
  num: Integer;
begin
  Switch(Entity);
  List.Clear();
  DS.DataSet.First();
  while not DS.DataSet.Eof do
  begin
    MS := DS.DataSet.CreateBlobStream(DS.DataSet.FieldByName(FieldName), bmRead);
    num := List.AddObject(DS.DataSet.FieldByName('ID').AsString, TPictureUsed.Create);
    try
      (List.Objects[num] as TPictureUsed).LoadFromStream(MS);
    finally
      DS.DataSet.Next();
    end;
    MS.Free;
  end;
end;

procedure TArsenalDB.LoadPictureToBitmap(Bitmap: TPictureUsed; DataSet: TDataSet; FieldName: string);
var
  MS: TStream;
begin
  MS := DataSet.CreateBlobStream(DataSet.FieldByName(FieldName), bmRead);
  Bitmap.LoadFromStream(MS);
  MS.Free;
end;

procedure TArsenalDB.UploadPicture(Entity: TEntity; Bmp: TPictureUsed; ParamName: string = ''; Index: Integer = 0);
var
  DP : TParameter;
  MS: TMemoryStream;
begin
  Switch(Entity);
  if ParamName <> '' then
    DP := SP.Parameters.ParamByName(ParamName)
  else
  if Index <> 0 then
    DP := SP.Parameters[Index]
  else
    Assert(False, SPicLoadError);
  MS := TMemoryStream.Create;
  try
    Bmp.SaveToStream(MS);
    MS.Position := 0;
    DP.LoadFromStream(MS, ftBlob);
  finally
    MS.Free();
  end;
end;

{$ENDREGION ' TArsenalDB '}

{$REGION ' TManager '}

function TManager.AddImage(Name: string; Picture: TPictureUsed; FileSize: Integer): Integer;
begin
  Result := FImages.AddObject(Name, TImageData.Create('', Name, Picture, FileSize));
end;

procedure TManager.ClearImages;
begin
  FImages.Clear;
end;

procedure TManager.CommitImages;
//var
//  I: Integer;
begin
//  for I := 0 to ImagesCount - 1 do
//    if Images[i].ID = ID_NULL then
//      Images[i].ID := Insert(xePicture, [Images[i].Name, Images[i].Picture]);
end;

constructor TManager.Create;
begin
  inherited; // in case TComponent is anceStor
  FConnect := TConnect.Create(ArsenalInstance.ADOConn);
  FImages := TOOStringList.Create;
end;

procedure TManager.Delete(Entity: TEntity; Params: array of const; WithRefresh: Boolean = True);
begin
  try
    ArsenalInstance.ExecSP(xeCommon, ArsenalInstance.ProcNames[Entity, daDelete].Name, Params);
    if WithRefresh then
      GetData(Entity);
  except
    if WithRefresh then
      GetData(Entity);
    raise;
  end;
end;

destructor TManager.Destroy;
begin
  FreeAndNil(FImages);
  FreeAndNil(FConnect);
  inherited;
end;

function TManager.FindImage(out Idx: Integer; Mode: NSearchMode = smName;
  Name: string = ''; ID: string = ''; Obj: TImageData = nil): Boolean;
var
  i: Integer;
begin
  Result := False;
  Idx := -1;
  case Mode of
    smName:
      begin
        Idx := FImages.IndexOf(Name);
        Result := Idx <> -1;
      end;
    smID:
      for i := 0 to ImagesCount - 1 do
        if Images[i].ID = ID then
        begin
          Idx := i;
          Result := True;
          Break;
        end;
    smObj:
      begin
        Idx := FImages.IndexOfObject(Obj);
        Result := Idx <> -1;
      end;
  end;
end;

function TManager.GetData(Entity: TEntity; AProcName: string = ''): TDataSet;
begin
  Result := GetData(Entity, [], AProcName);
end;

function TManager.GetData(Entity: TEntity; Params: array of const; AProcName: string = ''): TDataSet;
var
  BM: TBookMark;
begin
  BM := nil;
  ArsenalInstance.Switch(Entity);
  if Assigned(ArsenalInstance.DS) and Assigned(ArsenalInstance.DS.Dataset) then
    BM := ArsenalInstance.DS.DataSet.GetBookMark;
  if AProcName = '' then
    AProcName := ArsenalInstance.ProcNames[Entity, daSelect].Name;
  try
    with ArsenalInstance do
      ExecSP(Entity, AProcName, Params, nil, True);
  except
    ShowMessage('��� ���� �� ��������� Select' + EnumText(EntP, Ord(Entity)));
  end;
  Result := ArsenalInstance.DS.DataSet;
  Result.Tag := Ord(Entity);
  GotoBookMark(Result, BM);
end;

function TManager.GetImage(Index: Integer): TImageData;
begin
  try
    Result := FImages.Objects[Index] as TImageData;
  except
    on EListError do ;
  else
    raise;
  end;
end;

procedure TManager.GetImages;
//var
//  bm: TBookmark;
//  ImageName, ImageIDs: TStringList;
//  ImageBmp: TOOStringList;
//  i: Integer;
begin
//  ArsenalInstance.Switch(xePicture);
//  //bm := ArsenalInstance.DS.DataSet.GetBookmark;
//  GetData(xePicture);
//  ImageName := TStringList.Create;
//  ImageBmp := TOOStringList.Create;
//  ImageIDs := TStringList.Create;
//  ArsenalInstance.FetchToList(xePicture, ImageName, 'Name');
//  ArsenalInstance.FetchImagesToList(xePicture, ImageBmp, 'Picture');
//  ArsenalInstance.FetchToList(xePicture, ImageIDs, 'ID');
//  ClearImages;
//  for i  := 0 to ImageIDs.Count - 1 do
//    FImages.AddObject(ImageName[i], TImageData.Create(ImageIDs[i], ImageName[i],
//      ImageBmp.Objects[i] as TPictureUsed, 0{maybe oject can store size too}));
//  FreeAndNil(ImageName);
//  FreeAndNil(ImageBmp);
//  FreeAndNil(ImageIDs);
//  try
//    ArsenalInstance.DS.DataSet.GotoBookmark(bm);
//  except
//  end;
end;    

function TManager.GetImagesCount: Integer;
begin
  Result := FImages.Count;
end;

function TManager.GetPersonFIO: string;
begin
  Result := Format('%s %s %s', [Portfolio[pdLastName],
      Portfolio[pdFirstName], Portfolio[pdMiddleName]]);
end;

function TManager.GetPortfolio(Index: TPortfolioData): string;
begin
  Result := FPortfolio[Index];
end;

function TManager.Insert(Entity: TEntity; const Params1: array of const;
  Params2: TStrings = nil; WithRefresh: Boolean = True; SelectParam1: string = ''): TID;
//var
//  ADS: TDataSet;
begin
  try
    with ArsenalInstance do
      ExecSP(xeCommon, ProcNames[Entity, daInsert].Name, Params1, Params2, True);
    //ADS := GetData(Entity);
    Result := ArsenalInstance.GetInsertedID(xeCommon);
    if WithRefresh then
      if Trim(SelectParam1) = '' then
        GetData(Entity, []).Locate('ID', Result, [loCaseInsensitive])
      else
        GetData(Entity, [SelectParam1]).Locate('ID', Result, [loCaseInsensitive])
//    ArsenalInstance.DS.DataSet.Locate('ID', Result, []);
  except
    if WithRefresh then
      if Trim(SelectParam1) = '' then
        GetData(Entity, [])
      else
        GetData(Entity, [SelectParam1]);
    raise;
  end;
end;

procedure TManager.LoadData;
var
  Data: TDataSet;
  pd: TPortfolioData;
begin
  ArsenalInstance.SetupAccess;
  ArsenalInstance.ExecSP(xeCommon, 'Office.Portfolio', [Manager.Connect.ConnectData[ciLogin]], nil, True);
  Data := ArsenalInstance.DS.DataSet;
  Data.First;
  for pd := Low(TPortfolioData) to High(TPortfolioData) do
    FPortfolio[pd] := VarToStrDef(Data.Fields[Ord(pd)].Value, '');
end;

function TManager.PesonalID(Entity: TEntity): TID;
begin
  Result := TernOP(ArsenalInstance.EntityPersonalized[Entity], Portfolio[pdID], ID_NULL);
end;

procedure TManager.SpecAction(Action: TSpecAction;
  const Params1: array of const; WithRefresh: Boolean = True);
var
  Entity: TEntity;
begin
  Entity := SpecActionToEntity[Action];
  try
    ArsenalInstance.ExecSP(Entity, ArsenalInstance.SpecProcName[Action], Params1, nil, True);
    if WithRefresh then
        GetData(Entity);
  except
    if WithRefresh then
      GetData(Entity);
    raise;
  end;
end;

function TManager.Update(Entity: TEntity; {ID: TID;} const Params1: array of const;
  Params2: TStrings = nil; WithRefresh: Boolean = True): Integer;
var
  BM: TBookMark;
begin
  BM := nil;
  ArsenalInstance.Switch(Entity);
  if Assigned(ArsenalInstance.DS.Dataset) then
    BM := ArsenalInstance.DS.DataSet.GetBookMark;
  try
    with ArsenalInstance do
      Result := ExecSP(Entity, ProcNames[Entity, daUpdate].Name, Params1, Params2);
    if WithRefresh then
      GetData(Entity);
  except
    if WithRefresh then
      GetData(Entity);
    raise;
  end;
  GotoBookMark(ArsenalInstance.DS.DataSet, BM);
end;

{$ENDREGION ' TManager '}

var
  FManager: TManager;
  Arsenal: TArsenalDB;
  FEntP, FActP, FSpActP: Pointer;

function ArsenalInstance: TArsenalDB;
begin
  if not Assigned(Arsenal) then
    Application.CreateForm(TArsenalDB, Arsenal);
  Result := Arsenal;
end;

function Manager: TManager;
begin
  if not Assigned(FManager) then
    FManager := TManager.Create;
  Result := FManager;
end;

function EntP: Pointer;
begin
  Result := FEntP;
end;

function ActP: Pointer;
begin
  Result := FActP;
end;

function SpActP: Pointer;
begin
  Result := FSpActP;
end;

var
  SLRussian: TStringList;
  BarcodePrintState: Integer = -1;

function CheckBarcodePrintReady: Boolean;
begin
  if BarcodePrintState = -1 then
  begin
    BarcodePrintState := Integer(FileExists(EanFontFName));
    if BarcodePrintState = 1 then
    begin
      BarcodePrintState := AddFontResource(EanFontFName);
//      SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
    end;
  end;
  Result := Boolean(BarcodePrintState);
end;

function Russian(FieldName: string): string;
begin
  if SLRussian.IndexOfName(FieldName) = -1 then
    Result := FieldName
  else
    Result := SLRussian.Values[FieldName];
end;

initialization
  FEntP := TypeInfo(TEntity);
  FActP := TypeInfo(TDataAction);
  FSpActP := TypeInfo(TSpecAction);
  SLRussian := TStringList.Create;
  SLRussian.Text := 'Name=���.���,Person=���������,Operation=��������,' +
    'Content=����������,Number=�����,State=������,Login=�����,StartDate=����,' +
    'Sequence=����������,Color=����,FirstName=���,LastName=�������,Type=���,' +
    'Caption=������������,Password=������,Department=�����,BeginDate=���� ������,' +
    'EndDate=���� ����������,Parts=�������������,Unit=�������,Quantity=���-��,' +
    'Date=����,Measure=��.���.,BirthDate=���� ��������,Publisher=�����������,' +
    'TryCount=�������,IsFired=������,IsActive=������,Phone=�������,' +
    'Job=����,MiddleName=��������,Comment=�����������,Accept=���� ���������,' +
    'EMail=��.�����,Accepter=����������,Priority=���������,Organisation=�����������,' +
    'Position=���������,Document=��������,Description=��������,Questions=�������,' +
    'Time=�����(��:��:��),Duration=������������,FullCount=��������,Score=�����,' +
    'Vendor=�������������,Bill=���������,EquipmentType=��� ������������,Type=���,' +
    'Equipment=������������,Price=����,Lifetime=���� ������,Settings=��������,' +
    'InvNumber=����������� �,VisualNumber=���������� �,Barcode=��������,Cabinet=�������,' +
    'Workplace=������� �����,Inventarisation=��������������,Barcode=��������,' +
    'Movings=�������� ������������,MovingType=��� ��������,Installed=�����������,' +
    'Product=������,Note=����������,Part=�������������,Structure=���������,' +
    'Buyer=����������,Seller=��������,Invoice=���������,Address=�����,' +
    'Worker=���������,CurrentWorkplace=������� ���.�����,Search=�����,Purpose=���';
  SLRussian.DelimitedText := StringReplace(SLRussian.Text, ' ', '/', [rfReplaceAll]);
  SLRussian.Text := StringReplace(SLRussian.Text, '/', ' ', [rfReplaceAll]);

finalization
  FManager.Free;
  FreeAndNil(SLRussian);

end.

