 unit UHelpForm;

interface

uses
// Delphi
  Windows, SysUtils, Classes, Controls, Forms, Dialogs, ExtCtrls, ComCtrls,
  Menus, StdCtrls, IniFiles, XMLIntf, ToolWin, Buttons,
// ThirdParty
  SynGdiPlus, RxRichEd,
// Own
  Common;

type

  THelpForm = class(TForm)
    redLesson: TRxRichEdit;
    tvLessons: TTreeView;
    mmMenu: TMainMenu;
    miFile: TMenuItem;
    pgcMain: TPageControl;
    tsLesson: TTabSheet;
    miExit: TMenuItem;
    Splitter1: TSplitter;
    miLessonEditor: TMenuItem;
    tlbEditor: TToolBar;
    btnAddNode: TSpeedButton;
    btnAddChildNode: TSpeedButton;
    btnDelNode: TSpeedButton;
    btnSave: TSpeedButton;
    btnReload: TSpeedButton;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miExitClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miLessonEditorClick(Sender: TObject);
    procedure tvLessonsChange(Sender: TObject; Node: TTreeNode);
    procedure btnDelNodeClick(Sender: TObject);
    procedure btnAddNodeClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnReloadClick(Sender: TObject);
    procedure btnAddChildNodeClick(Sender: TObject);
    procedure tvLessonsChanging(Sender: TObject; Node: TTreeNode; var AllowChange: Boolean);
    procedure tvLessonsEdited(Sender: TObject; Node: TTreeNode; var S: string);
  private
    // ��������� ������
    Lessons: IXMLDocument;
    // ���������� ������� ���������
    FEditorMode: Boolean;
    procedure SetEditorMode(const Value: Boolean);
    property EditorMode: Boolean read FEditorMode write SetEditorMode;
    // ���������������
    function IsDataNode(const AXMLNode: IXMLNode): Boolean;
    function IsContentNode(const AXMLNode: IXMLNode): Boolean;
    // �������� ������
    procedure ReloadDoc;
    procedure LoadLessons;
    procedure ProcXmlNode(const AXMLNode: IXMLNode; aTree: TTreeView; aTreeNode: TTreeNode);
    // ��������������
    function DelNode(const aInd : Integer) : Boolean;
    function AddNode(AsChild: Boolean): IXMLNode;
  end;

implementation

{$R *.dfm}

uses
// Delphi
  XMLDoc, Variants, Math;

// ��������� ��� ������ �������� ������ � ������
const
  Title = '�������';
  AdminPass = '123';
  LessonFile = 'Help.xml';
  NameAttrib = 'name';

type
  TPXmlNode = ^IXMLNode;         

//������� ������. �������� ������ �� ���������� XML ����� � ������������ ������,
//������� ��� ���������� ���� TPXmlNode. �������� ����� ������.
procedure ClearTreeView(aTree : TTreeView);
var
  TreeNode : TTreeNode;
  PXmlNode : TPXmlNode;
  i : Integer;
begin
  if aTree = nil then
    Exit;
  //���������� ��� ���� ������ � ������� ��������� � ���� ������ �� ���������� XML �����.
  for i := 0 to aTree.Items.Count - 1 do
  begin
    TreeNode := aTree.Items[i];
    PXmlNode := TPXmlNode(TreeNode.Data);
    //�������� �� ������ ���������� ���� TPXmlNode. ��� ����, ������������� ���������
    //������ �� ��������� XML ���� ( Finalize(TPXmlNode(TreeNode.Data)^) ).
    if PXmlNode <> nil then Dispose( TPXmlNode(TreeNode.Data) );
  end;
  //�������� ����� ������.
  aTree.Items.Clear;
end;

//����������� ��������� ��� ���������� ������ �� XML ���������.
//������������� ��� �������� ���� XML ����� ��������� � ������ � aXmlNode.
//��� �������� XML ���� �������� ��������������� ���� � TTreeView.
//������ �� ��������� XML ���� ������������� � �������� ������ � ���������� ���� ������.
//��� ���� �������� XML ����� ����������� ����������� ����� ProcXmlNode().
procedure THelpForm.ProcXmlNode(const AXMLNode: IXMLNode; aTree: TTreeView; aTreeNode: TTreeNode);
var
  NewTreeNode : TTreeNode;
  PXmlNode : TPXmlNode;
  ANodeName : String;
  i : Integer;
begin
  if IsDataNode(AXMLNode) then
    Exit;
  //������ ����� ���� ������.
  NewTreeNode := TTreeNode.Create(aTree.Items);
  //������ ��������� ��� �������� ������ �� ��������� XML ����.
  New(PXmlNode);
  //���������� � ��������� ������ �� ��������� XML ����.
  PXmlNode^ := AXMLNode;
  //���������� ��� ���� � ������.

  if AXMLNode.HasAttribute(NameAttrib) then
    ANodeName := AXMLNode.Attributes[NameAttrib]
  else
    ANodeName := AXMLNode.NodeValue;
  //��������� ���� � ������. � ����������� � ���� ��������� �� ���������, � �������
  //�������� ������ �� ��������� ���������������� XML ����.
  // naAdd - ���� �������� ������, naAddChild - �������� ����
  NewTreeNode := aTree.Items.AddNode(NewTreeNode, aTreeNode, ANodeName, PXmlNode,
    TernOp(aTreeNode = nil, naAdd, naAddChild));
  //��� �������� XML ����� ��������� ����������� ����� ProcXmlNode().
  for i := 0 to AXMLNode.ChildNodes.Count - 1 do
    ProcXmlNode(AXMLNode.ChildNodes[i], aTree, NewTreeNode);
end;

// ������� RTF  � ������ ����� �����
function RichTextToStr(red : TRxRichEdit): string;
var
  ss : TStringStream;
begin
  ss := TStringStream.Create('');
  try
    red.Lines.SaveToStream(ss);
    Result := ss.DataString;
  finally
    ss.Free;
  end;
end;

// ��������� �������� ���� ������ ��� ��������������
procedure THelpForm.tvLessonsChanging(Sender: TObject; Node: TTreeNode; var AllowChange: Boolean);
var
  XmlNode : IXMLNode;
  CDataNode: IXMLNode;
begin
  if not EditorMode or not Assigned(tvLessons.Selected) then
    Exit;
  // ��������� ��������� ��� ������
  XmlNode := TPXmlNode(tvLessons.Selected.Data)^;
  if not IsContentNode(XmlNode) then
  begin
    CDataNode := Lessons.CreateNode('', ntCData);
    XMLNode.ChildNodes.Add(CDataNode);
  end
  else // ��� �� ���� ������������
    CDataNode := XMLNode.ChildNodes.First;
  // ��������� ������  
  CDataNode.NodeValue := RichTextToStr(redLesson);
end;

// �������������� ����� ����
procedure THelpForm.tvLessonsEdited(Sender: TObject; Node: TTreeNode; var S: string);
begin
  // ��������, ���� ������� ����
  if Assigned(tvLessons.Selected) then
    TPXmlNode(tvLessons.Selected.Data)^.Attributes[NameAttrib] := S;
end;

procedure THelpForm.ReloadDoc;
begin
  //������� ������� � �������� �������� ������.
  ClearTreeView(tvLessons);
  //���������� ������.
  ProcXmlNode(Lessons.DocumentElement, tvLessons, nil);
  tvLessons.FullExpand;
end;

// �������� ������ �� �����
procedure THelpForm.LoadLessons;
begin
  // ��������� ������� �����
  if not FileExists(LessonFile) then
  begin
    ShowMessage('���� ������ �����������');
    Exit;
  end;
  //�������� XML ���������.
  if not Assigned(Lessons) then
    Lessons := TXMLDocument.Create(nil);
  Lessons.LoadFromFile(LessonFile);
  // ����������� ������
  ReloadDoc;
end;

// ������������ ������� �����
procedure THelpForm.tvLessonsChange(Sender: TObject; Node: TTreeNode);
var
  XmlNode: IXMLNode;
begin
  XmlNode := TPXmlNode(Node.Data)^;
  // ����������� �������� ������ �� ����-������ ����
  if IsContentNode(XmlNode) then
    redLesson.Text := VarToStrDef(XmlNode.ChildNodes.First.NodeValue, '')
  else
    redLesson.Text := '';
end;

//������� ������� XML ����. ��� ���� ����� ����� XML ���� � ��������������� ���� TreeView.
function THelpForm.DelNode(const aInd : Integer) : Boolean;
var
  XmlNode : IXMLNode;
  PXmlNode : TPXmlNode;
  Node : TTreeNode;
begin
  Result := False;
  if (aInd < 0) or (aInd >= tvLessons.Items.Count) then
    Exit;
 
  //�������� ������ �� ���� TreeView �� �������.
  Node := tvLessons.Items[aInd];
  //�������� ��������� �� ���������, ������� ������ ������ �� ��������� XML ����.
  PXmlNode := Pointer(Node.Data);
  //�������� ������ �� ��������� ���������������� XML ����.
  XmlNode := PXmlNode^;
 
  //��������� - ���� XML ���� �������� �������� ����� XML ���������, �� ��������
  //�������� ��������. ������, ��� �������� ���� XML ��������� ������� ������.
  if XmlNode.ParentNode = nil then
    Exit;
 
  //���������� � ��������.
 
  //1. �������� ���� TreeView.
  Node.Delete;
 
  //2. ����������� ������, ������� ��� ����������, ������� ������ ������ ��
  //��������� XML ����.
  Dispose(PXmlNode);
 
  //3. ������� XML ����. ����� �� ���������� � ����, ������� �������� ������������
  //�� ��������� � ���������� ����: XmlNode.ParentNode � ����� ��� ���������
  //�������� ��������� XmlNode.ParentNode.ChildNodes ��������� �������� �� �������
  XmlNode.ParentNode.ChildNodes.Delete(XmlNode.ParentNode.ChildNodes.IndexOf(XmlNode));
 
  Result := True;
end;

// ���������� ����/�������
function THelpForm.AddNode(AsChild: Boolean): IXMLNode;
var
  s: string;
  AXMLNode: IXMLNode;
begin
  // ��������, ���� ������� ����
  if not Assigned(tvLessons.Selected) then
    Exit;
  // �������� � ��������� � ������������ ������ ��� ��������  
  s := InputBox('������� ��� ' + TernOp(AsChild, '���', '') + '����', '', '');
  s := TernOP(Trim(s) = '', tvLessons.Selected.Text, s);
  // ���������� ���� ��� ���������� - ������� ��� ������������
  AXMLNode := TPXmlNode(tvLessons.Selected.Data)^;
  if not AsChild then
    AXMLNode := AXMLNode.ParentNode;
  // ��������� ���� � �������  
  Result := AXMLNode.AddChild(AXMLNode.NodeName + 'child');
  if Trim(s) <> '' then
    Result.Attributes[NameAttrib] := s;
  // �����������
  ReloadDoc;
end;

procedure THelpForm.btnAddChildNodeClick(Sender: TObject);
begin
  AddNode(True);
end;

procedure THelpForm.btnAddNodeClick(Sender: TObject);
begin
  AddNode(False);
end;

// ���� � ������ ���������
procedure THelpForm.miLessonEditorClick(Sender: TObject);
begin
  if miLessonEditor.Checked then
    // ���� ������ �� ������� ������ � �����������
    if InputBox('��������', '������� ������ �������������', '') = AdminPass then
      // ������� ����������� ���������
      EditorMode := True
    else
      miLessonEditor.Checked := False
  else
    // ������ ����������� ���������
    EditorMode := False;
end;    

// ����� ���������
procedure THelpForm.SetEditorMode(const Value: Boolean);
begin
  FEditorMode := Value;
  tlbEditor.Visible := Value;
  redLesson.ReadOnly := not Value;
  tvLessons.ReadOnly := not Value;
end;

// ��� ������� ����� �������� ������� ���������� ��� ���������� �����������
procedure THelpForm.FormResize(Sender: TObject);
begin
  tvLessons.ClientWidth := Clamp(ClientWidth div 3, 150, 300);
end;

function THelpForm.IsContentNode(const AXMLNode: IXMLNode): Boolean;
begin
  Result := AXMLNode.HasChildNodes and IsDataNode(AXMLNode.ChildNodes.First);
end;

function THelpForm.IsDataNode(const AXMLNode: IXMLNode): Boolean;
begin
  Result := AXMLNode.NodeType = ntCData;
end;

procedure THelpForm.btnReloadClick(Sender: TObject);
begin
  if Confirm('�����������?'#13'������������� ������ ������� �������� ����� ��������.') then
    LoadLessons;
end;

procedure THelpForm.btnSaveClick(Sender: TObject);
begin
  if Confirm('���������?') then
    Lessons.SaveToFile(LessonFile);
end;

// �������� �����
procedure THelpForm.FormCreate(Sender: TObject);
begin
// ��������� �����
  Caption := Title;
//��������� ����� 
  LoadLessons;
  tvLessons.FullExpand;
end;

procedure THelpForm.FormDestroy(Sender: TObject);
begin
  ClearTreeView(tvLessons);
  Lessons := nil;
end;

procedure THelpForm.miExitClick(Sender: TObject);
begin
  Close;
end;

procedure THelpForm.btnDelNodeClick(Sender: TObject);
begin
  // ��� �������� ���� ��������� ����������� � ����������� �������������
  if not Assigned(tvLessons.Selected) or (tvLessons.Selected.Level = 0) or not Confirm('������� ����?') then
    Exit;
  DelNode(tvLessons.Selected.AbsoluteIndex);
  ReloadDoc;
end;

end.

