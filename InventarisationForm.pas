unit InventarisationForm;

interface

uses
// Delphi units
  Windows, SysUtils, Variants, Classes, Controls, Forms, StdCtrls, DB, CommCtrl,
  DBCtrls, Dialogs, XPMan, Buttons, Grids, ExtCtrls, ComCtrls, DBGrids, ImgList,
  Graphics,
// Third Party
  sSkinProvider, sBitBtn, sPanel, sDBLookupComboBox, sDBNavigator, sGroupBox,
  sEdit, RXDBCtrl, acAlphaImageList, DBCtrlsEh, sMemo, sSpeedButton,
// Own units
  Common, ArsenalDB, ChildForms;

type

  TInvBtns = (ibStart, ibDo, ibEnd, ibReport);

  TInventarisationForm = class(TCommonChild)
  private
    ID_Inv: TID;
    InvBtns: array[TInvBtns] of TButtonUsed;
  protected
    procedure InsertCmd; override;
    procedure UpdateCmd; override;
    procedure Cmd(IsUpd: Boolean);
    procedure InvChanged(DataSet: TDataSet);
    procedure GridDblClick(Sender: TObject);
    procedure StartInv(Sender: TObject);
    procedure DoInv(Sender: TObject);
    procedure EndInv(Sender: TObject);
    procedure Report(Sender: TObject);
//    procedure SpecModifying(ADataSet: TDataSet; FldCnt: Integer;
//      FieldName: string; IsLookup: Boolean); override;
    procedure AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity); override;
    function V(Name: string): Variant;
    procedure OperationChange(Action: TButtonType; Value: Boolean); override;
  public
    procedure Start; override;
    procedure Stop(NeedsSave: Boolean = False); override;
  end;

  TStructureFilter = (sfOrg, sfDep, sfWP);

  TInvFlowForm = class(TBaseForm)
    edtInvNumber: TsEdit;
    edtBarcode: TsEdit;
    btnSearch: TsBitBtn;
    btnClearSearch: TsBitBtn;
    btnClearFilter: TsBitBtn;
    sPanel2: TsPanel;
    procedure btnSearchClick(Sender: TObject);
    procedure btnClearSearchClick(Sender: TObject);
    procedure btnClearFilterClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  published
    cbbOrganisation: TsDBLookupComboBox;
    cbbWorkplace: TsDBLookupComboBox;
    cbbDepartment: TsDBLookupComboBox;
    sPanel1: TsPanel;
    dbnEquipment: TsDBNavigator;
    grpEquipment: TsGroupBox;
    dgEquipment: TRxDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    FID: TID;
    FFilter: array[TStructureFilter] of Boolean;
    function GetFilter(sf: TStructureFilter): Boolean;
    procedure SetFilter(sf: TStructureFilter; const Value: Boolean);
  protected
    procedure DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure ObtainData; override;
    procedure StructChanged(ADataSet: TDataSet);
    procedure AfterEditEquip(ADataSet: TDataSet);
    procedure SetupFilter(sf: TStructureFilter);
    property OrgFilter: Boolean Index sfOrg read GetFilter write SetFilter;
    property DepFilter: Boolean Index sfDep read GetFilter write SetFilter;
    property WPFilter: Boolean Index sfWP read GetFilter write SetFilter;
  public
    procedure Open(AID: TID; AName: string);
    property ID: TID read FID;
  end;

implementation

{$R *.dfm}

uses
//Own
  MainForm, Reports;

const
  SDetails = '�������������� ';


{$REGION ' TInvFlowForm '}

procedure TInvFlowForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  cbbOrganisation.ListSource.DataSet.AfterScroll := nil;
  cbbDepartment.ListSource.DataSet.AfterScroll := nil;
  DataSet.BeforeEdit := nil;
end;

procedure TInvFlowForm.FormCreate(Sender: TObject);
begin
  grpEquipment.Caption := Russian('Equipment');
  cbbOrganisation.BoundLabel.Caption := Russian('Organisation');
  cbbDepartment.BoundLabel.Caption := Russian('Department');
  cbbWorkplace.BoundLabel.Caption := Russian('Workplace');
  edtBarcode.BoundLabel.Caption := Russian('Barcode');
  edtInvNumber.BoundLabel.Caption := Russian('InvNumber');
  btnSearch.Caption := Russian('Search');
  btnClearSearch.Caption := '����� ������';
  btnClearFilter.Caption := '����� �������';
end;

function TInvFlowForm.GetFilter(sf: TStructureFilter): Boolean;
begin
  Result := FFilter[sf];
end;

procedure TInvFlowForm.ObtainData;
begin
  FDataSet := Manager.GetData(xeResult, [ID]);
  ModifyDataSet(DataSet, xeResult);
  DataSet.FieldByName('CurrentWorkplace').ReadOnly := True;
  DataSet.FieldByName('Equipment').ReadOnly := True;
  DataSet.FieldByName('InvNumber').ReadOnly := True;
  DataSet.FieldByName('Barcode').ReadOnly := True;
  DataSet.FieldByName('Inventarisation').Visible := False;
  DataSet.FieldByName('Organisation').Visible := False;
  DataSet.FieldByName('Department').Visible := False;  
  ManageColumns(Dataset, dgEquipment);
end;

procedure TInvFlowForm.Open(AID: TID; AName: string);
var
  CanUpdate: Boolean;
begin
  Start;
  FID := AID;
  Caption := SDetails + AName;

  ObtainData;
  ArsenalInstance.Switch(xeResult);
  DataSource := ArsenalInstance.DS;
  FDataSet := DataSource.DataSet;
  dbnEquipment.DataSource := DataSource;
  dgEquipment.DataSource := DataSource;
  DataSet.AfterEdit := AfterEditEquip;

  Lookup(DataSet, cbbOrganisation, xeOrganisation, '', 'Caption', [1], False);
  Lookup(DataSet, cbbDepartment, xeDepartment, '', 'Caption', [], False);
  ArsenalInstance.Ally := 1;
  Lookup(DataSet, cbbWorkplace, xeWorkPlace, '', 'Caption', [], False);
  ArsenalInstance.Ally := 0;

  cbbOrganisation.ListSource.DataSet.AfterScroll := StructChanged;
  cbbDepartment.ListSource.DataSet.AfterScroll := StructChanged;
  cbbWorkplace.ListSource.DataSet.AfterScroll := StructChanged;

  CanUpdate := ArsenalInstance.EntityActionAccessible[xeResult, daUpdate];
  ShowDBNavButton(dbnEquipment, nbInsert, False);
  ShowDBNavButton(dbnEquipment, nbEdit, CanUpdate);
  ShowDBNavButton(dbnEquipment, nbPost, CanUpdate);
  ShowDBNavButton(dbnEquipment, nbDelete, False);

  dbnEquipment.BeforeAction := DBNavBeforeAction;
  dbnEquipment.Tag := Ord(xeResult);

  SetupGrid(dgEquipment);
  Resize;
  ShowModal;
end;

procedure TInvFlowForm.StructChanged(ADataSet: TDataSet);
begin
  case ADataSet.Tag of
    Ord(xeOrganisation): SetFilter(sfOrg, not VarIsNull(cbbOrganisation.KeyValue));
    Ord(xeDepartment): SetFilter(sfDep, not VarIsNull(cbbDepartment.KeyValue));
    Ord(xeWorkPlace): SetFilter(sfWP, not VarIsNull(cbbWorkplace.KeyValue));
  end;
end;

procedure TInvFlowForm.SetFilter(sf: TStructureFilter; const Value: Boolean);
begin
  FFilter[sf] := Value;
  SetupFilter(sf);
end;

procedure TInvFlowForm.SetupFilter(sf: TStructureFilter);
  procedure ComboFilter(Struct: TStructureFilter; ACombo, AValueCombo: TComboUsed; AField: string);
  begin
    if Struct = sf then
      if GetFilter(Struct) then
      begin
        ACombo.ListSource.DataSet.Filter := AField + ' = ' + AValueCombo.KeyValue;
        ACombo.ListSource.DataSet.Filtered := True;
      end
      else
        ACombo.ListSource.DataSet.Filtered := False;
  end;
begin
  DataSet.Filter :=
    TernOP(OrgFilter, 'ID_Organisation = ' + VarToStrDef(cbbOrganisation.KeyValue, ''), '') +
    TernOP(OrgFilter and DepFilter, ' AND ', '') +
    TernOP(DepFilter, 'ID_Department = ' + VarToStrDef(cbbDepartment.KeyValue, ''), '') +
    TernOP((OrgFilter or DepFilter) and WPFilter, ' AND ', '') +
    TernOP(WPFilter, 'ID_CurrentWorkplace = ' + VarToStrDef(cbbWorkplace.KeyValue, ''), '');
  DataSet.Filtered := DataSet.Filter <> '';
  ComboFilter(sfOrg, cbbDepartment, cbbOrganisation, 'ID_Organisation');
  ComboFilter(sfDep, cbbWorkplace, cbbDepartment, 'ID_Department');
end;

procedure TInvFlowForm.AfterEditEquip(ADataSet: TDataSet);
begin
  if not VarIsNull(cbbWorkplace.KeyValue) then
    DataSet.FieldValues['ID_Workplace'] := cbbWorkplace.KeyValue;
end;

procedure TInvFlowForm.btnClearFilterClick(Sender: TObject);
begin
  cbbOrganisation.KeyValue := Null;
  cbbDepartment.KeyValue := Null;
  cbbWorkplace.KeyValue := Null;
end;

procedure TInvFlowForm.btnClearSearchClick(Sender: TObject);
begin
  edtInvNumber.Clear;
  edtBarcode.Clear;
end;

procedure TInvFlowForm.btnSearchClick(Sender: TObject);
var
  Number: Int64;
begin
  if TryStrToInt64(edtInvNumber.Text, Number) then
    DataSet.Locate('InvNumber', Number, [])
  else if TryStrToInt64(edtBarcode.Text, Number) then
    DataSet.Locate('Barcode', Number, [])
  else
    Exit;  
  DataSet.Edit;
end;

procedure TInvFlowForm.DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
var
  DBNav: TsDBNavigator absolute Sender;
  Entity: TEntity;
  DS: TDataSet;
  function V(Name: string): Variant;
  begin
    Result := DS.FieldValues[Name];
  end;
begin
  if not (Sender is TsDBNavigator) then
    Exit;
  if not (Button in [nbPost, nbDelete]) then
    Exit;
  DS := DBNav.DataSource.DataSet;
  Entity := TEntity(DBNav.Tag);
  if (Entity = xeResult) and (DS.State = dsEdit) then
    if Manager.Update(Entity, [V('ID'), ID, V('ID_Equipment'), V('ID_Workplace')]) <> 0 then
      ShowMessage('������ ������ ������������������� ������������, ��������, ������������ ������� �����');
  Resize;
  Abort; // cancel standard dbnav action
end;

{$ENDREGION ' TInvFlowForm '}

{$REGION ' TInventarisationForm '}

procedure TInventarisationForm.AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity);
begin
  inherited;
  FDataSet.FieldByName('Started').Visible := False;
  FDataSet.FieldByName('Ended').Visible := False;
end;

procedure TInventarisationForm.Cmd(IsUpd: Boolean);
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  SL.Assign(SG.Rows[DataRowNumber]);
  if DenyInvAltering then
  begin
    SL.Delete(SG.Rows[HeaderRowNumber].IndexOf(Russian('DateBegin')));
    SL.Delete(SG.Rows[HeaderRowNumber].IndexOf(Russian('DateEnd')));  
  end;
  if IsUpd then
    Manager.Update(Entity, [V('ID')], SL)
  else
    Manager.Insert(Entity, [], SL);
  FreeAndNil(SL);
end;

procedure TInventarisationForm.EndInv(Sender: TObject);
begin
  if Confirm('�� ������������� ��������� �������������� ?') then
  begin
    Manager.Update(Entity, [ID_Inv, V('ID_Worker'), V('Caption'), V('BeginDate'), DateToStr(Now)]);
    Requery;
  end;
end;

procedure TInventarisationForm.GridDblClick(Sender: TObject);
begin
  if DBGrid1.ScreenToClient(Mouse.CursorPos).y > TGridUsed(DBGrid1).RowHeights[0] then
    IsUpdating := True;
end;

procedure TInventarisationForm.InsertCmd;
begin
  Cmd(False);
end;

procedure TInventarisationForm.DoInv(Sender: TObject);
var
  Details: TInvFlowForm;
  Ico: TIcon;
begin
  if SameID(ID_Inv, ID_NULL) then
    Exit;
  Details := TInvFlowForm.Create(Application);
  begin
    Ico := TIcon.Create;
    MainFormInstance.TreeIcons.GetIcon(Ord(xeResult), Ico);
    Details.Icon := Ico;
    FreeAndNil(Ico);
  end;
  Details.Open(ID_Inv, V('Caption'));
  Details.Free;
  Requery;
  ManageColumns(DataSet, DBGrid1);
  Resize;
end;

procedure TInventarisationForm.InvChanged(DataSet: TDataSet);
  function S(IsStart: Boolean): Boolean;
  begin
    Result := Dataset.FieldByName(TernOP(IsStart, 'Started', 'Ended')).AsBoolean;
  end;
begin
  ArsenalInstance.Switch(xeInventarisation);
  try
    if DataSet.RecordCount <> 0 then
      ID_Inv := ArsenalInstance.CurrentID;
  except
    ID_Inv := ID_NULL;
  end;
  InvBtns[ibStart].Enabled := not SameID(ID_Inv, ID_NULL) and not S(True) and not S(False);
  InvBtns[ibDo].Enabled := not SameID(ID_Inv, ID_NULL) and (S(True) or S(False))
    and (not DenyInvAltering or (S(True) and not S(False)));
  InvBtns[ibEnd].Enabled := InvBtns[ibDo].Enabled;
  InvBtns[ibReport].Enabled := not SameID(ID_Inv, ID_NULL) and not S(True) and S(False);
end;

procedure TInventarisationForm.OperationChange(Action: TButtonType; Value: Boolean);
begin
  inherited;
  if Action = btDelete then
    InvChanged(DataSet);
end;

procedure TInventarisationForm.Report(Sender: TObject);
begin
  if not SameID(ID_Inv, ID_NULL) then
    InvReport(ID_Inv);
end;

procedure TInventarisationForm.UpdateCmd;
begin
  Cmd(True);
end;

function TInventarisationForm.V(Name: string): Variant;
begin
  Result := Dataset.FieldValues[Name];
end;

//procedure TInventarisationForm.SpecModifying(ADataSet: TDataSet;
//  FldCnt: Integer; FieldName: string; IsLookup: Boolean);
//begin
//  if (FieldName = 'Started') or (FieldName = 'Ended') then
//    FDataSet.FieldDefs.Find(FieldName).Attributes := FDataSet.FieldDefs.Find(FieldName).Attributes + [faHiddenCol];
//  inherited;
//end;

procedure TInventarisationForm.Start;
var
  BtnPanel: TPanelUsed;
  ib: TInvBtns;
begin
  inherited;
  DataSet.AfterScroll := InvChanged;
  ManageColumns(DataSet, DBGrid1);
  DBGrid1.DataSource := DataSource;
  DBGrid1.OnDblClick := GridDblClick;
  Panel1.Height := 130;
  ClearClick(nil);
  BtnPanel := TPanelUsed.Create(Self);
  BtnPanel.Parent := Panel1;
  BtnPanel.Caption := '';
  BtnPanel.Align := alBottom;
  for ib := High(TInvBtns) downto Low(TInvBtns) do
  begin
    InvBtns[ib] := TButtonUsed.Create(Self);
    InvBtns[ib].Align := alRight;
    InvBtns[ib].Parent := BtnPanel;
    InvBtns[ib].Width := 120;
  end;
  InvBtns[ibStart].OnClick := StartInv;
  InvBtns[ibDo].OnClick := DoInv;
  InvBtns[ibEnd].OnClick := EndInv;
  InvBtns[ibReport].OnClick := Report;
  InvBtns[ibStart].Caption := '������';
  InvBtns[ibDo].Caption := '�����������';
  InvBtns[ibEnd].Caption := '���������';
  InvBtns[ibReport].Caption := '�����';
  InvBtns[ibReport].Images := MainFormInstance.TreeIcons;
  InvBtns[ibReport].ImageIndex := Ord(xeReport);
  Resize;
  InvChanged(Dataset);
end;

procedure TInventarisationForm.StartInv(Sender: TObject);
begin
  if Confirm('�� ������������� ������ �������������� ?') then
  begin
    Manager.Update(Entity, [ID_Inv, V('ID_Worker'), V('Caption'), DateToStr(Now)]);
    Requery;
  end;
end;

procedure TInventarisationForm.Stop(NeedsSave: Boolean = False);
begin
  inherited;
  DataSet.AfterScroll := nil;
end;

{$ENDREGION ' TInventarisationForm '}

end.

