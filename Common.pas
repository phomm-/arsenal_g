unit Common;

interface

uses
// Delphi
  SysUtils, Controls, Variants, Grids, Graphics, Classes, ComCtrls, DBCtrls, DBGrids,
// ThirdParty
  sPanel, sSpeedButton, RXDBCtrl, DBCtrlsEh, sDBLookupComboBox, sDBNavigator,
  SynGdiPlus, RxRichEd;

type

  TConnectInfo = (ciServer, ciDBName, ciLogin, ciPassword, ciIntegratedSecurity);

  TConnectData = array[TConnectInfo] of AnsiString;


  TJobMode = (jmView, jmAdd, jmEdit);

  TID = AnsiString;

  TGridClass = TStringGrid; // �����.

  THackControl = class(TWinControl)
  end;

  TPanelUsed = TsPanel;
  TButtonUsed = TsSpeedButton;
  TComboUsed = TsDBLookupComboBox;
  TDaterUsed = TDBDateTimeEditEh;
  TDBGridUsed = TRxDBGrid;
  TRichEditUsed = TRxRichEdit;
  TRTFConverter = TRichEdit;
  TPictureUsed = TSynPicture;

  TGridUsed = class(TStringGrid)
  public
    procedure ShowCaret;
  end;

  TStringObj = class(TObject)
  private
    FS: string;
  public
    constructor Create(Astr: string);
    property S: string read FS write FS;
  end;
  
  TOOStringList = class(TStringList)
  private
    FOwnsObject: Boolean;
  public
    constructor Create(AOwnsObjects: Boolean = True);
    destructor Destroy; override;
    procedure Clear; override;
    procedure Delete(Index: Integer); override;
    property OwnsObjects: Boolean read FOwnsObject write FOwnsObject;
  end;
  
  TImageData = class(TObject)
  private
    FName: string;
    FPicture: TPictureUsed;
    FID: TID;
    FFileSize: Integer;
  protected
  public
    constructor Create(AID: TID; AName: string; APic: TPictureUsed; AFileSize: Integer);
    destructor Destroy(); override;
    property Name: string read FName write FName;
    property Picture: TPictureUsed read FPicture write FPicture;
    property ID: TID read FID write FID;
    property FileSize: Integer read FFileSize write FFileSize;
  end;

resourcestring
  rsWishLeave = '�� ���������� ���� %s. ��������� ���������?';
  SRecCnt = '������� ����� : ';

var
  AppPath: string;

const
  FullAuthInfo = [ciServer, ciDBName, ciLogin, ciPassword];
  BasicAuthInfo = [ciLogin, ciPassword];
  ConnectFile = '1.dat';
  DateFormat = 'DD.MM.YYYY';
  DSep1 = '-';
  DSep2 = '.';
  ParseDateFormat1 = 'yyyy' + DSep1 + 'mm' + DSep1 + 'dd';
  ParseDateFormat2 = 'dd' + DSep2 + 'mm' + DSep2 + 'yyyy';
  TimeFormat = 'HH:NN:SS';
  DateTimeFormat = 'DD.MM.YYYY HH:NN';
  DateTimeFormatForFileName = 'DD.MM.YYYY_HH-NN';
  //EmptyDateFormat = '__.__.____';

  SDataSetError = 'Cannot operate with Dataset';
  SInputError = 'Input field is empty';
  SPicLoadError = 'Picture upload error';
  STitle = '��� ����� � �������������� ������������ � �������������� IT-��������';
  SInput = '������� ';
  SAdd = '�������� ';
  SDel = '������� ';
  SConnect = '����������� � ���� ������';
  SConnectFail = '���������� � ����� ������ �� ����������� !';
//  SExec = 'EXEC %s';
//  SExecI = 'EXEC %s %d';
  SSaveChanges = '��������� �� ���������, ��������� ?';
  SCheckFields = '��������� ���������� ����������� �����';
  SFErrorIn = '������ ��� %s';
  SRepeatedNumber = '��������, ����� �� ��������';
  SFValueInvalid = '�������� %s ������ ���������� � �����, � �������� �� ���� � ����';
  strleaveMainSys = '����� �� ��������� ?';
  SToBeHidden = 'ToBeHidden';

  ConnectNames: TConnectData = ('������', '��� ��', '�����', '������', 'Windows-�����������');

  // Names for parameters of connection string in ADOConnection
  ConnectParams: TConnectData = ('Data Source', 'Initial Catalog', 'User ID',
    'Password', 'Integrated Security');

  WrongGUID = '{00000000-0000-0000-0000-000000000000}';

  ID_NULL = '';

  SManaging = '�����������������';

/// <summary>
/// Checks condition ACond and if passed shows message with AHint text
/// </summary>
function CondHint(AHint: string; ACond: Boolean = True): Boolean; overLoad;
/// <summary>
/// Checks if Control.Text is Empty and if passed shows message with AHint text
/// </summary>
function CondHint(Control: TWinControl; AHint: string): Boolean; overLoad;


/// <summary>
///  Asks Question with Astr text and mbYes, mbNo buttons
/// </summary>
/// <returns>
///  True if Answered Yes, False if No
/// </returns>
function Confirm(AStr: string): Boolean;

/// <summary>
///  Ternary Operator. Checks condition ACond, if passed returns IfTrue else IfFalse
/// </summary>
function TernOP(ACond: Boolean; IfTrue, IfFalse: Variant): Variant;

function Clamp(Value, AMin, AMax: Integer): Integer;

function EnumText(PEnum: Pointer; Ordinal: Integer; EnumPrefixLen: Byte = 2): AnsiString;
/// <summary>
///  Searches specific element in Enum, matching textual Name to EnumPrefix + Caption
/// </summary>
/// <returns>
///  Int >= 0 if match found, -1 otherwise
/// </returns>
function EnumValue(PEnum: Pointer; Caption: string; EnumPrefix: string = ''): Integer;

function SameID(ID1, ID2: TID): Boolean;
//function SameVarID(ID1, ID2: Variant): Boolean;

procedure NotImplError();

function PluralCapFirst(const Str: string): string;

function CapFirstLetter(const Str: string): string;

function PostInc(var AInt: Integer; const Modifier: Integer = 1): Integer;

function MatchSubText(const AText: string; const AValues: array of string): Boolean;

function RichTextToStr(red : TRichEditUsed): string;

procedure ShowDBNavButton(DBNav: TsDBNavigator; Button: TNavigateBtn; DoShow: Boolean);

procedure SetupGrid(AGrid: TDBGridUsed);

//--------------------------------------------------------------------------

implementation

uses
// Delphi
  Dialogs, TypInfo, Windows, StrUtils;

function RichTextToStr(red : TRichEditUsed): string;
var
  ss : TStringStream;
begin
  ss := TStringStream.Create('');
  try
    red.Lines.SaveToStream(ss);
    Result := ss.DataString;
  finally
    ss.Free;
  end;
end;

function MatchSubText(const AText: string; const AValues: array of string): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := Low(AValues) to High(AValues) do
    if AnsiContainsText(AText, AValues[I]) then
    begin
      Result := True;
      Break;
    end;
end;


function CapFirstLetter(const Str: string): string;
begin
  Result := Trim(Str);
  if Result <> '' then
    Result[1] := AnsiUpperCase(Result[1])[1];
end; 

function PluralCapFirst(const Str: string): string;
begin
  Result := CapFirstLetter(Result) + '�';
end;

function PostInc(var AInt: Integer; const Modifier: Integer = 1): Integer;
begin
  Result := AInt;
  AInt := AInt + Modifier;
end;

function EnumText(PEnum: Pointer; Ordinal: Integer; EnumPrefixLen: Byte = 2): AnsiString;
begin
  Result := AnsiString(GetEnumName(PEnum, Ordinal));
  Delete(Result, 1, EnumPrefixLen);
end;

function EnumValue(PEnum: Pointer; Caption: string; EnumPrefix: string = ''): Integer;
begin
  Result := GetEnumValue(PEnum, EnumPrefix + Caption);
end;

function Clamp(Value, AMin, AMax: Integer): Integer;
begin
  Result := Value;
  if Value < AMin  then
    Result := AMin;
  if Value > AMax  then
    Result := AMax;
end;

function Confirm(AStr: string): Boolean;
begin
  Result := MessageDlg(AStr, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
end;

function CondHint(AHint: string; ACond: Boolean = True): Boolean;
begin
  Result := ACond;
  if ACond then
    ShowMessage(AHint);
end;

function CondHint(Control: TWinControl; AHint: string): Boolean;
begin
  Result := CondHint(AHint, Assigned(Control) and (Trim(THackControl(Control).Text) = ''));
  if Result then
    Control.SetFocus;
end;

function TernOP(ACond: Boolean; IfTrue, IfFalse: Variant): Variant;
begin
  if ACond then
    Result := IfTrue
  else
    Result := IfFalse;
end;

function SameID(ID1, ID2: TID): Boolean;
begin
  Result := AnsiSameText(ID1, ID2);
end;

//function SameVarID(ID1, ID2: TID): Boolean;
//begin
//  Result := not ((ID1 = Unassigned) or (ID1 = Null) or (ID2 = Unassigned) or (ID2 = Null));
//  Result := Result and SameID(ID1, ID2);
//end;

procedure NotImplError();
begin
//  Assert(False, 'Not implemented');
  ShowMessage('Not implemented');
end;

procedure ShowDBNavButton(DBNav: TsDBNavigator; Button: TNavigateBtn; DoShow: Boolean);
begin
  if DoShow then
    DBNav.VisibleButtons := DBNav.VisibleButtons + [Button]
  else
    DBNav.VisibleButtons := DBNav.VisibleButtons - [Button];
end;

procedure SetupGrid(AGrid: TDBGridUsed);
begin
  AGrid.Options := AGrid.Options + [dgAlwaysShowEditor, dgTabs];
end;


{$REGION ' TOOStringList '}

procedure TOOStringList.Clear;
var
  I: Integer;
begin
  if OwnsObjects then
    for I := 0 to Count - 1 do
      Objects[I].Free;
  inherited;
end;

constructor TOOStringList.Create(AOwnsObjects: Boolean = True);
begin
  inherited Create;
  FOwnsObject := AOwnsObjects;
end;

procedure TOOStringList.Delete(Index: Integer);
begin
  if OwnsObjects then
    Objects[Index].Free;
  inherited;
end;

destructor TOOStringList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

{$ENDREGION ' TOOStringList '}


{$REGION ' TImageData '}

constructor TImageData.Create(AID: TID; AName: string; APic: TPictureUsed; AFileSize: Integer);
begin
  FID := AID;
  FName := AName;
  FPicture := TPictureUsed.Create;
  FPicture.Assign(APic);
  FFileSize := AFileSize;
end;

destructor TImageData.Destroy;
begin
  FPicture.Free;
  inherited;
end;

{$ENDREGION ' TImageData '}


{ TGridUsed }

procedure TGridUsed.ShowCaret;
begin
  try
    SetFocus;
    if Assigned(InplaceEditor) then
      InplaceEditor.SelStart := 0;
  except
  end;
end;


{ TStringObject }

constructor TStringObj.Create(Astr: string);
begin
  FS := AStr;
end;

initialization

  AppPath := ExtractFilePath(ParamStr(0));

end.

