unit MainForm;

interface

uses
// Delphi
  Windows, SysUtils, Classes, Dialogs, Graphics, Forms, Menus, ImgList, ToolWin,
  Controls, ExtCtrls, ComCtrls, StdActns, ActnList, StdCtrls, XPMan,
// ThirdParty
  sDialogs, sTreeView, sGroupBox, sToolBar, sStatusBar, sSplitter, sSkinManager,
  acAlphaImageList, sPanel, sTabControl, sLabel,
// Own
  ArsenalDB, ChildForms;

type
  TMainForm = class(TForm)
  {$REGION ' NonVisuals '}
    MenuIcons: TImageList;
    TreeIcons: TsAlphaImageList;
    ButtonIcons: TsAlphaImageList;
    OpenDialog: TsOpenDialog;
    SaveDialog: TsSaveDialog;
    SkinManager: TsSkinManager;
    XPManifest: TXPManifest;
  {$ENDREGION ' NonVisuals '}
    MainMenu: TMainMenu;
  {$REGION ' MenuItems '}
    File1: TMenuItem;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    FileCloseItem: TMenuItem;
    Window1: TMenuItem;
    Help1: TMenuItem;
    N1: TMenuItem;
    FileExitItem: TMenuItem;
    WindowCascadeItem: TMenuItem;
    WindowTileItem: TMenuItem;
    WindowArrangeItem: TMenuItem;
    HelpAboutItem: TMenuItem;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    Edit1: TMenuItem;
    CutItem: TMenuItem;
    CopyItem: TMenuItem;
    PasteItem: TMenuItem;
    WindowMinimizeItem: TMenuItem;
    miCloseAll: TMenuItem;
    mniOpenDB: TMenuItem;
    WindowTileItem2: TMenuItem;
    miQuery: TMenuItem;
    miRefreshQuery: TMenuItem;
    miRefreshAll: TMenuItem;
    miHelp: TMenuItem;
  {$ENDREGION ' MenuItems '}
    ActionList: TActionList;
  {$REGION ' Actions '}
    FileNew1: TAction;
    FileSave1: TAction;
    FileExit1: TAction;
    FileOpen1: TAction;
    FileSaveAs1: TAction;
    HelpAbout1: TAction;
    CloseAll1: TAction;
    actOpenDB: TAction;
    WindowCascade1: TWindowCascade;
    WindowTileHorizontal1: TWindowTileHorizontal;
    WindowArrangeAll1: TWindowArrange;
    WindowMinimizeAll1: TWindowMinimizeAll;
    FileClose1: TWindowClose;
    WindowTileVertical1: TWindowTileVertical;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    actRefreshQuery: TAction;
    actRefreshAll: TAction;
  {$ENDREGION ' Actions '}
    ToolBar: TsToolBar;
  {$REGION ' ToolButtons '}
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
  {$ENDREGION ' ToolButtons '}
    StatusBar: TsStatusBar;
    GBTools: TsGroupBox;
    TVOperations: TsTreeView;
    sPanel1: TsPanel;
    Splitter1: TsSplitter;
    Splitter2: TSplitter;
    MainTabs: TsTabControl;
    pnlPerson: TsPanel;
    lblFIO: TsLabel;
    lblDepartment: TsLabel;
    lblOrganisation: TsLabel;
    FilterIcons: TsAlphaImageList;
    sPanel2: TsPanel;
    DBNavBtns: TsAlphaImageList;
    procedure FileNew1Execute(Sender: TObject);
    procedure FileOpen1Execute(Sender: TObject);
    procedure HelpAbout1Execute(Sender: TObject);
    procedure FileExit1Execute(Sender: TObject);
    procedure FileSave1Execute(Sender: TObject);
    procedure CloseAll1Execute(Sender: TObject);
    procedure FileSaveAs1Execute(Sender: TObject);
    procedure actOpenDBExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actCommonUpdate(Sender: TObject);
    procedure actRefreshExecute(Sender: TObject);
    procedure TVOperationsKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MainTabsChange(Sender: TObject);
    procedure miHelpClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FormMan: TFormMan;
    procedure CreateMDIChild(AEntity: TEntity);
    procedure InitControls;
    procedure FillTabs;
    function GetIsManaging: Boolean;
    function GetCurEntity: TEntity;
  public
    property IsManaging: Boolean read GetIsManaging;
    property CurEntity: TEntity read GetCurEntity;
  end;

// singletone emulation  
function MainFormInstance: TMainForm;

function RtfToText(RTF: string; ReplaceLineFeedWithSpace: Boolean = False): string;

//------------------------------------------------------------------------------
implementation

{$R *.dfm}

uses
// Own
  Common, ConnectForm, AboutBox, UHelpForm;

var
  TheMainForm: TMainForm = nil;
  RTFConverter: TRTFConverter;

function MainFormInstance: TMainForm;
begin
  if not Assigned(TheMainForm) then
    Application.CreateForm(TMainForm, TheMainForm);
  Result := TheMainForm;
end;

function RtfToText(RTF: string; ReplaceLineFeedWithSpace: Boolean = False): string;
var
  MyStringStream: TStringStream;
begin
  MyStringStream := TStringStream.Create(RTF);
  try
    RTFConverter.PlainText := False;
    RTFConverter.Font.Charset := RUSSIAN_CHARSET;
    RTFConverter.Lines.LoadFromStream(MyStringStream);
    RTFConverter.PlainText := True;
    RTFConverter.Lines.StrictDelimiter := True;
    if ReplaceLineFeedWithSpace then
      RTFConverter.Lines.Delimiter := ' '
    else
      RTFConverter.Lines.Delimiter := #13;
    Result := RTFConverter.Lines.Text;
  finally
    MyStringStream.Free;
  end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not Confirm(strleaveMainSys) then
    Action := caNone; // ������ ������ � ������ �� ���������
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  ConnectForm: TConnectForm;
  ConnData: TConnectData;
begin
  // if block spams user with reQuests until DB is connected or user quits
  if not Manager.Connect.LoadConnection then // tries connect once, from Datafile
  begin
    ConnectForm := TConnectForm.Create(nil);
    try
      repeat
        Cursor := crDefault;
        ConnectForm.Clear;        // else - multiple reQuests
        if Manager.Connect.SecondTry then
          ShowMessage(SConnectFail);
        ConnectForm.Open(Manager.Connect.ConnData);
        repeat
          Sleep(99);
          Application.ProcessMessages;
        until ConnectForm.ModalResult <> mrNone;
        Cursor := crHourGlass;
        if ConnectForm.ModalResult = mrOk then
          ConnectForm.GetConnect(ConnData)
        else
          Abort;
      until Manager.Connect.SaveConnection(ConnData); // until connected
    except
      on EAbort do
      begin
        Application.ShowMainForm := False;
        ConnectForm.Free;
        Application.Terminate;
        Exit;
      end
      else
        raise;
    end;
  end;
  InitControls;
  Cursor := crDefault;
  ConnectForm.Free;
end;

procedure TMainForm.InitControls;
var
  I: Integer;
begin
  RTFConverter := TRTFConverter.CreateParented(HWND_MESSAGE);
  Caption := STitle;
  Manager.LoadData;
  with Manager do
  begin
    lblFIO.Caption := Manager.PersonFIO;
    lblDepartment.Caption := Portfolio[pdDepartment];
    lblOrganisation.Caption := Portfolio[pdOrganisation];
  end;
  Application.Title := STitle;
  FormMan := TFormMan.Create;
  TTreeViewDecor.Populate(TVOperations, FormMan);
  for I := 0 to TVOperations.Items.Count - 1 do
  begin
    if Integer(TVOperations.Items[i].Data) < TVOperations.Images.Count then
      TVOperations.Items[i].ImageIndex := Integer(TVOperations.Items[i].Data)
    else
      TVOperations.Items[i].ImageIndex := Ord(xeReport);
    TVOperations.Items[i].SelectedIndex := TVOperations.Items[i].ImageIndex;
  end;
  FillTabs;
end;

procedure TMainForm.FillTabs;
//var
//  SL: TStringList;
//  I: Integer;
begin
  // disabled for now
  {
  SL := TStringList.Create;
  try
    Manager.GetData(xeType);
    ArsenalInstance.FetchToList(xeType, SL, 'Name');
    MainTabs.Tabs.Clear;
    MainTabs.Tabs.Add(SManaging);
    for I := 0 to SL.Count - 1 do
      MainTabs.Tabs.Add(SL[I]);
  finally
    SL.Free;
  end;
  }
end;

procedure TMainForm.MainTabsChange(Sender: TObject);
begin
  GBTools.Visible := IsManaging;
  CloseAll1.Execute;
//  if not IsManaging then
//    CreateMDIChild(xeExam);
end;

procedure TMainForm.miHelpClick(Sender: TObject);
var
  HelpForm: THelpForm;
begin
  HelpForm := THelpForm.Create(Self);
  HelpForm.ShowModal;
  FreeAndNil(HelpForm);
end;

procedure TMainForm.TVOperationsKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    FileNew1.Execute;
end;

procedure TMainForm.actOpenDBExecute(Sender: TObject);
begin
  //EditConnectionString(DataModule1.con1);
end;

procedure TMainForm.actRefreshExecute(Sender: TObject);
var
  i: Integer;
begin
  if Sender = actRefreshAll then
    for i:= MdiChildCount - 1 downto 0 do
      (MDIChildren[i] as TBaseChildForm).Requery
  else
    (ActiveMDIChild as TBaseChildForm).Requery;
end;

procedure TMainForm.actCommonUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(ActiveMDIChild);
end;

procedure TMainForm.CloseAll1Execute(Sender: TObject);
var
  i: Integer;
begin
  for i:= MdiChildCount - 1 downto 0 do
    MDIChildren[i].Free;
end;

procedure TMainForm.CreateMDIChild(AEntity: TEntity);
var
  Child: TBaseChildForm;
  Ico: TIcon;
  i: Integer;
begin
  if AEntity in [xeCommon] then
    Exit;
  for i:= MdiChildCount - 1 downto 0 do
    if Assigned(MDIChildren[i]) and (TBaseChildForm(MDIChildren[i]).Entity = AEntity)
    then
    begin
      MDIChildren[i].BringToFront;
      Exit;
    end;
  { create a new MDI child window }
  Child := FormMan.BuildForm(Self, AEntity);
  if not Assigned(Child) then
    Exit;
  if {IsManaging and} (TVOperations.Selected.ImageIndex <> -1) then
  begin
    Ico := TIcon.Create;
    TVOperations.Images.GetIcon(TVOperations.Selected.ImageIndex, Ico);
    Child.Icon := Ico;
    FreeAndNil(Ico);
  end;
  Child.Start;
  //if not IsManaging then
    Child.WindowState := wsMaximized;
//  Child.Align := alClient;
//  if FileExists(Name) then
//    Child.Memo1.Lines.LoadFromFile(Name);
end;

procedure TMainForm.FileNew1Execute(Sender: TObject);
begin
  CreateMDIChild(CurEntity);
end;

procedure TMainForm.FileOpen1Execute(Sender: TObject);
begin
//  if OpenDialog.Execute then
//    CreateMDIChild(OpenDialog.FileName);
end;

procedure TMainForm.FileSave1Execute(Sender: TObject);
//var
//  FName : string;
begin
//  if FileExists(ActiveMDIChild.Caption) then
//    FName := ActiveMDIChild.Caption
//  else if SaveDialog.Execute then
//    FName := SaveDialog.FileName;
  //(ActiveMDIChild as TMDIChild).Memo1.Lines.SaveToFile(FName);
end;

procedure TMainForm.FileSaveAs1Execute(Sender: TObject);
begin
  //if SaveDialog1.Execute then
//    (ActiveMDIChild as TMDIChild).Memo1.Lines.SaveToFile(SaveDialog1.FileName);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FormMan);
  FreeAndNil(RTFConverter);
end;

function TMainForm.GetCurEntity: TEntity;
begin
  Result := xeCommon;
  if Assigned(TVOperations.Selected) then
    Result := TEntity(TVOperations.Selected.Data);
end;

function TMainForm.GetIsManaging: Boolean;
begin
  Result := MainTabs.TabIndex = 0;
end;

procedure TMainForm.HelpAbout1Execute(Sender: TObject);
var
  About: TAboutBox;
begin
  About := TAboutBox.Create(Self);
  About.ShowModal;
  FreeAndNil(About);
end;

procedure TMainForm.FileExit1Execute(Sender: TObject);
begin
  Close;
end;

end.
