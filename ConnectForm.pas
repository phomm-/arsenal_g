unit ConnectForm;

interface

uses
// Delphi units
  Windows, SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, Buttons,
  XPMan, ComCtrls,
// Third Party
  sBitBtn, sCheckBox,
// Own units
  Common;

type

  TConnectForm = class;

  TProgressThread = class(TThread)
  protected
    Form: TConnectForm;
    procedure Execute; override;
  end;

  TConnectForm = class(TForm)
    BOk: TsBitBtn;
    BClose: TsBitBtn;
    XPManifest1: TXPManifest;
    chkShowPswd: TsCheckBox;
    chkTrusted: TsCheckBox;
    chkShowFullAuth: TsCheckBox;
    lblLoad: TLabel;
    pbLoad: TProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure chkShowPswdClick(Sender: TObject);
    procedure chkTrustedClick(Sender: TObject);
    procedure EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure chkShowFullAuthClick(Sender: TObject);
    procedure BOkClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Edits: array[TConnectInfo] of TLabeledEdit;
    FocusEdit: TConnectInfo;
    FProgress: TProgressThread;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DoStep;
    procedure KillThread;
  public
    procedure GetConnect(var ConnData: TConnectData);
    function Open(ConnData: TConnectData): TModalResult;
    procedure Clear;
  end;

implementation

{$R *.dfm}

{$REGION ' TConnectForm '}

// http://forum.sources.ru/index.php?showtopic=331771
procedure TConnectForm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
end;

procedure TConnectForm.Clear;
begin
  ModalResult := mrNone;
  KillThread;
  lblLoad.Hide;
  pbLoad.Hide;
  pbLoad.Position := 5;
end;

procedure TConnectForm.GetConnect(var ConnData: TConnectData);
var
  i: TConnectInfo;
begin
  for i := Low(TConnectInfo) to High(TConnectInfo) do
    if i <> ciIntegratedSecurity then
      ConnData[i] := Edits[i].Text;
  if chkTrusted.Checked then
    ConnData[ciIntegratedSecurity] := 'SSPI'
  else
    ConnData[ciIntegratedSecurity] := '';
end;

procedure TConnectForm.KillThread;
begin
  if Assigned(FProgress) then
    FProgress.Terminate;
  FreeAndNil(FProgress);
end;

function TConnectForm.Open(ConnData: TConnectData): TModalResult;
var
  i: TConnectInfo;
begin
  for i := High(TConnectInfo) downto Low(TConnectInfo) do
    if i <> ciIntegratedSecurity then
    begin
      Edits[i].Text := ConnData[i];
      if ConnData[i] = '' then
        FocusEdit := i;
    end;
  chkTrusted.Checked := AnsiSameText(ConnData[ciIntegratedSecurity], 'SSPI');
  Result := mrNone; // ShowModal;
  Show;
end;

procedure TConnectForm.DoStep;
begin
  pbLoad.StepIt;
  Refresh;
  Application.ProcessMessages;
end;

procedure TConnectForm.BOkClick(Sender: TObject);
begin
  lblLoad.Show;
  pbLoad.Show;
  FProgress := TProgressThread.Create(True);
  FProgress.Form := Self;
  FProgress.Resume;
end;

procedure TConnectForm.chkShowFullAuthClick(Sender: TObject);
var
  i: TConnectInfo;
begin
  for i := Low(TConnectInfo) to High(TConnectInfo) do
    if not (i in BasicAuthInfo + [ciIntegratedSecurity]) then
      Edits[i].Visible := not Edits[i].Visible;
end;

procedure TConnectForm.chkShowPswdClick(Sender: TObject);
begin
  Edits[ciPassword].PasswordChar := Char(Ord('*') - Ord(Edits[ciPassword].PasswordChar));
end;

procedure TConnectForm.chkTrustedClick(Sender: TObject);
begin
  Edits[ciLogin].Enabled := not chkTrusted.Checked;
  Edits[ciPassword].Enabled := not chkTrusted.Checked;
end;

procedure TConnectForm.EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    Key := VK_TAB;
end;

procedure TConnectForm.FormActivate(Sender: TObject);
begin
  Edits[FocusEdit].SetFocus;
end;

procedure TConnectForm.FormCreate(Sender: TObject);
const
  EditSpacing = 40;
var
  i: TConnectInfo;
begin
  for i := Low(TConnectInfo) to High(TConnectInfo) do
  begin
    if not (i in FullAuthInfo) then
      Continue;
    Edits[i] := TLabeledEdit.Create(Self); // Edits put in form, for freeing
    Edits[i].EditLabel.Caption := ConnectNames[i];
    Edits[i].Parent := Self;
    Edits[i].Top := (Ord(i) + 1) * EditSpacing;
    Edits[i].LabelPosition := lpLeft;
    Edits[i].Left := (ClientWidth - Edits[i].Width) div 2;
    Edits[i].OnKeyUp := EditKeyUp;
    Edits[i].Visible := i in BasicAuthInfo;
  end;
  KeyPreview := True;
  chkShowPswd.Checked := True;
  chkShowPswd.Caption := '�������� ������ *';
  chkShowFullAuth.Caption := '���������� ���������';
  lblLoad.Caption := '��������';
  chkTrusted.Caption := ConnectNames[ciIntegratedSecurity];
  Caption := STitle; // SConnect;
end;

procedure TConnectForm.FormDestroy(Sender: TObject);
begin
  KillThread;
end;

{$ENDREGION ' TConnectForm '}

{ TProgressThread }

procedure TProgressThread.Execute;
begin
  while not Terminated do
  begin
    Synchronize(Form.DoStep);
    Sleep(99);
  end;
end;

end.

