unit Reports;

interface

uses
// Delphi
  SysUtils, Classes, Variants, ComObj, Dialogs, XMLIntf, XMLDoc, DB, Controls,
// Own
  Common, ArsenalDB, MainForm, ChildForms;

const
  ReportCount = Ord(saDisposedReport) - Ord(saEquipmentReport) + 1;
  ResultsDir = 'results';
  SRun = '������������';
type

  TBaseReportForm = class(TBaseChildForm)
  private
    BtnRun: TButtonUsed;
  protected
    function GetSpecAction: TSpecAction; virtual;
    procedure DoRun(Sender: TObject); virtual;
    procedure ObtainData; override;
  public
    procedure Start; override;
    property SpecAction: TSpecAction read GetSpecAction;
  end;

  TDisposeForm = class(TBaseReportForm)
  private
    cbbOrganisation: TComboUsed;
    dtpDateBegin, dtpDateEnd: TDaterUsed;
  protected
    procedure DoRun(Sender: TObject); override;
  public
    procedure Start; override;
  end;

  TForDisposeForm = class(TDisposeForm)
  protected
    function GetSpecAction: TSpecAction; override;
  end;

  TDisposedForm = class(TDisposeForm)
  protected
    function GetSpecAction: TSpecAction; override;
  end;

  ReportFormClass = class of TBaseReportForm;

  BookMark = record
    Name, Value: string;
  end;

  TReportInfo = record
    Name, FName: string;
    Form: ReportFormClass;
  end;

  TBookmarks = array of BookMark;

const
  ReportInfo: array[0..ReportCount - 1] of TReportInfo = (
    (Name:'������� ������������'; FName:'eq.dat'; Form: TBaseReportForm),
    (Name:'������ ����������'; FName:''; Form: TBaseReportForm),    
    (Name:'������� ���.�����'; FName:'wp.dat'; Form: TBaseReportForm),
    (Name:'�������� ������������'; FName:'em.dat'; Form: TBaseReportForm),
    (Name:'��������������'; FName:'inv.dat'; Form: TBaseReportForm),
    (Name:'��� ��������'; FName:'fd.dat'; Form: TForDisposeForm),
    (Name:'�������� ������������'; FName:'dpd.dat'; Form: TDisposedForm)
  );

  SNoTemplate = '��� ����� ����� ������';

procedure EquipReport(AID: TID);
procedure BarcodeReport();
procedure MovingReport(AID: TID);
procedure WPReport(AID: TID);
procedure InvReport(AID: TID);
procedure DisposeReport(sa: TSpecAction; OrgID: TID; DateBegin: TDateTime = 0;
  DateEnd: TDateTime = 0);

implementation

function BM(AName, AValue: string): BookMark;
begin
  Result.Name := AName;
  Result.Value := AValue;
end;

function BMValue(BMs: TBookmarks; AName: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to High(BMs) do
    if BMs[i].Name = AName then
    begin
      Result := BMs[i].Value;
      Break;
    end;
end;

procedure EquipReport(AID: TID);
var
  W, BMCol: OleVariant;
  i, Idx, RepIdx: Integer;
  ReportFName: string;
  DS: TDataSet;
  Bookmarks: TBookMarks;
  procedure AddBM(AName, AValue: string);
  begin
    Bookmarks[PostInc(Idx)] := BM(AName, AValue);
  end;
begin
    try
      RepIdx := Ord(saEquipmentReport) - 1;
      ReportFName := AppPath + ReportInfo[RepIdx].FName;
      if not FileExists(ReportFName) then
        raise Exception.Create(SNoTemplate);

      ForceDirectories(AppPath + ResultsDir);
      W := CreateOleObject('Word.Application');
      W.Options.CheckSpellingAsYouType := False;
      W.Options.CheckGrammarAsYouType := False;
      W.Documents.Open(ReportFName);
      BMCol := W.ActiveDocument.Bookmarks;

      Manager.SpecAction(saEquipmentReport, [AID], False);
      DS := ArsenalInstance.DS.DataSet;
      SetLength(Bookmarks, DS.Fields.Count);
      Idx := 0;
      DS.First;
      for I := 0 to DS.Fields.Count - 1 do
        AddBM(DS.Fields[i].FieldName, DS.Fields[i].Value);
      for I := 0 to High(Bookmarks) do
        if BMCol.Exists(Bookmarks[I].Name) then
        begin
          W.Selection.GoTo(-1, , , Bookmarks[I].Name);
          W.Selection.TypeText(Bookmarks[I].Value);
        end;
      ReportFName := Format('%s%s\%s_%s %s_%s.doc', [AppPath, ResultsDir,
        ReportInfo[RepIdx].Name, '���.�', BMValue(Bookmarks, 'InvN'),
        StringReplace(BMValue(Bookmarks, 'DateTitle'), ':', '-', [rfReplaceAll])]);
      W.ActiveDocument.SaveAs(ReportFName);
      W.ActiveDocument.Close;
      W.Documents.Open(ReportFName);

      W.Visible := True;
    except
      on E: Exception do
      begin
        ShowMessage(e.Message);
        if not VarIsNull(W) then
          W.Quit;
      end;
    end;
end;

procedure BarcodeReport();
const
  ColCount = 4;
var
  W, Table, Cell, CaptionPar: OleVariant;
  i, Idx, RepIdx, Count: Integer;
  ReportFName: string;
  DS: TDataSet;
  FontName: string;
begin
  if not CheckBarcodePrintReady then
    Exit;
  try
    ArsenalInstance.Switch(xeEquipment);
    DS := ArsenalInstance.DS.DataSet;
    Count := DS.RecordCount;
    if Count = 0 then
      Exit;

    RepIdx := Ord(saBarcodeReport) - 1;

    ForceDirectories(AppPath + ResultsDir);
    W := CreateOleObject('Word.Application');
    W.Options.CheckSpellingAsYouType := False;
    W.Options.CheckGrammarAsYouType := False;
    W.Documents.Add;

    W.ActiveDocument.Tables.Add(W.Selection.Range, Count div ColCount + 1, ColCount);
    Table := W.ActiveDocument.Tables.Item(1);
    Table.Columns.AutoFit;
    Table.Borders.OutsideLineStyle := 1; // wdLineStyleSingle;
    Table.Borders.InsideLineStyle := 1; // wdLineStyleSingle;
    Idx := 0;
    FontName := ChangeFileExt(EanFontFName, '');
    DS.First;
    while not DS.Eof do
    begin
      Cell := Table.Cell(Idx mod ColCount + 1, Idx div ColCount + 1);
      Cell.Range.Text := DS.FieldValues['Barcode'];
      Cell.Range.Font.Name := FontName;
      Cell.Range.Font.Size := 36;
      CaptionPar := Cell.Range.Paragraphs.Add;
      CaptionPar.Range.Font.Name := 'Arial';
      CaptionPar.Range.Font.Size := 12;
      CaptionPar.Range.Text := DS.FieldValues['Caption'];
      DS.Next;
    end;

    ReportFName := Format('%s%s\%s_%s.doc', [AppPath, ResultsDir,
      ReportInfo[RepIdx].Name, FormatDateTime(DateTimeFormatForFileName, Now)]);
    W.ActiveDocument.SaveAs(ReportFName);
    W.ActiveDocument.Close;
    W.Documents.Open(ReportFName);

    W.Visible := True;
  except
    on E: Exception do
    begin
      ShowMessage(e.Message);
      if not VarIsNull(W) then
        W.Quit;
    end;
  end;
end;

procedure MovingReport(AID: TID);
var
  W, BMCol, Table: OleVariant;
  i, Idx, RepIdx, RowCount, Col, Row: Integer;
  ReportFName: string;
  DS: TDataSet;
  Bookmarks: TBookMarks;
  procedure AddBM(AName, AValue: string);
  begin
    Bookmarks[PostInc(Idx)] := BM(AName, AValue);
  end;
  function NextCell: OleVariant;
  begin
    Result := Table.Cell(Row, PostInc(Col)).Range;
  end;
begin
    try
      RepIdx := Ord(saMovingReport) - 1;
      ReportFName := AppPath + ReportInfo[RepIdx].FName;
      if not FileExists(ReportFName) then
        raise Exception.Create(SNoTemplate);

      ForceDirectories(AppPath + ResultsDir);
      W := CreateOleObject('Word.Application');
      W.Options.CheckSpellingAsYouType := False;
      W.Options.CheckGrammarAsYouType := False;
      W.Documents.Open(ReportFName);
      BMCol := W.ActiveDocument.Bookmarks;

      Manager.SpecAction(saMovingReport, [AID], False);
      DS := ArsenalInstance.DS.DataSet;
      SetLength(Bookmarks, DS.RecordCount);
      Idx := 0;
      RowCount := 0;
      DS.First;
      while not DS.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
          AddBM(DS.FieldValues['Reason'], DS.FieldValues['Worker'])
        else
          Inc(RowCount);
        DS.Next;
      end;
      for I := 0 to High(Bookmarks) do
        if BMCol.Exists(Bookmarks[I].Name) then
        begin
          W.Selection.GoTo(-1, , , Bookmarks[I].Name);
          W.Selection.TypeText(Bookmarks[I].Value);
        end;

      Table := W.ActiveDocument.Tables.Item(3);
      for I := 0 to RowCount - 2 do
        Table.Rows.Add(Table.Rows.Item(2));
//      Table.Columns.AutoFit;
//      Table.Borders.OutsideLineStyle := 1; // wdLineStyleSingle;
//      Table.Borders.InsideLineStyle := 1; // wdLineStyleSingle;

      DS.First;
      Row := 2;
      while not ds.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
        begin
          DS.Next;
          Continue;
        end;
        Col := 1;
        NextCell.Text := DS.FieldValues['idx'];
        NextCell.Text := DS.FieldValues['Workplace'];
        NextCell.Text := DS.FieldValues['MoveDate'];
        NextCell.Text := DS.FieldValues['Reason'];
        NextCell.Text := DS.FieldValues['Worker'];
        Inc(Row);
        DS.Next;
      end;
        
      ReportFName := Format('%s%s\%s_%s %s_%s.doc', [AppPath, ResultsDir,
        ReportInfo[RepIdx].Name, '���.�', BMValue(Bookmarks, 'InvN'),
        StringReplace(BMValue(Bookmarks, 'DateTitle'), ':', '-', [rfReplaceAll])]);
      W.ActiveDocument.SaveAs(ReportFName);
      W.ActiveDocument.Close;
      W.Documents.Open(ReportFName);

      W.Visible := True;
    except
      on E: Exception do
      begin
        ShowMessage(e.Message);
        if not VarIsNull(W) then
          W.Quit;
      end;
    end;
end;

procedure WPReport(AID: TID);
var
  W, BMCol, Table: OleVariant;
  i, Idx, RepIdx, RowCount, Col, Row: Integer;
  ReportFName: string;
  DS: TDataSet;
  Bookmarks: TBookMarks;
  procedure AddBM(AName, AValue: string);
  begin
    Bookmarks[PostInc(Idx)] := BM(AName, AValue);
  end;
  function NextCell: OleVariant;
  begin
    Result := Table.Cell(Row, PostInc(Col)).Range;
  end;
begin
    try
      RepIdx := Ord(saWPReport) - 1;
      ReportFName := AppPath + ReportInfo[RepIdx].FName;
      if not FileExists(ReportFName) then
        raise Exception.Create(SNoTemplate);

      ForceDirectories(AppPath + ResultsDir);
      W := CreateOleObject('Word.Application');
      W.Options.CheckSpellingAsYouType := False;
      W.Options.CheckGrammarAsYouType := False;
      W.Documents.Open(ReportFName);
      BMCol := W.ActiveDocument.Bookmarks;

      Manager.SpecAction(saWPReport, [AID], False);
      DS := ArsenalInstance.DS.DataSet;
      SetLength(Bookmarks, DS.RecordCount);
      Idx := 0;
      RowCount := 0;
      DS.First;
      while not DS.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
          AddBM(DS.FieldValues['type'], DS.FieldValues['InvN'])
        else
          Inc(RowCount);
        DS.Next;
      end;
      for I := 0 to High(Bookmarks) do
        if BMCol.Exists(Bookmarks[I].Name) then
        begin
          W.Selection.GoTo(-1, , , Bookmarks[I].Name);
          W.Selection.TypeText(Bookmarks[I].Value);
        end;

      Table := W.ActiveDocument.Tables.Item(2);
      for I := 0 to RowCount - 2 do
        Table.Rows.Add(Table.Rows.Item(2));
//      Table.Columns.AutoFit;
//      Table.Borders.OutsideLineStyle := 1; // wdLineStyleSingle;
//      Table.Borders.InsideLineStyle := 1; // wdLineStyleSingle;

      DS.First;
      Row := 2;
      while not ds.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
        begin
          DS.Next;
          Continue;
        end;
        Col := 1;
        NextCell.Text := DS.FieldValues['idx'];
        NextCell.Text := DS.FieldValues['Type'];
        NextCell.Text := DS.FieldValues['InvN'];
        NextCell.Text := DS.FieldValues['Model'];
        NextCell.Text := DS.FieldValues['InstDate'];
        NextCell.Text := DS.FieldValues['Price'];
        Inc(Row);
        DS.Next;
      end;
        
      ReportFName := Format('%s%s\%s_%s_%s.doc', [AppPath, ResultsDir,
        ReportInfo[RepIdx].Name, BMValue(Bookmarks, 'WPName'),
        StringReplace(BMValue(Bookmarks, 'DateTitle'), ':', '-', [rfReplaceAll])]);
      W.ActiveDocument.SaveAs(ReportFName);
      W.ActiveDocument.Close;
      W.Documents.Open(ReportFName);

      W.Visible := True;
    except
      on E: Exception do
      begin
        ShowMessage(e.Message);
        if not VarIsNull(W) then
          W.Quit;
      end;
    end;
end;

procedure InvReport(AID: TID);
var
  W, BMCol, Table: OleVariant;
  i, Idx, RepIdx, RowCount, Col, Row: Integer;
  ReportFName: string;
  DS: TDataSet;
  Bookmarks: TBookMarks;
  procedure AddBM(AName, AValue: string);
  begin
    Bookmarks[PostInc(Idx)] := BM(AName, AValue);
  end;
  function NextCell: OleVariant;
  begin
    Result := Table.Cell(Row, PostInc(Col)).Range;
  end;
begin
    try
      RepIdx := Ord(saInvReport) - 1;
      ReportFName := AppPath + ReportInfo[RepIdx].FName;
      if not FileExists(ReportFName) then
        raise Exception.Create(SNoTemplate);

      ForceDirectories(AppPath + ResultsDir);
      W := CreateOleObject('Word.Application');
      W.Options.CheckSpellingAsYouType := False;
      W.Options.CheckGrammarAsYouType := False;
      W.Documents.Open(ReportFName);
      BMCol := W.ActiveDocument.Bookmarks;

      Manager.SpecAction(saInvReport, [AID], False);
      DS := ArsenalInstance.DS.DataSet;
      SetLength(Bookmarks, DS.RecordCount);
      Idx := 0;
      RowCount := 0;
      DS.First;
      while not DS.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
          AddBM(DS.FieldValues['Model'], DS.FieldValues['InvN'])
        else
          Inc(RowCount);
        DS.Next;
      end;
      for I := 0 to High(Bookmarks) do
        if BMCol.Exists(Bookmarks[I].Name) then
        begin
          W.Selection.GoTo(-1, , , Bookmarks[I].Name);
          W.Selection.TypeText(Bookmarks[I].Value);
        end;

      Table := W.ActiveDocument.Tables.Item(1);
      for I := 0 to RowCount - 2 do
        Table.Rows.Add(Table.Rows.Item(2));
//      Table.Columns.AutoFit;
//      Table.Borders.OutsideLineStyle := 1; // wdLineStyleSingle;
//      Table.Borders.InsideLineStyle := 1; // wdLineStyleSingle;

      DS.First;
      Row := 2;
      while not ds.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
        begin
          DS.Next;
          Continue;
        end;
        Col := 1;
        NextCell.Text := DS.FieldValues['idx'];
        NextCell.Text := DS.FieldValues['Model'];
        NextCell.Text := DS.FieldValues['InvN'];
        NextCell.Text := DS.FieldValues['BuyDate'];
        NextCell.Text := DS.FieldValues['Workplace'];
        NextCell.Text := DS.FieldValues['Price'];
        Inc(Row);
        DS.Next;
      end;
        
      ReportFName := Format('%s%s\%s_%s_%s.doc', [AppPath, ResultsDir,
        ReportInfo[RepIdx].Name, BMValue(Bookmarks, 'InvName'),
        StringReplace(BMValue(Bookmarks, 'DateTitle'), ':', '-', [rfReplaceAll])]);
      W.ActiveDocument.SaveAs(ReportFName);
      W.ActiveDocument.Close;
      W.Documents.Open(ReportFName);

      W.Visible := True;
    except
      on E: Exception do
      begin
        ShowMessage(e.Message);
        if not VarIsNull(W) then
          W.Quit;
      end;
    end;
end;

procedure DisposeReport(sa: TSpecAction; OrgID: TID; DateBegin: TDateTime = 0;
  DateEnd: TDateTime = 0);
var
  W, BMCol, Table: OleVariant;
  i, Idx, RepIdx, RowCount, Col, Row: Integer;
  ReportFName: string;
  DS: TDataSet;
  Bookmarks: TBookMarks;
  procedure AddBM(AName, AValue: string);
  begin
    Bookmarks[PostInc(Idx)] := BM(AName, AValue);
  end;
  function NextCell: OleVariant;
  begin
    Result := Table.Cell(Row, PostInc(Col)).Range;
  end;
begin
    try
      RepIdx := Ord(sa) - 1;
      ReportFName := AppPath + ReportInfo[RepIdx].FName;
      if not FileExists(ReportFName) then
        raise Exception.Create(SNoTemplate);

      ForceDirectories(AppPath + ResultsDir);
      W := CreateOleObject('Word.Application');
      W.Options.CheckSpellingAsYouType := False;
      W.Options.CheckGrammarAsYouType := False;
      W.Documents.Open(ReportFName);
      BMCol := W.ActiveDocument.Bookmarks;

      Manager.SpecAction(sa, [OrgID, TernOP(DateBegin = 0, ID_NULL,
        DateBegin), TernOP(DateEnd = 0, ID_NULL, DateEnd)], False);
      DS := ArsenalInstance.DS.DataSet;
      SetLength(Bookmarks, DS.RecordCount);
      Idx := 0;
      RowCount := 0;
      DS.First;
      while not DS.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
          AddBM(DS.FieldValues['Model'], DS.FieldValues['InvN'])
        else
          Inc(RowCount);
        DS.Next;
      end;
      for I := 0 to High(Bookmarks) do
        if BMCol.Exists(Bookmarks[I].Name) then
        begin
          W.Selection.GoTo(-1, , , Bookmarks[I].Name);
          W.Selection.TypeText(Bookmarks[I].Value);
        end;

      Table := W.ActiveDocument.Tables.Item(1);
      for I := 0 to RowCount - 2 do
        Table.Rows.Add(Table.Rows.Item(2));
//      Table.Columns.AutoFit;
//      Table.Borders.OutsideLineStyle := 1; // wdLineStyleSingle;
//      Table.Borders.InsideLineStyle := 1; // wdLineStyleSingle;

      DS.First;
      Row := 2;
      while not ds.Eof do
      begin
        if DS.FieldByName('idx').AsInteger = 0 then
        begin
          DS.Next;
          Continue;
        end;
        Col := 1;
        NextCell.Text := DS.FieldValues['idx'];
        NextCell.Text := DS.FieldValues['Model'];
        NextCell.Text := DS.FieldValues['InvN'];
        NextCell.Text := DS.FieldValues['BuyDate'];
        NextCell.Text := DS.FieldValues[TernOP(
          sa = saForDisposeReport, 'Workplace', 'DisposeDate')];
        NextCell.Text := DS.FieldValues['Price'];
        Inc(Row);
        DS.Next;
      end;
        
      ReportFName := Format('%s%s\%s_%s_%s.doc', [AppPath, ResultsDir,
        ReportInfo[RepIdx].Name, BMValue(Bookmarks, 'BuyerTitle'),
        StringReplace(BMValue(Bookmarks, 'DateTitle'), ':', '-', [rfReplaceAll])]);
      W.ActiveDocument.SaveAs(ReportFName);
      W.ActiveDocument.Close;
      W.Documents.Open(ReportFName);

      W.Visible := True;
    except
      on E: Exception do
      begin
        ShowMessage(e.Message);
        if not VarIsNull(W) then
          W.Quit;
      end;
    end;
end;

{$REGION ' BaseReportForm '}

procedure TBaseReportForm.DoRun(Sender: TObject);
begin

end;

function TBaseReportForm.GetSpecAction: TSpecAction;
begin
  Result := saNone;
end;

procedure TBaseReportForm.ObtainData;
begin
  // nothing
end;

procedure TBaseReportForm.Start;
begin
  FreeAndNil(DBGrid1);
  Panel2.Align := alBottom;
//  Panel1.Constraints.MaxHeight := 40;
  BtnRun := TButtonUsed.Create(Self);
  BtnRun.Parent := Panel2;
  BtnRun.Caption := SRun;
  BtnRun.OnClick := DoRun;
  BtnRun.Width := 130;
  MainFormInstance.TreeIcons.GetBitmap(Ord(xeReport), BtnRun.Glyph);
  BtnRun.Align := alLeft;
end;

{$ENDREGION ' BaseReportForm '}

{$ENDREGION ' TDisposeForm '}

procedure TDisposeForm.DoRun(Sender: TObject);
var
  DB, DE: TDateTime;
begin
  if VarIsNull(dtpDateBegin.Value) then
    DB := 0
  else
    DB := VarToDateTime(dtpDateBegin.Value);
  if VarIsNull(dtpDateEnd.Value) then
    DE := 0
  else
    DE := VarToDateTime(dtpDateEnd.Value);
  DisposeReport(SpecAction, cbbOrganisation.KeyValue, DB, DE);
end;

procedure TDisposeForm.Start;
begin
  inherited;
  dtpDateEnd := TDaterUsed.Create(Self);
  dtpDateEnd.Parent := Panel1;
  dtpDateEnd.Align := alLeft;
  dtpDateBegin := TDaterUsed.Create(Self);
  dtpDateBegin.Parent := Panel1;
  dtpDateBegin.Align := alLeft;
  cbbOrganisation := TComboUsed.Create(Self);
  cbbOrganisation.Parent := Panel1;
  cbbOrganisation.Align := alLeft;
  Manager.GetData(xeOrganisation, [1]);
  cbbOrganisation.ListSource := ArsenalInstance.DS;
  cbbOrganisation.KeyField := 'ID';
  cbbOrganisation.ListField := 'Caption';
  cbbOrganisation.BoundLabel.Caption := Russian('Organisation');
end;

{ TDisposedForm }

function TDisposedForm.GetSpecAction: TSpecAction;
begin
  Result := saDisposedReport
end;

{ TForDisposeForm }

function TForDisposeForm.GetSpecAction: TSpecAction;
begin
  Result := saForDisposeReport;
end;

{$ENDREGION ' TDisposeForm '}

end.

