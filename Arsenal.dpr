program Arsenal;

uses
  Forms,
  UHelpForm in 'UHelpForm.pas' {HelpForm},
  ChildForms in 'ChildForms.pas' {BaseChildForm},
  Common in 'Common.pas',
  ArsenalDB in 'ArsenalDB.pas',
  MainForm in 'MainForm.pas' {MainForm},
  ConnectForm in 'ConnectForm.pas' {ConnectForm},
  AboutBox in 'AboutBox.pas',
  DetailsForm in 'DetailsForm.pas',
  SpecialForms in 'SpecialForms.pas',
  PartDetailsForm in 'PartDetailsForm.pas',
  StructureForm in 'StructureForm.pas',
  InventarisationForm in 'InventarisationForm.pas' {InvFlowForm};

{$R *.res}

begin
  {$IFDEF DEBUG}
  {$IF COMPILERVERSION >= 18}
  ReportMemoryLeaksOnShutdown := True;
  {$IFEND}
  {$ENDIF}
  Randomize;
  Application.Initialize;
  ArsenalInstance;
  MainFormInstance;
  Application.Run;
end.
