unit StructureForm;

interface

uses
// Delphi units
  Windows, SysUtils, Variants, Classes, Controls, Forms, StdCtrls, DB, CommCtrl,
  DBCtrls, Dialogs, XPMan, Buttons, Grids, ExtCtrls, ComCtrls, DBGrids, ImgList,
  Graphics, Contnrs,
// Third Party
  sSkinProvider, sBitBtn, sPanel, sDBLookupComboBox, sDBNavigator,
  sGroupBox, sEdit, RXDBCtrl, acAlphaImageList, DBCtrlsEh,
// Own units
  Common, ArsenalDB, ChildForms, sMemo, sSpeedButton, sTreeView;

type

  TStructureForm = class(TBaseChildForm)
  published
    grpStructure: TsGroupBox;
    TVStructure: TsTreeView;
    dgStructure: TRxDBGrid;
    sPanel1: TsPanel;
    dbnStructure: TsDBNavigator;
    grpEquipment: TsGroupBox;
    dgEquipment: TRxDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure TVStructureChange(Sender: TObject; Node: TTreeNode);
    procedure FormDestroy(Sender: TObject);
  private
    IDs: TObjectList;
    IDWorkPlace: TID;
    DSEquipment: TDataSet;
    SrcEquipment: TDataSource;
  protected
    procedure DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure RequeryEquipment;
    procedure FillStructure;
    procedure ClearStructure;
    procedure ObtainData; override;
  public
    procedure Start(); override;
    procedure Stop(NeedsSave: Boolean = False); override;
  end;

implementation

{$R *.dfm}

uses
//Own
  MainForm;

const
  STitle = '��������� � ������ �����������';

{$REGION ' TStructureForm '}

procedure TStructureForm.FillStructure;
var
  PrevLevel, Level: Integer;
  tn, vn, root: TTreeNode;
  Obj: TObject;
  Attach: TNodeAttachMode;
  function V(Name: string): Variant;
  begin
    Result := DataSet.FieldValues[Name];
  end;
begin
  Level := 0;
  ClearStructure;
  DataSet.First;
  tn := nil;
  vn := nil;
  root := nil;
  TVStructure.Images := MainFormInstance.TreeIcons;
  while not DataSet.Eof do
  begin
    PrevLevel := Level;
    Level := V('Level');
    Attach := naAdd;
    if Level > PrevLevel then
    begin
      Attach := naAddChild;
    end;  

    if Level = 2 then
      Obj := IDs[IDs.Add(TStringObj.Create(V('ID')))]
    else
      Obj := nil;
    if Level = 0 then
      tn := root;
    if (Level = 1) and (vn <> nil) then
      tn := vn;

    tn := TVStructure.Items.AddNode(nil, tn, V('Caption'), Obj, Attach);
    if (Level = 0) then vn := nil;
    if (Level = 1) then vn := tn;
    case Level of
      0: tn.ImageIndex := Ord(xeOrganisation);
      1: tn.ImageIndex := Ord(xeDepartment);
      2: tn.ImageIndex := Ord(xeWorkplace);
    end;
    tn.SelectedIndex := tn.ImageIndex;
    if Level = 0 then
      root := tn;
    Dataset.Next;
  end;
//  TVStructure.FullExpand;
//  WindowState := wsMaximized;
end;

procedure TStructureForm.FormCreate(Sender: TObject);
begin
  Caption := STitle;
  grpEquipment.Caption := Russian('Equipment');
  grpStructure.Caption := Russian('Structure');
  Panel1.Hide;
  IDs := TObjectList.Create;
end;

procedure TStructureForm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(IDs);
end;

procedure TStructureForm.Start();
var
  CanInsert, CanUpdate: Boolean;
begin
  inherited;
  ArsenalInstance.Switch(xeStructure);
  DataSource := ArsenalInstance.DS;
  FDataSet := DataSource.DataSet;

  dbnStructure.DataSource := DataSource;

  CanInsert := ArsenalInstance.EntityActionAccessible[xeMoving, daInsert];
  CanUpdate := ArsenalInstance.EntityActionAccessible[xeMoving, daUpdate];
  ShowDBNavButton(dbnStructure, nbInsert, CanInsert);
  ShowDBNavButton(dbnStructure, nbEdit, CanUpdate);
  ShowDBNavButton(dbnStructure, nbPost, CanInsert or CanUpdate);
  ShowDBNavButton(dbnStructure, nbDelete, ArsenalInstance.EntityActionAccessible[xeMoving, daDelete]);

  dbnStructure.BeforeAction := DBNavBeforeAction;
  dbnStructure.Tag := Ord(xeStructure);

  SetupGrid(dgEquipment);
//  FillStructure;
end;

procedure TStructureForm.Stop(NeedsSave: Boolean = False);
begin
  inherited;
  ClearStructure;
end;

procedure TStructureForm.TVStructureChange(Sender: TObject; Node: TTreeNode);
begin
  inherited;
  if Assigned(Node.Data) then
  begin
    IDWorkPlace := TStringObj(Node.Data).S;
    RequeryEquipment;
  end
  else
    dgEquipment.DataSource := nil;
end;

procedure TStructureForm.ObtainData;
begin
  inherited;
  FillStructure;
end;

procedure TStructureForm.RequeryEquipment;
begin
  if not Assigned(DSEquipment) then
  begin
    ArsenalInstance.Ally := 1;
    DSEquipment := Manager.GetData(xeEquipment, []);
    SrcEquipment := ArsenalInstance.DS;
    ArsenalInstance.Ally := 0;
    ModifyDataSet(DSEquipment, xeEquipment);
    DSEquipment.FieldByName('EquipmentType').Visible := False;
    DSEquipment.FieldByName('Vendor').Visible := False;
    DSEquipment.FieldByName('Bill').Visible := False;
    DSEquipment.FieldByName('VisualNumber').Visible := False;
    DSEquipment.FieldByName('Barcode').Visible := False;
    DSEquipment.FieldByName('LifeTime').Visible := False;
    DSEquipment.FieldByName('Product').Visible := False;
    DSEquipment.FieldByName('Price').Visible := False;
    DSEquipment.FieldByName('Settings').Visible := False;
    DSEquipment.FieldByName('WorkPlace').Visible := False;
    DSEquipment.FieldByName('InvNumber').Index := 0;
  end;
  DSEquipment.DisableControls;
  dgEquipment.DataSource := SrcEquipment;
  DSEquipment.Filtered := False;
  DSEquipment.Filter := 'ID_WorkPlace = ' + QuotedStr(IDWorkplace);
  DSEquipment.Filtered := True;
  ManageColumns(DSEquipment, dgEquipment);
  DSEquipment.EnableControls;
  Resize;
end;

procedure TStructureForm.ClearStructure;
begin
  TVStructure.Items.Clear;
  IDs.Clear;
end;

procedure TStructureForm.DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
//var
//  DBNav: TsDBNavigator absolute Sender;
//  Entity: TEntity;
//  DS: TDataSet;
//  function V(Name: string): Variant;
//  begin
//    Result := DS.FieldValues[Name];
//  end;
begin
//  if not (Sender is TsDBNavigator) then
//    Exit;
//  if not (Button in [nbPost, nbDelete]) then
//    Exit;
//  DS := DBNav.DataSource.DataSet;
//  Entity := TEntity(DBNav.Tag);
//  if Entity = xeMoving then
//    if DS.State = dsInsert then
//      Manager.Insert(Entity, [V('ID_Workplace'), V('ID_MovingType'),
//        ID, ID_NULL, V('ID_Person'), V('Date'), V('Note')])
//    else if DS.State = dsEdit then
//      if Manager.Update(Entity, [V('ID'), V('ID_Workplace'), V('ID_MovingType'),
//        ID, ID_NULL, V('ID_Person'), V('Date'), V('Note')]) <> 0
//      then
//        ShowMessage('������ ������� �������� ��������������, ��������, ������ � ��������� �����');
//  if Entity = xeMoving then
//    RequeryEquipment;
//  Resize;
  Abort; // cancel standard dbnav action
end;

{$ENDREGION ' TStructureForm '}

end.

