
IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'Arsenal')	
CREATE
--USE MASTER DROP
DATABASE [Arsenal] 
GO

USE [Arsenal]
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Office')	
	EXEC('CREATE Schema [Office]')
	GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Arsenal')		
	EXEC('CREATE Schema [Arsenal]')
	GO

IF OBJECT_ID('[Arsenal].dbo.[Operations]') IS NULL	
	CREATE TABLE [Arsenal].dbo.[Operations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Operation_ID PRIMARY KEY, 
		ProcedureName nvarchar(64) NOT NULL,
		Caption nvarchar(256) NOT NULL
	)

IF OBJECT_ID('[Arsenal].[Office].[Purposes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Purposes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Purpose_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		[Priority] tinyint NOT NULL DEFAULT 255
	)

IF OBJECT_ID('[Arsenal].[Office].[Organisations]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Organisations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Organisation_ID PRIMARY KEY, 
		--Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		[Address] nvarchar(256) NULL,
		[Phone] nvarchar(64) NULL
	)

IF OBJECT_ID('[Arsenal].[Office].[OrganisationPurposes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[OrganisationPurposes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_OrganisationPurpose_ID PRIMARY KEY, 
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		ID_Purpose uniqueidentifier NOT NULL REFERENCES Office.Purposes(ID)
	)

IF OBJECT_ID('[Arsenal].[Office].[Persons]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Persons]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Person_ID PRIMARY KEY,
		[Login] nvarchar(32) NOT NULL,
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),		
		LastName nvarchar(32) NOT NULL,
		FirstName nvarchar(32) NOT NULL,
		MiddleName nvarchar(32) NOT NULL default '',
		BirthDate date NULL,
		Phone nvarchar(128) NULL default '',
		EMail nvarchar(128) NULL default '',
		Caption AS LastName + ' ' + FirstName + ' ' + MiddleName,
		CONSTRAINT UQ_Person_Login UNIQUE([Login])
	)  

IF OBJECT_ID('[Arsenal].[Office].[Jobs]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Jobs]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Job_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		CONSTRAINT UQ_Job_Name UNIQUE([Name])
	)
IF OBJECT_ID('[Arsenal].[Office].[PersonJobs]') IS NULL		
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[PersonJobs]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_PersonJob_ID PRIMARY KEY, 
		ID_Person uniqueidentifier NOT NULL REFERENCES Office.Persons(ID),
		ID_Job uniqueidentifier NOT NULL REFERENCES Office.Jobs(ID),
		BeginDate date NOT NULL,
		EndDate date NULL
	)

IF OBJECT_ID('[Arsenal].[Office].[JobOperations]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[JobOperations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_JobOperation_ID PRIMARY KEY, 
		ID_Job uniqueidentifier NOT NULL REFERENCES Office.Jobs(ID),
		ID_Operation uniqueidentifier NOT NULL REFERENCES Operations(ID),
		BeginDate date NOT NULL,
		EndDate date NULL,
		Managable bit NOT NULL default 1,
		Personal bit NOT NULL default 0
	)

IF OBJECT_ID('[Arsenal].[Office].[Departments]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Departments]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Department_ID PRIMARY KEY, 
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		--Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL
		-- department requisites
	) 

IF OBJECT_ID('[Arsenal].[Office].[Workers]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Workers]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Worker_ID PRIMARY KEY,
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),		
		LastName nvarchar(32) NOT NULL,
		FirstName nvarchar(32) NOT NULL,
		MiddleName nvarchar(32) NOT NULL default '',
		Phone nvarchar(128) NULL default '',
		EMail nvarchar(128) NULL default '',
		Caption AS LastName + ' ' + FirstName + ' ' + MiddleName
	) 

IF OBJECT_ID('[Arsenal].[Office].[WorkPlaces]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[WorkPlaces]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_WorkPlace_ID PRIMARY KEY, 
		ID_Department uniqueidentifier NOT NULL REFERENCES Office.Departments(ID),
		ID_Worker uniqueidentifier NULL REFERENCES Office.Workers(ID),
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		Cabinet nvarchar(16) NULL default '',
		CONSTRAINT UQ_Workplace_Name UNIQUE([Name])
	) 

IF OBJECT_ID('[Arsenal].[Office].[Bills]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Bills]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Bill_ID PRIMARY KEY, 
		ID_Buyer uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		ID_Seller uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		Date date NOT NULL,
		Invoice nvarchar(64) NOT NULL default ''
	) 	 

IF OBJECT_ID('[Arsenal].[Arsenal].[Vendors]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Vendors]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Vendor_ID PRIMARY KEY, 
		--Name nvarchar(64) NOT NULL,
		Caption nvarchar(256) NOT NULL
		--,CONSTRAINT UQ_Vendor_Name UNIQUE([Name])
	)

IF OBJECT_ID('[Arsenal].[Arsenal].[EquipmentTypes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[EquipmentTypes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_EquipmentType_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL
	)

IF OBJECT_ID('[Arsenal].[Arsenal].[MovingTypes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[MovingTypes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_MovingType_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		CONSTRAINT UQ_MovingType_Name UNIQUE([Name])
	)		 

IF OBJECT_ID('[Arsenal].[Arsenal].[Inventarisations]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Inventarisations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Inventarisation_ID PRIMARY KEY, 
		ID_Worker uniqueidentifier NULL REFERENCES [Office].[Workers](ID),
		Caption nvarchar(256) NOT NULL,
		BeginDate date NULL,
		EndDate date NULL
	)
	
IF OBJECT_ID('[Arsenal].[Arsenal].[Equipments]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Equipments]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Equipment_ID PRIMARY KEY, 
		ID_EquipmentType uniqueidentifier NULL REFERENCES Arsenal.EquipmentTypes(ID),
		ID_Bill uniqueidentifier NULL REFERENCES [Office].[Bills](ID),
		ID_Vendor uniqueidentifier NULL REFERENCES Arsenal.Vendors(ID),
		Caption nvarchar(256) NOT NULL default '',
		InvNumber nvarchar(32) NULL,
		VisualNumber nvarchar(32) NULL,
		Barcode bigint NULL,
		Price money NULL,
		Lifetime int NULL,
		Settings nvarchar(512) NULL
	)	
	
IF OBJECT_ID('[Arsenal].[Arsenal].[Parts]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Parts]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Part_ID PRIMARY KEY, 
		ID_Equipment uniqueidentifier NULL REFERENCES Arsenal.Equipments(ID),
		ID_EquipmentType uniqueidentifier NULL REFERENCES Arsenal.EquipmentTypes(ID),
		ID_Bill uniqueidentifier NULL REFERENCES [Office].[Bills](ID),
		ID_Vendor uniqueidentifier NULL REFERENCES Arsenal.Vendors(ID),
		Caption nvarchar(256) NOT NULL default '',
		VisualNumber nvarchar(32) NULL,
		Price money NULL,
		Settings nvarchar(512)
	)	

IF OBJECT_ID('[Arsenal].[Arsenal].[Movings]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Movings]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Moving_ID PRIMARY KEY, 
		ID_Workplace uniqueidentifier NOT NULL REFERENCES Office.Workplaces(ID),
		ID_MovingType uniqueidentifier NOT NULL REFERENCES Arsenal.MovingTypes(ID),
		ID_Equipment uniqueidentifier NULL REFERENCES Arsenal.Equipments(ID),
		ID_Part uniqueidentifier NULL REFERENCES Arsenal.Parts(ID),
		ID_Person uniqueidentifier NULL REFERENCES [Office].[Persons](ID),
		Date datetime NOT NULL,
		Note nvarchar(512) NULL,
		CONSTRAINT [���� ���� ������������ ���� ��������������] CHECK 
			((CASE WHEN ID_Equipment IS NOT NULL THEN 1 ELSE 0 END + CASE WHEN ID_Part IS NOT NULL THEN 1 ELSE 0 END) = 1)
	)	  

IF OBJECT_ID('[Arsenal].[Arsenal].[Results]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Results]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Result_ID PRIMARY KEY, 
		ID_Inventarisation uniqueidentifier NULL REFERENCES [Arsenal].[Inventarisations](ID),
		ID_Equipment uniqueidentifier NULL REFERENCES Arsenal.Equipments(ID),
		ID_WorkPlace uniqueidentifier NULL REFERENCES [Office].[WorkPlaces](ID)
	)																		   	
-- System IsDeleted fields

ALTER TABLE [Arsenal].[dbo].[Operations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Persons] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Jobs] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[PersonJobs] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[JobOperations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Purposes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Organisations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[OrganisationPurposes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Departments] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Workers] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[WorkPlaces] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Bills] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Vendors] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[EquipmentTypes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[MovingTypes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Inventarisations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Equipments] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Parts] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Movings] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Results] ADD IsDeleted bit NOT NULL DEFAULT 0

DECLARE	@ID_Org uniqueidentifier = '9CE38B9F-766E-477B-A9AC-BDA05BBB048C'
DECLARE @ID_Admin uniqueidentifier = 'ADADADAD-ADAD-ADAD-ADAD-ADADADADADAD'	

IF NOT EXISTS(SELECT 1 FROM [Arsenal].[Office].[Organisations] WHERE ID = @ID_Org)
	INSERT [Arsenal].[Office].[Organisations](ID, Caption)
	SELECT @ID_Org, '�������� ������'

-- adding admin account without triggers and procedures, just for info-panel, admins' logins work independent
INSERT [Arsenal].Office.Persons([ID], [Login], [ID_Organisation], FirstName, LastName, BirthDate) SELECT @ID_Admin, 'Admin', @ID_Org, 'Admin', '', '19000101'

-- triggers for automatization and security

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'TR_CreateProcedure' AND type = 'TR')
   EXEC('CREATE TRIGGER [TR_CreateProcedure] ON DATABASE FOR CREATE_PROCEDURE AS BEGIN DECLARE @I int END')
GO

	ALTER TRIGGER [TR_CreateProcedure]
	ON DATABASE
	FOR CREATE_PROCEDURE
	AS
	BEGIN
		DECLARE @Command nvarchar(300)
		SET @Command='EXEC dbo.InsertOperation ''' + 
			EVENTDATA().value('(/EVENT_INSTANCE/SchemaName)[1]','nvarchar(300)')+'.'+EVENTDATA().value('(/EVENT_INSTANCE/ObjectName)[1]','nvarchar(300)') +
			''',''' + EVENTDATA().value('(/EVENT_INSTANCE/ObjectName)[1]','nvarchar(300)') + ''''
		EXECUTE(@Command)
	END

	GO
	ENABLE TRIGGER [TR_CreateProcedure] ON DATABASE


IF OBJECT_ID('[Office].[AlterPersonJob]') IS NULL	
   EXEC('CREATE TRIGGER [Office].[AlterPersonJob] ON [Office].[PersonJobs] FOR INSERT AS BEGIN DECLARE @I int END')
GO
	ALTER 
	--DROP 
	TRIGGER [Office].[AlterPersonJob]
	ON Office.PersonJobs
	FOR INSERT, UPDATE, DELETE  
	AS
	BEGIN
		DECLARE @JobRoleDel nvarchar(32), @UserNameDel nvarchar(32), @JobRoleIns nvarchar(32), @UserNameIns nvarchar(32)

		SELECT @JobRoleIns = Name
		FROM Office.Jobs oj
		JOIN INSERTED ON oj.ID = INSERTED.ID_Job
		WHERE INSERTED.IsDeleted = 0

		SELECT @UserNameIns = [Login]
		FROM Office.Persons om
		JOIN INSERTED ON om.ID = INSERTED.ID_Person
		WHERE INSERTED.IsDeleted = 0

		SELECT @JobRoleDel = Name
		FROM Office.Jobs oj
		JOIN DELETED ON oj.ID = DELETED.ID_Job

		SELECT @UserNameDel = [Login]
		FROM Office.Persons om
		JOIN DELETED ON om.ID = DELETED.ID_Person

		IF EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED) 
		BEGIN
			SELECT @JobRoleDel = Name
			FROM Office.Jobs oj
			JOIN INSERTED ON oj.ID = INSERTED.ID_Job
			WHERE INSERTED.IsDeleted = 1

			SELECT @UserNameDel = [Login]
			FROM Office.Persons om
			JOIN INSERTED ON om.ID = INSERTED.ID_Person
			WHERE INSERTED.IsDeleted = 1
		END

		IF (@JobRoleDel IS NOT NULL) AND (@UserNameDel IS NOT NULL)
		AND EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobRoleDel and Type = 'R') 
			EXEC ('sp_droprolemember ''' + @JobRoleDel + ''',''' + @UserNameDel + '''')
		IF (@JobRoleIns IS NOT NULL) AND (@UserNameIns IS NOT NULL)
		AND EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobRoleIns and Type = 'R')
			EXEC ('sp_addrolemember ''' + @JobRoleIns + ''',''' + @UserNameIns + '''')
	END
	GO

IF OBJECT_ID('[Office].[AlterJobRole]') IS NULL
   EXEC('CREATE TRIGGER [Office].[AlterJobRole] ON [Office].[Jobs] FOR INSERT AS BEGIN DECLARE @I int END')
GO
	ALTER 
	--DROP 
	TRIGGER  [Office].[AlterJobRole]
	ON Office.Jobs
	FOR INSERT, UPDATE, DELETE 
	AS
	BEGIN
		DECLARE @JobNameDel nvarchar(32), @JobNameIns nvarchar(32)
		SELECT TOP 1 @JobNameIns = Name FROM INSERTED WHERE INSERTED.IsDeleted = 0
		SELECT TOP 1 @JobNameDel = Name FROM DELETED
		IF EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED) 
			SELECT TOP 1 @JobNameDel = Name FROM INSERTED WHERE INSERTED.IsDeleted = 1
		IF EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobNameDel and Type = 'R')
			EXEC ('DROP ROLE ' + @JobNameDel)
		IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobNameDel and Type = 'R')
			EXEC ('CREATE ROLE ' + @JobNameIns)
	END
	GO

IF OBJECT_ID('[Office].[AlterJobOperation]') IS NULL
   EXEC('CREATE TRIGGER [Office].[AlterJobOperation] ON [Office].[JobOperations] FOR INSERT AS BEGIN DECLARE @I int END')
GO
	ALTER 
	--DROP 
	TRIGGER [Office].[AlterJobOperation]
	ON [Office].[JobOperations]
	FOR INSERT, UPDATE, DELETE  
	AS
	BEGIN		
		DECLARE @JobRoleDel nvarchar(32), @ProcNameDel nvarchar(64), @JobRoleIns nvarchar(32), @ProcNameIns nvarchar(64)
		SELECT @JobRoleIns = Name
		FROM Office.Jobs oj
		JOIN INSERTED ON oj.ID = INSERTED.ID_Job
		WHERE INSERTED.IsDeleted = 0

		SELECT @ProcNameIns = ProcedureName
		FROM Operations op
		JOIN INSERTED ON op.ID = INSERTED.ID_Operation
		WHERE INSERTED.IsDeleted = 0

		SELECT @JobRoleDel = Name
		FROM Office.Jobs oj
		JOIN DELETED ON oj.ID = DELETED.ID_Job

		SELECT @ProcNameDel = ProcedureName
		FROM Operations op
		JOIN DELETED ON op.ID = DELETED.ID_Operation

		IF EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED)
		BEGIN
			SELECT @JobRoleIns = Name
			FROM Office.Jobs oj
			JOIN INSERTED ON oj.ID = INSERTED.ID_Job
			WHERE INSERTED.IsDeleted = 1

			SELECT @ProcNameIns = ProcedureName
			FROM Operations op
			JOIN INSERTED ON op.ID = INSERTED.ID_Operation
			WHERE INSERTED.IsDeleted = 1
		END

		IF @ProcNameDel IS NOT NULL AND @JobRoleDel IS NOT NULL
			EXEC ('DENY EXECUTE ON ' + @ProcNameDel + ' TO ' + @JobRoleDel)
		IF @ProcNameIns IS NOT NULL AND @JobRoleIns IS NOT NULL
			EXEC ('GRANT EXECUTE ON ' + @ProcNameIns + ' TO ' + @JobRoleIns)
	END
	GO
	

GO