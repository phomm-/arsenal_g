-------------------------------------------- procedures dropper
/*
SELECT 'delete [dbo].[Operations] WHERE caption like''%examacces'''
UNION ALL
SELECT 'drop proc '+ss.name+'.'+st.name
FROM sys.objects st
JOIN sys.schemas ss ON ss.schema_id=st.schema_id
WHERE st.type = 'P' and st.name like '%examacces'
*/

-- dbo FIELDS FOR TABLES

USE [Arsenal]
GO

SELECT 'ALTER TABLE [Arsenal].['+ss.name+'].['+st.name+'] ADD IsDeleted bit NOT NULL DEFAULT 0'
FROM sys.tables st
	JOIN sys.schemas ss ON ss.schema_id=st.schema_id
WHERE st.object_id not in
	(SELECT sc.object_id
	FROM sys.columns sc
	WHERE sc.Name='IsDeleted')
GO

-- used for generation, doesn't need dbofields
IF OBJECT_ID('[Arsenal].[dbo].[TableSpecifications]') IS NULL
	CREATE 
	--DROP 
	TABLE [Arsenal].[dbo].[TableSpecifications]
	(
		[Schema] nvarchar(64), 
		Plural nvarchar(64), 
		Single nvarchar(64),
		IncludedColumns nvarchar(256),
		ExcludedColumns nvarchar(256),
		NullableColumns nvarchar(256),
		ToBeExcluded bit default 0
	)


DELETE [Arsenal].[dbo].[TableSpecifications]

INSERT [Arsenal].[dbo].[TableSpecifications]
SELECT 'dbo', 'sysdiagrams', '', '', '', '', 1
UNION SELECT 'dbo', 'TableSpecifications', '', '', '', '', 1
UNION SELECT 'Office', 'Persons', 'Person', NULL, 'Caption' , NULL, 0
UNION SELECT 'Office', 'Workers', 'Worker', NULL, 'Caption' , NULL, 0
UNION SELECT 'dbo', 'Operations', 'Operation', '', '', '', 1
UNION SELECT 'Arsenal', 'Inventarisations', 'Inventarisation', NULL, NULL , 'BeginDate;EndDate', 0
UNION SELECT 'Office', 'OrganisationPurposes', '', '', '', '', 1

--�����

USE [Arsenal]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE  name = 'Split' AND type = 'IF')
   EXEC('CREATE FUNCTION dbo.[Split] () RETURNS TABLE AS RETURN (SELECT 0 AS O)')
GO

ALTER FUNCTION dbo.[Split]
(
    @str nvarchar(max), 
    @separator char(1)
)
RETURNS TABLE
AS
	--SELECT * FROM dbo.Split('02000000-0000-0000-0000-000000000000,04000000-0000-0000-0000-000000000000',',')
RETURN 
	(
		WITH tokens(p, a, b) AS 
		(
			SELECT 
			   CAST(1 as bigint), 
			   CAST(1 as bigint), 
			   CHARINDEX(@separator, @str)
			UNION all
			SELECT
			   p + 1, 
			   b + 1, 
			   charindex(@separator, @str, b + 1)
			FROM tokens
			WHERE b > 0
		)
		SELECT
			p-1 as [Index],
			RTRIM(LTRIM(SUBSTRING(@str, a, CASE WHEN b > 0 THEN b-a ELSE LEN(@str) END))) 
			AS Value
		FROM tokens
	);

GO
--�����

USE [Arsenal]
GO

IF OBJECT_ID('dbo.[SplitString]') IS NULL
   EXEC('CREATE FUNCTION dbo.[SplitString]() RETURNS int AS BEGIN RETURN 0 END')
GO


ALTER FUNCTION dbo.[SplitString](@String nvarchar(max), @Delimiter nvarchar(30), @Index int)
RETURNS nvarchar(300)
AS
BEGIN
	--SELECT dbo.[SplitString]('1,3,5',',',3)
	DECLARE @i int
	DECLARE @pos int
	DECLARE @substring nvarchar(300)
	SET @i=0

	IF @index<=0
		RETURN ''

	WHILE (not @i>=@Index)
		IF (LEN(@String)>0)
		BEGIN		
			SET @pos=PATINDEX ('%'+@Delimiter+'%',@String)
			IF @pos>0
			BEGIN
				SET @substring=SUBSTRING(@String,0,@pos)
				SET @String=SUBSTRING(@String,@pos+1,LEN(@String)-@pos+1)
				SET @i=@i+1
				IF @i=@Index
					RETURN @substring
			END
			ELSE
				IF @Index=@i+1
					RETURN @String
				ELSE
					RETURN ''
		END
		ELSE
			RETURN ''
	RETURN ''
END
GO
-- ����� ��� ������� � ���� ������ ���� �� ����� � ����, ���������� ��� ������� (���� �������� 3 �����)

USE [Arsenal]
GO

IF OBJECT_ID('dbo.[TableAlias]') IS NULL
   EXEC('CREATE FUNCTION dbo.[TableAlias]() RETURNS int AS BEGIN RETURN 0 END')
GO
 
ALTER FUNCTION dbo.[TableAlias] 
(
	@Object nvarchar(256)
)
RETURNS nvarchar(max)
AS
BEGIN
/*	
	DECLARE @Object nvarchar(256)
	SELECT @Object = 'Geo.Points'
	SELECT dbo.[TableAlias](@Object)
*/	
	DECLARE @TableCols TABLE ([Name] nvarchar(128), [Type] nvarchar(256))
	DECLARE 
		@Result nvarchar(8),
		@idx int,
		@Schema nvarchar(64),
		@Plural nvarchar(64),
		@Found bit

	SELECT 
		@Found = 1,
		@Schema = ss.name,
		@Plural = st.name
	FROM sys.tables st
		JOIN sys.schemas ss ON ss.schema_id=st.schema_id
	WHERE st.object_id = OBJECT_ID(@Object)

	IF @Found IS NULL
		SELECT 
			@Schema = dbo.[SplitString](REPLACE(REPLACE(@Object, '[', ''), ']', ''), '.', 1),
			@Plural = dbo.[SplitString](REPLACE(REPLACE(@Object, '[', ''), ']', ''), '.', 2)

	--SELECT 
	--	@Result,
	--	@idx ,
	--	@Schema ,
	--	@Plural,
	--	OBJECT_ID(@Object) 

	SELECT @idx = PATINDEX('%_[ABCDEFGHIJKLMNOPQRSTUVWZYZ]%', @Plural COLLATE Latin1_General_CS_AS)

	SELECT @Result = LOWER(SUBSTRING(@Schema, 1, 1) + SUBSTRING(@Plural, 1, 1) + SUBSTRING(@Plural, CASE @idx WHEN 0 THEN 0 ELSE @idx + 1 END, 1))

	RETURN @Result
END

GO

USE [Arsenal]
GO

IF OBJECT_ID('dbo.[TableColumns]') IS NULL
   EXEC('CREATE FUNCTION [dbo].[TableColumns]() RETURNS @b TABLE(b bit) AS begin insert @b Select 1 return end')
GO
     
ALTER FUNCTION dbo.[TableColumns] (@Object nvarchar(256) = NULL)
RETURNS @Result TABLE ([Name] nvarchar(128), [Type] nvarchar(256))
AS
BEGIN
/*
	SELECT * FROM dbo.[TableColumns]('Geo.Points')
*/	
	DECLARE @TableNames table ([Catalog] nvarchar(128), [Schema] nvarchar(128), [Name] nvarchar(128))
	DECLARE 
		@KVP nvarchar(128),
		@Catalog nvarchar(128),
		@FieldsDeclare nvarchar(max),
		@ParamsDeclare nvarchar(max)


	SELECT @KVP = REPLACE(REPLACE(@Object, '[', ''), ']', '')	
	SELECT @Catalog = 'Arsenal'

	INSERT INTO @TableNames
	SELECT ISNULL(PARSENAME(@KVP, 3), @Catalog), PARSENAME(@KVP, 2), PARSENAME(@KVP, 1) 
	
	INSERT INTO @Result
	SELECT
		cl.COLUMN_NAME,
		cl.DATA_TYPE + ISNULL('('+CONVERT(nvarchar(8), cl.CHARACTER_MAXIMUM_LENGTH)+')', '')
	FROM INFORMATION_SCHEMA.COLUMNS cl
		JOIN @TableNames fl ON cl.TABLE_CATALOG = fl.[Catalog] and cl.TABLE_SCHEMA = fl.[Schema] and cl.TABLE_NAME = fl.Name
	
	DELETE FROM @Result
	WHERE (Name = 'ID') OR
		 (Name = 'dboDate')
		OR (Name = 'IsDeleted')

	--SELECT @FieldsDeclare = COALESCE(@FieldsDeclare + ', ', '') +  '['+Name+']' FROM @TableColumns
	--SELECT @ParamsDeclare = COALESCE(@ParamsDeclare + ', ', '') +  '@'+Name FROM @TableColumns
	RETURN 
END

GO

--���������������� ��� ������� � ������������ SQL ������ �� ����� �������
USE [Arsenal]
GO

IF OBJECT_ID('dbo.[TableColumnsString]') IS NULL
   EXEC('CREATE FUNCTION dbo.[TableColumnsString]() RETURNS int AS BEGIN RETURN 0 END')
GO
 
ALTER FUNCTION [dbo].[TableColumnsString] 
(
	@Object nvarchar(256), 
	@Include nvarchar(256) = NULL,
	@Exclude nvarchar(256) = NULL, 
	@Nulls nvarchar(256) = NULL, 
	@ForInsertFields bit = 0, 
	@ForSelectFields bit = 0, 
	@ForParams bit = 0, 
	@ForParamValues bit = 0, 
	@ForParamCalls bit = 0,
	@ForAlterByParams bit,		
	@IsHorizontalORPad int = 0, -- negative(-1) is horizontal 0+ - pad count, with tab-char
	@UseAlias bit = 0,
	@AddTrailingComma bit = 0,
	@NulledParams bit = 0
)
RETURNS nvarchar(max)
AS
BEGIN
/*	
	DECLARE 
		@Object nvarchar(256),
		@Include nvarchar(256), 
		@Exclude nvarchar(256), 
		@Nulls nvarchar(256), 
		@ForInsertFields bit, 
		@ForSelectFields bit, 
		@ForParams bit, 
		@ForParamValues bit, 
		@ForParamCalls bit,
		@ForAlterByParams bit,		
		@IsHorizontalORPad int,
		@UseAlias bit,
		@AddTrailingComma bit,
		@NulledParams bit
	SELECT 
		@Object = 'Geo.Points',
		@Include = null,--'Caption',
		@Exclude = 'ID_Int',
		@Nulls = 'Code', 
		@ForInsertFields = 0, 
		@ForSelectFields = 0, 
		@ForParams = 0, 
		@ForParamValues = 1, 
		@ForParamCalls = 0,
		@ForAlterByParams = 0,
		@IsHorizontalORPad = 0,
		@UseAlias = 1,
		@AddTrailingComma = 0,
		@NulledParams = 0
	SELECT [dbo].[TableColumnsString]
	(
		@Object,
		@Include,
		@Exclude,
		@Nulls,  
		@ForInsertFields, 
		@ForSelectFields, 
		@ForParams, 
		@ForParamValues, 
		@ForParamCalls,
		@ForAlterByParams,		
		@IsHorizontalORPad,
		@UseAlias,
		@AddTrailingComma,
		@NulledParams	
	)
*/	
	DECLARE @TableColumns TABLE ([Name] nvarchar(128), [Type] nvarchar(256))
	DECLARE 
		@Result nvarchar(max),
		@LF varchar(1),
		@Alias nvarchar(8),
		@Pad nvarchar(16)

	INSERT @TableColumns
	SELECT * FROM [dbo].[TableColumns](@Object)
	WHERE ((@Include IS NULL) OR (Name IN (SELECT Value FROM [dbo].[Split](@Include, ';') WHERE Value <> '')))

	--IF NOT EXISTS (SELECT Value FROM [dbo].[Split](@Include, ';') WHERE Value = 'ID')
	--DELETE @TableColumns	
	--WHERE Name = 'ID'

	DELETE @TableColumns
	WHERE Name IN (SELECT Value FROM [dbo].[Split](@Exclude, ';') WHERE Value <> '')

	IF @IsHorizontalORPad < 0
		SELECT @LF = '', @Pad = ''
	ELSE
		SELECT @LF = CHAR(13), @Pad = REPLICATE(CHAR(9), @IsHorizontalORPad)

	IF @UseAlias = 1
		SELECT @Alias = [dbo].[TableAlias](@Object) + '.'
	ELSE
		SELECT @Alias = ''
	
	IF @ForInsertFields = 1
		SELECT @Result = COALESCE(@Result + ', ' + @LF, '') + @Pad + '[' + Name + ']' FROM @TableColumns

	IF @ForSelectFields = 1
		SELECT @Result = COALESCE(@Result + ', ' + @LF, '') + @Pad + @Alias + '[' + Name + ']' FROM @TableColumns

	IF @ForAlterByParams = 1
		SELECT @Result = COALESCE(@Result + ', ' + @LF, '') + @Pad + @Alias + '['+ Name +'] = @' + Name FROM @TableColumns

	IF @ForParamCalls = 1
		SELECT @Result = COALESCE(@Result + ', ' + @LF, '') + @Pad + '@' + Name FROM @TableColumns

	IF @ForParams = 1
		SELECT @Result = COALESCE(@Result + ', ' + @LF, '') + @Pad + '@' + Name + ' ' + [Type] + CASE WHEN (n.Value IS NOT NULL OR @NulledParams = 1) THEN ' = NULL'  ELSE '' END 
		FROM @TableColumns tc
		LEFT JOIN (SELECT Value FROM [dbo].[Split](@Nulls, ';') WHERE Value <> '') n ON tc.[Name] = n.Value

	IF @ForParamValues = 1
		SELECT @Result = COALESCE(@Result + ', ' + @LF, '') + @Pad + '@' + Name + 
		CASE WHEN @NulledParams = 1 THEN ' = NULL' 
			ELSE
			CASE [Type] 
				WHEN 'uniqueidentifier' THEN ' = ''00000000-0000-0000-0000-000000000000'''
				WHEN 'bit' THEN ' = 0'
				WHEN 'int' THEN ' = 0'
				WHEN 'tinyint' THEN ' = 0'
				WHEN 'smallint' THEN ' = 0'
				WHEN 'float' THEN ' = 0'
				WHEN 'money' THEN ' = 0'
				WHEN 'smallmoney' THEN ' = 0'
				WHEN 'bigint' THEN ' = 0'
				WHEN 'datetime' THEN ' = GETDATE()'
				ELSE ' = ''test''' END --string
			END
		FROM @TableColumns

	SELECT @Result = REPLACE(REPLACE(@Result, 'nvarchar(-1)', 'nvarchar(max)'), 'varbinary(-1)', 'varbinary(max)')

	IF @AddTrailingComma = 1
		SELECT @Result = @Result + ','
	
	RETURN @Result
END

