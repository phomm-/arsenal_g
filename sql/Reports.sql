USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[EquipmentReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[EquipmentReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[EquipmentReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = '55D88E1B-B2B4-E511-8B87-0023540F263B'
        EXEC [Arsenal].[EquipmentReport]
			@ID
    */

    SELECT
		CAST(ae.[Barcode] AS nvarchar(99)) AS Barcode, 
		ISNULL(CONVERT(nvarchar(10), ob.[Date], 104), '-') AS BillDate,
		oob.Caption AS Buyer, 
		oob.Caption AS BuyerTitle,
		CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108) AS DateTitle,
		ISNULL(ae.[Settings], '-') AS Descr,
		ISNULL(CONVERT(nvarchar(10), ei.InvDate, 104), '-') AS InvDate,
		ISNULL(ae.[InvNumber], '-') AS [InvN], 
		ob.Invoice, 
		CAST(ae.[Lifetime] AS nvarchar(99)) + ' ����' AS [Lifetime], 
		ae.[Caption] AS Model,
		CONVERT(nvarchar(10), em.MoveDate, 104) AS MoveDate,
		'-' AS Note, --ob.Note,
		ae.[Price],
		amt.Caption AS Reason, 
		oos.Caption AS Seller,
		aet.[Caption] AS [Type], 
		av.Caption AS Vendor, 
		ae.[VisualNumber] AS VisN,
		op.Caption AS Worker, 
		owp.Caption AS WorkPlace
    FROM [Arsenal].[Equipments] ae
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
		LEFT JOIN (SELECT * FROM (
			SELECT ID_Equipment, ID_WorkPlace, Note, ID_MovingType, ID_Person, [Date] MoveDate, 
				ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1) em 
		ON em.ID_Equipment = ae.ID
		LEFT JOIN (SELECT * FROM (
			SELECT ID_Equipment, [BeginDate] InvDate, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [BeginDate] DESC) rn 
			FROM Arsenal.Inventarisations ai 
			JOIN Arsenal.Results ar ON ar.ID_Inventarisation = ai.ID AND ar.IsDeleted = 0
			WHERE ai.IsDeleted = 0) a WHERE rn = 1) ei 
		ON ei.ID_Equipment = ae.ID
		LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
		LEFT JOIN Office.Organisations oos ON ob.ID_Seller = oos.ID AND oos.IsDeleted = 0
		LEFT JOIN Office.Organisations oob ON ob.ID_Buyer = oob.ID AND oob.IsDeleted = 0
		LEFT JOIN Office.WorkPlaces owp ON owp.ID = em.ID_Workplace AND owp.IsDeleted = 0		
		LEFT JOIN Arsenal.MovingTypes amt ON amt.ID = em.ID_MovingType AND amt.[IsDeleted] = 0
		LEFT JOIN Office.Persons op ON op.ID = em.ID_Person AND op.IsDeleted = 0
    WHERE 
		ae.[IsDeleted] = 0
		AND ae.ID = @ID

END
GO

USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[MovingReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[MovingReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[MovingReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = '55D88E1B-B2B4-E511-8B87-0023540F263B'
        EXEC [Arsenal].[MovingReport]
			@ID
    */
	DECLARE
		@Barcode nvarchar(99), 
		@BuyerTitle nvarchar(99), 
		@DateTitle nvarchar(99), 
		@Descr nvarchar(99), 
		@InvDate nvarchar(99), 
		@InvN nvarchar(99),
		@Lifetime nvarchar(99), 
		@Model nvarchar(99),
		@Type nvarchar(99), 
		@Vendor nvarchar(99), 
		@VisN nvarchar(99)
    SELECT
		@Barcode = CAST(ae.[Barcode] AS nvarchar(99)), 
		@BuyerTitle = oob.Caption,
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@Descr = ISNULL(ae.[Settings], '-'),
		@InvDate = ISNULL(CONVERT(nvarchar(10), ei.InvDate, 104), '-'),
		@InvN = ISNULL(ae.[InvNumber], '-'), 
		@Lifetime = CAST(ae.[Lifetime] AS nvarchar(99)) + ' �.', 
		@Model = ae.[Caption],
		@Type = aet.[Caption], 
		@Vendor = av.Caption, 
		@VisN = ae.[VisualNumber]
    FROM [Arsenal].[Equipments] ae
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
		LEFT JOIN (SELECT * FROM (
			SELECT ID_Equipment, [BeginDate] InvDate, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [BeginDate] DESC) rn 
			FROM Arsenal.Inventarisations ai 
			JOIN Arsenal.Results ar ON ar.ID_Inventarisation = ai.ID AND ar.IsDeleted = 0
			WHERE ai.IsDeleted = 0) a WHERE rn = 1) ei 
		ON ei.ID_Equipment = ae.ID
		LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
		LEFT JOIN Office.Organisations oob ON ob.ID_Buyer = oob.ID AND oob.IsDeleted = 0
    WHERE 
		ae.[IsDeleted] = 0
		AND ae.ID = @ID

	SELECT 0 AS Idx, '' AS Workplace, '' AS MoveDate, 'Barcode' AS Reason, @Barcode AS Worker
	UNION ALL SELECT 0, '', '', 'BuyerTitle', @BuyerTitle
	UNION ALL SELECT 0, '', '', 'DateTitle', @DateTitle
	UNION ALL SELECT 0, '', '', 'Descr', @Descr
	UNION ALL SELECT 0, '', '', 'InvDate', @InvDate
	UNION ALL SELECT 0, '', '', 'InvN', @InvN
	UNION ALL SELECT 0, '', '', 'Lifetime', @Lifetime
	UNION ALL SELECT 0, '', '', 'Model', @Model
	UNION ALL SELECT 0, '', '', 'Type', @Type
	UNION ALL SELECT 0, '', '', 'Vendor', @Vendor
	UNION ALL SELECT 0, '', '', 'VisN', @VisN
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY am.[Date]), 
		owp.Caption, 
		CONVERT(nvarchar(10), am.[Date], 104),
		amt.Caption, 
		op.Caption	
	FROM Arsenal.Movings am		
	LEFT JOIN Office.WorkPlaces owp ON owp.ID = am.ID_Workplace AND owp.IsDeleted = 0		
	LEFT JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.[IsDeleted] = 0
	LEFT JOIN Office.Persons op ON op.ID = am.ID_Person AND op.IsDeleted = 0
	WHERE 
		am.IsDeleted = 0
		AND am.ID_Equipment = @ID
	ORDER BY MoveDate
END
GO

USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[WPReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[WPReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[WPReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = '6AECCFE9-9B49-4428-8904-049BE52720FA'
        EXEC [Arsenal].[WPReport]
			@ID
    */
	DECLARE
		@BuyerTitle nvarchar(99), 
		@DateTitle nvarchar(99),
		@InvDate nvarchar(99), 
		@WPName nvarchar(99), 
		@Department nvarchar(99), 
		@Worker nvarchar(99),
		@Cabinet nvarchar(99), 
		@FullPrice nvarchar(99)
    SELECT
		@BuyerTitle = oo.Caption,
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@InvDate = ISNULL(CONVERT(nvarchar(10), ei.InvDate, 104), '-'),
		@WPName = owp.[Caption],
		@Department = od.[Caption],
		@Worker = ISNULL(ow.[Caption], '-'), 
		@Cabinet = ISNULL(owp.Cabinet, '-'), 
		@FullPrice = ISNULL(CAST(em.FullPrice as nvarchar(99)), '-')
    FROM [Office].[WorkPlaces] owp
		LEFT JOIN Office.Departments od ON od.ID = owp.ID_Department AND od.IsDeleted = 0
		LEFT JOIN Office.Organisations oo ON oo.ID = od.ID_Organisation AND oo.IsDeleted = 0
        LEFT JOIN Office.Workers ow ON ow.ID = owp.ID_Worker AND ow.IsDeleted = 0
		LEFT JOIN (SELECT a.ID_WorkPlace, SUM(ae.Price) FullPrice FROM (
			SELECT ID_WorkPlace, ID_Equipment, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a 
			JOIN Arsenal.Equipments ae ON ae.ID = a.ID_Equipment AND ae.IsDeleted = 0 
			WHERE rn = 1 
			GROUP BY ID_WorkPlace) em 
		ON em.ID_WorkPlace = owp.ID
		LEFT JOIN (SELECT * FROM (
			SELECT ID_WorkPlace, [BeginDate] InvDate, ROW_NUMBER() OVER(PARTITION BY ID_WorkPlace ORDER BY [BeginDate] DESC) rn 
			FROM Arsenal.Inventarisations ai 
			JOIN Arsenal.Results ar ON ar.ID_Inventarisation = ai.ID AND ar.IsDeleted = 0
			WHERE ai.IsDeleted = 0) a WHERE rn = 1) ei 
		ON ei.ID_WorkPlace = owp.ID
    WHERE 
		owp.[IsDeleted] = 0
		AND owp.ID = @ID

	SELECT 0 AS Idx, 'InvDate' AS [Type], @InvDate AS InvN, '' AS Model, '' AS InstDate, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT 0, 'WPName', @WPName, '', '', ''
	UNION ALL SELECT 0, 'Department', @Department, '', '', ''
	UNION ALL SELECT 0, 'Worker', @Worker, '', '', ''
	UNION ALL SELECT 0, 'Cabinet', @Cabinet, '', '', ''
	UNION ALL SELECT 0, 'FullPrice', @FullPrice, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY am.[Date]), 
		aet.Caption, 
		ISNULL(ae.InvNumber, ''),
		ISNULL(av.Caption + ' ', '') + ae.Caption, 
		CONVERT(nvarchar(10), am.[Date], 104),		
		CAST(ae.Price AS nvarchar(99))
	FROM (SELECT * FROM (SELECT ID_WorkPlace, ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) am
	JOIN Arsenal.Equipments ae ON ae.ID = am.ID_Equipment AND ae.IsDeleted = 0 
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	WHERE 		
		am.ID_Workplace = @ID
	ORDER BY InstDate
END
GO

IF OBJECT_ID('[Arsenal].[InvReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InvReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InvReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = 'C8212219-C8B3-E511-8B87-0023540F263B'
        EXEC [Arsenal].[InvReport]
			@ID
    */
	DECLARE
		@BuyerTitle nvarchar(99),
		@DateTitle nvarchar(99), 
		@EndDate nvarchar(99), 
		@InvName nvarchar(99), 
		@Worker nvarchar(99)
    SELECT
		@BuyerTitle = ISNULL(oo.Caption, ''),
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@EndDate = ISNULL(CONVERT(nvarchar(10), ai.EndDate, 104), '-'),
		@InvName  = ISNULL(ai.[Caption], ''),
		@Worker = ISNULL(ow.[Caption], '-')
    FROM [Arsenal].[Inventarisations] ai
		LEFT JOIN Office.Workers ow ON ow.ID = ai.ID_Worker AND ow.IsDeleted = 0
		LEFT JOIN Office.Organisations oo ON oo.ID = ow.ID_Organisation AND oo.IsDeleted = 0
    WHERE 
		ai.[IsDeleted] = 0
		AND ai.ID = @ID

	SELECT 0 AS Idx, 'InvName' AS Model, @InvName AS InvN, '' AS BuyDate, '' AS Workplace, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT 0, 'EndName', @EndDate, '', '', ''
	UNION ALL SELECT 0, 'Worker', @Worker, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption), 
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption AS Caption, 
		ISNULL(ae.InvNumber, ''),
		CONVERT(nvarchar(10), am.[Date], 104),				
		ISNULL(owp.Caption, ''), 		
		CAST(ae.Price AS nvarchar(99))
	FROM Arsenal.Results ar
	JOIN (SELECT * FROM (SELECT ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] ASC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) am 
	ON am.ID_Equipment = ar.ID_Equipment
	JOIN Arsenal.Equipments ae ON ae.ID = ar.ID_Equipment AND ae.IsDeleted = 0 
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	LEFT JOIN Office.WorkPlaces owp ON owp.ID = ar.ID_Workplace AND owp.IsDeleted = 0		
	WHERE 		
		ar.ID_Inventarisation = @ID
	ORDER BY Model
END
GO

IF OBJECT_ID('[Arsenal].[ForDisposeReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[ForDisposeReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[ForDisposeReport]
	@ID_Organisation uniqueidentifier,
	@DateBegin datetime	= NULL,
	@DateEnd datetime = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = 'C8212219-C8B3-E511-8B87-0023540F263B'
        EXEC [Arsenal].[ForDisposeReport]
			@ID
    */

	IF @DateBegin IS NULL
		SELECT @DateBegin = CAST(0 AS datetime)
	IF @DateEnd IS NULL
		SELECT @DateEnd = DATEADD(DAY, DATEDIFF(DAY, '19000101', GETDATE()), '23:59:59')

	DECLARE
		@BuyerTitle nvarchar(99),
		@DateTitle nvarchar(99), 
		@DateRange nvarchar(99)
    SELECT
		@BuyerTitle = ISNULL(oo.Caption, ''),
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@DateRange = CONVERT(nvarchar(10), @DateBegin, 104) + '-' + CONVERT(nvarchar(10), @DateEnd, 104)
    FROM Office.Organisations oo 
    WHERE 
		oo.[IsDeleted] = 0
		AND oo.ID = @ID_Organisation

	DECLARE @SysMTs TABLE(Name nvarchar(99))
	INSERT @SysMTs
	--SELECT 'Buy' UNION 
	SELECT 'Install'
	UNION SELECT 'Repair'
	UNION SELECT 'Relocate'
	UNION SELECT 'Dispose'
	UNION SELECT 'PreDispose'

	SELECT 0 AS Idx, 'DateRange' AS Model, @DateRange AS InvN, '' AS BuyDate, '' AS Workplace, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption), 
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption AS Caption, 
		ISNULL(ae.InvNumber, ''),
		CONVERT(nvarchar(10), amb.[Date], 104),				
		ISNULL(owp.Caption, ''), 		
		CAST(ae.Price AS nvarchar(99))
	FROM Arsenal.Equipments ae 
	JOIN (SELECT * FROM (SELECT ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] ASC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) amb 
	ON amb.ID_Equipment = ae.ID
	JOIN (SELECT * FROM (SELECT ID_Equipment, amt.Name,	[Date],
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings am 
		JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.IsDeleted = 0 
		WHERE am.IsDeleted = 0) a WHERE rn = 1 AND Name = 'PreDispose') amd 
	ON amd.ID_Equipment = ae.ID
	JOIN (SELECT * FROM (SELECT ID_Equipment, ID_Workplace,
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings am 
		JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.IsDeleted = 0
		LEFT JOIN @SysMTs smt ON smt.Name = amt.Name
		WHERE am.IsDeleted = 0 AND smt.Name IS NULL) a WHERE rn = 1 ) am 
	ON am.ID_Equipment = ae.ID	
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	LEFT JOIN Office.WorkPlaces owp ON owp.ID = am.ID_Workplace AND owp.IsDeleted = 0
	LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
	WHERE 		
		ae.IsDeleted = 0 
		AND amd.[Date] BETWEEN @DateBegin AND @DateEnd
		AND ob.ID_Buyer = @ID_Organisation
	ORDER BY Model
END
GO

IF OBJECT_ID('[Arsenal].[DisposedReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DisposedReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[DisposedReport]
	@ID_Organisation uniqueidentifier,
	@DateBegin datetime	= NULL,
	@DateEnd datetime = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = 'C8212219-C8B3-E511-8B87-0023540F263B'
        EXEC [Arsenal].[DisposedReport]
			@ID
    */

	IF @DateBegin IS NULL
		SELECT @DateBegin = CAST(0 AS datetime)
	IF @DateEnd IS NULL
		SELECT @DateEnd = DATEADD(DAY, DATEDIFF(DAY, '19000101', GETDATE()), '23:59:59')

	DECLARE
		@BuyerTitle nvarchar(99),
		@DateTitle nvarchar(99), 
		@DateRange nvarchar(99)
    SELECT
		@BuyerTitle = ISNULL(oo.Caption, ''),
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@DateRange = CONVERT(nvarchar(10), @DateBegin, 104) + '-' + CONVERT(nvarchar(10), @DateEnd, 104)
    FROM Office.Organisations oo 
    WHERE 
		oo.[IsDeleted] = 0
		AND oo.ID = @ID_Organisation

	DECLARE @SysMTs TABLE(Name nvarchar(99))
	INSERT @SysMTs
	--SELECT 'Buy' UNION 
	SELECT 'Install'
	UNION SELECT 'Repair'
	UNION SELECT 'Relocate'
	UNION SELECT 'Dispose'
	UNION SELECT 'PreDispose'

	SELECT 0 AS Idx, 'DateRange' AS Model, @DateRange AS InvN, '' AS BuyDate, '' AS DisposeDate, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption), 
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption AS Caption, 
		ISNULL(ae.InvNumber, ''),
		CONVERT(nvarchar(10), amb.[Date], 104),				
		CONVERT(nvarchar(10), amd.[Date], 104), 		
		CAST(ae.Price AS nvarchar(99))
	FROM Arsenal.Equipments ae 
	JOIN (SELECT * FROM (SELECT ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] ASC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) amb 
	ON amb.ID_Equipment = ae.ID
	JOIN (SELECT * FROM (SELECT ID_Equipment, amt.Name,	[Date],
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings am 
		JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.IsDeleted = 0 
		WHERE am.IsDeleted = 0) a WHERE rn = 1 AND Name = 'Dispose') amd 
	ON amd.ID_Equipment = ae.ID
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
	WHERE 		
		ae.IsDeleted = 0 
		AND amd.[Date] BETWEEN @DateBegin AND @DateEnd
		AND ob.ID_Buyer = @ID_Organisation
	ORDER BY Model
END
GO
