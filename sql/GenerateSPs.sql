USE [Arsenal]
GO

        DECLARE
            @TableName nvarchar(256),
            @ToDataSet bit,
            @SkipExisting bit

        SELECT 
            @TableName = null,--'office.ExamGroups',--
            @ToDataSet = 0,
			@SkipExisting = 0

    DECLARE @TableAsParam TABLE ([Catalog] nvarchar(128), [Schema] nvarchar(128), [Name] nvarchar(128))
	DECLARE 
		@KVP nvarchar(128),
		@Catalog nvarchar(128)

    IF @TableName IS NOT NULL
	BEGIN
        SELECT @KVP = REPLACE(REPLACE(@TableName, '[', ''), ']', '')	
        SELECT @Catalog = 'Arsenal'
        INSERT INTO @TableAsParam
        SELECT ISNULL(PARSENAME(@KVP, 3), @Catalog), PARSENAME(@KVP, 2), PARSENAME(@KVP, 1)
    END


    DECLARE @Result TABLE (SelectSP nvarchar(max), InsertSP nvarchar(max), UpdateSP nvarchar(max), DeleteSP nvarchar(max))

    DECLARE @Exclusions TABLE ([Schema] nvarchar(64), TableName nvarchar(64))
    DECLARE @Specific TABLE ([Schema] nvarchar(64), Plural nvarchar(64), Single nvarchar(64))
    DECLARE @TableNames TABLE ([Schema] nvarchar(64), Plural nvarchar(64), Single nvarchar(64))
    DECLARE @Tables TABLE (ID int IDENTITY, [Schema] nvarchar(64), Plural nvarchar(64), Single nvarchar(64))

    INSERT @Specific
    SELECT [Schema], Plural, Single FROM [dbo].[TableSpecifications]
	WHERE ToBeExcluded = 0
	
	--SELECT 'Geo', 'Countries', 'Country'
 --   UNION SELECT 'Security', 'Identities', 'Identity'   
 --   UNION SELECT 'Security', 'Activities', 'Activity'
 --   UNION SELECT 'Auto', 'Capacity', 'Capacities'
 --   UNION SELECT 'Sales', 'Categories', 'Category'
 --   UNION SELECT 'Sales', 'GatheringCategoryRouteType', 'GatheringCategoryRouteType'
 --   UNION SELECT 'dbo', 'Configuration', 'Configuration'

    INSERT @Exclusions
	SELECT [Schema], Plural FROM [dbo].[TableSpecifications]
	WHERE ToBeExcluded = 1
    --SELECT 'Sales', 'Tickets'
    --UNION SELECT 'Trans', 'Places'
    --UNION SELECT 'Security', 'Principals'

    INSERT @TableNames
    SELECT ss.name, st.name, LEFT(st.name, LEN(st.name) - 1)
    FROM sys.tables st
    JOIN sys.schemas ss ON ss.schema_id=st.schema_id
    WHERE ss.name + st.name NOT IN (SELECT [Schema] + Plural FROM @Specific)
        and ss.name + st.name NOT IN (SELECT [Schema] + TableName FROM @Exclusions)



    INSERT @TableNames
    SELECT * FROM @Specific	 

    INSERT @Tables
    SELECT * FROM @TableNames
    WHERE
        NOT EXISTS(SELECT TOP 1 1 FROM @TableAsParam) OR
        [Schema] + Plural IN (SELECT [Schema] + [Name] FROM @TableAsParam)
        
    ORDER BY [Schema], Plural

    DECLARE @Fields TABLE(ID int IDENTITY, Name nvarchar(64), [Type] nvarchar(64))
    DECLARE @SysFields TABLE(ID int IDENTITY, Name nvarchar(64), [Type] nvarchar(64))

    INSERT @SysFields
    SELECT 'IsDeleted', 'bit'
    UNION SELECT 'ReplicaDate', 'int'
    UNION SELECT 'ReplicaTime', 'int'
        
    DECLARE 
        @MaxStep int,
        @Step int,
        @Schema nvarchar(64),
        @Plural nvarchar(64),
        @Single nvarchar(64),
        @Select nvarchar(max),
        @Insert nvarchar(max),
        @Update nvarchar(max),
        @Delete nvarchar(max),
        @SPName nvarchar(64),
        @SPExists bit,
		@Alias nvarchar(8),
		@Object nvarchar(128),
		@HasCaption nvarchar(256),
		@HasName nvarchar(256),
        @HasCode nvarchar(256),
		@IncludedColumns nvarchar(256),
		@ExcludedColumns nvarchar(256),
		@NullableColumns nvarchar(256)


    SELECT @MaxStep = COUNT(ID) FROM @Tables
    SELECT @step = 1

    WHILE @Step <= @MaxStep
    BEGIN

        SELECT 
            @Schema = t.[Schema],
            @Plural = t.Plural,
            @Single = t.Single,
			@Object = '[' + t.[Schema] + '].[' + t.Plural + ']',
			@IncludedColumns  = sts.IncludedColumns,
			@ExcludedColumns  = sts.ExcludedColumns,
			@NullableColumns  = sts.NullableColumns
        FROM @Tables t
		LEFT JOIN [dbo].[TableSpecifications] sts on t.[Schema] = sts.[Schema] and t.Plural = sts.Plural
        WHERE
            ID = @Step
        
		SELECT @HasCaption = ''
        SELECT @HasCaption = NULL
		FROM sys.all_columns sc
		JOIN sys.tables st ON st.object_id = sc.object_id
		JOIN sys.schemas ss ON ss.schema_id = st.schema_id
		WHERE ss.name = @Schema and st.name = @Plural and sc.name = 'Caption' and sc.is_computed = 0

		SELECT @HasCode = ''
        SELECT @HasCode = NULL
		FROM sys.all_columns sc
		JOIN sys.tables st ON st.object_id = sc.object_id
		JOIN sys.schemas ss ON ss.schema_id = st.schema_id
		WHERE ss.name = @Schema and st.name = @Plural and sc.name = 'Code' and sc.is_computed = 0

		SELECT @HasName = ''
        SELECT @HasName = NULL
		FROM sys.all_columns sc
		JOIN sys.tables st ON st.object_id = sc.object_id
		JOIN sys.schemas ss ON ss.schema_id = st.schema_id
		WHERE ss.name = @Schema and st.name = @Plural and sc.name = 'Name' and sc.is_computed = 0

                                                                              
        IF NOT @ToDataSet = 1
        BEGIN
            PRINT '---------------------------------------------------------------------------------------------'
            PRINT '------------------------------------------' + @Schema + '.' + @Plural + REPLICATE('-', 50 - LEN(@Schema + @Plural))
            PRINT '---------------------------------------------------------------------------------------------'
        END

    
        SELECT @SPName = 'Select' + @Single

        IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
	        PRINT '--------------------------' + @Schema + '.' + @SPName + '-----------------------------'
        
        IF EXISTS(
            SELECT 1 FROM sys.objects so JOIN sys.schemas ss ON ss.schema_id = so.schema_id 
            WHERE (so.[Name] = @SPName) AND (ss.[Name] = @Schema) AND (so.type = 'P')
            )
            SELECT @SPExists = 1
	    ELSE
            SELECT @SPExists = 0
	
        
		SELECT @Alias = [dbo].[TableAlias](@Object)

        SELECT @Select = 
            '-- ������� �� ������� ' + @Schema + '.' + @Plural + '
USE [Arsenal]
GO

IF OBJECT_ID(''[' + @Schema + '].[' + @SPName + ']'') IS NULL
   EXEC(''CREATE PROCEDURE [' + @Schema + '].[' + @SPName + '] AS BEGIN SET NOCOUNT ON; END'')
GO

ALTER PROC [' + @Schema + '].[' + @SPName + ']
' + [dbo].[TableColumnsString](@Object, @IncludedColumns, @ExcludedColumns, @NullableColumns, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1) + '
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
'		+ [dbo].[TableColumnsString](@Object, @IncludedColumns, @ExcludedColumns, @NullableColumns, 0, 0, 1, 0, 0, 0, 3, 0, 0, 0) + '
        SELECT
'		+ [dbo].[TableColumnsString](@Object, @IncludedColumns, @ExcludedColumns, @NullableColumns, 0, 0, 0, 1, 0, 0, 3, 0, 0, 1) + '
        EXEC [' + @Schema + '].[' + @SPName + ']
'		+ [dbo].[TableColumnsString](@Object, @IncludedColumns, @ExcludedColumns, @NullableColumns, 0, 0, 0, 0, 1, 0, 3, 0, 0, 0) + '
    */

    SELECT
		' + @Alias + '.ID,
'		+ [dbo].[TableColumnsString](@Object, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, 2, 1, 0, 0) +
' 
    FROM [' + @Schema + '].[' + @Plural + '] ' + @Alias + '
        --LEFT JOIN  ON .ID = ' + @Alias + '.ID_ AND .[IsDeleted] = 0
    WHERE 
		' + @Alias + '.[IsDeleted] = 0
' + ISNULL(@HasCaption, '		AND (@Caption IS NULL OR ' + @Alias + '.[Caption] LIKE @Caption + ''%'')
') + ISNULL(@HasName, '		AND (@Name IS NULL OR ' + @Alias + '.[Name] LIKE @Name + ''%'')
') + ISNULL(@HasCode, '		AND (@Code IS NULL OR ' + @Alias + '.[Code] = @Code)
') + ISNULL(@HasCaption, '	ORDER BY ' + @Alias + '.[Caption]') + '
END
GO
'   
        
    IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
		PRINT @Select
        
    


-------------------------------------------------------------------INSERT
        
        SELECT @SPName = 'Insert' + @Single
 
		IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
	        PRINT '--------------------------' + @Schema + '.' + @SPName + '-----------------------------'
        
        IF EXISTS(
            SELECT 1 FROM sys.objects so JOIN sys.schemas ss ON ss.schema_id = so.schema_id 
            WHERE (so.[Name] = @SPName) AND (ss.[Name] = @Schema) AND (so.type = 'P')
            )
            SELECT @SPExists = 1
	    ELSE
            SELECT @SPExists = 0
            
        SELECT @Insert = 
        '-- ������� ������ � ������� ' + @Schema + '.' + @Plural + '
USE [Arsenal]
GO

IF OBJECT_ID(''[' + @Schema + '].[' + @SPName + ']'') IS NULL
   EXEC(''CREATE PROCEDURE [' + @Schema + '].[' + @SPName + '] AS BEGIN SET NOCOUNT ON; END'')
GO

ALTER PROC [' + @Schema + '].[' + @SPName + ']
' + [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, @NullableColumns, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0) + '
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
'		+ [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 1, 0, 0, 0, 3, 0, 0, 0) + '
        SELECT
'		+ [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 0, 1, 0, 0, 3, 0, 0, 0) + '
        EXEC [' + @Schema + '].[' + @SPName + '] 
'		+ [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 0, 0, 1, 0, 3, 0, 0, 0) + '
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT [' + @Schema + '].[' + @Plural + '] 
		(' + [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 1, 0, 0, 0, 0, 0, -1, 0, 0, 0) + ')
	OUTPUT INSERTED.ID INTO @outid
	SELECT ' + [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 0, 0, 1, 0, -1, 0, 0, 0) + '
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
'
        
		IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
	        PRINT @Insert    

------------------------------------------------------------UPDATE
            
        SELECT @SPName = 'Update' + @Single

		IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
	        PRINT '--------------------------' + @Schema + '.' + @SPName + '-----------------------------'
        
        IF EXISTS(
            SELECT 1 FROM sys.objects so JOIN sys.schemas ss ON ss.schema_id = so.schema_id 
            WHERE (so.[Name] = @SPName) AND (ss.[Name] = @Schema) AND (so.type = 'P')
            )
            SELECT @SPExists = 1
	    ELSE
            SELECT @SPExists = 0
            
        SELECT @Update = 
        '-- ���������� ������ � ������� ' + @Schema + '.' + @Plural + '
USE [Arsenal]
GO

IF OBJECT_ID(''[' + @Schema + '].[' + @SPName + ']'') IS NULL
   EXEC(''CREATE PROCEDURE [' + @Schema + '].[' + @SPName + '] AS BEGIN SET NOCOUNT ON; END'')
GO

ALTER PROC [' + @Schema + '].[' + @SPName + ']
    @ID uniqueidentifier,
' + [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, @NullableColumns, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0) + '
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
'		+ [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 1, 0, 0, 0, 3, 0, 0, 0) + '
        SELECT
'		+ [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 0, 1, 0, 0, 3, 0, 0, 0) + '
        EXEC [' + @Schema + '].[' + @SPName + '] 
'		+ [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 0, 0, 1, 0, 3, 0, 0, 0) + '
    */
    
    UPDATE [' + @Schema + '].[' + @Plural + ']
    SET
' + [dbo].[TableColumnsString](@Object, NULL, @ExcludedColumns, NULL, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0) + '
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
'

		IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
	        PRINT @Update

------------------------------------------------------------ DELETE

        SELECT @SPName = 'Delete' + @Single

		IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
	        PRINT '--------------------------' + @Schema + '.' + @SPName + '-----------------------------'
        
        IF EXISTS(
            SELECT 1 FROM sys.objects so JOIN sys.schemas ss ON ss.schema_id = so.schema_id 
            WHERE (so.[Name] = @SPName) AND (ss.[Name] = @Schema) AND (so.type = 'P')
            )
            SELECT @SPExists = 1
	    ELSE
            SELECT @SPExists = 0
            
        SELECT @Delete = 
        '-- �������� ������ �� ������� ' + @Schema + '.' + @Plural + '
USE [Arsenal]
GO

IF OBJECT_ID(''[' + @Schema + '].[' + @SPName + ']'') IS NULL
   EXEC(''CREATE PROCEDURE [' + @Schema + '].[' + @SPName + '] AS BEGIN SET NOCOUNT ON; END'')
GO


ALTER PROC [' + @Schema + '].[' + @SPName + ']
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = ''' + '00000000-0000-0000-0000-000000000000' + '''
        EXEC [' + @Schema + '].[' + @SPName + '] @ID
    */
    
    UPDATE [' + @Schema + '].[' + @Plural + ']
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO'
        
		IF @ToDataSet = 0 AND (@SkipExisting = 0 OR @SPExists = 0)
	        PRINT @Delete
            
        IF @ToDataSet = 1
            INSERT @Result
            VALUES (@Select, @Insert, @Update, @Delete)
                

	    SELECT @Step = @Step + 1
    END -- cycle end

    IF @ToDataSet = 1
        SELECT * FROM @Result 






--SELECT 'update  '+ss.name+'.'+st.name+'  set dbodate = [dbo].[Date](NULL)'  --+ Convert(nvarchar(128), sdc.name)-- dboDate datetime DEFAULT [dbo].[Date](NULL)
--FROM sys.tables st
--	JOIN sys.schemas ss ON ss.schema_id=st.schema_id
--	--JOIN sys.columns sc ON st.object_id = sc.object_id 
--	--JOIN sys.default_constraints sdc ON sdc.parent_object_id=st.object_id and definition = '([dbo].[Date](NULL))'
--WHERE st.object_id in
--	(SELECT sc.object_id
--	FROM sys.columns sc
--	WHERE sc.Name='dboDate')

--exec dbo.commontableproc 'geo.points'