USE Arsenal
GO

IF OBJECT_ID('[Office].[Portfolio]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[Portfolio] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[Portfolio]
	@Login nvarchar(256)
WITH EXECUTE AS OWNER
AS
BEGIN
SET NOCOUNT ON

    /*        
        DECLARE @Login nvarchar(256)
        SELECT @Login  = 'abc' 
        EXEC [Office].[Portfolio] @Login
    */
		
	IF IS_SRVROLEMEMBER('sysadmin', @Login) = 1
		SELECT 
			op.ID,
			op.[Login] AS [Login], 
			op.[Login] AS [Job], 
			op.FirstName, 
			op.LastName, 
			op.MiddleName,
			'' AS Department,
			'' AS Organisation
		FROM Office.Persons op
		WHERE (op.[Login] = 'Admin') 
	ELSE
		SELECT TOP 1
			op.ID, 
			op.[Login] AS [Login], 
			oj.Caption AS [Job], 
			op.FirstName, 
			op.LastName, 
			op.MiddleName,
			'' AS Department,
			oo.Caption AS Organisation
		FROM Office.Persons op 
		LEFT JOIN Office.PersonJobs opj ON op.ID = opj.ID_Person AND opj.[IsDeleted] = 0
		LEFT JOIN Office.Jobs oj ON oj.ID = opj.ID_Job AND oj.[IsDeleted] = 0
		LEFT JOIN Office.Organisations oo ON oo.ID = op.ID_Organisation AND oo.[IsDeleted] = 0
		WHERE op.[IsDeleted] = 0    
			AND (op.[Login] = @Login)
			AND ((opj.EndDate IS NULL) OR (opj.EndDate > GETDATE()))
			AND ((opj.BeginDate IS NULL) OR (opj.BeginDate < GETDATE()))
		ORDER BY opj.BeginDate DESC		
END
GO

GRANT EXECUTE ON [Office].[Portfolio] TO public

GO



IF OBJECT_ID('dbo.[SelectAvailableOperation]') IS NULL
   EXEC('CREATE PROCEDURE [dbo].[SelectAvailableOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[SelectAvailableOperation]
	@Login nvarchar(256)
WITH EXECUTE AS OWNER
AS
BEGIN
SET NOCOUNT ON

    /*        
        DECLARE @Login nvarchar(256)
        SELECT @Login  = 'sa' 
        EXEC [dbo].[SelectAvailableOperation] @Login
    */
		
	IF IS_SRVROLEMEMBER('sysadmin', @Login) = 1
		SELECT 
			op.ProcedureName,
			CAST(1 AS bit) AS Managable,
			CAST(0 AS bit) AS Personal
		FROM [dbo].[Operations] AS op	
		ORDER BY op.ProcedureName
	ELSE
		SELECT 
			op.ProcedureName,
			ojo.Managable,
			ojo.Personal
		FROM [dbo].[Operations] AS op
		JOIN Office.JobOperations AS ojo ON op.ID = ojo.ID_Operation AND ojo.[IsDeleted] = 0
		JOIN Office.PersonJobs omj ON omj.ID_Job = ojo.ID_Job AND omj.[IsDeleted] = 0
		JOIN Office.Persons om ON om.ID = omj.ID_Person AND om.[IsDeleted] = 0
		WHERE op.[IsDeleted] = 0    
			AND (om.[Login] = @Login)
			AND ((omj.EndDate IS NULL) OR (omj.EndDate > GETDATE()))
			AND (omj.BeginDate < GETDATE())
		ORDER BY op.ProcedureName
END

GRANT EXECUTE ON [dbo].[SelectAvailableOperation] TO public

GO


GO	

IF OBJECT_ID('[Office].[InsertPerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertPerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[InsertPerson]
	@Login nvarchar(32),
	@ID_Organisation uniqueidentifier,
	@LastName nvarchar(32), 
	@FirstName nvarchar(32), 
	@MiddleName nvarchar(32) = NULL, 
	@BirthDate date, 
	@Phone nvarchar(128) = NULL, 
	@EMail nvarchar(128) = NULL,
	@Password nvarchar(128)
AS
BEGIN
SET NOCOUNT ON
	/*
        DECLARE 
			@Login nvarchar(32), 
			@ID_Organisation uniqueidentifier,
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128),
			@Password nvarchar(128)
        SELECT
            @Login = 'testo',
			@ID_Organisation = '',
			@LastName = 'test',
			@FirstName = 'test',
			@MiddleName = NULL,
			@BirthDate = '1900.01.01',
			@Phone = 'test', 
			@EMail = NULL,
			@Password = 'test'
        EXEC [Office].[InsertPerson] 
			@Login,
			@ID_Organisation, 
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail,
			@Password
    */
		
	DECLARE @LoginDoubled int	
		
	SELECT @LoginDoubled = COUNT(ID)
	FROM Office.Persons
	WHERE [Login] = @Login
	
	SELECT @LoginDoubled = @LoginDoubled + COUNT(sid)
	FROM master..syslogins
	WHERE name = @Login
	
	IF @LoginDoubled > 0 
	BEGIN
		SELECT NULL AS ID
		RETURN 1
	END
	
	DECLARE 
		@ID_Inserted uniqueidentifier
	DECLARE 
		@INSERTED TABLE(ID uniqueidentifier)
		
    INSERT Office.Persons ([Login], ID_Organisation, LastName, FirstName, MiddleName, BirthDate, [Phone], [EMail])
    OUTPUT INSERTED.ID INTO @INSERTED
	VALUES (@Login,	@ID_Organisation, @LastName, @FirstName,	ISNULL(@MiddleName, ''),	@BirthDate, ISNULL(@Phone, ''), ISNULL(@EMail, ''))
	
	IF @@ERROR = 0
	BEGIN
		EXEC('CREATE LOGIN ' + @Login + ' WITH PASSWORD = ''' + @Password + ''', CHECK_POLICY=OFF')
		EXEC('CREATE USER ' + @Login + ' FOR LOGIN ' + @Login)
	END

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK
		SELECT NULL AS ID
		RETURN @@ERROR
	END
			 
	SELECT TOP 1 @ID_Inserted = ID 
	FROM @INSERTED
	
	SELECT @ID_Inserted as ID

    RETURN @@ERROR
END
GO

IF OBJECT_ID('[Office].[InsertJobOperation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertJobOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[InsertJobOperation]
	@ID_Job uniqueidentifier,
	@ID_Operation uniqueidentifier,
	@BeginDate date,
	@EndDate date = NULL,
	@Managable bit = NULL,
	@Personal bit = NULL
AS
BEGIN
SET NOCOUNT ON
	/*
        DECLARE 
            @ID_Job uniqueidentifier,
			@ID_Operation uniqueidentifier,
			@BeginDate date,
			@EndDate date,
			@Managable bit,
			@Personal bit
        SELECT
            @ID_Job = 'D009B6E1-9607-E411-B9EC-0013D4068ECA', -- SELECT * FROM Office.Jobs WHERE Name = 'testjob'
			@ID_Operation = 'B4503185-8C07-E411-B9EC-0013D4068ECA', -- SELECT * FROM Operations WHERE ProcedureName like '%SelectDepartment'
			@BeginDate = '1900.01.01',
			@EndDate = NULL,
			@Managable = NULL,
			@Personal = NULL
        EXEC [Office].[InsertJobOperation]
			@ID_Job,
			@ID_Operation,
			@BeginDate,
			@EndDate,
			@Managable,
			@Personal
    */
	
	--  check of existent pair of job and operation	
	DECLARE @JobOpExistent uniqueidentifier	
		
	SELECT @JobOpExistent = ID
	FROM Office.JobOperations
	WHERE 
		ID_Job = @ID_Job
		AND ID_Operation = @ID_Operation

	SELECT
		@Managable = ISNULL(@Managable, 1),
		@Personal = ISNULL(@Personal, 0)
	
	IF @JobOpExistent IS NOT NULL 
	BEGIN
		UPDATE Office.JobOperations
		SET [BeginDate] = @BeginDate, [EndDate] = @EndDate, [IsDeleted] = 0
		WHERE 
			ID = @JobOpExistent
		SELECT @JobOpExistent AS ID

		RETURN 0
	END	
	
	DECLARE 
		@ID_Inserted uniqueidentifier
	DECLARE 
		@INSERTED TABLE(ID uniqueidentifier)
		
    INSERT Office.JobOperations (ID_Job, ID_Operation, BeginDate, EndDate, Managable, Personal)
    OUTPUT INSERTED.ID INTO @INSERTED
	VALUES (@ID_Job, @ID_Operation,	@BeginDate,	@EndDate, @Managable, @Personal)
	
	SELECT TOP 1 @ID_Inserted = ID 
	FROM @INSERTED
	
	SELECT @ID_Inserted as ID

    RETURN @@ERROR
END
GO

IF OBJECT_ID('[Office].[InsertPersonJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertPersonJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[InsertPersonJob]
	@ID_Person uniqueidentifier,
	@ID_Job uniqueidentifier,
	@BeginDate date,
	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON
	/*
        DECLARE 
            @ID_Person uniqueidentifier,
			@ID_Job uniqueidentifier,
			@BeginDate date,
			@EndDate date
        SELECT
            @ID_Person = '94E190AB-9707-E411-B9EC-0013D4068ECA', -- SELECT * FROM Office.Persons WHERE Login = 'supa'
			@ID_Job = 'D009B6E1-9607-E411-B9EC-0013D4068ECA', -- SELECT * FROM Office.Jobs WHERE Name = 'testjob'
			@BeginDate = '1900.01.01',
			@EndDate = NULL
        EXEC [Office].[InsertPersonJob]
			@ID_Person,
			@ID_Job,
			@BeginDate,
			@EndDate
    */
	
	-- check of existent pair of job and Person	
	DECLARE @MJobExistent uniqueidentifier	
		
	SELECT @MJobExistent = ID
	FROM Office.PersonJobs
	WHERE 
		ID_Person = @ID_Person
		AND ID_Job = @ID_Job
	
	IF @MJobExistent IS NOT NULL 
	BEGIN
		UPDATE Office.PersonJobs
		SET [BeginDate] = @BeginDate, [EndDate] = @EndDate, [IsDeleted] = 0
		WHERE 
			ID = @MJobExistent
		SELECT @MJobExistent AS ID

		RETURN 0
	END	
	
	DECLARE 
		@ID_Inserted uniqueidentifier
	DECLARE 
		@INSERTED TABLE(ID uniqueidentifier)
		
    INSERT Office.PersonJobs (ID_Person, ID_Job, BeginDate, EndDate)
	OUTPUT INSERTED.ID INTO @INSERTED
	VALUES (@ID_Person, @ID_Job, @BeginDate, @EndDate)
	
	SELECT TOP 1 @ID_Inserted = ID 
	FROM @INSERTED
	
	SELECT @ID_Inserted as ID

    RETURN @@ERROR
	
END

GO


---------------------------------------------------------------------------------------------
------------------------------------------dbo.Operations-------------------------------------
---------------------------------------------------------------------------------------------
--------------------------dbo.SelectOperation-----------------------------
-- ������� �� ������� dbo.Operations
USE [Arsenal]
GO

IF OBJECT_ID('[dbo].[SelectOperation]') IS NULL
   EXEC('CREATE PROCEDURE [dbo].[SelectOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [dbo].[SelectOperation]
	@ID uniqueidentifier = NULL, 
	@ProcedureName nvarchar(64) = NULL, 
	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier, 
			@ProcedureName nvarchar(64), 
			@Caption nvarchar(256)
        SELECT
			@ID = NULL, 
			@ProcedureName = NULL, 
			@Caption = NULL
        EXEC [dbo].[SelectOperation]
			@ID, 
			@ProcedureName, 
			@Caption
    */

    SELECT
		do.[ID], 
		do.[ProcedureName], 
		do.[Caption] 
    FROM [dbo].[Operations] do
        --LEFT JOIN  ON .ID = do.ID_ AND .[IsDeleted] = 0
    WHERE 
		do.[IsDeleted] = 0
		AND ((@ProcedureName IS NULL) OR (do.ProcedureName LIKE @ProcedureName + '%'))
		AND ((@Caption IS NULL) OR (do.[Caption] LIKE @Caption + '%'))
	ORDER BY do.[Caption]
END
GO
--------------------------dbo.UpdateOperation-----------------------------
-- ���������� ������ � ������� dbo.Operations
USE [Arsenal]
GO

IF OBJECT_ID('[dbo].[UpdateOperation]') IS NULL
   EXEC('CREATE PROCEDURE [dbo].[UpdateOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [dbo].[UpdateOperation]
    @ID uniqueidentifier,
	@ProcedureName nvarchar(64), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID uniqueidentifier, 
			@ProcedureName nvarchar(64), 
			@Caption nvarchar(256)
        SELECT
			@ID = '00000000-0000-0000-0000-000000000000', 
			@ProcedureName = 'test', 
			@Caption = 'test'
        EXEC [dbo].[UpdateOperation] 
			@ID, 
			@ProcedureName, 
			@Caption
    */
    
    UPDATE [dbo].[Operations]
    SET
		[ID] = @ID, 
		--[ProcedureName] = @ProcedureName, -- must not be changed
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID as ID

    RETURN @@ERROR 

END
GO

USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdatePerson]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdatePerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdatePerson]
    @ID uniqueidentifier,
	@Login nvarchar(32),
	@ID_Organisation uniqueidentifier,	 
	@LastName nvarchar(32), 
	@FirstName nvarchar(32), 
	@MiddleName nvarchar(32), 
	@BirthDate date, 
	@Phone nvarchar(128), 
	@EMail nvarchar(128)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Login nvarchar(32),
			@ID_Organisation uniqueidentifier,	 
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@ID uniqueidentifier,
			@Login = 'test', 
			@ID_Organisation uniqueidentifier,	
			@LastName = 'test', 
			@FirstName = 'test', 
			@MiddleName = 'test', 
			@BirthDate = 'test', 
			@Phone = 'test', 
			@EMail = 'test'
        EXEC [Office].[UpdatePerson] 
			@ID uniqueidentifier,
			@Login,
			@ID_Organisation uniqueidentifier,
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail
    */
    
    UPDATE [Office].[Persons]
    SET
		--[Login] = @Login, -- cannot be modified
		[ID_Organisation] = @ID_Organisation,	
		[LastName] = @LastName, 
		[FirstName] = @FirstName, 
		[MiddleName] = @MiddleName, 
		[BirthDate] = @BirthDate, 
		[Phone] = @Phone, 
		[EMail] = @EMail
    WHERE ID = @ID
	
    SELECT @ID as ID

    RETURN @@ERROR 

END
GO

USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectPerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectPerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectPerson]
	@Login nvarchar(32) = NULL, 
	@ID_Organisation uniqueidentifier = NULL,	
	@LastName nvarchar(32) = NULL, 
	@FirstName nvarchar(32) = NULL, 
	@MiddleName nvarchar(32) = NULL, 
	@BirthDate date = NULL, 
	@Phone nvarchar(128) = NULL, 
	@EMail nvarchar(128) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Login nvarchar(32),
			@ID_Organisation uniqueidentifier,	 
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@Login = NULL, 
			@ID_Organisation = NULL,	
			@LastName = NULL, 
			@FirstName = NULL, 
			@MiddleName = NULL, 
			@BirthDate = NULL, 
			@Phone = NULL, 
			@EMail = NULL
        EXEC [Office].[SelectPerson]
			@Login,
			@ID_Organisation,	 
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail
    */

    SELECT
		op.ID,
		op.[Login],
		op.[ID_Organisation],	 
		op.[LastName], 
		op.[FirstName], 
		op.[MiddleName], 
		op.[BirthDate], 
		op.[Phone], 
		op.[EMail], 
		op.[Caption] 
    FROM [Office].[Persons] op
        --LEFT JOIN  ON .ID = op.ID_ AND .[IsDeleted] = 0
    WHERE 
		op.[IsDeleted] = 0

END
GO

--------------------------Office.DeletePerson-----------------------------
-- �������� ������ �� ������� Office.Persons
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeletePerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeletePerson] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeletePerson]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeletePerson] @ID
    */
    
    DECLARE
        @Login nvarchar(32),
		@vID uniqueidentifier

	SELECT
		@vID = ID,
		@Login = [Login]
	FROM [Office].[Persons]
    WHERE 
		ID = @ID
		AND [IsDeleted] = 0
		AND [Login] <> 'Admin'

    UPDATE [Office].[Persons]
    SET
	    [IsDeleted] = 1
    WHERE ID = @vID
    
    IF @@ERROR = 0 AND @Login IS NOT NULL
    BEGIN
		EXEC('DROP USER ' + @Login)
		EXEC('DROP LOGIN ' + @Login)
    END

    IF @@ERROR <> 0
    BEGIN
		ROLLBACK
		SELECT NULL AS ID
		RETURN @@ERROR
    END

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO

-- ���������� ������ � ������� Office.Jobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateJob]
    @ID uniqueidentifier,
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Office].[UpdateJob] 
			@Name, 
			@Caption
    */
    
    UPDATE [Office].[Jobs]
    SET
		--[Name] = @Name, 
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID as ID

    RETURN @@ERROR 

END
GO