-- ������� �� ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectEquipment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectEquipment]
	@ID_EquipmentType uniqueidentifier = NULL, 
	@ID_Bill uniqueidentifier = NULL, 
	@ID_Vendor uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL, 
	@InvNumber nvarchar(32) = NULL, 
	@VisualNumber nvarchar(32) = NULL, 
	@Barcode bigint = NULL, 
	@Price money = NULL, 
	@Lifetime int = NULL, 
	@Settings nvarchar(max) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@InvNumber nvarchar(32), 
			@VisualNumber nvarchar(32), 
			@Barcode bigint, 
			@Price money, 
			@Lifetime int, 
			@Settings nvarchar(max)
        SELECT
			@ID_EquipmentType = NULL, 
			@ID_Bill = NULL, 
			@ID_Vendor = NULL, 
			@Caption = NULL, 
			@InvNumber = NULL, 
			@VisualNumber = NULL, 
			@Barcode = NULL, 
			@Price = NULL, 
			@Lifetime = NULL, 
			@Settings = NULL
        EXEC [Arsenal].[SelectEquipment]
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@InvNumber, 
			@VisualNumber, 
			@Barcode, 
			@Price, 
			@Lifetime, 
			@Settings
    */

    SELECT
		ae.ID,
		ae.[ID_EquipmentType], 
		ae.[ID_Bill], 
		ae.[ID_Vendor], 
		ae.[Caption] AS Product,
		ISNULL(aet.Caption + ' ', '') + ISNULL(av.Caption + ' ', '') + ae.Caption AS Caption,
		ae.[InvNumber], 
		ae.[VisualNumber], 
		ae.[Barcode], 
		ae.[Price], 
		ae.[Lifetime], 
		ae.[Settings],
		em.ID_WorkPlace 
    FROM [Arsenal].[Equipments] ae
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
		LEFT JOIN (SELECT ID_Equipment AS ID, ID_WorkPlace FROM (
			SELECT ID_Equipment, ID_WorkPlace, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY Date DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1) em ON em.ID = ae.ID
    WHERE 
		ae.[IsDeleted] = 0
		AND (@Caption IS NULL OR ae.[Caption] LIKE @Caption + '%')
	ORDER BY ae.[Caption]
END
GO

--------------------------Arsenal.InsertEquipment-----------------------------
-- ������� ������ � ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertEquipment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertEquipment]
	@ID_EquipmentType uniqueidentifier, 
	@ID_Bill uniqueidentifier, 
	@ID_Vendor uniqueidentifier, 
	@Caption nvarchar(256), 
	@InvNumber nvarchar(32), 
	@VisualNumber nvarchar(32), 
	@Barcode bigint, 
	@Price money, 
	@Lifetime int, 
	@Settings nvarchar(max)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@InvNumber nvarchar(32), 
			@VisualNumber nvarchar(32), 
			@Barcode bigint, 
			@Price money, 
			@Lifetime int, 
			@Settings nvarchar(max)
        SELECT
			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 
			@ID_Bill = '00000000-0000-0000-0000-000000000000', 
			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@InvNumber = 0, 
			@VisualNumber = 0, 
			@Barcode = 0, 
			@Price = 0, 
			@Lifetime = 0, 
			@Settings = 'test'
        EXEC [Arsenal].[InsertEquipment] 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@InvNumber, 
			@VisualNumber, 
			@Barcode, 
			@Price, 
			@Lifetime, 
			@Settings
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Equipments] 
		([ID_EquipmentType], [ID_Bill], [ID_Vendor], [Caption], [InvNumber], [VisualNumber], [Barcode], [Price], [Lifetime], [Settings])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_EquipmentType, @ID_Bill, @ID_Vendor, ISNULL(@Caption, ''), @InvNumber, @VisualNumber, @Barcode, @Price, @Lifetime, @Settings)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
-- ������� �� ������� Arsenal.Movings
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectMoving]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectMoving] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectMoving]
	@ID_Workplace uniqueidentifier = NULL, 
	@ID_MovingType uniqueidentifier = NULL, 
	@ID_Equipment uniqueidentifier = NULL, 
	@ID_Part uniqueidentifier = NULL, 
	@ID_Person uniqueidentifier = NULL, 
	@Date datetime = NULL, 
	@Note nvarchar(512) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Workplace uniqueidentifier, 
			@ID_MovingType uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_Part uniqueidentifier, 
			@ID_Person uniqueidentifier, 
			@Date datetime, 
			@Note nvarchar(512)
        SELECT
			@ID_Workplace = NULL, 
			@ID_MovingType = NULL, 
			@ID_Equipment = NULL, 
			@ID_Part = NULL, 
			@ID_Person = NULL, 
			@Date = NULL, 
			@Note = NULL
        EXEC [Arsenal].[SelectMoving]
			@ID_Workplace, 
			@ID_MovingType, 
			@ID_Equipment, 
			@ID_Part, 
			@ID_Person, 
			@Date, 
			@Note
    */

	SELECT
		am.ID,
		am.[ID_Workplace], 
		am.[ID_MovingType], 
		am.[ID_Equipment], 
		am.[ID_Part], 
		am.[ID_Person], 
		am.[Date], 
		am.[Note] 
	FROM [Arsenal].[Movings] am
	WHERE 
		am.[IsDeleted] = 0
		AND ((@ID_Equipment IS NULL AND am.ID_Part = @ID_Part) OR (am.ID_Equipment = @ID_Equipment))
	ORDER BY [Date]
END
GO


--------------------------Office.SelectBill-----------------------------
-- ������� �� ������� Office.Bills
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectBill]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectBill] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectBill]
	@ID_Buyer uniqueidentifier = NULL, 
	@ID_Seller uniqueidentifier = NULL, 
	@Date date = NULL, 
	@Invoice nvarchar(64) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Buyer uniqueidentifier, 
			@ID_Seller uniqueidentifier, 
			@Date date, 
			@Invoice nvarchar(64)
        SELECT
			@ID_Buyer = NULL, 
			@ID_Seller = NULL, 
			@Date = NULL, 
			@Invoice = NULL
        EXEC [Office].[SelectBill]
			@ID_Buyer, 
			@ID_Seller, 
			@Date, 
			@Invoice
    */

    SELECT
		ob.ID,
		ob.[ID_Buyer], 
		ob.[ID_Seller], 
		ob.[Date], 
		ob.[Invoice],
		ob.[Invoice] + ' �� ' + CONVERT(nvarchar(10), ob.[Date], 104) AS [Caption] 
    FROM [Office].[Bills] ob
        --LEFT JOIN  ON .ID = ob.ID_ AND .[IsDeleted] = 0
    WHERE 
		ob.[IsDeleted] = 0

END
GO

-- ������� �� ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectPart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectPart] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectPart]
	@ID_Equipment uniqueidentifier = NULL, 
	@ID_EquipmentType uniqueidentifier = NULL, 
	@ID_Bill uniqueidentifier = NULL, 
	@ID_Vendor uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL, 
	@VisualNumber nvarchar(32) = NULL, 
	@Price money = NULL, 
	@Settings nvarchar(512) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Equipment uniqueidentifier, 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@VisualNumber nvarchar(32), 
			@Price money, 
			@Settings nvarchar(512)
        SELECT
			@ID_Equipment = '1FEE4730-7B9D-E511-8B87-0023540F263B', 
			@ID_EquipmentType = NULL, 
			@ID_Bill = NULL, 
			@ID_Vendor = NULL, 
			@Caption = NULL, 
			@VisualNumber = NULL, 
			@Price = NULL, 
			@Settings = NULL
        EXEC [Arsenal].[SelectPart]
			@ID_Equipment, 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@VisualNumber, 
			@Price, 
			@Settings
    */

    SELECT
		ap.ID,
		ap.[ID_Equipment], 
		ap.[ID_EquipmentType], 
		ap.[ID_Bill], 
		ap.[ID_Vendor], 
		ap.[Caption] AS Product,
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ap.Caption AS Caption,  
		CAST(CASE WHEN ap.[ID_Equipment] IS NULL THEN 0 ELSE 1 END AS bit) AS Installed, 
		ap.[VisualNumber], 
		ap.[Price], 
		ap.[Settings]
    FROM [Arsenal].[Parts] ap
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ap.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ap.ID_Vendor AND av.[IsDeleted] = 0
    WHERE 
		ap.[IsDeleted] = 0
		AND (@Caption IS NULL OR ap.[Caption] LIKE @Caption + '%')
		AND (@ID_Equipment IS NULL OR @ID_Equipment = ap.[ID_Equipment] OR (ap.[ID_Equipment] IS NULL AND @ID_Equipment = '00000000-0000-0000-0000-000000000000'))
	ORDER BY ap.[Caption]
END
GO


-- ������� ���������
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectStructure]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectStructure] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectStructure]
AS
BEGIN
SET NOCOUNT ON

    /*
        EXEC [Arsenal].[SelectStructure]
    */

	DECLARE @ID_BuyerPurpose uniqueidentifier

	SELECT @ID_BuyerPurpose = op.ID
	FROM [Office].[Purposes] op
	WHERE
		op.[IsDeleted] = 0
		AND op.[Name] = 'Buyer'

    SELECT
		owp.Caption,
		owp.ID,
		oo.Caption AS Organisation,
		oo.[Address], 
		oo.Phone,
		od.Caption AS Department, 
		owp.Caption AS Workplace, 
		owp.Cabinet, 
		owp.[ID_Worker],
		2 AS [Level]
    FROM Office.WorkPlaces owp
        JOIN Office.Departments od ON owp.ID_Department = od.ID AND owp.[IsDeleted] = 0
		JOIN [Office].[Organisations] oo ON od.ID_Organisation = oo.ID AND od.[IsDeleted] = 0
		JOIN [Office].[OrganisationPurposes] oop ON oop.ID_Organisation = oo.ID AND oop.ID_Purpose = @ID_BuyerPurpose AND oop.IsDeleted = 0
    WHERE 
		oo.[IsDeleted] = 0
	UNION ALL SELECT
		od.Caption ,
		NULL AS ID,
		oo.Caption AS Organisation,
		oo.[Address], 
		oo.Phone,
		od.Caption AS Department, 
		NULL AS Workplace, 
		NULL AS Cabinet, 
		NULL AS [ID_Worker],
		1 AS [Level]
    FROM Office.Departments od
		JOIN [Office].[Organisations] oo ON od.ID_Organisation = oo.ID AND od.[IsDeleted] = 0
		JOIN [Office].[OrganisationPurposes] oop ON oop.ID_Organisation = oo.ID AND oop.ID_Purpose = @ID_BuyerPurpose AND oop.IsDeleted = 0 
    WHERE 
		od.[IsDeleted] = 0
	UNION ALL SELECT
	 	oo.Caption,
		NULL AS ID, 
		oo.Caption AS Organisation,
		oo.[Address], 
		oo.Phone,
		NULL AS Department, 
		NULL AS Workplace, 
		NULL AS Cabinet, 
		NULL AS [ID_Worker],
		0 AS [Level]
    FROM Office.[Organisations] oo
	JOIN [Office].[OrganisationPurposes] oop ON oop.ID_Organisation = oo.ID AND oop.ID_Purpose = @ID_BuyerPurpose AND oop.IsDeleted = 0 
    WHERE 
		oo.[IsDeleted] = 0						
	ORDER BY oo.[Caption], od.Caption, [Level], owp.Caption
END
GO

--------------------------Arsenal.InsertPart-----------------------------
-- ������� ������ � ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertPart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertPart] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertPart]
	@ID_Equipment uniqueidentifier, 	@ID_EquipmentType uniqueidentifier, 	@ID_Bill uniqueidentifier, 	@ID_Vendor uniqueidentifier, 	@Caption nvarchar(256), 	@VisualNumber nvarchar(32), 	@Price money, 	@Settings nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Equipment uniqueidentifier, 			@ID_EquipmentType uniqueidentifier, 			@ID_Bill uniqueidentifier, 			@ID_Vendor uniqueidentifier, 			@Caption nvarchar(256), 			@VisualNumber nvarchar(32), 			@Price money, 			@Settings nvarchar(512)
        SELECT
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 			@ID_Bill = '00000000-0000-0000-0000-000000000000', 			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 			@Caption = 'test', 			@VisualNumber = 0, 			@Price = 0, 			@Settings = 'test'
        EXEC [Arsenal].[InsertPart] 
			@ID_Equipment, 			@ID_EquipmentType, 			@ID_Bill, 			@ID_Vendor, 			@Caption, 			@VisualNumber, 			@Price, 			@Settings
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Parts] 
		([ID_Equipment], [ID_EquipmentType], [ID_Bill], [ID_Vendor], [Caption], [VisualNumber], [Price], [Settings])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Equipment, @ID_EquipmentType, @ID_Bill, @ID_Vendor, ISNULL(@Caption, ''), @VisualNumber, @Price, @Settings)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO

--------------------------Arsenal.SelectResult-----------------------------
-- ������� �� ������� Arsenal.Results
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectResult]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectResult] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectResult]
	@ID_Inventarisation uniqueidentifier = NULL, 	@ID_Equipment uniqueidentifier = NULL, 	@ID_WorkPlace uniqueidentifier = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Inventarisation uniqueidentifier, 			@ID_Equipment uniqueidentifier, 			@ID_WorkPlace uniqueidentifier
        SELECT
			@ID_Inventarisation = NULL, 			@ID_Equipment = NULL, 			@ID_WorkPlace = NULL
        EXEC [Arsenal].[SelectResult]
			@ID_Inventarisation, 			@ID_Equipment, 			@ID_WorkPlace
    */

    SELECT
		ar.ID,
		ar.[ID_Inventarisation], 		ar.[ID_Equipment],		ae.[InvNumber], 		ae.[Barcode], 		em.[ID_WorkPlace] AS ID_CurrentWorkplace,
		ar.[ID_WorkPlace],
		owp.ID_Department,
		od.ID_Organisation
    FROM [Arsenal].[Results] ar
		JOIN [Arsenal].[Equipments] ae ON ae.ID = ar.ID_Equipment AND ae.IsDeleted = 0
        LEFT JOIN (SELECT ID_Equipment AS ID, ID_WorkPlace FROM (
			SELECT ID_Equipment, ID_WorkPlace, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY Date DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1) em ON em.ID = ar.ID_Equipment
		LEFT JOIN [Office].[WorkPlaces] owp ON owp.ID = em.ID_Workplace AND owp.IsDeleted = 0
		LEFT JOIN [Office].[Departments] od ON od.ID = owp.ID_Department AND od.IsDeleted = 0
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
    WHERE 
		ar.[IsDeleted] = 0
		AND (@ID_Inventarisation IS NULL OR ar.[ID_Inventarisation] = @ID_Inventarisation)
	ORDER BY ISNULL(aet.Caption + ' ', '') + ISNULL(av.Caption + ' ', '') + ae.Caption
END
GO

--------------------------Arsenal.SelectInventarisation-----------------------------
-- ������� �� ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectInventarisation]
	@ID_Worker uniqueidentifier = NULL, 	@Caption nvarchar(256) = NULL, 	@BeginDate date = NULL, 	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Worker uniqueidentifier, 			@Caption nvarchar(256), 			@BeginDate date, 			@EndDate date
        SELECT
			@ID_Worker = NULL, 			@Caption = NULL, 			@BeginDate = NULL, 			@EndDate = NULL
        EXEC [Arsenal].[SelectInventarisation]
			@ID_Worker, 			@Caption, 			@BeginDate, 			@EndDate
    */

    SELECT
		ai.ID,
		ai.[ID_Worker], 		ai.[Caption], 		ai.[BeginDate], 		ai.[EndDate],
		CAST(CASE WHEN ai.BeginDate IS NULL THEN 0 ELSE CASE WHEN ai.[EndDate] IS NULL THEN 1 ELSE 0 END END AS bit) AS [Started], 
		CAST(CASE WHEN ai.BeginDate IS NULL THEN 0 ELSE CASE WHEN ai.[EndDate] IS NULL THEN 0 ELSE 1 END END AS bit) AS [Ended]
    FROM [Arsenal].[Inventarisations] ai
        --LEFT JOIN  ON .ID = ai.ID_ AND .[IsDeleted] = 0
    WHERE 
		ai.[IsDeleted] = 0
		AND (@Caption IS NULL OR ai.[Caption] LIKE @Caption + '%')
	ORDER BY ai.[Caption]
END
GO

-- ������� ������ � ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertInventarisation]
	@ID_Worker uniqueidentifier, 	@Caption nvarchar(256), 	@BeginDate date = NULL, 	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Worker uniqueidentifier, 			@Caption nvarchar(256), 			@BeginDate date, 			@EndDate date
        SELECT
			@ID_Worker = '00000000-0000-0000-0000-000000000000', 			@Caption = 'test', 			@BeginDate = 'test', 			@EndDate = 'test'
        EXEC [Arsenal].[InsertInventarisation] 
			@ID_Worker, 			@Caption, 			@BeginDate, 			@EndDate
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT [Arsenal].[Inventarisations] 
		([ID_Worker], [Caption], [BeginDate], [EndDate])
	OUTPUT INSERTED.ID INTO @outid
	SELECT @ID_Worker, @Caption, @BeginDate, @EndDate
	
	SELECT TOP 1 @ID = ID FROM @outid

	INSERT [Arsenal].[Results] (ID_Inventarisation, ID_Equipment)
	SELECT @ID, ae.ID
	FROM [Arsenal].[Equipments] ae
	WHERE ae.[IsDeleted] = 0
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO

--------------------------Office.SelectDepartment-----------------------------
-- ������� �� ������� Office.Departments
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectDepartment]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectDepartment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectDepartment]
	@ID_Organisation uniqueidentifier = NULL, 	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Organisation uniqueidentifier, 			@Caption nvarchar(256)
        SELECT
			@ID_Organisation = NULL, 			@Caption = NULL
        EXEC [Office].[SelectDepartment]
			@ID_Organisation, 			@Caption
    */

    SELECT
		od.ID,
		od.[ID_Organisation], 		od.[Caption] + '(' + oo.Caption + ')' AS Caption
    FROM [Office].[Departments] od
        JOIN Office.Organisations oo ON oo.ID = od.ID_Organisation AND oo.[IsDeleted] = 0
    WHERE 
		od.[IsDeleted] = 0
		AND (@Caption IS NULL OR od.[Caption] LIKE @Caption + '%')
	ORDER BY od.[Caption]
END
GO

-- ������� �� ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectOrganisation]
	@BuyersOnly int = NULL,
	@Purpose nvarchar(32) = NULL,
	@Caption nvarchar(256) = NULL	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@BuyersOnly int,
			@Purpose nvarchar(32),
			@Caption nvarchar(256)
        SELECT
			@BuyersOnly = 1,
			@Purpose  = NULL,
			@Caption = NULL
        EXEC [Office].[SelectOrganisation]
			@BuyersOnly,
			@Purpose,
			@Caption
    */

    SELECT
		oo.ID,
		oo.[Caption], 
		oo.[Address], 
		oo.[Phone],
		oop.ID_Purpose,
		oop.Name AS PurposeName
    FROM [Office].[Organisations] oo
        LEFT JOIN (
			SELECT ID_Organisation, ID_Purpose, Name, MIN([Priority]) OVER (PARTITION BY ID_Organisation) p
			FROM [Office].[OrganisationPurposes] oop
			JOIN [Office].[Purposes] op ON op.ID = oop.ID_Purpose AND op.IsDeleted = 0 
			WHERE oop.[IsDeleted] = 0
		) oop ON oop.ID_Organisation = oo.ID 
    WHERE 
		oo.[IsDeleted] = 0
		AND (@Caption IS NULL OR oo.[Caption] LIKE @Caption + '%')
		AND (ISNULL(@BuyersOnly, 0) = 0 OR oop.[Name] = 'Buyer')
		AND (@Purpose IS NULL OR oop.[Name] = @Purpose)
	ORDER BY oo.[Caption]
END
GO
--------------------------Office.InsertOrganisation-----------------------------
-- ������� ������ � ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertOrganisation]
	@Caption nvarchar(256), 
	@Address nvarchar(256), 
	@Phone nvarchar(64),
	@ID_Purpose uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Caption nvarchar(256), 
			@Address nvarchar(256), 
			@Phone nvarchar(64),
			@ID_Purpose uniqueidentifier
        SELECT
			@Caption = 'test', 
			@Address = 'test', 
			@Phone = 'test',
			@ID_Purpose = ''
        EXEC [Office].[InsertOrganisation] 
			@Caption, 
			@Address, 
			@Phone,
			@ID_Purpose
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Organisations] 
		([Caption], [Address], [Phone])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Caption, @Address, @Phone)
	
	SELECT TOP 1 @ID = ID FROM @outid

	INSERT [Office].[OrganisationPurposes]
		(ID_Organisation, ID_Purpose)
	SELECT @ID, @ID_Purpose
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateOrganisation-----------------------------
-- ���������� ������ � ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateOrganisation]
    @ID uniqueidentifier,
	@Caption nvarchar(256), 
	@Address nvarchar(256), 
	@Phone nvarchar(64),
	@ID_Purpose uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Caption nvarchar(256), 
			@Address nvarchar(256), 
			@Phone nvarchar(64),
			@ID_Purpose uniqueidentifier
        SELECT
			@Caption = 'test', 
			@Address = 'test', 
			@Phone = 'test',
			@ID_Purpose = ''
        EXEC [Office].[UpdateOrganisation] 
			@Caption, 
			@Address, 
			@Phone,
			@ID_Purpose 
    */
    
    UPDATE [Office].[Organisations]
    SET
		[Caption] = @Caption, 
		[Address] = @Address, 
		[Phone] = @Phone
    WHERE ID = @ID

	DECLARE 
		@ID_OrgPurposeOld uniqueidentifier

	SELECT @ID_OrgPurposeOld = oop.ID
	FROM [Office].[OrganisationPurposes] oop
	JOIN [Office].[Purposes] op ON op.ID = oop.ID_Purpose AND op.IsDeleted = 0
	WHERE 
		oop.ID_Organisation = @ID
		AND oop.IsDeleted = 0
	ORDER BY op.[Priority] ASC

	IF @ID_OrgPurposeOld IS NOT NULL
		UPDATE [Office].[OrganisationPurposes]
		SET ID_Purpose = @ID_Purpose
		WHERE ID = @ID_OrgPurposeOld
	ELSE
		INSERT [Office].[OrganisationPurposes]
			(ID_Organisation, ID_Purpose)
		SELECT @ID, @ID_Purpose
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO