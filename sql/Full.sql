
IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'Arsenal')	
CREATE
--USE MASTER DROP
DATABASE [Arsenal] 
GO

USE [Arsenal]
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Office')	
	EXEC('CREATE Schema [Office]')
	GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Arsenal')		
	EXEC('CREATE Schema [Arsenal]')
	GO

IF OBJECT_ID('[Arsenal].dbo.[Operations]') IS NULL	
	CREATE TABLE [Arsenal].dbo.[Operations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Operation_ID PRIMARY KEY, 
		ProcedureName nvarchar(64) NOT NULL,
		Caption nvarchar(256) NOT NULL
	)

IF OBJECT_ID('[Arsenal].[Office].[Purposes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Purposes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Purpose_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		[Priority] tinyint NOT NULL DEFAULT 255
	)

IF OBJECT_ID('[Arsenal].[Office].[Organisations]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Organisations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Organisation_ID PRIMARY KEY, 
		--Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		[Address] nvarchar(256) NULL,
		[Phone] nvarchar(64) NULL
	)

IF OBJECT_ID('[Arsenal].[Office].[OrganisationPurposes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[OrganisationPurposes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_OrganisationPurpose_ID PRIMARY KEY, 
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		ID_Purpose uniqueidentifier NOT NULL REFERENCES Office.Purposes(ID)
	)

IF OBJECT_ID('[Arsenal].[Office].[Persons]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Persons]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Person_ID PRIMARY KEY,
		[Login] nvarchar(32) NOT NULL,
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),		
		LastName nvarchar(32) NOT NULL,
		FirstName nvarchar(32) NOT NULL,
		MiddleName nvarchar(32) NOT NULL default '',
		BirthDate date NULL,
		Phone nvarchar(128) NULL default '',
		EMail nvarchar(128) NULL default '',
		Caption AS LastName + ' ' + FirstName + ' ' + MiddleName,
		CONSTRAINT UQ_Person_Login UNIQUE([Login])
	)  

IF OBJECT_ID('[Arsenal].[Office].[Jobs]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Jobs]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Job_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		CONSTRAINT UQ_Job_Name UNIQUE([Name])
	)
IF OBJECT_ID('[Arsenal].[Office].[PersonJobs]') IS NULL		
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[PersonJobs]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_PersonJob_ID PRIMARY KEY, 
		ID_Person uniqueidentifier NOT NULL REFERENCES Office.Persons(ID),
		ID_Job uniqueidentifier NOT NULL REFERENCES Office.Jobs(ID),
		BeginDate date NOT NULL,
		EndDate date NULL
	)

IF OBJECT_ID('[Arsenal].[Office].[JobOperations]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[JobOperations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_JobOperation_ID PRIMARY KEY, 
		ID_Job uniqueidentifier NOT NULL REFERENCES Office.Jobs(ID),
		ID_Operation uniqueidentifier NOT NULL REFERENCES Operations(ID),
		BeginDate date NOT NULL,
		EndDate date NULL,
		Managable bit NOT NULL default 1,
		Personal bit NOT NULL default 0
	)

IF OBJECT_ID('[Arsenal].[Office].[Departments]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Departments]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Department_ID PRIMARY KEY, 
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		--Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL
		-- department requisites
	) 

IF OBJECT_ID('[Arsenal].[Office].[Workers]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Workers]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Worker_ID PRIMARY KEY,
		ID_Organisation uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),		
		LastName nvarchar(32) NOT NULL,
		FirstName nvarchar(32) NOT NULL,
		MiddleName nvarchar(32) NOT NULL default '',
		Phone nvarchar(128) NULL default '',
		EMail nvarchar(128) NULL default '',
		Caption AS LastName + ' ' + FirstName + ' ' + MiddleName
	) 

IF OBJECT_ID('[Arsenal].[Office].[WorkPlaces]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[WorkPlaces]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_WorkPlace_ID PRIMARY KEY, 
		ID_Department uniqueidentifier NOT NULL REFERENCES Office.Departments(ID),
		ID_Worker uniqueidentifier NULL REFERENCES Office.Workers(ID),
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		Cabinet nvarchar(16) NULL default '',
		CONSTRAINT UQ_Workplace_Name UNIQUE([Name])
	) 

IF OBJECT_ID('[Arsenal].[Office].[Bills]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Office].[Bills]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Bill_ID PRIMARY KEY, 
		ID_Buyer uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		ID_Seller uniqueidentifier NOT NULL REFERENCES Office.Organisations(ID),
		Date date NOT NULL,
		Invoice nvarchar(64) NOT NULL default ''
	) 	 

IF OBJECT_ID('[Arsenal].[Arsenal].[Vendors]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Vendors]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Vendor_ID PRIMARY KEY, 
		--Name nvarchar(64) NOT NULL,
		Caption nvarchar(256) NOT NULL
		--,CONSTRAINT UQ_Vendor_Name UNIQUE([Name])
	)

IF OBJECT_ID('[Arsenal].[Arsenal].[EquipmentTypes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[EquipmentTypes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_EquipmentType_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL
	)

IF OBJECT_ID('[Arsenal].[Arsenal].[MovingTypes]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[MovingTypes]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_MovingType_ID PRIMARY KEY, 
		Name nvarchar(32) NOT NULL,
		Caption nvarchar(256) NOT NULL,
		CONSTRAINT UQ_MovingType_Name UNIQUE([Name])
	)		 

IF OBJECT_ID('[Arsenal].[Arsenal].[Inventarisations]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Inventarisations]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Inventarisation_ID PRIMARY KEY, 
		ID_Worker uniqueidentifier NULL REFERENCES [Office].[Workers](ID),
		Caption nvarchar(256) NOT NULL,
		BeginDate date NULL,
		EndDate date NULL
	)
	
IF OBJECT_ID('[Arsenal].[Arsenal].[Equipments]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Equipments]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Equipment_ID PRIMARY KEY, 
		ID_EquipmentType uniqueidentifier NULL REFERENCES Arsenal.EquipmentTypes(ID),
		ID_Bill uniqueidentifier NULL REFERENCES [Office].[Bills](ID),
		ID_Vendor uniqueidentifier NULL REFERENCES Arsenal.Vendors(ID),
		Caption nvarchar(256) NOT NULL default '',
		InvNumber nvarchar(32) NULL,
		VisualNumber nvarchar(32) NULL,
		Barcode bigint NULL,
		Price money NULL,
		Lifetime int NULL,
		Settings nvarchar(512) NULL
	)	
	
IF OBJECT_ID('[Arsenal].[Arsenal].[Parts]') IS NULL	
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Parts]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Part_ID PRIMARY KEY, 
		ID_Equipment uniqueidentifier NULL REFERENCES Arsenal.Equipments(ID),
		ID_EquipmentType uniqueidentifier NULL REFERENCES Arsenal.EquipmentTypes(ID),
		ID_Bill uniqueidentifier NULL REFERENCES [Office].[Bills](ID),
		ID_Vendor uniqueidentifier NULL REFERENCES Arsenal.Vendors(ID),
		Caption nvarchar(256) NOT NULL default '',
		VisualNumber nvarchar(32) NULL,
		Price money NULL,
		Settings nvarchar(512)
	)

IF OBJECT_ID('[Arsenal].[Arsenal].[Movings]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Movings]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Moving_ID PRIMARY KEY, 
		ID_Workplace uniqueidentifier NOT NULL REFERENCES Office.Workplaces(ID),
		ID_MovingType uniqueidentifier NOT NULL REFERENCES Arsenal.MovingTypes(ID),
		ID_Equipment uniqueidentifier NULL REFERENCES Arsenal.Equipments(ID),
		ID_Part uniqueidentifier NULL REFERENCES Arsenal.Parts(ID),
		ID_Person uniqueidentifier NULL REFERENCES [Office].[Persons](ID),
		Date datetime NOT NULL,
		Note nvarchar(512) NULL,
		CONSTRAINT [���� ���� ������������ ���� ��������������] CHECK 
			((CASE WHEN ID_Equipment IS NOT NULL THEN 1 ELSE 0 END + CASE WHEN ID_Part IS NOT NULL THEN 1 ELSE 0 END) = 1)
	)	  

IF OBJECT_ID('[Arsenal].[Arsenal].[Results]') IS NULL
	CREATE 
	--DROP
	TABLE [Arsenal].[Arsenal].[Results]
	(
		ID uniqueidentifier default NEWSEQUENTIALID() ROWGUIDCOL CONSTRAINT PK_Result_ID PRIMARY KEY, 
		ID_Inventarisation uniqueidentifier NULL REFERENCES [Arsenal].[Inventarisations](ID),
		ID_Equipment uniqueidentifier NULL REFERENCES Arsenal.Equipments(ID),
		ID_WorkPlace uniqueidentifier NULL REFERENCES [Office].[WorkPlaces](ID)
	)		
-- System IsDeleted fields

ALTER TABLE [Arsenal].[dbo].[Operations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Persons] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Jobs] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[PersonJobs] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[JobOperations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Purposes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Organisations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[OrganisationPurposes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Departments] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Workers] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[WorkPlaces] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Office].[Bills] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Vendors] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[EquipmentTypes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[MovingTypes] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Inventarisations] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Equipments] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Parts] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Movings] ADD IsDeleted bit NOT NULL DEFAULT 0
ALTER TABLE [Arsenal].[Arsenal].[Results] ADD IsDeleted bit NOT NULL DEFAULT 0

DECLARE	@ID_Org uniqueidentifier = '9CE38B9F-766E-477B-A9AC-BDA05BBB048C'
DECLARE @ID_Admin uniqueidentifier = 'ADADADAD-ADAD-ADAD-ADAD-ADADADADADAD'	

IF NOT EXISTS(SELECT 1 FROM [Arsenal].[Office].[Organisations] WHERE ID = @ID_Org)
	INSERT [Arsenal].[Office].[Organisations](ID, Caption)
	SELECT @ID_Org, '�������� ������'

-- adding admin account without triggers and procedures, just for info-panel, admins' logins work independent
INSERT [Arsenal].Office.Persons([ID], [Login], [ID_Organisation], FirstName, LastName, BirthDate) SELECT @ID_Admin, 'Admin', @ID_Org, 'Admin', '', '19000101'


------------------------------------------------------------------------------------------------------------------------------------------------------ Fundamentals

USE [Arsenal]
GO

IF OBJECT_ID('[dbo].[InsertOperation]') IS NULL	
   EXEC('CREATE PROCEDURE [dbo].[InsertOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [dbo].[InsertOperation]
	@ProcedureName nvarchar(64) = NULL
	,@Caption nvarchar(256) = NULL
	
AS
BEGIN
SET NOCOUNT ON

    /*    
		DECLARE    
			@ProcedureName nvarchar(64)
			,@Caption nvarchar(256)
			
		SELECT
			@ProcedureName = 'TestOperation'
			,@Caption = 'TestOperation'
			 
        EXEC [dbo].[InsertOperation] 
			@ProcedureName
			,@Caption
			
    */
    
    DECLARE 
		@ID_Inserted uniqueidentifier
	DECLARE 
		@INSERTED TABLE(ID uniqueidentifier)
    
    INSERT [dbo].[Operations] ([ProcedureName], [Caption])
	OUTPUT INSERTED.ID INTO @INSERTED
	VALUES (@ProcedureName, @Caption)
	
	SELECT TOP 1 @ID_Inserted = ID 
	FROM @INSERTED
	
	SELECT @ID_Inserted as ID

    RETURN @@ERROR

END
GO


IF OBJECT_ID('[Office].[Portfolio]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[Portfolio] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[Portfolio]
	@Login nvarchar(256)
WITH EXECUTE AS OWNER
AS
BEGIN
SET NOCOUNT ON

    /*        
        DECLARE @Login nvarchar(256)
        SELECT @Login  = 'abc' 
        EXEC [Office].[Portfolio] @Login
    */
		
	IF IS_SRVROLEMEMBER('sysadmin', @Login) = 1
		SELECT 
			op.ID,
			op.[Login] AS [Login], 
			op.[Login] AS [Job], 
			op.FirstName, 
			op.LastName, 
			op.MiddleName,
			'' AS Department,
			'' AS Organisation
		FROM Office.Persons op
		WHERE (op.[Login] = 'Admin') 
	ELSE
		SELECT TOP 1
			op.ID, 
			op.[Login] AS [Login], 
			oj.Caption AS [Job], 
			op.FirstName, 
			op.LastName, 
			op.MiddleName,
			'' AS Department,
			oo.Caption AS Organisation
		FROM Office.Persons op 
		LEFT JOIN Office.PersonJobs opj ON op.ID = opj.ID_Person AND opj.[IsDeleted] = 0
		LEFT JOIN Office.Jobs oj ON oj.ID = opj.ID_Job AND oj.[IsDeleted] = 0
		LEFT JOIN Office.Organisations oo ON oo.ID = op.ID_Organisation AND oo.[IsDeleted] = 0
		WHERE op.[IsDeleted] = 0    
			AND (op.[Login] = @Login)
			AND ((opj.EndDate IS NULL) OR (opj.EndDate > GETDATE()))
			AND ((opj.BeginDate IS NULL) OR (opj.BeginDate < GETDATE()))
		ORDER BY opj.BeginDate DESC		
END
GO

GRANT EXECUTE ON [Office].[Portfolio] TO public

GO



IF OBJECT_ID('dbo.[SelectAvailableOperation]') IS NULL
   EXEC('CREATE PROCEDURE [dbo].[SelectAvailableOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[SelectAvailableOperation]
	@Login nvarchar(256)
WITH EXECUTE AS OWNER
AS
BEGIN
SET NOCOUNT ON

    /*        
        DECLARE @Login nvarchar(256)
        SELECT @Login  = 'sa' 
        EXEC [dbo].[SelectAvailableOperation] @Login
    */
		
	IF IS_SRVROLEMEMBER('sysadmin', @Login) = 1
		SELECT 
			op.ProcedureName,
			CAST(1 AS bit) AS Managable,
			CAST(0 AS bit) AS Personal
		FROM [dbo].[Operations] AS op	
		ORDER BY op.ProcedureName
	ELSE
		SELECT 
			op.ProcedureName,
			ojo.Managable,
			ojo.Personal
		FROM [dbo].[Operations] AS op
		JOIN Office.JobOperations AS ojo ON op.ID = ojo.ID_Operation AND ojo.[IsDeleted] = 0
		JOIN Office.PersonJobs omj ON omj.ID_Job = ojo.ID_Job AND omj.[IsDeleted] = 0
		JOIN Office.Persons om ON om.ID = omj.ID_Person AND om.[IsDeleted] = 0
		WHERE op.[IsDeleted] = 0    
			AND (om.[Login] = @Login)
			AND ((omj.EndDate IS NULL) OR (omj.EndDate > GETDATE()))
			AND (omj.BeginDate < GETDATE())
		ORDER BY op.ProcedureName
END

GRANT EXECUTE ON [dbo].[SelectAvailableOperation] TO public

GO


GO	


-- triggers for automatization and security

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'TR_CreateProcedure' AND type = 'TR')
   EXEC('CREATE TRIGGER [TR_CreateProcedure] ON DATABASE FOR CREATE_PROCEDURE AS BEGIN DECLARE @I int END')
GO

	ALTER TRIGGER [TR_CreateProcedure]
	ON DATABASE
	FOR CREATE_PROCEDURE
	AS
	BEGIN
		DECLARE @Command nvarchar(300)
		SET @Command='EXEC dbo.InsertOperation ''' + 
			EVENTDATA().value('(/EVENT_INSTANCE/SchemaName)[1]','nvarchar(300)')+'.'+EVENTDATA().value('(/EVENT_INSTANCE/ObjectName)[1]','nvarchar(300)') +
			''',''' + EVENTDATA().value('(/EVENT_INSTANCE/ObjectName)[1]','nvarchar(300)') + ''''
		EXECUTE(@Command)
	END

	GO
	ENABLE TRIGGER [TR_CreateProcedure] ON DATABASE


IF OBJECT_ID('[Office].[AlterPersonJob]') IS NULL	
   EXEC('CREATE TRIGGER [Office].[AlterPersonJob] ON [Office].[PersonJobs] FOR INSERT AS BEGIN DECLARE @I int END')
GO
	ALTER 
	--DROP 
	TRIGGER [Office].[AlterPersonJob]
	ON Office.PersonJobs
	FOR INSERT, UPDATE, DELETE  
	AS
	BEGIN
		DECLARE @JobRoleDel nvarchar(32), @UserNameDel nvarchar(32), @JobRoleIns nvarchar(32), @UserNameIns nvarchar(32)

		SELECT @JobRoleIns = Name
		FROM Office.Jobs oj
		JOIN INSERTED ON oj.ID = INSERTED.ID_Job
		WHERE INSERTED.IsDeleted = 0

		SELECT @UserNameIns = [Login]
		FROM Office.Persons om
		JOIN INSERTED ON om.ID = INSERTED.ID_Person
		WHERE INSERTED.IsDeleted = 0

		SELECT @JobRoleDel = Name
		FROM Office.Jobs oj
		JOIN DELETED ON oj.ID = DELETED.ID_Job

		SELECT @UserNameDel = [Login]
		FROM Office.Persons om
		JOIN DELETED ON om.ID = DELETED.ID_Person

		IF EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED) 
		BEGIN
			SELECT @JobRoleDel = Name
			FROM Office.Jobs oj
			JOIN INSERTED ON oj.ID = INSERTED.ID_Job
			WHERE INSERTED.IsDeleted = 1

			SELECT @UserNameDel = [Login]
			FROM Office.Persons om
			JOIN INSERTED ON om.ID = INSERTED.ID_Person
			WHERE INSERTED.IsDeleted = 1
		END

		IF (@JobRoleDel IS NOT NULL) AND (@UserNameDel IS NOT NULL)
		AND EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobRoleDel and Type = 'R') 
			EXEC ('sp_droprolemember ''' + @JobRoleDel + ''',''' + @UserNameDel + '''')
		IF (@JobRoleIns IS NOT NULL) AND (@UserNameIns IS NOT NULL)
		AND EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobRoleIns and Type = 'R')
			EXEC ('sp_addrolemember ''' + @JobRoleIns + ''',''' + @UserNameIns + '''')
	END
	GO

IF OBJECT_ID('[Office].[AlterJobRole]') IS NULL
   EXEC('CREATE TRIGGER [Office].[AlterJobRole] ON [Office].[Jobs] FOR INSERT AS BEGIN DECLARE @I int END')
GO
	ALTER 
	--DROP 
	TRIGGER  [Office].[AlterJobRole]
	ON Office.Jobs
	FOR INSERT, UPDATE, DELETE 
	AS
	BEGIN
		DECLARE @JobNameDel nvarchar(32), @JobNameIns nvarchar(32)
		SELECT TOP 1 @JobNameIns = Name FROM INSERTED WHERE INSERTED.IsDeleted = 0
		SELECT TOP 1 @JobNameDel = Name FROM DELETED
		IF EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED) 
			SELECT TOP 1 @JobNameDel = Name FROM INSERTED WHERE INSERTED.IsDeleted = 1
		IF EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobNameDel and Type = 'R')
			EXEC ('DROP ROLE ' + @JobNameDel)
		IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE Name = @JobNameDel and Type = 'R')
			EXEC ('CREATE ROLE ' + @JobNameIns)
	END
	GO

IF OBJECT_ID('[Office].[AlterJobOperation]') IS NULL
   EXEC('CREATE TRIGGER [Office].[AlterJobOperation] ON [Office].[JobOperations] FOR INSERT AS BEGIN DECLARE @I int END')
GO
	ALTER 
	--DROP 
	TRIGGER [Office].[AlterJobOperation]
	ON [Office].[JobOperations]
	FOR INSERT, UPDATE, DELETE  
	AS
	BEGIN		
		DECLARE @JobRoleDel nvarchar(32), @ProcNameDel nvarchar(64), @JobRoleIns nvarchar(32), @ProcNameIns nvarchar(64)
		SELECT @JobRoleIns = Name
		FROM Office.Jobs oj
		JOIN INSERTED ON oj.ID = INSERTED.ID_Job
		WHERE INSERTED.IsDeleted = 0

		SELECT @ProcNameIns = ProcedureName
		FROM Operations op
		JOIN INSERTED ON op.ID = INSERTED.ID_Operation
		WHERE INSERTED.IsDeleted = 0

		SELECT @JobRoleDel = Name
		FROM Office.Jobs oj
		JOIN DELETED ON oj.ID = DELETED.ID_Job

		SELECT @ProcNameDel = ProcedureName
		FROM Operations op
		JOIN DELETED ON op.ID = DELETED.ID_Operation

		IF EXISTS(SELECT 1 FROM INSERTED) AND EXISTS(SELECT 1 FROM DELETED)
		BEGIN
			SELECT @JobRoleIns = Name
			FROM Office.Jobs oj
			JOIN INSERTED ON oj.ID = INSERTED.ID_Job
			WHERE INSERTED.IsDeleted = 1

			SELECT @ProcNameIns = ProcedureName
			FROM Operations op
			JOIN INSERTED ON op.ID = INSERTED.ID_Operation
			WHERE INSERTED.IsDeleted = 1
		END

		IF @ProcNameDel IS NOT NULL AND @JobRoleDel IS NOT NULL
			EXEC ('DENY EXECUTE ON ' + @ProcNameDel + ' TO ' + @JobRoleDel)
		IF @ProcNameIns IS NOT NULL AND @JobRoleIns IS NOT NULL
			EXEC ('GRANT EXECUTE ON ' + @ProcNameIns + ' TO ' + @JobRoleIns)
	END
	GO
	

GO

--------------------------------------------------------------------------------------------------------------------------- generated procedures

---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.Equipments---------------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectEquipment-----------------------------
-- ������� �� ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectEquipment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectEquipment]
	@ID_EquipmentType uniqueidentifier = NULL, 
	@ID_Bill uniqueidentifier = NULL, 
	@ID_Vendor uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL, 
	@InvNumber nvarchar(32) = NULL, 
	@VisualNumber nvarchar(32) = NULL, 
	@Barcode bigint = NULL, 
	@Price money = NULL, 
	@Lifetime int = NULL, 
	@Settings nvarchar(512) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@InvNumber nvarchar(32), 
			@VisualNumber nvarchar(32), 
			@Barcode bigint, 
			@Price money, 
			@Lifetime int, 
			@Settings nvarchar(512)
        SELECT
			@ID_EquipmentType = NULL, 
			@ID_Bill = NULL, 
			@ID_Vendor = NULL, 
			@Caption = NULL, 
			@InvNumber = NULL, 
			@VisualNumber = NULL, 
			@Barcode = NULL, 
			@Price = NULL, 
			@Lifetime = NULL, 
			@Settings = NULL
        EXEC [Arsenal].[SelectEquipment]
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@InvNumber, 
			@VisualNumber, 
			@Barcode, 
			@Price, 
			@Lifetime, 
			@Settings
    */

    SELECT
		ae.ID,
		ae.[ID_EquipmentType], 
		ae.[ID_Bill], 
		ae.[ID_Vendor], 
		ae.[Caption], 
		ae.[InvNumber], 
		ae.[VisualNumber], 
		ae.[Barcode], 
		ae.[Price], 
		ae.[Lifetime], 
		ae.[Settings] 
    FROM [Arsenal].[Equipments] ae
        --LEFT JOIN  ON .ID = ae.ID_ AND .[IsDeleted] = 0
    WHERE 
		ae.[IsDeleted] = 0
		AND (@Caption IS NULL OR ae.[Caption] LIKE @Caption + '%')
	ORDER BY ae.[Caption]
END
GO
--------------------------Arsenal.InsertEquipment-----------------------------
-- ������� ������ � ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertEquipment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertEquipment]
	@ID_EquipmentType uniqueidentifier, 
	@ID_Bill uniqueidentifier, 
	@ID_Vendor uniqueidentifier, 
	@Caption nvarchar(256), 
	@InvNumber nvarchar(32), 
	@VisualNumber nvarchar(32), 
	@Barcode bigint, 
	@Price money, 
	@Lifetime int, 
	@Settings nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@InvNumber nvarchar(32), 
			@VisualNumber nvarchar(32), 
			@Barcode bigint, 
			@Price money, 
			@Lifetime int, 
			@Settings nvarchar(512)
        SELECT
			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 
			@ID_Bill = '00000000-0000-0000-0000-000000000000', 
			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@InvNumber = 'test', 
			@VisualNumber = 'test', 
			@Barcode = 0, 
			@Price = 0, 
			@Lifetime = 0, 
			@Settings = 'test'
        EXEC [Arsenal].[InsertEquipment] 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@InvNumber, 
			@VisualNumber, 
			@Barcode, 
			@Price, 
			@Lifetime, 
			@Settings
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT [Arsenal].[Equipments] 
		([ID_EquipmentType], [ID_Bill], [ID_Vendor], [Caption], [InvNumber], [VisualNumber], [Barcode], [Price], [Lifetime], [Settings])
	OUTPUT INSERTED.ID INTO @outid
	SELECT @ID_EquipmentType, @ID_Bill, @ID_Vendor, @Caption, @InvNumber, @VisualNumber, @Barcode, @Price, @Lifetime, @Settings
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdateEquipment-----------------------------
-- ���������� ������ � ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdateEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdateEquipment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdateEquipment]
    @ID uniqueidentifier,
	@ID_EquipmentType uniqueidentifier, 
	@ID_Bill uniqueidentifier, 
	@ID_Vendor uniqueidentifier, 
	@Caption nvarchar(256), 
	@InvNumber nvarchar(32), 
	@VisualNumber nvarchar(32), 
	@Barcode bigint, 
	@Price money, 
	@Lifetime int, 
	@Settings nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@InvNumber nvarchar(32), 
			@VisualNumber nvarchar(32), 
			@Barcode bigint, 
			@Price money, 
			@Lifetime int, 
			@Settings nvarchar(512)
        SELECT
			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 
			@ID_Bill = '00000000-0000-0000-0000-000000000000', 
			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@InvNumber = 'test', 
			@VisualNumber = 'test', 
			@Barcode = 0, 
			@Price = 0, 
			@Lifetime = 0, 
			@Settings = 'test'
        EXEC [Arsenal].[UpdateEquipment] 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@InvNumber, 
			@VisualNumber, 
			@Barcode, 
			@Price, 
			@Lifetime, 
			@Settings
    */
    
    UPDATE [Arsenal].[Equipments]
    SET
		[ID_EquipmentType] = @ID_EquipmentType, 
		[ID_Bill] = @ID_Bill, 
		[ID_Vendor] = @ID_Vendor, 
		[Caption] = @Caption, 
		[InvNumber] = @InvNumber, 
		[VisualNumber] = @VisualNumber, 
		[Barcode] = @Barcode, 
		[Price] = @Price, 
		[Lifetime] = @Lifetime, 
		[Settings] = @Settings
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeleteEquipment-----------------------------
-- �������� ������ �� ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeleteEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeleteEquipment] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeleteEquipment]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeleteEquipment] @ID
    */
    
    UPDATE [Arsenal].[Equipments]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.EquipmentTypes-----------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectEquipmentType-----------------------------
-- ������� �� ������� Arsenal.EquipmentTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectEquipmentType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectEquipmentType] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectEquipmentType]
	@Name nvarchar(32) = NULL, 
	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = NULL, 
			@Caption = NULL
        EXEC [Arsenal].[SelectEquipmentType]
			@Name, 
			@Caption
    */

    SELECT
		aet.ID,
		aet.[Name], 
		aet.[Caption] 
    FROM [Arsenal].[EquipmentTypes] aet
        --LEFT JOIN  ON .ID = aet.ID_ AND .[IsDeleted] = 0
    WHERE 
		aet.[IsDeleted] = 0
		AND (@Caption IS NULL OR aet.[Caption] LIKE @Caption + '%')
		AND (@Name IS NULL OR aet.[Name] LIKE @Name + '%')
	ORDER BY aet.[Caption]
END
GO
--------------------------Arsenal.InsertEquipmentType-----------------------------
-- ������� ������ � ������� Arsenal.EquipmentTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertEquipmentType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertEquipmentType] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertEquipmentType]
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Arsenal].[InsertEquipmentType] 
			@Name, 
			@Caption
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[EquipmentTypes] 
		([Name], [Caption])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Name, @Caption)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdateEquipmentType-----------------------------
-- ���������� ������ � ������� Arsenal.EquipmentTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdateEquipmentType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdateEquipmentType] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdateEquipmentType]
    @ID uniqueidentifier,
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Arsenal].[UpdateEquipmentType] 
			@Name, 
			@Caption
    */
    
    UPDATE [Arsenal].[EquipmentTypes]
    SET
		[Name] = @Name, 
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeleteEquipmentType-----------------------------
-- �������� ������ �� ������� Arsenal.EquipmentTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeleteEquipmentType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeleteEquipmentType] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeleteEquipmentType]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeleteEquipmentType] @ID
    */
    
    UPDATE [Arsenal].[EquipmentTypes]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.Inventarisations---------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectInventarisation-----------------------------
-- ������� �� ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectInventarisation]
	@ID_Worker uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL, 
	@BeginDate date = NULL, 
	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Worker uniqueidentifier, 
			@Caption nvarchar(256), 
			@BeginDate date, 
			@EndDate date
        SELECT
			@ID_Worker = NULL, 
			@Caption = NULL, 
			@BeginDate = NULL, 
			@EndDate = NULL
        EXEC [Arsenal].[SelectInventarisation]
			@ID_Worker, 
			@Caption, 
			@BeginDate, 
			@EndDate
    */

    SELECT
		ai.ID,
		ai.[ID_Worker], 
		ai.[Caption], 
		ai.[BeginDate], 
		ai.[EndDate] 
    FROM [Arsenal].[Inventarisations] ai
        --LEFT JOIN  ON .ID = ai.ID_ AND .[IsDeleted] = 0
    WHERE 
		ai.[IsDeleted] = 0
		AND (@Caption IS NULL OR ai.[Caption] LIKE @Caption + '%')
	ORDER BY ai.[Caption]
END
GO
-- ������� ������ � ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertInventarisation]
	@ID_Worker uniqueidentifier, 
	@Caption nvarchar(256), 
	@BeginDate date = NULL, 
	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Worker uniqueidentifier, 
			@Caption nvarchar(256), 
			@BeginDate date, 
			@EndDate date
        SELECT
			@ID_Worker = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@BeginDate = 'test', 
			@EndDate = 'test'
        EXEC [Arsenal].[InsertInventarisation] 
			@ID_Worker, 
			@Caption, 
			@BeginDate, 
			@EndDate
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Inventarisations] 
		([ID_Worker], [Caption], [BeginDate], [EndDate])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Worker, @Caption, @BeginDate, @EndDate)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdateInventarisation-----------------------------
-- ���������� ������ � ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdateInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdateInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdateInventarisation]
    @ID uniqueidentifier,
	@ID_Worker uniqueidentifier, 
	@Caption nvarchar(256), 
	@BeginDate date = NULL, 
	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Worker uniqueidentifier, 
			@Caption nvarchar(256), 
			@BeginDate date, 
			@EndDate date
        SELECT
			@ID_Worker = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@BeginDate = 'test', 
			@EndDate = 'test'
        EXEC [Arsenal].[UpdateInventarisation] 
			@ID_Worker, 
			@Caption, 
			@BeginDate, 
			@EndDate
    */
    
    UPDATE [Arsenal].[Inventarisations]
    SET
		[ID_Worker] = @ID_Worker, 
		[Caption] = @Caption, 
		[BeginDate] = @BeginDate, 
		[EndDate] = @EndDate
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeleteInventarisation-----------------------------
-- �������� ������ �� ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeleteInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeleteInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeleteInventarisation]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeleteInventarisation] @ID
    */
    
    UPDATE [Arsenal].[Inventarisations]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.Movings------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectMoving-----------------------------
-- ������� �� ������� Arsenal.Movings
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectMoving]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectMoving] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectMoving]
	@ID_Workplace uniqueidentifier = NULL, 
	@ID_MovingType uniqueidentifier = NULL, 
	@ID_Equipment uniqueidentifier = NULL, 
	@ID_Part uniqueidentifier = NULL, 
	@ID_Person uniqueidentifier = NULL, 
	@Date datetime = NULL, 
	@Note nvarchar(512) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Workplace uniqueidentifier, 
			@ID_MovingType uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_Part uniqueidentifier, 
			@ID_Person uniqueidentifier, 
			@Date datetime, 
			@Note nvarchar(512)
        SELECT
			@ID_Workplace = NULL, 
			@ID_MovingType = NULL, 
			@ID_Equipment = NULL, 
			@ID_Part = NULL, 
			@ID_Person = NULL, 
			@Date = NULL, 
			@Note = NULL
        EXEC [Arsenal].[SelectMoving]
			@ID_Workplace, 
			@ID_MovingType, 
			@ID_Equipment, 
			@ID_Part, 
			@ID_Person, 
			@Date, 
			@Note
    */

    SELECT
		am.ID,
		am.[ID_Workplace], 
		am.[ID_MovingType], 
		am.[ID_Equipment], 
		am.[ID_Part], 
		am.[ID_Person], 
		am.[Date], 
		am.[Note] 
    FROM [Arsenal].[Movings] am
        --LEFT JOIN  ON .ID = am.ID_ AND .[IsDeleted] = 0
    WHERE 
		am.[IsDeleted] = 0

END
GO
--------------------------Arsenal.InsertMoving-----------------------------
-- ������� ������ � ������� Arsenal.Movings
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertMoving]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertMoving] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertMoving]
	@ID_Workplace uniqueidentifier, 
	@ID_MovingType uniqueidentifier, 
	@ID_Equipment uniqueidentifier, 
	@ID_Part uniqueidentifier, 
	@ID_Person uniqueidentifier, 
	@Date datetime, 
	@Note nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Workplace uniqueidentifier, 
			@ID_MovingType uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_Part uniqueidentifier, 
			@ID_Person uniqueidentifier, 
			@Date datetime, 
			@Note nvarchar(512)
        SELECT
			@ID_Workplace = '00000000-0000-0000-0000-000000000000', 
			@ID_MovingType = '00000000-0000-0000-0000-000000000000', 
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 
			@ID_Part = '00000000-0000-0000-0000-000000000000', 
			@ID_Person = '00000000-0000-0000-0000-000000000000', 
			@Date = GETDATE(), 
			@Note = 'test'
        EXEC [Arsenal].[InsertMoving] 
			@ID_Workplace, 
			@ID_MovingType, 
			@ID_Equipment, 
			@ID_Part, 
			@ID_Person, 
			@Date, 
			@Note
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Movings] 
		([ID_Workplace], [ID_MovingType], [ID_Equipment], [ID_Part], [ID_Person], [Date], [Note])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Workplace, @ID_MovingType, @ID_Equipment, @ID_Part, @ID_Person, @Date, @Note)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdateMoving-----------------------------
-- ���������� ������ � ������� Arsenal.Movings
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdateMoving]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdateMoving] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdateMoving]
    @ID uniqueidentifier,
	@ID_Workplace uniqueidentifier, 
	@ID_MovingType uniqueidentifier, 
	@ID_Equipment uniqueidentifier, 
	@ID_Part uniqueidentifier, 
	@ID_Person uniqueidentifier, 
	@Date datetime, 
	@Note nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Workplace uniqueidentifier, 
			@ID_MovingType uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_Part uniqueidentifier, 
			@ID_Person uniqueidentifier, 
			@Date datetime, 
			@Note nvarchar(512)
        SELECT
			@ID_Workplace = '00000000-0000-0000-0000-000000000000', 
			@ID_MovingType = '00000000-0000-0000-0000-000000000000', 
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 
			@ID_Part = '00000000-0000-0000-0000-000000000000', 
			@ID_Person = '00000000-0000-0000-0000-000000000000', 
			@Date = GETDATE(), 
			@Note = 'test'
        EXEC [Arsenal].[UpdateMoving] 
			@ID_Workplace, 
			@ID_MovingType, 
			@ID_Equipment, 
			@ID_Part, 
			@ID_Person, 
			@Date, 
			@Note
    */
    
    UPDATE [Arsenal].[Movings]
    SET
		[ID_Workplace] = @ID_Workplace, 
		[ID_MovingType] = @ID_MovingType, 
		[ID_Equipment] = @ID_Equipment, 
		[ID_Part] = @ID_Part, 
		[ID_Person] = @ID_Person, 
		[Date] = @Date, 
		[Note] = @Note
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeleteMoving-----------------------------
-- �������� ������ �� ������� Arsenal.Movings
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeleteMoving]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeleteMoving] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeleteMoving]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeleteMoving] @ID
    */
    
    UPDATE [Arsenal].[Movings]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.MovingTypes--------------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectMovingType-----------------------------
-- ������� �� ������� Arsenal.MovingTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectMovingType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectMovingType] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectMovingType]
	@Name nvarchar(32) = NULL, 
	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = NULL, 
			@Caption = NULL
        EXEC [Arsenal].[SelectMovingType]
			@Name, 
			@Caption
    */

    SELECT
		amt.ID,
		amt.[Name], 
		amt.[Caption] 
    FROM [Arsenal].[MovingTypes] amt
        --LEFT JOIN  ON .ID = amt.ID_ AND .[IsDeleted] = 0
    WHERE 
		amt.[IsDeleted] = 0
		AND (@Caption IS NULL OR amt.[Caption] LIKE @Caption + '%')
		AND (@Name IS NULL OR amt.[Name] LIKE @Name + '%')
	ORDER BY amt.[Caption]
END
GO
--------------------------Arsenal.InsertMovingType-----------------------------
-- ������� ������ � ������� Arsenal.MovingTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertMovingType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertMovingType] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertMovingType]
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Arsenal].[InsertMovingType] 
			@Name, 
			@Caption
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[MovingTypes] 
		([Name], [Caption])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Name, @Caption)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdateMovingType-----------------------------
-- ���������� ������ � ������� Arsenal.MovingTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdateMovingType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdateMovingType] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdateMovingType]
    @ID uniqueidentifier,
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Arsenal].[UpdateMovingType] 
			@Name, 
			@Caption
    */
    
    UPDATE [Arsenal].[MovingTypes]
    SET
		[Name] = @Name, 
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeleteMovingType-----------------------------
-- �������� ������ �� ������� Arsenal.MovingTypes
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeleteMovingType]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeleteMovingType] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeleteMovingType]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeleteMovingType] @ID
    */
    
    UPDATE [Arsenal].[MovingTypes]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.Parts--------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectPart-----------------------------
-- ������� �� ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectPart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectPart] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectPart]
	@ID_Equipment uniqueidentifier = NULL, 
	@ID_EquipmentType uniqueidentifier = NULL, 
	@ID_Bill uniqueidentifier = NULL, 
	@ID_Vendor uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL, 
	@VisualNumber nvarchar(32) = NULL, 
	@Price money = NULL, 
	@Settings nvarchar(512) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Equipment uniqueidentifier, 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@VisualNumber nvarchar(32), 
			@Price money, 
			@Settings nvarchar(512)
        SELECT
			@ID_Equipment = NULL, 
			@ID_EquipmentType = NULL, 
			@ID_Bill = NULL, 
			@ID_Vendor = NULL, 
			@Caption = NULL, 
			@VisualNumber = NULL, 
			@Price = NULL, 
			@Settings = NULL
        EXEC [Arsenal].[SelectPart]
			@ID_Equipment, 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@VisualNumber, 
			@Price, 
			@Settings
    */

    SELECT
		ap.ID,
		ap.[ID_Equipment], 
		ap.[ID_EquipmentType], 
		ap.[ID_Bill], 
		ap.[ID_Vendor], 
		ap.[Caption], 
		ap.[VisualNumber], 
		ap.[Price], 
		ap.[Settings] 
    FROM [Arsenal].[Parts] ap
        --LEFT JOIN  ON .ID = ap.ID_ AND .[IsDeleted] = 0
    WHERE 
		ap.[IsDeleted] = 0
		AND (@Caption IS NULL OR ap.[Caption] LIKE @Caption + '%')
	ORDER BY ap.[Caption]
END
GO
--------------------------Arsenal.InsertPart-----------------------------
-- ������� ������ � ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertPart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertPart] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertPart]
	@ID_Equipment uniqueidentifier, 
	@ID_EquipmentType uniqueidentifier, 
	@ID_Bill uniqueidentifier, 
	@ID_Vendor uniqueidentifier, 
	@Caption nvarchar(256), 
	@VisualNumber nvarchar(32), 
	@Price money, 
	@Settings nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Equipment uniqueidentifier, 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@VisualNumber nvarchar(32), 
			@Price money, 
			@Settings nvarchar(512)
        SELECT
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 
			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 
			@ID_Bill = '00000000-0000-0000-0000-000000000000', 
			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@VisualNumber = 'test', 
			@Price = 0, 
			@Settings = 'test'
        EXEC [Arsenal].[InsertPart] 
			@ID_Equipment, 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@VisualNumber, 
			@Price, 
			@Settings
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT [Arsenal].[Parts] 
		([ID_Equipment], [ID_EquipmentType], [ID_Bill], [ID_Vendor], [Caption], [VisualNumber], [Price], [Settings])
	OUTPUT INSERTED.ID INTO @outid
	SELECT @ID_Equipment, @ID_EquipmentType, @ID_Bill, @ID_Vendor, @Caption, @VisualNumber, @Price, @Settings
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdatePart-----------------------------
-- ���������� ������ � ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdatePart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdatePart] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdatePart]
    @ID uniqueidentifier,
	@ID_Equipment uniqueidentifier, 
	@ID_EquipmentType uniqueidentifier, 
	@ID_Bill uniqueidentifier, 
	@ID_Vendor uniqueidentifier, 
	@Caption nvarchar(256), 
	@VisualNumber nvarchar(32), 
	@Price money, 
	@Settings nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Equipment uniqueidentifier, 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@VisualNumber nvarchar(32), 
			@Price money, 
			@Settings nvarchar(512)
        SELECT
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 
			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 
			@ID_Bill = '00000000-0000-0000-0000-000000000000', 
			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@VisualNumber = 'test', 
			@Price = 0, 
			@Settings = 'test'
        EXEC [Arsenal].[UpdatePart] 
			@ID_Equipment, 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@VisualNumber, 
			@Price, 
			@Settings
    */
    
    UPDATE [Arsenal].[Parts]
    SET
		[ID_Equipment] = @ID_Equipment, 
		[ID_EquipmentType] = @ID_EquipmentType, 
		[ID_Bill] = @ID_Bill, 
		[ID_Vendor] = @ID_Vendor, 
		[Caption] = @Caption, 
		[VisualNumber] = @VisualNumber, 
		[Price] = @Price, 
		[Settings] = @Settings
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeletePart-----------------------------
-- �������� ������ �� ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeletePart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeletePart] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeletePart]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeletePart] @ID
    */
    
    UPDATE [Arsenal].[Parts]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.Results------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectResult-----------------------------
-- ������� �� ������� Arsenal.Results
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectResult]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectResult] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectResult]
	@ID_Inventarisation uniqueidentifier = NULL, 
	@ID_Equipment uniqueidentifier = NULL, 
	@ID_WorkPlace uniqueidentifier = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Inventarisation uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_WorkPlace uniqueidentifier
        SELECT
			@ID_Inventarisation = NULL, 
			@ID_Equipment = NULL, 
			@ID_WorkPlace = NULL
        EXEC [Arsenal].[SelectResult]
			@ID_Inventarisation, 
			@ID_Equipment, 
			@ID_WorkPlace
    */

    SELECT
		ar.ID,
		ar.[ID_Inventarisation], 
		ar.[ID_Equipment], 
		ar.[ID_WorkPlace] 
    FROM [Arsenal].[Results] ar
        --LEFT JOIN  ON .ID = ar.ID_ AND .[IsDeleted] = 0
    WHERE 
		ar.[IsDeleted] = 0

END
GO
--------------------------Arsenal.InsertResult-----------------------------
-- ������� ������ � ������� Arsenal.Results
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertResult]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertResult] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertResult]
	@ID_Inventarisation uniqueidentifier, 
	@ID_Equipment uniqueidentifier, 
	@ID_WorkPlace uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Inventarisation uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_WorkPlace uniqueidentifier
        SELECT
			@ID_Inventarisation = '00000000-0000-0000-0000-000000000000', 
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 
			@ID_WorkPlace = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[InsertResult] 
			@ID_Inventarisation, 
			@ID_Equipment, 
			@ID_WorkPlace
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Results] 
		([ID_Inventarisation], [ID_Equipment], [ID_WorkPlace])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Inventarisation, @ID_Equipment, @ID_WorkPlace)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdateResult-----------------------------
-- ���������� ������ � ������� Arsenal.Results
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdateResult]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdateResult] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdateResult]
    @ID uniqueidentifier,
	@ID_Inventarisation uniqueidentifier, 
	@ID_Equipment uniqueidentifier, 
	@ID_WorkPlace uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Inventarisation uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_WorkPlace uniqueidentifier
        SELECT
			@ID_Inventarisation = '00000000-0000-0000-0000-000000000000', 
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 
			@ID_WorkPlace = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[UpdateResult] 
			@ID_Inventarisation, 
			@ID_Equipment, 
			@ID_WorkPlace
    */
    
    UPDATE [Arsenal].[Results]
    SET
		[ID_Inventarisation] = @ID_Inventarisation, 
		[ID_Equipment] = @ID_Equipment, 
		[ID_WorkPlace] = @ID_WorkPlace
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeleteResult-----------------------------
-- �������� ������ �� ������� Arsenal.Results
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeleteResult]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeleteResult] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeleteResult]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeleteResult] @ID
    */
    
    UPDATE [Arsenal].[Results]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Arsenal.Vendors------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Arsenal.SelectVendor-----------------------------
-- ������� �� ������� Arsenal.Vendors
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectVendor]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectVendor] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectVendor]
	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Caption nvarchar(256)
        SELECT
			@Caption = NULL
        EXEC [Arsenal].[SelectVendor]
			@Caption
    */

    SELECT
		av.ID,
		av.[Caption] 
    FROM [Arsenal].[Vendors] av
        --LEFT JOIN  ON .ID = av.ID_ AND .[IsDeleted] = 0
    WHERE 
		av.[IsDeleted] = 0
		AND (@Caption IS NULL OR av.[Caption] LIKE @Caption + '%')
	ORDER BY av.[Caption]
END
GO
--------------------------Arsenal.InsertVendor-----------------------------
-- ������� ������ � ������� Arsenal.Vendors
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertVendor]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertVendor] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertVendor]
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Caption nvarchar(256)
        SELECT
			@Caption = 'test'
        EXEC [Arsenal].[InsertVendor] 
			@Caption
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Vendors] 
		([Caption])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Caption)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Arsenal.UpdateVendor-----------------------------
-- ���������� ������ � ������� Arsenal.Vendors
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[UpdateVendor]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[UpdateVendor] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[UpdateVendor]
    @ID uniqueidentifier,
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Caption nvarchar(256)
        SELECT
			@Caption = 'test'
        EXEC [Arsenal].[UpdateVendor] 
			@Caption
    */
    
    UPDATE [Arsenal].[Vendors]
    SET
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Arsenal.DeleteVendor-----------------------------
-- �������� ������ �� ������� Arsenal.Vendors
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[DeleteVendor]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DeleteVendor] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Arsenal].[DeleteVendor]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Arsenal].[DeleteVendor] @ID
    */
    
    UPDATE [Arsenal].[Vendors]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.Bills---------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectBill-----------------------------
-- ������� �� ������� Office.Bills
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectBill]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectBill] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectBill]
	@ID_Buyer uniqueidentifier = NULL, 
	@ID_Seller uniqueidentifier = NULL, 
	@Date date = NULL, 
	@Invoice nvarchar(64) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Buyer uniqueidentifier, 
			@ID_Seller uniqueidentifier, 
			@Date date, 
			@Invoice nvarchar(64)
        SELECT
			@ID_Buyer = NULL, 
			@ID_Seller = NULL, 
			@Date = NULL, 
			@Invoice = NULL
        EXEC [Office].[SelectBill]
			@ID_Buyer, 
			@ID_Seller, 
			@Date, 
			@Invoice
    */

    SELECT
		ob.ID,
		ob.[ID_Buyer], 
		ob.[ID_Seller], 
		ob.[Date], 
		ob.[Invoice] 
    FROM [Office].[Bills] ob
        --LEFT JOIN  ON .ID = ob.ID_ AND .[IsDeleted] = 0
    WHERE 
		ob.[IsDeleted] = 0

END
GO
--------------------------Office.InsertBill-----------------------------
-- ������� ������ � ������� Office.Bills
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertBill]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertBill] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertBill]
	@ID_Buyer uniqueidentifier, 
	@ID_Seller uniqueidentifier, 
	@Date date, 
	@Invoice nvarchar(64)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Buyer uniqueidentifier, 
			@ID_Seller uniqueidentifier, 
			@Date date, 
			@Invoice nvarchar(64)
        SELECT
			@ID_Buyer = '00000000-0000-0000-0000-000000000000', 
			@ID_Seller = '00000000-0000-0000-0000-000000000000', 
			@Date = 'test', 
			@Invoice = 'test'
        EXEC [Office].[InsertBill] 
			@ID_Buyer, 
			@ID_Seller, 
			@Date, 
			@Invoice
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Bills] 
		([ID_Buyer], [ID_Seller], [Date], [Invoice])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Buyer, @ID_Seller, @Date, @Invoice)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateBill-----------------------------
-- ���������� ������ � ������� Office.Bills
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateBill]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateBill] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateBill]
    @ID uniqueidentifier,
	@ID_Buyer uniqueidentifier, 
	@ID_Seller uniqueidentifier, 
	@Date date, 
	@Invoice nvarchar(64)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Buyer uniqueidentifier, 
			@ID_Seller uniqueidentifier, 
			@Date date, 
			@Invoice nvarchar(64)
        SELECT
			@ID_Buyer = '00000000-0000-0000-0000-000000000000', 
			@ID_Seller = '00000000-0000-0000-0000-000000000000', 
			@Date = 'test', 
			@Invoice = 'test'
        EXEC [Office].[UpdateBill] 
			@ID_Buyer, 
			@ID_Seller, 
			@Date, 
			@Invoice
    */
    
    UPDATE [Office].[Bills]
    SET
		[ID_Buyer] = @ID_Buyer, 
		[ID_Seller] = @ID_Seller, 
		[Date] = @Date, 
		[Invoice] = @Invoice
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeleteBill-----------------------------
-- �������� ������ �� ������� Office.Bills
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeleteBill]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeleteBill] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeleteBill]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeleteBill] @ID
    */
    
    UPDATE [Office].[Bills]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.Departments---------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectDepartment-----------------------------
-- ������� �� ������� Office.Departments
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectDepartment]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectDepartment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectDepartment]
	@ID_Organisation uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Organisation uniqueidentifier, 
			@Caption nvarchar(256)
        SELECT
			@ID_Organisation = NULL, 
			@Caption = NULL
        EXEC [Office].[SelectDepartment]
			@ID_Organisation, 
			@Caption
    */

    SELECT
		od.ID,
		od.[ID_Organisation], 
		od.[Caption] 
    FROM [Office].[Departments] od
        --LEFT JOIN  ON .ID = od.ID_ AND .[IsDeleted] = 0
    WHERE 
		od.[IsDeleted] = 0
		AND (@Caption IS NULL OR od.[Caption] LIKE @Caption + '%')
	ORDER BY od.[Caption]
END
GO
--------------------------Office.InsertDepartment-----------------------------
-- ������� ������ � ������� Office.Departments
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertDepartment]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertDepartment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertDepartment]
	@ID_Organisation uniqueidentifier, 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Organisation uniqueidentifier, 
			@Caption nvarchar(256)
        SELECT
			@ID_Organisation = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test'
        EXEC [Office].[InsertDepartment] 
			@ID_Organisation, 
			@Caption
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Departments] 
		([ID_Organisation], [Caption])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Organisation, @Caption)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateDepartment-----------------------------
-- ���������� ������ � ������� Office.Departments
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateDepartment]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateDepartment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateDepartment]
    @ID uniqueidentifier,
	@ID_Organisation uniqueidentifier, 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Organisation uniqueidentifier, 
			@Caption nvarchar(256)
        SELECT
			@ID_Organisation = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test'
        EXEC [Office].[UpdateDepartment] 
			@ID_Organisation, 
			@Caption
    */
    
    UPDATE [Office].[Departments]
    SET
		[ID_Organisation] = @ID_Organisation, 
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeleteDepartment-----------------------------
-- �������� ������ �� ������� Office.Departments
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeleteDepartment]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeleteDepartment] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeleteDepartment]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeleteDepartment] @ID
    */
    
    UPDATE [Office].[Departments]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.JobOperations-------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectJobOperation-----------------------------
-- ������� �� ������� Office.JobOperations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectJobOperation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectJobOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectJobOperation]
	@ID_Job uniqueidentifier = NULL, 
	@ID_Operation uniqueidentifier = NULL, 
	@BeginDate date = NULL, 
	@EndDate date = NULL, 
	@Managable bit = NULL, 
	@Personal bit = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Job uniqueidentifier, 
			@ID_Operation uniqueidentifier, 
			@BeginDate date, 
			@EndDate date, 
			@Managable bit, 
			@Personal bit
        SELECT
			@ID_Job = NULL, 
			@ID_Operation = NULL, 
			@BeginDate = NULL, 
			@EndDate = NULL, 
			@Managable = NULL, 
			@Personal = NULL
        EXEC [Office].[SelectJobOperation]
			@ID_Job, 
			@ID_Operation, 
			@BeginDate, 
			@EndDate, 
			@Managable, 
			@Personal
    */

    SELECT
		ojo.ID,
		ojo.[ID_Job], 
		ojo.[ID_Operation], 
		ojo.[BeginDate], 
		ojo.[EndDate], 
		ojo.[Managable], 
		ojo.[Personal] 
    FROM [Office].[JobOperations] ojo
        --LEFT JOIN  ON .ID = ojo.ID_ AND .[IsDeleted] = 0
    WHERE 
		ojo.[IsDeleted] = 0

END
GO
--------------------------Office.InsertJobOperation-----------------------------
-- ������� ������ � ������� Office.JobOperations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertJobOperation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertJobOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertJobOperation]
	@ID_Job uniqueidentifier, 
	@ID_Operation uniqueidentifier, 
	@BeginDate date, 
	@EndDate date, 
	@Managable bit, 
	@Personal bit
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Job uniqueidentifier, 
			@ID_Operation uniqueidentifier, 
			@BeginDate date, 
			@EndDate date, 
			@Managable bit, 
			@Personal bit
        SELECT
			@ID_Job = '00000000-0000-0000-0000-000000000000', 
			@ID_Operation = '00000000-0000-0000-0000-000000000000', 
			@BeginDate = 'test', 
			@EndDate = 'test', 
			@Managable = 0, 
			@Personal = 0
        EXEC [Office].[InsertJobOperation] 
			@ID_Job, 
			@ID_Operation, 
			@BeginDate, 
			@EndDate, 
			@Managable, 
			@Personal
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[JobOperations] 
		([ID_Job], [ID_Operation], [BeginDate], [EndDate], [Managable], [Personal])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Job, @ID_Operation, @BeginDate, @EndDate, @Managable, @Personal)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateJobOperation-----------------------------
-- ���������� ������ � ������� Office.JobOperations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateJobOperation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateJobOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateJobOperation]
    @ID uniqueidentifier,
	@ID_Job uniqueidentifier, 
	@ID_Operation uniqueidentifier, 
	@BeginDate date, 
	@EndDate date, 
	@Managable bit, 
	@Personal bit
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Job uniqueidentifier, 
			@ID_Operation uniqueidentifier, 
			@BeginDate date, 
			@EndDate date, 
			@Managable bit, 
			@Personal bit
        SELECT
			@ID_Job = '00000000-0000-0000-0000-000000000000', 
			@ID_Operation = '00000000-0000-0000-0000-000000000000', 
			@BeginDate = 'test', 
			@EndDate = 'test', 
			@Managable = 0, 
			@Personal = 0
        EXEC [Office].[UpdateJobOperation] 
			@ID_Job, 
			@ID_Operation, 
			@BeginDate, 
			@EndDate, 
			@Managable, 
			@Personal
    */
    
    UPDATE [Office].[JobOperations]
    SET
		[ID_Job] = @ID_Job, 
		[ID_Operation] = @ID_Operation, 
		[BeginDate] = @BeginDate, 
		[EndDate] = @EndDate, 
		[Managable] = @Managable, 
		[Personal] = @Personal
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeleteJobOperation-----------------------------
-- �������� ������ �� ������� Office.JobOperations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeleteJobOperation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeleteJobOperation] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeleteJobOperation]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeleteJobOperation] @ID
    */
    
    UPDATE [Office].[JobOperations]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.Jobs----------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectJob-----------------------------
-- ������� �� ������� Office.Jobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectJob]
	@Name nvarchar(32) = NULL, 
	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = NULL, 
			@Caption = NULL
        EXEC [Office].[SelectJob]
			@Name, 
			@Caption
    */

    SELECT
		oj.ID,
		oj.[Name], 
		oj.[Caption] 
    FROM [Office].[Jobs] oj
        --LEFT JOIN  ON .ID = oj.ID_ AND .[IsDeleted] = 0
    WHERE 
		oj.[IsDeleted] = 0
		AND (@Caption IS NULL OR oj.[Caption] LIKE @Caption + '%')
		AND (@Name IS NULL OR oj.[Name] LIKE @Name + '%')
	ORDER BY oj.[Caption]
END
GO
--------------------------Office.InsertJob-----------------------------
-- ������� ������ � ������� Office.Jobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertJob]
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Office].[InsertJob] 
			@Name, 
			@Caption
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Jobs] 
		([Name], [Caption])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Name, @Caption)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateJob-----------------------------
-- ���������� ������ � ������� Office.Jobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateJob]
    @ID uniqueidentifier,
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Office].[UpdateJob] 
			@Name, 
			@Caption
    */
    
    UPDATE [Office].[Jobs]
    SET
		[Name] = @Name, 
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeleteJob-----------------------------
-- �������� ������ �� ������� Office.Jobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeleteJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeleteJob] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeleteJob]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeleteJob] @ID
    */
    
    UPDATE [Office].[Jobs]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.Organisations-------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectOrganisation-----------------------------
-- ������� �� ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectOrganisation]
	@Caption nvarchar(256) = NULL, 
	@Address nvarchar(256) = NULL, 
	@Phone nvarchar(64) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Caption nvarchar(256), 
			@Address nvarchar(256), 
			@Phone nvarchar(64)
        SELECT
			@Caption = NULL, 
			@Address = NULL, 
			@Phone = NULL
        EXEC [Office].[SelectOrganisation]
			@Caption, 
			@Address, 
			@Phone
    */

    SELECT
		oo.ID,
		oo.[Caption], 
		oo.[Address], 
		oo.[Phone] 
    FROM [Office].[Organisations] oo
        --LEFT JOIN  ON .ID = oo.ID_ AND .[IsDeleted] = 0
    WHERE 
		oo.[IsDeleted] = 0
		AND (@Caption IS NULL OR oo.[Caption] LIKE @Caption + '%')
	ORDER BY oo.[Caption]
END
GO
--------------------------Office.InsertOrganisation-----------------------------
-- ������� ������ � ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertOrganisation]
	@Caption nvarchar(256), 
	@Address nvarchar(256), 
	@Phone nvarchar(64)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Caption nvarchar(256), 
			@Address nvarchar(256), 
			@Phone nvarchar(64)
        SELECT
			@Caption = 'test', 
			@Address = 'test', 
			@Phone = 'test'
        EXEC [Office].[InsertOrganisation] 
			@Caption, 
			@Address, 
			@Phone
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Organisations] 
		([Caption], [Address], [Phone])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Caption, @Address, @Phone)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateOrganisation-----------------------------
-- ���������� ������ � ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateOrganisation]
    @ID uniqueidentifier,
	@Caption nvarchar(256), 
	@Address nvarchar(256), 
	@Phone nvarchar(64)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Caption nvarchar(256), 
			@Address nvarchar(256), 
			@Phone nvarchar(64)
        SELECT
			@Caption = 'test', 
			@Address = 'test', 
			@Phone = 'test'
        EXEC [Office].[UpdateOrganisation] 
			@Caption, 
			@Address, 
			@Phone
    */
    
    UPDATE [Office].[Organisations]
    SET
		[Caption] = @Caption, 
		[Address] = @Address, 
		[Phone] = @Phone
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeleteOrganisation-----------------------------
-- �������� ������ �� ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeleteOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeleteOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeleteOrganisation]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeleteOrganisation] @ID
    */
    
    UPDATE [Office].[Organisations]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.PersonJobs----------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectPersonJob-----------------------------
-- ������� �� ������� Office.PersonJobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectPersonJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectPersonJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectPersonJob]
	@ID_Person uniqueidentifier = NULL, 
	@ID_Job uniqueidentifier = NULL, 
	@BeginDate date = NULL, 
	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Person uniqueidentifier, 
			@ID_Job uniqueidentifier, 
			@BeginDate date, 
			@EndDate date
        SELECT
			@ID_Person = NULL, 
			@ID_Job = NULL, 
			@BeginDate = NULL, 
			@EndDate = NULL
        EXEC [Office].[SelectPersonJob]
			@ID_Person, 
			@ID_Job, 
			@BeginDate, 
			@EndDate
    */

    SELECT
		opj.ID,
		opj.[ID_Person], 
		opj.[ID_Job], 
		opj.[BeginDate], 
		opj.[EndDate] 
    FROM [Office].[PersonJobs] opj
        --LEFT JOIN  ON .ID = opj.ID_ AND .[IsDeleted] = 0
    WHERE 
		opj.[IsDeleted] = 0

END
GO
--------------------------Office.InsertPersonJob-----------------------------
-- ������� ������ � ������� Office.PersonJobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertPersonJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertPersonJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertPersonJob]
	@ID_Person uniqueidentifier, 
	@ID_Job uniqueidentifier, 
	@BeginDate date, 
	@EndDate date
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Person uniqueidentifier, 
			@ID_Job uniqueidentifier, 
			@BeginDate date, 
			@EndDate date
        SELECT
			@ID_Person = '00000000-0000-0000-0000-000000000000', 
			@ID_Job = '00000000-0000-0000-0000-000000000000', 
			@BeginDate = 'test', 
			@EndDate = 'test'
        EXEC [Office].[InsertPersonJob] 
			@ID_Person, 
			@ID_Job, 
			@BeginDate, 
			@EndDate
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[PersonJobs] 
		([ID_Person], [ID_Job], [BeginDate], [EndDate])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Person, @ID_Job, @BeginDate, @EndDate)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdatePersonJob-----------------------------
-- ���������� ������ � ������� Office.PersonJobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdatePersonJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdatePersonJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdatePersonJob]
    @ID uniqueidentifier,
	@ID_Person uniqueidentifier, 
	@ID_Job uniqueidentifier, 
	@BeginDate date, 
	@EndDate date
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Person uniqueidentifier, 
			@ID_Job uniqueidentifier, 
			@BeginDate date, 
			@EndDate date
        SELECT
			@ID_Person = '00000000-0000-0000-0000-000000000000', 
			@ID_Job = '00000000-0000-0000-0000-000000000000', 
			@BeginDate = 'test', 
			@EndDate = 'test'
        EXEC [Office].[UpdatePersonJob] 
			@ID_Person, 
			@ID_Job, 
			@BeginDate, 
			@EndDate
    */
    
    UPDATE [Office].[PersonJobs]
    SET
		[ID_Person] = @ID_Person, 
		[ID_Job] = @ID_Job, 
		[BeginDate] = @BeginDate, 
		[EndDate] = @EndDate
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeletePersonJob-----------------------------
-- �������� ������ �� ������� Office.PersonJobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeletePersonJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeletePersonJob] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeletePersonJob]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeletePersonJob] @ID
    */
    
    UPDATE [Office].[PersonJobs]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.Persons-------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectPerson-----------------------------
-- ������� �� ������� Office.Persons
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectPerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectPerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectPerson]
	@Login nvarchar(32) = NULL, 
	@ID_Organisation uniqueidentifier = NULL, 
	@FirstName nvarchar(32) = NULL, 
	@LastName nvarchar(32) = NULL, 
	@MiddleName nvarchar(32) = NULL, 
	@BirthDate date = NULL, 
	@Phone nvarchar(128) = NULL, 
	@EMail nvarchar(128) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Login nvarchar(32), 
			@ID_Organisation uniqueidentifier, 
			@FirstName nvarchar(32), 
			@LastName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@Login = NULL, 
			@ID_Organisation = NULL, 
			@FirstName = NULL, 
			@LastName = NULL, 
			@MiddleName = NULL, 
			@BirthDate = NULL, 
			@Phone = NULL, 
			@EMail = NULL
        EXEC [Office].[SelectPerson]
			@Login, 
			@ID_Organisation, 
			@FirstName, 
			@LastName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail
    */

    SELECT
		op.ID,
		op.[Login], 
		op.[ID_Organisation], 
		op.[FirstName], 
		op.[LastName], 
		op.[MiddleName], 
		op.[BirthDate], 
		op.[Phone], 
		op.[EMail], 
		op.[Caption] 
    FROM [Office].[Persons] op
        --LEFT JOIN  ON .ID = op.ID_ AND .[IsDeleted] = 0
    WHERE 
		op.[IsDeleted] = 0

END
GO
--------------------------Office.InsertPerson-----------------------------
-- ������� ������ � ������� Office.Persons
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertPerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertPerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertPerson]
	@Login nvarchar(32), 
	@ID_Organisation uniqueidentifier, 
	@FirstName nvarchar(32), 
	@LastName nvarchar(32), 
	@MiddleName nvarchar(32), 
	@BirthDate date, 
	@Phone nvarchar(128), 
	@EMail nvarchar(128)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Login nvarchar(32), 
			@ID_Organisation uniqueidentifier, 
			@FirstName nvarchar(32), 
			@LastName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@Login = 'test', 
			@ID_Organisation = '00000000-0000-0000-0000-000000000000', 
			@FirstName = 'test', 
			@LastName = 'test', 
			@MiddleName = 'test', 
			@BirthDate = 'test', 
			@Phone = 'test', 
			@EMail = 'test'
        EXEC [Office].[InsertPerson] 
			@Login, 
			@ID_Organisation, 
			@FirstName, 
			@LastName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Persons] 
		([Login], [ID_Organisation], [FirstName], [LastName], [MiddleName], [BirthDate], [Phone], [EMail])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Login, @ID_Organisation, @FirstName, @LastName, @MiddleName, @BirthDate, @Phone, @EMail)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdatePerson-----------------------------
-- ���������� ������ � ������� Office.Persons
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdatePerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdatePerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdatePerson]
    @ID uniqueidentifier,
	@Login nvarchar(32), 
	@ID_Organisation uniqueidentifier, 
	@FirstName nvarchar(32), 
	@LastName nvarchar(32), 
	@MiddleName nvarchar(32), 
	@BirthDate date, 
	@Phone nvarchar(128), 
	@EMail nvarchar(128)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Login nvarchar(32), 
			@ID_Organisation uniqueidentifier, 
			@FirstName nvarchar(32), 
			@LastName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@Login = 'test', 
			@ID_Organisation = '00000000-0000-0000-0000-000000000000', 
			@FirstName = 'test', 
			@LastName = 'test', 
			@MiddleName = 'test', 
			@BirthDate = 'test', 
			@Phone = 'test', 
			@EMail = 'test'
        EXEC [Office].[UpdatePerson] 
			@Login, 
			@ID_Organisation, 
			@FirstName, 
			@LastName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail
    */
    
    UPDATE [Office].[Persons]
    SET
		[Login] = @Login, 
		[ID_Organisation] = @ID_Organisation, 
		[FirstName] = @FirstName, 
		[LastName] = @LastName, 
		[MiddleName] = @MiddleName, 
		[BirthDate] = @BirthDate, 
		[Phone] = @Phone, 
		[EMail] = @EMail
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeletePerson-----------------------------
-- �������� ������ �� ������� Office.Persons
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeletePerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeletePerson] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeletePerson]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeletePerson] @ID
    */
    
    UPDATE [Office].[Persons]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.Workers-------------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectWorker-----------------------------
-- ������� �� ������� Office.Workers
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectWorker]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectWorker] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectWorker]
	@ID_Organisation uniqueidentifier = NULL, 
	@LastName nvarchar(32) = NULL, 
	@FirstName nvarchar(32) = NULL, 
	@MiddleName nvarchar(32) = NULL, 
	@Phone nvarchar(128) = NULL, 
	@EMail nvarchar(128) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Organisation uniqueidentifier, 
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@ID_Organisation = NULL, 
			@LastName = NULL, 
			@FirstName = NULL, 
			@MiddleName = NULL, 
			@Phone = NULL, 
			@EMail = NULL
        EXEC [Office].[SelectWorker]
			@ID_Organisation, 
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@Phone, 
			@EMail
    */

    SELECT
		ow.ID,
		ow.[ID_Organisation], 
		ow.[LastName], 
		ow.[FirstName], 
		ow.[MiddleName], 
		ow.[Phone], 
		ow.[EMail], 
		ow.[Caption] 
    FROM [Office].[Workers] ow
        --LEFT JOIN  ON .ID = ow.ID_ AND .[IsDeleted] = 0
    WHERE 
		ow.[IsDeleted] = 0

END
GO
--------------------------Office.InsertWorker-----------------------------
-- ������� ������ � ������� Office.Workers
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertWorker]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertWorker] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertWorker]
	@ID_Organisation uniqueidentifier, 
	@LastName nvarchar(32), 
	@FirstName nvarchar(32), 
	@MiddleName nvarchar(32), 
	@Phone nvarchar(128), 
	@EMail nvarchar(128)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Organisation uniqueidentifier, 
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@ID_Organisation = '00000000-0000-0000-0000-000000000000', 
			@LastName = 'test', 
			@FirstName = 'test', 
			@MiddleName = 'test', 
			@Phone = 'test', 
			@EMail = 'test'
        EXEC [Office].[InsertWorker] 
			@ID_Organisation, 
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@Phone, 
			@EMail
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Workers] 
		([ID_Organisation], [LastName], [FirstName], [MiddleName], [Phone], [EMail])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Organisation, @LastName, @FirstName, @MiddleName, @Phone, @EMail)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateWorker-----------------------------
-- ���������� ������ � ������� Office.Workers
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateWorker]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateWorker] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateWorker]
    @ID uniqueidentifier,
	@ID_Organisation uniqueidentifier, 
	@LastName nvarchar(32), 
	@FirstName nvarchar(32), 
	@MiddleName nvarchar(32), 
	@Phone nvarchar(128), 
	@EMail nvarchar(128)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Organisation uniqueidentifier, 
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@ID_Organisation = '00000000-0000-0000-0000-000000000000', 
			@LastName = 'test', 
			@FirstName = 'test', 
			@MiddleName = 'test', 
			@Phone = 'test', 
			@EMail = 'test'
        EXEC [Office].[UpdateWorker] 
			@ID_Organisation, 
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@Phone, 
			@EMail
    */
    
    UPDATE [Office].[Workers]
    SET
		[ID_Organisation] = @ID_Organisation, 
		[LastName] = @LastName, 
		[FirstName] = @FirstName, 
		[MiddleName] = @MiddleName, 
		[Phone] = @Phone, 
		[EMail] = @EMail
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeleteWorker-----------------------------
-- �������� ������ �� ������� Office.Workers
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeleteWorker]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeleteWorker] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeleteWorker]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeleteWorker] @ID
    */
    
    UPDATE [Office].[Workers]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO
---------------------------------------------------------------------------------------------
------------------------------------------Office.WorkPlaces----------------------------------
---------------------------------------------------------------------------------------------
--------------------------Office.SelectWorkPlace-----------------------------
-- ������� �� ������� Office.WorkPlaces
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectWorkPlace]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectWorkPlace] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectWorkPlace]
	@ID_Department uniqueidentifier = NULL, 
	@ID_Worker uniqueidentifier = NULL, 
	@Name nvarchar(32) = NULL, 
	@Caption nvarchar(256) = NULL, 
	@Cabinet nvarchar(16) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Department uniqueidentifier, 
			@ID_Worker uniqueidentifier, 
			@Name nvarchar(32), 
			@Caption nvarchar(256), 
			@Cabinet nvarchar(16)
        SELECT
			@ID_Department = NULL, 
			@ID_Worker = NULL, 
			@Name = NULL, 
			@Caption = NULL, 
			@Cabinet = NULL
        EXEC [Office].[SelectWorkPlace]
			@ID_Department, 
			@ID_Worker, 
			@Name, 
			@Caption, 
			@Cabinet
    */

    SELECT
		owp.ID,
		owp.[ID_Department], 
		owp.[ID_Worker], 
		owp.[Name], 
		owp.[Caption], 
		owp.[Cabinet] 
    FROM [Office].[WorkPlaces] owp
        --LEFT JOIN  ON .ID = owp.ID_ AND .[IsDeleted] = 0
    WHERE 
		owp.[IsDeleted] = 0
		AND (@Caption IS NULL OR owp.[Caption] LIKE @Caption + '%')
		AND (@Name IS NULL OR owp.[Name] LIKE @Name + '%')
	ORDER BY owp.[Caption]
END
GO
--------------------------Office.InsertWorkPlace-----------------------------
-- ������� ������ � ������� Office.WorkPlaces
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertWorkPlace]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertWorkPlace] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertWorkPlace]
	@ID_Department uniqueidentifier, 
	@ID_Worker uniqueidentifier, 
	@Name nvarchar(32), 
	@Caption nvarchar(256), 
	@Cabinet nvarchar(16)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Department uniqueidentifier, 
			@ID_Worker uniqueidentifier, 
			@Name nvarchar(32), 
			@Caption nvarchar(256), 
			@Cabinet nvarchar(16)
        SELECT
			@ID_Department = '00000000-0000-0000-0000-000000000000', 
			@ID_Worker = '00000000-0000-0000-0000-000000000000', 
			@Name = 'test', 
			@Caption = 'test', 
			@Cabinet = 'test'
        EXEC [Office].[InsertWorkPlace] 
			@ID_Department, 
			@ID_Worker, 
			@Name, 
			@Caption, 
			@Cabinet
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[WorkPlaces] 
		([ID_Department], [ID_Worker], [Name], [Caption], [Cabinet])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Department, @ID_Worker, @Name, @Caption, @Cabinet)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateWorkPlace-----------------------------
-- ���������� ������ � ������� Office.WorkPlaces
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateWorkPlace]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateWorkPlace] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateWorkPlace]
    @ID uniqueidentifier,
	@ID_Department uniqueidentifier, 
	@ID_Worker uniqueidentifier, 
	@Name nvarchar(32), 
	@Caption nvarchar(256), 
	@Cabinet nvarchar(16)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID_Department uniqueidentifier, 
			@ID_Worker uniqueidentifier, 
			@Name nvarchar(32), 
			@Caption nvarchar(256), 
			@Cabinet nvarchar(16)
        SELECT
			@ID_Department = '00000000-0000-0000-0000-000000000000', 
			@ID_Worker = '00000000-0000-0000-0000-000000000000', 
			@Name = 'test', 
			@Caption = 'test', 
			@Cabinet = 'test'
        EXEC [Office].[UpdateWorkPlace] 
			@ID_Department, 
			@ID_Worker, 
			@Name, 
			@Caption, 
			@Cabinet
    */
    
    UPDATE [Office].[WorkPlaces]
    SET
		[ID_Department] = @ID_Department, 
		[ID_Worker] = @ID_Worker, 
		[Name] = @Name, 
		[Caption] = @Caption, 
		[Cabinet] = @Cabinet
    WHERE ID = @ID
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO
--------------------------Office.DeleteWorkPlace-----------------------------
-- �������� ������ �� ������� Office.WorkPlaces
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeleteWorkPlace]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeleteWorkPlace] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeleteWorkPlace]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeleteWorkPlace] @ID
    */
    
    UPDATE [Office].[WorkPlaces]
    SET
	    [IsDeleted] = 1
    WHERE ID = @ID

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO

-- ������� �� ������� Office.Purposes
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectPurpose]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectPurpose] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectPurpose]
	@Name nvarchar(32) = NULL, 	@Caption nvarchar(256) = NULL, 	@Priority tinyint = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Name nvarchar(32), 			@Caption nvarchar(256), 			@Priority tinyint
        SELECT
			@Name = NULL, 			@Caption = NULL, 			@Priority = NULL
        EXEC [Office].[SelectPurpose]
			@Name, 			@Caption, 			@Priority
    */

    SELECT
		op.ID,
		op.[Name], 		op.[Caption], 		op.[Priority] 
    FROM [Office].[Purposes] op
        --LEFT JOIN  ON .ID = op.ID_ AND .[IsDeleted] = 0
    WHERE 
		op.[IsDeleted] = 0
		AND (@Caption IS NULL OR op.[Caption] LIKE @Caption + '%')
		AND (@Name IS NULL OR op.[Name] LIKE @Name + '%')
	ORDER BY op.[Caption]
END
GO

-------------------------------------------------------------------------------------------------------------------------------------------- selfmade


IF OBJECT_ID('[Office].[InsertPerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertPerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[InsertPerson]
	@Login nvarchar(32),
	@ID_Organisation uniqueidentifier,
	@LastName nvarchar(32), 
	@FirstName nvarchar(32), 
	@MiddleName nvarchar(32) = NULL, 
	@BirthDate date, 
	@Phone nvarchar(128) = NULL, 
	@EMail nvarchar(128) = NULL,
	@Password nvarchar(128)
AS
BEGIN
SET NOCOUNT ON
	/*
        DECLARE 
			@Login nvarchar(32), 
			@ID_Organisation uniqueidentifier,
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128),
			@Password nvarchar(128)
        SELECT
            @Login = 'testo',
			@ID_Organisation = '',
			@LastName = 'test',
			@FirstName = 'test',
			@MiddleName = NULL,
			@BirthDate = '1900.01.01',
			@Phone = 'test', 
			@EMail = NULL,
			@Password = 'test'
        EXEC [Office].[InsertPerson] 
			@Login,
			@ID_Organisation, 
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail,
			@Password
    */
		
	DECLARE @LoginDoubled int	
		
	SELECT @LoginDoubled = COUNT(ID)
	FROM Office.Persons
	WHERE [Login] = @Login
	
	SELECT @LoginDoubled = @LoginDoubled + COUNT(sid)
	FROM master..syslogins
	WHERE name = @Login
	
	IF @LoginDoubled > 0 
	BEGIN
		SELECT NULL AS ID
		RETURN 1
	END
	
	DECLARE 
		@ID_Inserted uniqueidentifier
	DECLARE 
		@INSERTED TABLE(ID uniqueidentifier)
		
    INSERT Office.Persons ([Login], ID_Organisation, LastName, FirstName, MiddleName, BirthDate, [Phone], [EMail])
    OUTPUT INSERTED.ID INTO @INSERTED
	VALUES (@Login,	@ID_Organisation, @LastName, @FirstName,	ISNULL(@MiddleName, ''),	@BirthDate, ISNULL(@Phone, ''), ISNULL(@EMail, ''))
	
	IF @@ERROR = 0
	BEGIN
		EXEC('CREATE LOGIN ' + @Login + ' WITH PASSWORD = ''' + @Password + ''', CHECK_POLICY=OFF')
		EXEC('CREATE USER ' + @Login + ' FOR LOGIN ' + @Login)
	END

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK
		SELECT NULL AS ID
		RETURN @@ERROR
	END
			 
	SELECT TOP 1 @ID_Inserted = ID 
	FROM @INSERTED
	
	SELECT @ID_Inserted as ID

    RETURN @@ERROR
END
GO

IF OBJECT_ID('[Office].[InsertJobOperation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertJobOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[InsertJobOperation]
	@ID_Job uniqueidentifier,
	@ID_Operation uniqueidentifier,
	@BeginDate date,
	@EndDate date = NULL,
	@Managable bit = NULL,
	@Personal bit = NULL
AS
BEGIN
SET NOCOUNT ON
	/*
        DECLARE 
            @ID_Job uniqueidentifier,
			@ID_Operation uniqueidentifier,
			@BeginDate date,
			@EndDate date,
			@Managable bit,
			@Personal bit
        SELECT
            @ID_Job = 'D009B6E1-9607-E411-B9EC-0013D4068ECA', -- SELECT * FROM Office.Jobs WHERE Name = 'testjob'
			@ID_Operation = 'B4503185-8C07-E411-B9EC-0013D4068ECA', -- SELECT * FROM Operations WHERE ProcedureName like '%SelectDepartment'
			@BeginDate = '1900.01.01',
			@EndDate = NULL,
			@Managable = NULL,
			@Personal = NULL
        EXEC [Office].[InsertJobOperation]
			@ID_Job,
			@ID_Operation,
			@BeginDate,
			@EndDate,
			@Managable,
			@Personal
    */
	
	--  check of existent pair of job and operation	
	DECLARE @JobOpExistent uniqueidentifier	
		
	SELECT @JobOpExistent = ID
	FROM Office.JobOperations
	WHERE 
		ID_Job = @ID_Job
		AND ID_Operation = @ID_Operation

	SELECT
		@Managable = ISNULL(@Managable, 1),
		@Personal = ISNULL(@Personal, 0)
	
	IF @JobOpExistent IS NOT NULL 
	BEGIN
		UPDATE Office.JobOperations
		SET [BeginDate] = @BeginDate, [EndDate] = @EndDate, [IsDeleted] = 0
		WHERE 
			ID = @JobOpExistent
		SELECT @JobOpExistent AS ID

		RETURN 0
	END	
	
	DECLARE 
		@ID_Inserted uniqueidentifier
	DECLARE 
		@INSERTED TABLE(ID uniqueidentifier)
		
    INSERT Office.JobOperations (ID_Job, ID_Operation, BeginDate, EndDate, Managable, Personal)
    OUTPUT INSERTED.ID INTO @INSERTED
	VALUES (@ID_Job, @ID_Operation,	@BeginDate,	@EndDate, @Managable, @Personal)
	
	SELECT TOP 1 @ID_Inserted = ID 
	FROM @INSERTED
	
	SELECT @ID_Inserted as ID

    RETURN @@ERROR
END
GO

IF OBJECT_ID('[Office].[InsertPersonJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertPersonJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [Office].[InsertPersonJob]
	@ID_Person uniqueidentifier,
	@ID_Job uniqueidentifier,
	@BeginDate date,
	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON
	/*
        DECLARE 
            @ID_Person uniqueidentifier,
			@ID_Job uniqueidentifier,
			@BeginDate date,
			@EndDate date
        SELECT
            @ID_Person = '94E190AB-9707-E411-B9EC-0013D4068ECA', -- SELECT * FROM Office.Persons WHERE Login = 'supa'
			@ID_Job = 'D009B6E1-9607-E411-B9EC-0013D4068ECA', -- SELECT * FROM Office.Jobs WHERE Name = 'testjob'
			@BeginDate = '1900.01.01',
			@EndDate = NULL
        EXEC [Office].[InsertPersonJob]
			@ID_Person,
			@ID_Job,
			@BeginDate,
			@EndDate
    */
	
	-- check of existent pair of job and Person	
	DECLARE @MJobExistent uniqueidentifier	
		
	SELECT @MJobExistent = ID
	FROM Office.PersonJobs
	WHERE 
		ID_Person = @ID_Person
		AND ID_Job = @ID_Job
	
	IF @MJobExistent IS NOT NULL 
	BEGIN
		UPDATE Office.PersonJobs
		SET [BeginDate] = @BeginDate, [EndDate] = @EndDate, [IsDeleted] = 0
		WHERE 
			ID = @MJobExistent
		SELECT @MJobExistent AS ID

		RETURN 0
	END	
	
	DECLARE 
		@ID_Inserted uniqueidentifier
	DECLARE 
		@INSERTED TABLE(ID uniqueidentifier)
		
    INSERT Office.PersonJobs (ID_Person, ID_Job, BeginDate, EndDate)
	OUTPUT INSERTED.ID INTO @INSERTED
	VALUES (@ID_Person, @ID_Job, @BeginDate, @EndDate)
	
	SELECT TOP 1 @ID_Inserted = ID 
	FROM @INSERTED
	
	SELECT @ID_Inserted as ID

    RETURN @@ERROR
	
END

GO


---------------------------------------------------------------------------------------------
------------------------------------------dbo.Operations-------------------------------------
---------------------------------------------------------------------------------------------
--------------------------dbo.SelectOperation-----------------------------
-- ������� �� ������� dbo.Operations
USE [Arsenal]
GO

IF OBJECT_ID('[dbo].[SelectOperation]') IS NULL
   EXEC('CREATE PROCEDURE [dbo].[SelectOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [dbo].[SelectOperation]
	@ID uniqueidentifier = NULL, 
	@ProcedureName nvarchar(64) = NULL, 
	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier, 
			@ProcedureName nvarchar(64), 
			@Caption nvarchar(256)
        SELECT
			@ID = NULL, 
			@ProcedureName = NULL, 
			@Caption = NULL
        EXEC [dbo].[SelectOperation]
			@ID, 
			@ProcedureName, 
			@Caption
    */

    SELECT
		do.[ID], 
		do.[ProcedureName], 
		do.[Caption] 
    FROM [dbo].[Operations] do
        --LEFT JOIN  ON .ID = do.ID_ AND .[IsDeleted] = 0
    WHERE 
		do.[IsDeleted] = 0
		AND ((@ProcedureName IS NULL) OR (do.ProcedureName LIKE @ProcedureName + '%'))
		AND ((@Caption IS NULL) OR (do.[Caption] LIKE @Caption + '%'))
	ORDER BY do.[Caption]
END
GO
--------------------------dbo.UpdateOperation-----------------------------
-- ���������� ������ � ������� dbo.Operations
USE [Arsenal]
GO

IF OBJECT_ID('[dbo].[UpdateOperation]') IS NULL
   EXEC('CREATE PROCEDURE [dbo].[UpdateOperation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [dbo].[UpdateOperation]
    @ID uniqueidentifier,
	@ProcedureName nvarchar(64), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@ID uniqueidentifier, 
			@ProcedureName nvarchar(64), 
			@Caption nvarchar(256)
        SELECT
			@ID = '00000000-0000-0000-0000-000000000000', 
			@ProcedureName = 'test', 
			@Caption = 'test'
        EXEC [dbo].[UpdateOperation] 
			@ID, 
			@ProcedureName, 
			@Caption
    */
    
    UPDATE [dbo].[Operations]
    SET
		[ID] = @ID, 
		--[ProcedureName] = @ProcedureName, -- must not be changed
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID as ID

    RETURN @@ERROR 

END
GO
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdatePerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdatePerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdatePerson]
    @ID uniqueidentifier,
	@Login nvarchar(32),
	@ID_Organisation uniqueidentifier,	 
	@LastName nvarchar(32), 
	@FirstName nvarchar(32), 
	@MiddleName nvarchar(32), 
	@BirthDate date, 
	@Phone nvarchar(128), 
	@EMail nvarchar(128)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Login nvarchar(32),
			@ID_Organisation uniqueidentifier,	 
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@ID uniqueidentifier,
			@Login = 'test', 
			@ID_Organisation uniqueidentifier,	
			@LastName = 'test', 
			@FirstName = 'test', 
			@MiddleName = 'test', 
			@BirthDate = 'test', 
			@Phone = 'test', 
			@EMail = 'test'
        EXEC [Office].[UpdatePerson] 
			@ID uniqueidentifier,
			@Login,
			@ID_Organisation uniqueidentifier,
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail
    */
    
    UPDATE [Office].[Persons]
    SET
		--[Login] = @Login, -- cannot be modified
		[ID_Organisation] = @ID_Organisation,	
		[LastName] = @LastName, 
		[FirstName] = @FirstName, 
		[MiddleName] = @MiddleName, 
		[BirthDate] = @BirthDate, 
		[Phone] = @Phone, 
		[EMail] = @EMail
    WHERE ID = @ID
	
    SELECT @ID as ID

    RETURN @@ERROR 

END
GO

USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectPerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectPerson] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectPerson]
	@Login nvarchar(32) = NULL, 
	@ID_Organisation uniqueidentifier = NULL,	
	@LastName nvarchar(32) = NULL, 
	@FirstName nvarchar(32) = NULL, 
	@MiddleName nvarchar(32) = NULL, 
	@BirthDate date = NULL, 
	@Phone nvarchar(128) = NULL, 
	@EMail nvarchar(128) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@Login nvarchar(32),
			@ID_Organisation uniqueidentifier,	 
			@LastName nvarchar(32), 
			@FirstName nvarchar(32), 
			@MiddleName nvarchar(32), 
			@BirthDate date, 
			@Phone nvarchar(128), 
			@EMail nvarchar(128)
        SELECT
			@Login = NULL, 
			@ID_Organisation = NULL,	
			@LastName = NULL, 
			@FirstName = NULL, 
			@MiddleName = NULL, 
			@BirthDate = NULL, 
			@Phone = NULL, 
			@EMail = NULL
        EXEC [Office].[SelectPerson]
			@Login,
			@ID_Organisation,	 
			@LastName, 
			@FirstName, 
			@MiddleName, 
			@BirthDate, 
			@Phone, 
			@EMail
    */

    SELECT
		op.ID,
		op.[Login],
		op.[ID_Organisation],	 
		op.[LastName], 
		op.[FirstName], 
		op.[MiddleName], 
		op.[BirthDate], 
		op.[Phone], 
		op.[EMail], 
		op.[Caption] 
    FROM [Office].[Persons] op
        --LEFT JOIN  ON .ID = op.ID_ AND .[IsDeleted] = 0
    WHERE 
		op.[IsDeleted] = 0

END
GO

--------------------------Office.DeletePerson-----------------------------
-- �������� ������ �� ������� Office.Persons
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[DeletePerson]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[DeletePerson] AS BEGIN SET NOCOUNT ON; END')
GO


ALTER PROC [Office].[DeletePerson]
    @ID uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier
        SELECT
            @ID = '00000000-0000-0000-0000-000000000000'
        EXEC [Office].[DeletePerson] @ID
    */
    
    DECLARE
        @Login nvarchar(32),
		@vID uniqueidentifier

	SELECT
		@vID = ID,
		@Login = [Login]
	FROM [Office].[Persons]
    WHERE 
		ID = @ID
		AND [IsDeleted] = 0
		AND [Login] <> 'Admin'

    UPDATE [Office].[Persons]
    SET
	    [IsDeleted] = 1
    WHERE ID = @vID
    
    IF @@ERROR = 0 AND @Login IS NOT NULL
    BEGIN
		EXEC('DROP USER ' + @Login)
		EXEC('DROP LOGIN ' + @Login)
    END

    IF @@ERROR <> 0
    BEGIN
		ROLLBACK
		SELECT NULL AS ID
		RETURN @@ERROR
    END

    SELECT @ID AS ID
	
    RETURN @@ERROR    

END
GO

-- ���������� ������ � ������� Office.Jobs
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateJob]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateJob] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateJob]
    @ID uniqueidentifier,
	@Name nvarchar(32), 
	@Caption nvarchar(256)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Name nvarchar(32), 
			@Caption nvarchar(256)
        SELECT
			@Name = 'test', 
			@Caption = 'test'
        EXEC [Office].[UpdateJob] 
			@Name, 
			@Caption
    */
    
    UPDATE [Office].[Jobs]
    SET
		--[Name] = @Name, 
		[Caption] = @Caption
    WHERE ID = @ID
	
    SELECT @ID as ID

    RETURN @@ERROR 

END
GO

--------------------------------------------------------------------------------------------------------------------------- SPECIFIC

-- ������� �� ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectEquipment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectEquipment]
	@ID_EquipmentType uniqueidentifier = NULL, 
	@ID_Bill uniqueidentifier = NULL, 
	@ID_Vendor uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL, 
	@InvNumber nvarchar(32) = NULL, 
	@VisualNumber nvarchar(32) = NULL, 
	@Barcode bigint = NULL, 
	@Price money = NULL, 
	@Lifetime int = NULL, 
	@Settings nvarchar(max) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@InvNumber nvarchar(32), 
			@VisualNumber nvarchar(32), 
			@Barcode bigint, 
			@Price money, 
			@Lifetime int, 
			@Settings nvarchar(max)
        SELECT
			@ID_EquipmentType = NULL, 
			@ID_Bill = NULL, 
			@ID_Vendor = NULL, 
			@Caption = NULL, 
			@InvNumber = NULL, 
			@VisualNumber = NULL, 
			@Barcode = NULL, 
			@Price = NULL, 
			@Lifetime = NULL, 
			@Settings = NULL
        EXEC [Arsenal].[SelectEquipment]
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@InvNumber, 
			@VisualNumber, 
			@Barcode, 
			@Price, 
			@Lifetime, 
			@Settings
    */

    SELECT
		ae.ID,
		ae.[ID_EquipmentType], 
		ae.[ID_Bill], 
		ae.[ID_Vendor], 
		ae.[Caption] AS Product,
		ISNULL(aet.Caption + ' ', '') + ISNULL(av.Caption + ' ', '') + ae.Caption AS Caption,
		ae.[InvNumber], 
		ae.[VisualNumber], 
		ae.[Barcode], 
		ae.[Price], 
		ae.[Lifetime], 
		ae.[Settings],
		em.ID_WorkPlace 
    FROM [Arsenal].[Equipments] ae
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
		LEFT JOIN (SELECT ID_Equipment AS ID, ID_WorkPlace FROM (
			SELECT ID_Equipment, ID_WorkPlace, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY Date DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1) em ON em.ID = ae.ID
    WHERE 
		ae.[IsDeleted] = 0
		AND (@Caption IS NULL OR ae.[Caption] LIKE @Caption + '%')
	ORDER BY ae.[Caption]
END
GO

--------------------------Arsenal.InsertEquipment-----------------------------
-- ������� ������ � ������� Arsenal.Equipments
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertEquipment]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertEquipment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertEquipment]
	@ID_EquipmentType uniqueidentifier, 
	@ID_Bill uniqueidentifier, 
	@ID_Vendor uniqueidentifier, 
	@Caption nvarchar(256), 
	@InvNumber nvarchar(32), 
	@VisualNumber nvarchar(32), 
	@Barcode bigint, 
	@Price money, 
	@Lifetime int, 
	@Settings nvarchar(max)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@InvNumber nvarchar(32), 
			@VisualNumber nvarchar(32), 
			@Barcode bigint, 
			@Price money, 
			@Lifetime int, 
			@Settings nvarchar(max)
        SELECT
			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 
			@ID_Bill = '00000000-0000-0000-0000-000000000000', 
			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 
			@Caption = 'test', 
			@InvNumber = 0, 
			@VisualNumber = 0, 
			@Barcode = 0, 
			@Price = 0, 
			@Lifetime = 0, 
			@Settings = 'test'
        EXEC [Arsenal].[InsertEquipment] 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@InvNumber, 
			@VisualNumber, 
			@Barcode, 
			@Price, 
			@Lifetime, 
			@Settings
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Equipments] 
		([ID_EquipmentType], [ID_Bill], [ID_Vendor], [Caption], [InvNumber], [VisualNumber], [Barcode], [Price], [Lifetime], [Settings])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_EquipmentType, @ID_Bill, @ID_Vendor, ISNULL(@Caption, ''), @InvNumber, @VisualNumber, @Barcode, @Price, @Lifetime, @Settings)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
-- ������� �� ������� Arsenal.Movings
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectMoving]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectMoving] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectMoving]
	@ID_Workplace uniqueidentifier = NULL, 
	@ID_MovingType uniqueidentifier = NULL, 
	@ID_Equipment uniqueidentifier = NULL, 
	@ID_Part uniqueidentifier = NULL, 
	@ID_Person uniqueidentifier = NULL, 
	@Date datetime = NULL, 
	@Note nvarchar(512) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Workplace uniqueidentifier, 
			@ID_MovingType uniqueidentifier, 
			@ID_Equipment uniqueidentifier, 
			@ID_Part uniqueidentifier, 
			@ID_Person uniqueidentifier, 
			@Date datetime, 
			@Note nvarchar(512)
        SELECT
			@ID_Workplace = NULL, 
			@ID_MovingType = NULL, 
			@ID_Equipment = NULL, 
			@ID_Part = NULL, 
			@ID_Person = NULL, 
			@Date = NULL, 
			@Note = NULL
        EXEC [Arsenal].[SelectMoving]
			@ID_Workplace, 
			@ID_MovingType, 
			@ID_Equipment, 
			@ID_Part, 
			@ID_Person, 
			@Date, 
			@Note
    */

	SELECT
		am.ID,
		am.[ID_Workplace], 
		am.[ID_MovingType], 
		am.[ID_Equipment], 
		am.[ID_Part], 
		am.[ID_Person], 
		am.[Date], 
		am.[Note] 
	FROM [Arsenal].[Movings] am
	WHERE 
		am.[IsDeleted] = 0
		AND ((@ID_Equipment IS NULL AND am.ID_Part = @ID_Part) OR (am.ID_Equipment = @ID_Equipment))
	ORDER BY [Date]
END
GO


--------------------------Office.SelectBill-----------------------------
-- ������� �� ������� Office.Bills
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectBill]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectBill] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectBill]
	@ID_Buyer uniqueidentifier = NULL, 
	@ID_Seller uniqueidentifier = NULL, 
	@Date date = NULL, 
	@Invoice nvarchar(64) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Buyer uniqueidentifier, 
			@ID_Seller uniqueidentifier, 
			@Date date, 
			@Invoice nvarchar(64)
        SELECT
			@ID_Buyer = NULL, 
			@ID_Seller = NULL, 
			@Date = NULL, 
			@Invoice = NULL
        EXEC [Office].[SelectBill]
			@ID_Buyer, 
			@ID_Seller, 
			@Date, 
			@Invoice
    */

    SELECT
		ob.ID,
		ob.[ID_Buyer], 
		ob.[ID_Seller], 
		ob.[Date], 
		ob.[Invoice],
		ob.[Invoice] + ' �� ' + CONVERT(nvarchar(10), ob.[Date], 104) AS [Caption] 
    FROM [Office].[Bills] ob
        --LEFT JOIN  ON .ID = ob.ID_ AND .[IsDeleted] = 0
    WHERE 
		ob.[IsDeleted] = 0

END
GO

-- ������� �� ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectPart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectPart] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectPart]
	@ID_Equipment uniqueidentifier = NULL, 
	@ID_EquipmentType uniqueidentifier = NULL, 
	@ID_Bill uniqueidentifier = NULL, 
	@ID_Vendor uniqueidentifier = NULL, 
	@Caption nvarchar(256) = NULL, 
	@VisualNumber nvarchar(32) = NULL, 
	@Price money = NULL, 
	@Settings nvarchar(512) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Equipment uniqueidentifier, 
			@ID_EquipmentType uniqueidentifier, 
			@ID_Bill uniqueidentifier, 
			@ID_Vendor uniqueidentifier, 
			@Caption nvarchar(256), 
			@VisualNumber nvarchar(32), 
			@Price money, 
			@Settings nvarchar(512)
        SELECT
			@ID_Equipment = '1FEE4730-7B9D-E511-8B87-0023540F263B', 
			@ID_EquipmentType = NULL, 
			@ID_Bill = NULL, 
			@ID_Vendor = NULL, 
			@Caption = NULL, 
			@VisualNumber = NULL, 
			@Price = NULL, 
			@Settings = NULL
        EXEC [Arsenal].[SelectPart]
			@ID_Equipment, 
			@ID_EquipmentType, 
			@ID_Bill, 
			@ID_Vendor, 
			@Caption, 
			@VisualNumber, 
			@Price, 
			@Settings
    */

    SELECT
		ap.ID,
		ap.[ID_Equipment], 
		ap.[ID_EquipmentType], 
		ap.[ID_Bill], 
		ap.[ID_Vendor], 
		ap.[Caption] AS Product,
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ap.Caption AS Caption,  
		CAST(CASE WHEN ap.[ID_Equipment] IS NULL THEN 0 ELSE 1 END AS bit) AS Installed, 
		ap.[VisualNumber], 
		ap.[Price], 
		ap.[Settings]
    FROM [Arsenal].[Parts] ap
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ap.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ap.ID_Vendor AND av.[IsDeleted] = 0
    WHERE 
		ap.[IsDeleted] = 0
		AND (@Caption IS NULL OR ap.[Caption] LIKE @Caption + '%')
		AND (@ID_Equipment IS NULL OR @ID_Equipment = ap.[ID_Equipment] OR (ap.[ID_Equipment] IS NULL AND @ID_Equipment = '00000000-0000-0000-0000-000000000000'))
	ORDER BY ap.[Caption]
END
GO


-- ������� ���������
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectStructure]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectStructure] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectStructure]
AS
BEGIN
SET NOCOUNT ON

    /*
        EXEC [Arsenal].[SelectStructure]
    */

	DECLARE @ID_BuyerPurpose uniqueidentifier

	SELECT @ID_BuyerPurpose = op.ID
	FROM [Office].[Purposes] op
	WHERE
		op.[IsDeleted] = 0
		AND op.[Name] = 'Buyer'

    SELECT
		owp.Caption,
		owp.ID,
		oo.Caption AS Organisation,
		oo.[Address], 
		oo.Phone,
		od.Caption AS Department, 
		owp.Caption AS Workplace, 
		owp.Cabinet, 
		owp.[ID_Worker],
		2 AS [Level]
    FROM Office.WorkPlaces owp
        JOIN Office.Departments od ON owp.ID_Department = od.ID AND owp.[IsDeleted] = 0
		JOIN [Office].[Organisations] oo ON od.ID_Organisation = oo.ID AND od.[IsDeleted] = 0
		JOIN [Office].[OrganisationPurposes] oop ON oop.ID_Organisation = oo.ID AND oop.ID_Purpose = @ID_BuyerPurpose AND oop.IsDeleted = 0
    WHERE 
		oo.[IsDeleted] = 0
	UNION ALL SELECT
		od.Caption ,
		NULL AS ID,
		oo.Caption AS Organisation,
		oo.[Address], 
		oo.Phone,
		od.Caption AS Department, 
		NULL AS Workplace, 
		NULL AS Cabinet, 
		NULL AS [ID_Worker],
		1 AS [Level]
    FROM Office.Departments od
		JOIN [Office].[Organisations] oo ON od.ID_Organisation = oo.ID AND od.[IsDeleted] = 0
		JOIN [Office].[OrganisationPurposes] oop ON oop.ID_Organisation = oo.ID AND oop.ID_Purpose = @ID_BuyerPurpose AND oop.IsDeleted = 0 
    WHERE 
		od.[IsDeleted] = 0
	UNION ALL SELECT
	 	oo.Caption,
		NULL AS ID, 
		oo.Caption AS Organisation,
		oo.[Address], 
		oo.Phone,
		NULL AS Department, 
		NULL AS Workplace, 
		NULL AS Cabinet, 
		NULL AS [ID_Worker],
		0 AS [Level]
    FROM Office.[Organisations] oo
	JOIN [Office].[OrganisationPurposes] oop ON oop.ID_Organisation = oo.ID AND oop.ID_Purpose = @ID_BuyerPurpose AND oop.IsDeleted = 0 
    WHERE 
		oo.[IsDeleted] = 0						
	ORDER BY oo.[Caption], od.Caption, [Level], owp.Caption
END
GO

--------------------------Arsenal.InsertPart-----------------------------
-- ������� ������ � ������� Arsenal.Parts
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertPart]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertPart] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertPart]
	@ID_Equipment uniqueidentifier, 	@ID_EquipmentType uniqueidentifier, 	@ID_Bill uniqueidentifier, 	@ID_Vendor uniqueidentifier, 	@Caption nvarchar(256), 	@VisualNumber nvarchar(32), 	@Price money, 	@Settings nvarchar(512)
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Equipment uniqueidentifier, 			@ID_EquipmentType uniqueidentifier, 			@ID_Bill uniqueidentifier, 			@ID_Vendor uniqueidentifier, 			@Caption nvarchar(256), 			@VisualNumber nvarchar(32), 			@Price money, 			@Settings nvarchar(512)
        SELECT
			@ID_Equipment = '00000000-0000-0000-0000-000000000000', 			@ID_EquipmentType = '00000000-0000-0000-0000-000000000000', 			@ID_Bill = '00000000-0000-0000-0000-000000000000', 			@ID_Vendor = '00000000-0000-0000-0000-000000000000', 			@Caption = 'test', 			@VisualNumber = 0, 			@Price = 0, 			@Settings = 'test'
        EXEC [Arsenal].[InsertPart] 
			@ID_Equipment, 			@ID_EquipmentType, 			@ID_Bill, 			@ID_Vendor, 			@Caption, 			@VisualNumber, 			@Price, 			@Settings
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Arsenal].[Parts] 
		([ID_Equipment], [ID_EquipmentType], [ID_Bill], [ID_Vendor], [Caption], [VisualNumber], [Price], [Settings])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@ID_Equipment, @ID_EquipmentType, @ID_Bill, @ID_Vendor, ISNULL(@Caption, ''), @VisualNumber, @Price, @Settings)
	
	SELECT TOP 1 @ID = ID FROM @outid
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO

--------------------------Arsenal.SelectResult-----------------------------
-- ������� �� ������� Arsenal.Results
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectResult]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectResult] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectResult]
	@ID_Inventarisation uniqueidentifier = NULL, 	@ID_Equipment uniqueidentifier = NULL, 	@ID_WorkPlace uniqueidentifier = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Inventarisation uniqueidentifier, 			@ID_Equipment uniqueidentifier, 			@ID_WorkPlace uniqueidentifier
        SELECT
			@ID_Inventarisation = NULL, 			@ID_Equipment = NULL, 			@ID_WorkPlace = NULL
        EXEC [Arsenal].[SelectResult]
			@ID_Inventarisation, 			@ID_Equipment, 			@ID_WorkPlace
    */

    SELECT
		ar.ID,
		ar.[ID_Inventarisation], 		ar.[ID_Equipment],		ae.[InvNumber], 		ae.[Barcode], 		em.[ID_WorkPlace] AS ID_CurrentWorkplace,
		ar.[ID_WorkPlace],
		owp.ID_Department,
		od.ID_Organisation
    FROM [Arsenal].[Results] ar
		JOIN [Arsenal].[Equipments] ae ON ae.ID = ar.ID_Equipment AND ae.IsDeleted = 0
        LEFT JOIN (SELECT ID_Equipment AS ID, ID_WorkPlace FROM (
			SELECT ID_Equipment, ID_WorkPlace, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY Date DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1) em ON em.ID = ar.ID_Equipment
		LEFT JOIN [Office].[WorkPlaces] owp ON owp.ID = em.ID_Workplace AND owp.IsDeleted = 0
		LEFT JOIN [Office].[Departments] od ON od.ID = owp.ID_Department AND od.IsDeleted = 0
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
    WHERE 
		ar.[IsDeleted] = 0
		AND (@ID_Inventarisation IS NULL OR ar.[ID_Inventarisation] = @ID_Inventarisation)
	ORDER BY ISNULL(aet.Caption + ' ', '') + ISNULL(av.Caption + ' ', '') + ae.Caption
END
GO

--------------------------Arsenal.SelectInventarisation-----------------------------
-- ������� �� ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[SelectInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[SelectInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[SelectInventarisation]
	@ID_Worker uniqueidentifier = NULL, 	@Caption nvarchar(256) = NULL, 	@BeginDate date = NULL, 	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Worker uniqueidentifier, 			@Caption nvarchar(256), 			@BeginDate date, 			@EndDate date
        SELECT
			@ID_Worker = NULL, 			@Caption = NULL, 			@BeginDate = NULL, 			@EndDate = NULL
        EXEC [Arsenal].[SelectInventarisation]
			@ID_Worker, 			@Caption, 			@BeginDate, 			@EndDate
    */

    SELECT
		ai.ID,
		ai.[ID_Worker], 		ai.[Caption], 		ai.[BeginDate], 		ai.[EndDate],
		CAST(CASE WHEN ai.BeginDate IS NULL THEN 0 ELSE CASE WHEN ai.[EndDate] IS NULL THEN 1 ELSE 0 END END AS bit) AS [Started], 
		CAST(CASE WHEN ai.BeginDate IS NULL THEN 0 ELSE CASE WHEN ai.[EndDate] IS NULL THEN 0 ELSE 1 END END AS bit) AS [Ended]
    FROM [Arsenal].[Inventarisations] ai
        --LEFT JOIN  ON .ID = ai.ID_ AND .[IsDeleted] = 0
    WHERE 
		ai.[IsDeleted] = 0
		AND (@Caption IS NULL OR ai.[Caption] LIKE @Caption + '%')
	ORDER BY ai.[Caption]
END
GO

-- ������� ������ � ������� Arsenal.Inventarisations
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[InsertInventarisation]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InsertInventarisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InsertInventarisation]
	@ID_Worker uniqueidentifier, 	@Caption nvarchar(256), 	@BeginDate date = NULL, 	@EndDate date = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@ID_Worker uniqueidentifier, 			@Caption nvarchar(256), 			@BeginDate date, 			@EndDate date
        SELECT
			@ID_Worker = '00000000-0000-0000-0000-000000000000', 			@Caption = 'test', 			@BeginDate = 'test', 			@EndDate = 'test'
        EXEC [Arsenal].[InsertInventarisation] 
			@ID_Worker, 			@Caption, 			@BeginDate, 			@EndDate
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT [Arsenal].[Inventarisations] 
		([ID_Worker], [Caption], [BeginDate], [EndDate])
	OUTPUT INSERTED.ID INTO @outid
	SELECT @ID_Worker, @Caption, @BeginDate, @EndDate
	
	SELECT TOP 1 @ID = ID FROM @outid

	INSERT [Arsenal].[Results] (ID_Inventarisation, ID_Equipment)
	SELECT @ID, ae.ID
	FROM [Arsenal].[Equipments] ae
	WHERE ae.[IsDeleted] = 0
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO

--------------------------Office.SelectDepartment-----------------------------
-- ������� �� ������� Office.Departments
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectDepartment]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectDepartment] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectDepartment]
	@ID_Organisation uniqueidentifier = NULL, 	@Caption nvarchar(256) = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID_Organisation uniqueidentifier, 			@Caption nvarchar(256)
        SELECT
			@ID_Organisation = NULL, 			@Caption = NULL
        EXEC [Office].[SelectDepartment]
			@ID_Organisation, 			@Caption
    */

    SELECT
		od.ID,
		od.[ID_Organisation], 		od.[Caption] + '(' + oo.Caption + ')' AS Caption
    FROM [Office].[Departments] od
        JOIN Office.Organisations oo ON oo.ID = od.ID_Organisation AND oo.[IsDeleted] = 0
    WHERE 
		od.[IsDeleted] = 0
		AND (@Caption IS NULL OR od.[Caption] LIKE @Caption + '%')
	ORDER BY od.[Caption]
END
GO

-- ������� �� ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[SelectOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[SelectOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[SelectOrganisation]
	@BuyersOnly int = NULL,
	@Purpose nvarchar(32) = NULL,
	@Caption nvarchar(256) = NULL	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@BuyersOnly int,
			@Purpose nvarchar(32),
			@Caption nvarchar(256)
        SELECT
			@BuyersOnly = 1,
			@Purpose  = NULL,
			@Caption = NULL
        EXEC [Office].[SelectOrganisation]
			@BuyersOnly,
			@Purpose,
			@Caption
    */

    SELECT
		oo.ID,
		oo.[Caption], 
		oo.[Address], 
		oo.[Phone],
		oop.ID_Purpose,
		oop.Name AS PurposeName
    FROM [Office].[Organisations] oo
        LEFT JOIN (
			SELECT ID_Organisation, ID_Purpose, Name, MIN([Priority]) OVER (PARTITION BY ID_Organisation) p
			FROM [Office].[OrganisationPurposes] oop
			JOIN [Office].[Purposes] op ON op.ID = oop.ID_Purpose AND op.IsDeleted = 0 
			WHERE oop.[IsDeleted] = 0
		) oop ON oop.ID_Organisation = oo.ID 
    WHERE 
		oo.[IsDeleted] = 0
		AND (@Caption IS NULL OR oo.[Caption] LIKE @Caption + '%')
		AND (ISNULL(@BuyersOnly, 0) = 0 OR oop.[Name] = 'Buyer')
		AND (@Purpose IS NULL OR oop.[Name] = @Purpose)
	ORDER BY oo.[Caption]
END
GO
--------------------------Office.InsertOrganisation-----------------------------
-- ������� ������ � ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[InsertOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[InsertOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[InsertOrganisation]
	@Caption nvarchar(256), 
	@Address nvarchar(256), 
	@Phone nvarchar(64),
	@ID_Purpose uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
			@Caption nvarchar(256), 
			@Address nvarchar(256), 
			@Phone nvarchar(64),
			@ID_Purpose uniqueidentifier
        SELECT
			@Caption = 'test', 
			@Address = 'test', 
			@Phone = 'test',
			@ID_Purpose = ''
        EXEC [Office].[InsertOrganisation] 
			@Caption, 
			@Address, 
			@Phone,
			@ID_Purpose
    */
    
	DECLARE @ID uniqueidentifier
	DECLARE @outid TABLE (ID uniqueidentifier)
    
	INSERT INTO [Office].[Organisations] 
		([Caption], [Address], [Phone])
	OUTPUT INSERTED.ID INTO @outid
	VALUES (@Caption, @Address, @Phone)
	
	SELECT TOP 1 @ID = ID FROM @outid

	INSERT [Office].[OrganisationPurposes]
		(ID_Organisation, ID_Purpose)
	SELECT @ID, @ID_Purpose
	
	SELECT @ID AS ID

	RETURN @@ERROR	

END
GO
--------------------------Office.UpdateOrganisation-----------------------------
-- ���������� ������ � ������� Office.Organisations
USE [Arsenal]
GO

IF OBJECT_ID('[Office].[UpdateOrganisation]') IS NULL
   EXEC('CREATE PROCEDURE [Office].[UpdateOrganisation] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Office].[UpdateOrganisation]
    @ID uniqueidentifier,
	@Caption nvarchar(256), 
	@Address nvarchar(256), 
	@Phone nvarchar(64),
	@ID_Purpose uniqueidentifier
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE 
            @ID uniqueidentifier,
			@Caption nvarchar(256), 
			@Address nvarchar(256), 
			@Phone nvarchar(64),
			@ID_Purpose uniqueidentifier
        SELECT
			@Caption = 'test', 
			@Address = 'test', 
			@Phone = 'test',
			@ID_Purpose = ''
        EXEC [Office].[UpdateOrganisation] 
			@Caption, 
			@Address, 
			@Phone,
			@ID_Purpose 
    */
    
    UPDATE [Office].[Organisations]
    SET
		[Caption] = @Caption, 
		[Address] = @Address, 
		[Phone] = @Phone
    WHERE ID = @ID

	DECLARE 
		@ID_OrgPurposeOld uniqueidentifier

	SELECT @ID_OrgPurposeOld = oop.ID
	FROM [Office].[OrganisationPurposes] oop
	JOIN [Office].[Purposes] op ON op.ID = oop.ID_Purpose AND op.IsDeleted = 0
	WHERE 
		oop.ID_Organisation = @ID
		AND oop.IsDeleted = 0
	ORDER BY op.[Priority] ASC

	IF @ID_OrgPurposeOld IS NOT NULL
		UPDATE [Office].[OrganisationPurposes]
		SET ID_Purpose = @ID_Purpose
		WHERE ID = @ID_OrgPurposeOld
	ELSE
		INSERT [Office].[OrganisationPurposes]
			(ID_Organisation, ID_Purpose)
		SELECT @ID, @ID_Purpose
	
    SELECT @ID AS ID

    RETURN @@ERROR 

END
GO

-------------------------------------------------------------------------------------------------------------------------------------------------- REPORTS
USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[EquipmentReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[EquipmentReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[EquipmentReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = '55D88E1B-B2B4-E511-8B87-0023540F263B'
        EXEC [Arsenal].[EquipmentReport]
			@ID
    */

    SELECT
		CAST(ae.[Barcode] AS nvarchar(99)) AS Barcode, 
		ISNULL(CONVERT(nvarchar(10), ob.[Date], 104), '-') AS BillDate,
		oob.Caption AS Buyer, 
		oob.Caption AS BuyerTitle,
		CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108) AS DateTitle,
		ISNULL(ae.[Settings], '-') AS Descr,
		ISNULL(CONVERT(nvarchar(10), ei.InvDate, 104), '-') AS InvDate,
		ISNULL(ae.[InvNumber], '-') AS [InvN], 
		ob.Invoice, 
		CAST(ae.[Lifetime] AS nvarchar(99)) + ' ����' AS [Lifetime], 
		ae.[Caption] AS Model,
		CONVERT(nvarchar(10), em.MoveDate, 104) AS MoveDate,
		'-' AS Note, --ob.Note,
		ae.[Price],
		amt.Caption AS Reason, 
		oos.Caption AS Seller,
		aet.[Caption] AS [Type], 
		av.Caption AS Vendor, 
		ae.[VisualNumber] AS VisN,
		op.Caption AS Worker, 
		owp.Caption AS WorkPlace
    FROM [Arsenal].[Equipments] ae
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
		LEFT JOIN (SELECT * FROM (
			SELECT ID_Equipment, ID_WorkPlace, Note, ID_MovingType, ID_Person, [Date] MoveDate, 
				ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1) em 
		ON em.ID_Equipment = ae.ID
		LEFT JOIN (SELECT * FROM (
			SELECT ID_Equipment, [BeginDate] InvDate, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [BeginDate] DESC) rn 
			FROM Arsenal.Inventarisations ai 
			JOIN Arsenal.Results ar ON ar.ID_Inventarisation = ai.ID AND ar.IsDeleted = 0
			WHERE ai.IsDeleted = 0) a WHERE rn = 1) ei 
		ON ei.ID_Equipment = ae.ID
		LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
		LEFT JOIN Office.Organisations oos ON ob.ID_Seller = oos.ID AND oos.IsDeleted = 0
		LEFT JOIN Office.Organisations oob ON ob.ID_Buyer = oob.ID AND oob.IsDeleted = 0
		LEFT JOIN Office.WorkPlaces owp ON owp.ID = em.ID_Workplace AND owp.IsDeleted = 0		
		LEFT JOIN Arsenal.MovingTypes amt ON amt.ID = em.ID_MovingType AND amt.[IsDeleted] = 0
		LEFT JOIN Office.Persons op ON op.ID = em.ID_Person AND op.IsDeleted = 0
    WHERE 
		ae.[IsDeleted] = 0
		AND ae.ID = @ID

END
GO

USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[MovingReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[MovingReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[MovingReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = '55D88E1B-B2B4-E511-8B87-0023540F263B'
        EXEC [Arsenal].[MovingReport]
			@ID
    */
	DECLARE
		@Barcode nvarchar(99), 
		@BuyerTitle nvarchar(99), 
		@DateTitle nvarchar(99), 
		@Descr nvarchar(99), 
		@InvDate nvarchar(99), 
		@InvN nvarchar(99),
		@Lifetime nvarchar(99), 
		@Model nvarchar(99),
		@Type nvarchar(99), 
		@Vendor nvarchar(99), 
		@VisN nvarchar(99)
    SELECT
		@Barcode = CAST(ae.[Barcode] AS nvarchar(99)), 
		@BuyerTitle = oob.Caption,
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@Descr = ISNULL(ae.[Settings], '-'),
		@InvDate = ISNULL(CONVERT(nvarchar(10), ei.InvDate, 104), '-'),
		@InvN = ISNULL(ae.[InvNumber], '-'), 
		@Lifetime = CAST(ae.[Lifetime] AS nvarchar(99)) + ' �.', 
		@Model = ae.[Caption],
		@Type = aet.[Caption], 
		@Vendor = av.Caption, 
		@VisN = ae.[VisualNumber]
    FROM [Arsenal].[Equipments] ae
        LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0
		LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
		LEFT JOIN (SELECT * FROM (
			SELECT ID_Equipment, [BeginDate] InvDate, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [BeginDate] DESC) rn 
			FROM Arsenal.Inventarisations ai 
			JOIN Arsenal.Results ar ON ar.ID_Inventarisation = ai.ID AND ar.IsDeleted = 0
			WHERE ai.IsDeleted = 0) a WHERE rn = 1) ei 
		ON ei.ID_Equipment = ae.ID
		LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
		LEFT JOIN Office.Organisations oob ON ob.ID_Buyer = oob.ID AND oob.IsDeleted = 0
    WHERE 
		ae.[IsDeleted] = 0
		AND ae.ID = @ID

	SELECT 0 AS Idx, '' AS Workplace, '' AS MoveDate, 'Barcode' AS Reason, @Barcode AS Worker
	UNION ALL SELECT 0, '', '', 'BuyerTitle', @BuyerTitle
	UNION ALL SELECT 0, '', '', 'DateTitle', @DateTitle
	UNION ALL SELECT 0, '', '', 'Descr', @Descr
	UNION ALL SELECT 0, '', '', 'InvDate', @InvDate
	UNION ALL SELECT 0, '', '', 'InvN', @InvN
	UNION ALL SELECT 0, '', '', 'Lifetime', @Lifetime
	UNION ALL SELECT 0, '', '', 'Model', @Model
	UNION ALL SELECT 0, '', '', 'Type', @Type
	UNION ALL SELECT 0, '', '', 'Vendor', @Vendor
	UNION ALL SELECT 0, '', '', 'VisN', @VisN
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY am.[Date]), 
		owp.Caption, 
		CONVERT(nvarchar(10), am.[Date], 104),
		amt.Caption, 
		op.Caption	
	FROM Arsenal.Movings am		
	LEFT JOIN Office.WorkPlaces owp ON owp.ID = am.ID_Workplace AND owp.IsDeleted = 0		
	LEFT JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.[IsDeleted] = 0
	LEFT JOIN Office.Persons op ON op.ID = am.ID_Person AND op.IsDeleted = 0
	WHERE 
		am.IsDeleted = 0
		AND am.ID_Equipment = @ID
	ORDER BY MoveDate
END
GO

USE [Arsenal]
GO

IF OBJECT_ID('[Arsenal].[WPReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[WPReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[WPReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = '6AECCFE9-9B49-4428-8904-049BE52720FA'
        EXEC [Arsenal].[WPReport]
			@ID
    */
	DECLARE
		@BuyerTitle nvarchar(99), 
		@DateTitle nvarchar(99),
		@InvDate nvarchar(99), 
		@WPName nvarchar(99), 
		@Department nvarchar(99), 
		@Worker nvarchar(99),
		@Cabinet nvarchar(99), 
		@FullPrice nvarchar(99)
    SELECT
		@BuyerTitle = oo.Caption,
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@InvDate = ISNULL(CONVERT(nvarchar(10), ei.InvDate, 104), '-'),
		@WPName = owp.[Caption],
		@Department = od.[Caption],
		@Worker = ISNULL(ow.[Caption], '-'), 
		@Cabinet = ISNULL(owp.Cabinet, '-'), 
		@FullPrice = ISNULL(CAST(em.FullPrice as nvarchar(99)), '-')
    FROM [Office].[WorkPlaces] owp
		LEFT JOIN Office.Departments od ON od.ID = owp.ID_Department AND od.IsDeleted = 0
		LEFT JOIN Office.Organisations oo ON oo.ID = od.ID_Organisation AND oo.IsDeleted = 0
        LEFT JOIN Office.Workers ow ON ow.ID = owp.ID_Worker AND ow.IsDeleted = 0
		LEFT JOIN (SELECT a.ID_WorkPlace, SUM(ae.Price) FullPrice FROM (
			SELECT ID_WorkPlace, ID_Equipment, ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
			FROM Arsenal.Movings WHERE IsDeleted = 0) a 
			JOIN Arsenal.Equipments ae ON ae.ID = a.ID_Equipment AND ae.IsDeleted = 0 
			WHERE rn = 1 
			GROUP BY ID_WorkPlace) em 
		ON em.ID_WorkPlace = owp.ID
		LEFT JOIN (SELECT * FROM (
			SELECT ID_WorkPlace, [BeginDate] InvDate, ROW_NUMBER() OVER(PARTITION BY ID_WorkPlace ORDER BY [BeginDate] DESC) rn 
			FROM Arsenal.Inventarisations ai 
			JOIN Arsenal.Results ar ON ar.ID_Inventarisation = ai.ID AND ar.IsDeleted = 0
			WHERE ai.IsDeleted = 0) a WHERE rn = 1) ei 
		ON ei.ID_WorkPlace = owp.ID
    WHERE 
		owp.[IsDeleted] = 0
		AND owp.ID = @ID

	SELECT 0 AS Idx, 'InvDate' AS [Type], @InvDate AS InvN, '' AS Model, '' AS InstDate, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT 0, 'WPName', @WPName, '', '', ''
	UNION ALL SELECT 0, 'Department', @Department, '', '', ''
	UNION ALL SELECT 0, 'Worker', @Worker, '', '', ''
	UNION ALL SELECT 0, 'Cabinet', @Cabinet, '', '', ''
	UNION ALL SELECT 0, 'FullPrice', @FullPrice, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY am.[Date]), 
		aet.Caption, 
		ISNULL(ae.InvNumber, ''),
		ISNULL(av.Caption + ' ', '') + ae.Caption, 
		CONVERT(nvarchar(10), am.[Date], 104),		
		CAST(ae.Price AS nvarchar(99))
	FROM (SELECT * FROM (SELECT ID_WorkPlace, ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) am
	JOIN Arsenal.Equipments ae ON ae.ID = am.ID_Equipment AND ae.IsDeleted = 0 
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	WHERE 		
		am.ID_Workplace = @ID
	ORDER BY InstDate
END
GO

IF OBJECT_ID('[Arsenal].[InvReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[InvReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[InvReport]
	@ID uniqueidentifier	
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = 'C8212219-C8B3-E511-8B87-0023540F263B'
        EXEC [Arsenal].[InvReport]
			@ID
    */
	DECLARE
		@BuyerTitle nvarchar(99),
		@DateTitle nvarchar(99), 
		@EndDate nvarchar(99), 
		@InvName nvarchar(99), 
		@Worker nvarchar(99)
    SELECT
		@BuyerTitle = ISNULL(oo.Caption, ''),
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@EndDate = ISNULL(CONVERT(nvarchar(10), ai.EndDate, 104), '-'),
		@InvName  = ISNULL(ai.[Caption], ''),
		@Worker = ISNULL(ow.[Caption], '-')
    FROM [Arsenal].[Inventarisations] ai
		LEFT JOIN Office.Workers ow ON ow.ID = ai.ID_Worker AND ow.IsDeleted = 0
		LEFT JOIN Office.Organisations oo ON oo.ID = ow.ID_Organisation AND oo.IsDeleted = 0
    WHERE 
		ai.[IsDeleted] = 0
		AND ai.ID = @ID

	SELECT 0 AS Idx, 'InvName' AS Model, @InvName AS InvN, '' AS BuyDate, '' AS Workplace, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT 0, 'EndName', @EndDate, '', '', ''
	UNION ALL SELECT 0, 'Worker', @Worker, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption), 
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption AS Caption, 
		ISNULL(ae.InvNumber, ''),
		CONVERT(nvarchar(10), am.[Date], 104),				
		ISNULL(owp.Caption, ''), 		
		CAST(ae.Price AS nvarchar(99))
	FROM Arsenal.Results ar
	JOIN (SELECT * FROM (SELECT ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] ASC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) am 
	ON am.ID_Equipment = ar.ID_Equipment
	JOIN Arsenal.Equipments ae ON ae.ID = ar.ID_Equipment AND ae.IsDeleted = 0 
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	LEFT JOIN Office.WorkPlaces owp ON owp.ID = ar.ID_Workplace AND owp.IsDeleted = 0		
	WHERE 		
		ar.ID_Inventarisation = @ID
	ORDER BY Model
END
GO

IF OBJECT_ID('[Arsenal].[ForDisposeReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[ForDisposeReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[ForDisposeReport]
	@ID_Organisation uniqueidentifier,
	@DateBegin datetime	= NULL,
	@DateEnd datetime = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = 'C8212219-C8B3-E511-8B87-0023540F263B'
        EXEC [Arsenal].[ForDisposeReport]
			@ID
    */

	IF @DateBegin IS NULL
		SELECT @DateBegin = CAST(0 AS datetime)
	IF @DateEnd IS NULL
		SELECT @DateEnd = DATEADD(DAY, DATEDIFF(DAY, '19000101', GETDATE()), '23:59:59')

	DECLARE
		@BuyerTitle nvarchar(99),
		@DateTitle nvarchar(99), 
		@DateRange nvarchar(99)
    SELECT
		@BuyerTitle = ISNULL(oo.Caption, ''),
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@DateRange = CONVERT(nvarchar(10), @DateBegin, 104) + '-' + CONVERT(nvarchar(10), @DateEnd, 104)
    FROM Office.Organisations oo 
    WHERE 
		oo.[IsDeleted] = 0
		AND oo.ID = @ID_Organisation

	DECLARE @SysMTs TABLE(Name nvarchar(99))
	INSERT @SysMTs
	--SELECT 'Buy' UNION
	SELECT 'Install'
	UNION SELECT 'Repair'
	UNION SELECT 'Relocate'
	UNION SELECT 'Dispose'
	UNION SELECT 'PreDispose'

	SELECT 0 AS Idx, 'DateRange' AS Model, @DateRange AS InvN, '' AS BuyDate, '' AS Workplace, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption), 
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption AS Caption, 
		ISNULL(ae.InvNumber, ''),
		CONVERT(nvarchar(10), amb.[Date], 104),				
		ISNULL(owp.Caption, ''), 		
		CAST(ae.Price AS nvarchar(99))
	FROM Arsenal.Equipments ae 
	JOIN (SELECT * FROM (SELECT ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] ASC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) amb 
	ON amb.ID_Equipment = ae.ID
	JOIN (SELECT * FROM (SELECT ID_Equipment, amt.Name,	[Date],
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings am 
		JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.IsDeleted = 0 
		WHERE am.IsDeleted = 0) a WHERE rn = 1 AND Name = 'PreDispose') amd 
	ON amd.ID_Equipment = ae.ID
	JOIN (SELECT * FROM (SELECT ID_Equipment, ID_Workplace,
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings am 
		JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.IsDeleted = 0
		LEFT JOIN @SysMTs smt ON smt.Name = amt.Name
		WHERE am.IsDeleted = 0 AND smt.Name IS NULL) a WHERE rn = 1 ) am 
	ON am.ID_Equipment = ae.ID	
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	LEFT JOIN Office.WorkPlaces owp ON owp.ID = am.ID_Workplace AND owp.IsDeleted = 0
	LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
	WHERE 		
		ae.IsDeleted = 0 
		AND amd.[Date] BETWEEN @DateBegin AND @DateEnd
		AND ob.ID_Buyer = @ID_Organisation
	ORDER BY Model
END
GO

IF OBJECT_ID('[Arsenal].[DisposedReport]') IS NULL
   EXEC('CREATE PROCEDURE [Arsenal].[DisposedReport] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROC [Arsenal].[DisposedReport]
	@ID_Organisation uniqueidentifier,
	@DateBegin datetime	= NULL,
	@DateEnd datetime = NULL
AS
BEGIN
SET NOCOUNT ON

    /*
        DECLARE
			@ID uniqueidentifier
        SELECT
			@ID = 'C8212219-C8B3-E511-8B87-0023540F263B'
        EXEC [Arsenal].[DisposedReport]
			@ID
    */

	IF @DateBegin IS NULL
		SELECT @DateBegin = CAST(0 AS datetime)
	IF @DateEnd IS NULL
		SELECT @DateEnd = DATEADD(DAY, DATEDIFF(DAY, '19000101', GETDATE()), '23:59:59')

	DECLARE
		@BuyerTitle nvarchar(99),
		@DateTitle nvarchar(99), 
		@DateRange nvarchar(99)
    SELECT
		@BuyerTitle = ISNULL(oo.Caption, ''),
		@DateTitle = CONVERT(nvarchar(10), GETDATE(), 104) + ' ' + CONVERT(nvarchar(10), GETDATE(), 108),
		@DateRange = CONVERT(nvarchar(10), @DateBegin, 104) + '-' + CONVERT(nvarchar(10), @DateEnd, 104)
    FROM Office.Organisations oo 
    WHERE 
		oo.[IsDeleted] = 0
		AND oo.ID = @ID_Organisation

	DECLARE @SysMTs TABLE(Name nvarchar(99))
	INSERT @SysMTs
	--SELECT 'Buy' UNION
	SELECT 'Install'
	UNION SELECT 'Repair'
	UNION SELECT 'Relocate'
	UNION SELECT 'Dispose'
	UNION SELECT 'PreDispose'

	SELECT 0 AS Idx, 'DateRange' AS Model, @DateRange AS InvN, '' AS BuyDate, '' AS DisposeDate, '' AS Price
	UNION ALL SELECT 0, 'BuyerTitle', @BuyerTitle, '', '', ''
	UNION ALL SELECT 0, 'DateTitle', @DateTitle, '', '', ''
	UNION ALL SELECT
		ROW_NUMBER() OVER(ORDER BY aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption), 
		aet.Caption + ISNULL(' ' + av.Caption, '') + ' ' + ae.Caption AS Caption, 
		ISNULL(ae.InvNumber, ''),
		CONVERT(nvarchar(10), amb.[Date], 104),				
		CONVERT(nvarchar(10), amd.[Date], 104), 		
		CAST(ae.Price AS nvarchar(99))
	FROM Arsenal.Equipments ae 
	JOIN (SELECT * FROM (SELECT ID_Equipment, [Date], 
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] ASC) rn 
		FROM Arsenal.Movings WHERE IsDeleted = 0) a WHERE rn = 1 ) amb 
	ON amb.ID_Equipment = ae.ID
	JOIN (SELECT * FROM (SELECT ID_Equipment, amt.Name,	[Date],
		ROW_NUMBER() OVER(PARTITION BY ID_Equipment ORDER BY [Date] DESC) rn 
		FROM Arsenal.Movings am 
		JOIN Arsenal.MovingTypes amt ON amt.ID = am.ID_MovingType AND amt.IsDeleted = 0 
		WHERE am.IsDeleted = 0) a WHERE rn = 1 AND Name = 'Dispose') amd 
	ON amd.ID_Equipment = ae.ID
	LEFT JOIN Arsenal.EquipmentTypes aet ON aet.ID = ae.ID_EquipmentType AND aet.[IsDeleted] = 0	
	LEFT JOIN Arsenal.Vendors av ON av.ID = ae.ID_Vendor AND av.[IsDeleted] = 0
	LEFT JOIN Office.Bills ob ON ob.ID = ae.ID_Bill AND ob.IsDeleted = 0
	WHERE 		
		ae.IsDeleted = 0 
		AND amd.[Date] BETWEEN @DateBegin AND @DateEnd
		AND ob.ID_Buyer = @ID_Organisation
	ORDER BY Model
END
GO

------------------------------------------------------------------------------------------------------------------------- default initialization

-- SELECT NEWID()

--DECLARE @ID_Org uniqueidentifier = '9CE38B9F-766E-477B-A9AC-BDA05BBB048C'	
--DECLARE @ID_Admin uniqueidentifier = 'ADADADAD-ADAD-ADAD-ADAD-ADADADADADAD'	

DECLARE
	@ID_Org uniqueidentifier,
	@ID_Buy uniqueidentifier = '055BDBBB-4174-45A5-B916-5C2494B094B8', 
	@ID_Install uniqueidentifier = 'FC72714C-91D1-4BAF-9E0E-580ACDFD15A8',
	@ID_Repair uniqueidentifier = '076DA437-956B-4DAE-9A2A-40703699062F',
	@ID_Relocate uniqueidentifier = '777F1B61-A10C-4956-9F34-6B330FB100A5',
	@ID_Dispose uniqueidentifier = 'BF26155C-58F4-425B-8205-3C2FA2E4BF4E',
	@ID_PreDispose uniqueidentifier = 'F3B08A09-4CC5-4271-8488-094B722E229D',
	@ID_JobD uniqueidentifier = '0795B370-A107-43DE-ACA7-CEDF1AD1F41E',
	@ID_Job uniqueidentifier,
	@ID_JobOp uniqueidentifier,
	@ID_User uniqueidentifier = '6D4E6FCC-8316-4276-8F03-3E7B5585FB6B',
	@ID_Dep uniqueidentifier = '204371B6-3C8D-41C6-B181-A4BB2548AD3B',
	@ID_PC uniqueidentifier = '4327D1AC-B486-4804-9B5A-D6E262162BDA',
	@ID_Monitor	uniqueidentifier = 'A2852F72-2E1F-4FBA-A0D5-8B80E2000740',
	@ID_Stock uniqueidentifier = '3E2E6965-B7D4-4B9F-B937-F1055D99FB77',
	@ID_Scrap uniqueidentifier = '6AECCFE9-9B49-4428-8904-049BE52720FA',
	@ID_PreScrap uniqueidentifier = '16046DE5-9D66-4E3D-94CF-3621955F2255',
	@ID_Workshop uniqueidentifier = '8335D77F-5BCC-4244-AEF5-AD10F4B42A49',
	@ID_Lost uniqueidentifier = 'F0416D8F-8CE0-4E1A-A60B-38076A5BF698',
	@ID_PurposeBuyer uniqueidentifier = '880AD9F7-B6B4-4904-A1C3-F93F893DA23A',
	@ID_PurposeSeller uniqueidentifier = 'A8FAA67B-B52F-4B62-9321-2994F470CC33'

INSERT [Arsenal].[Office].[Purposes](ID, Name, Caption, [Priority])
SELECT @ID_PurposeBuyer, 'Buyer', '�����. ���-���', 1
UNION SELECT @ID_PurposeSeller, 'Seller', '���������', 2

INSERT [Arsenal].[Arsenal].[MovingTypes](ID, Name, Caption)
SELECT @ID_Buy, 'Buy', '������� � ������������ �� �����'
UNION SELECT @ID_Install, 'Install', '��������� �� ������� �����'
UNION SELECT @ID_Repair, 'Repair', '������' 
UNION SELECT @ID_Relocate, 'Relocate', '������� �� ������ ������� �����' 
UNION SELECT @ID_Dispose, 'Dispose', '��������'
UNION SELECT @ID_PreDispose, 'PreDispose', '������������ � ��������'

INSERT [Arsenal].[Arsenal].[EquipmentTypes](ID, Name, Caption)
SELECT @ID_PC, 'PC', '������������ ���������'
UNION SELECT @ID_Monitor, 'Monitor', '�������'

SELECT @ID_Org = ID 
FROM [Arsenal].[Office].[Organisations] 
WHERE Caption = '�������� ������' 

IF @ID_Org IS NULL
	INSERT [Arsenal].[Office].[Organisations](ID, Caption)
	SELECT @ID_Org, '�������� ������'

INSERT [Arsenal].[Office].[Departments](ID, [ID_Organisation], Caption)
SELECT @ID_Dep, @ID_Org, '�������� �������������'

INSERT [Arsenal].[Office].[WorkPlaces](ID, [ID_Department], Name, Caption)
SELECT @ID_Stock, @ID_Dep, 'Stock', '����� ������������'
UNION SELECT @ID_Scrap, @ID_Dep, 'Scrap', '����� ��������'
UNION SELECT @ID_PreScrap, @ID_Dep, 'PreScrap', '����� ��������������� ��� ��������'
UNION SELECT @ID_Workshop, @ID_Dep, 'Workshop', '��������� ����������'
UNION SELECT @ID_Lost, @ID_Dep, 'Lost', '���������������'

DECLARE @JobOps TABLE (ID uniqueidentifier, Code int identity, Managable bit)
DECLARE @OutID TABLE (ID uniqueidentifier)

INSERT @JobOps
SELECT 
	ID, 
	CASE WHEN Caption IN ('SelectPerson', 'SelectMovingType', 'SelectDepartment', 'SelectBill') THEN 0 ELSE 1 END
FROM dbo.Operations
WHERE ProcedureName IN (
	'Arsenal.SelectEquipmentType',
	'Arsenal.InsertEquipmentType',
	--'Arsenal.UpdateEquipmentType',
	--'Arsenal.DeleteEquipmentType',
	'Arsenal.SelectMovingType',
	'Arsenal.SelectEquipment',
	'Arsenal.InsertEquipment',
	'Arsenal.UpdateEquipment',
	'Arsenal.DeleteEquipment',
	'Arsenal.SelectStructure',
	'Arsenal.SelectPart',
	'Arsenal.InsertPart',
	'Arsenal.UpdatePart',
	'Arsenal.DeletePart',
	'Arsenal.SelectMoving',
	'Arsenal.InsertMoving',
	'Arsenal.UpdateMoving',
	'Arsenal.DeleteMoving',
	'Office.SelectWorkPlace',
	'Office.InsertWorkPlace',
	'Office.UpdateWorkPlace',
	'Office.DeleteWorkPlace',
	'Office.SelectDepartment',
	--'Office.InsertDepartment',
	--'Office.UpdateDepartment',
	--'Office.DeleteDepartment',
	--'Office.SelectOrganisation',
	--'Office.InsertOrganisation',
	--'Office.UpdateOrganisation',
	--'Office.DeleteOrganisation',
	'Office.SelectPerson',
	'Arsenal.SelectVendor',
	'Office.SelectBill')

SELECT @ID_Job = ID
FROM [Arsenal].[Office].[Jobs]
WHERE Name = 'Operator'

IF @ID_Job IS NULL
BEGIN
	INSERT @OutID EXEC [Arsenal].[Office].InsertJob 'Operator', '�������� ����� ������������'
	SELECT @ID_Job = ID FROM @OutID
	DELETE @OutID

	UPDATE [Office].[Jobs]
	SET ID = @ID_JobD
	WHERE ID = @ID_Job
END

BEGIN
	INSERT @OutID EXEC [Arsenal].[Office].InsertPerson 'Operator1', @ID_Org, '������', '����', '��������', '19000101', NULL, NULL, 'test' -- has check for duplicates
	SELECT @ID_User = ID FROM @OutID
	DELETE @OutID
	IF @ID_User IS NOT NULL
		EXEC [Arsenal].[Office].InsertPersonJob @ID_User, @ID_JobD, '19000101', NULL -- has check for duplicates
END	 

DECLARE
	@Step int,
	@MaxStep int,
	@Managable bit

SELECT 
	@Step = 1, 
	@MaxStep = COUNT(ID)
FROM @JobOps

WHILE @Step <= @MaxStep
BEGIN
	SELECT 
		@ID_JobOp = ID,
		@Managable = Managable,
		@Step = @Step + 1
	FROM @JobOps
	WHERE Code = @Step
	EXEC [Arsenal].[Office].InsertJobOperation @ID_JobD, @ID_Jobop, '19000101', NULL, @Managable, 0  -- has check for duplicates
END