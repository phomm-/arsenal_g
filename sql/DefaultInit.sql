
-- default initialization

-- SELECT NEWID()

--DECLARE @ID_Org uniqueidentifier = '9CE38B9F-766E-477B-A9AC-BDA05BBB048C'	
--DECLARE @ID_Admin uniqueidentifier = 'ADADADAD-ADAD-ADAD-ADAD-ADADADADADAD'	

DECLARE
	@ID_Org uniqueidentifier,
	@ID_Buy uniqueidentifier = '055BDBBB-4174-45A5-B916-5C2494B094B8', 
	@ID_Install uniqueidentifier = 'FC72714C-91D1-4BAF-9E0E-580ACDFD15A8',
	@ID_Repair uniqueidentifier = '076DA437-956B-4DAE-9A2A-40703699062F',
	@ID_Relocate uniqueidentifier = '777F1B61-A10C-4956-9F34-6B330FB100A5',
	@ID_Dispose uniqueidentifier = 'BF26155C-58F4-425B-8205-3C2FA2E4BF4E',
	@ID_PreDispose uniqueidentifier = 'F3B08A09-4CC5-4271-8488-094B722E229D',
	@ID_JobD uniqueidentifier = '0795B370-A107-43DE-ACA7-CEDF1AD1F41E',
	@ID_Job uniqueidentifier,
	@ID_JobOp uniqueidentifier,
	@ID_User uniqueidentifier = '6D4E6FCC-8316-4276-8F03-3E7B5585FB6B',
	@ID_Dep uniqueidentifier = '204371B6-3C8D-41C6-B181-A4BB2548AD3B',
	@ID_PC uniqueidentifier = '4327D1AC-B486-4804-9B5A-D6E262162BDA',
	@ID_Monitor	uniqueidentifier = 'A2852F72-2E1F-4FBA-A0D5-8B80E2000740',
	@ID_Stock uniqueidentifier = '3E2E6965-B7D4-4B9F-B937-F1055D99FB77',
	@ID_Scrap uniqueidentifier = '6AECCFE9-9B49-4428-8904-049BE52720FA',
	@ID_PreScrap uniqueidentifier = '16046DE5-9D66-4E3D-94CF-3621955F2255',
	@ID_Workshop uniqueidentifier = '8335D77F-5BCC-4244-AEF5-AD10F4B42A49',
	@ID_Lost uniqueidentifier = 'F0416D8F-8CE0-4E1A-A60B-38076A5BF698',
	@ID_PurposeBuyer uniqueidentifier = '880AD9F7-B6B4-4904-A1C3-F93F893DA23A',
	@ID_PurposeSeller uniqueidentifier = 'A8FAA67B-B52F-4B62-9321-2994F470CC33'

INSERT [Arsenal].[Office].[Purposes](ID, Name, Caption, [Priority])
SELECT @ID_PurposeBuyer, 'Buyer', '�����. ���-���', 1
UNION SELECT @ID_PurposeSeller, 'Seller', '���������', 2

INSERT [Arsenal].[Arsenal].[MovingTypes](ID, Name, Caption)
SELECT @ID_Buy, 'Buy', '������� � ������������ �� �����'
UNION SELECT @ID_Install, 'Install', '��������� �� ������� �����'
UNION SELECT @ID_Repair, 'Repair', '������' 
UNION SELECT @ID_Relocate, 'Relocate', '������� �� ������ ������� �����' 
UNION SELECT @ID_Dispose, 'Dispose', '��������'
UNION SELECT @ID_PreDispose, 'PreDispose', '������������ � ��������'

INSERT [Arsenal].[Arsenal].[EquipmentTypes](ID, Name, Caption)
SELECT @ID_PC, 'PC', '������������ ���������'
UNION SELECT @ID_Monitor, 'Monitor', '�������'

SELECT @ID_Org = ID 
FROM [Arsenal].[Office].[Organisations] 
WHERE Caption = '�������� ������' 

IF @ID_Org IS NULL
	INSERT [Arsenal].[Office].[Organisations](ID, Caption)
	SELECT @ID_Org, '�������� ������'

INSERT [Arsenal].[Office].[Departments](ID, [ID_Organisation], Caption)
SELECT @ID_Dep, @ID_Org, '�������� �������������'

INSERT [Arsenal].[Office].[WorkPlaces](ID, [ID_Department], Name, Caption)
SELECT @ID_Stock, @ID_Dep, 'Stock', '����� ������������'
UNION SELECT @ID_Scrap, @ID_Dep, 'Scrap', '����� ��������'
UNION SELECT @ID_PreScrap, @ID_Dep, 'PreScrap', '����� ��������������� ��� ��������'
UNION SELECT @ID_Workshop, @ID_Dep, 'Workshop', '��������� ����������'
UNION SELECT @ID_Lost, @ID_Dep, 'Lost', '���������������'

DECLARE @JobOps TABLE (ID uniqueidentifier, Code int identity, Managable bit)
DECLARE @OutID TABLE (ID uniqueidentifier)

INSERT @JobOps
SELECT 
	ID, 
	CASE WHEN Caption IN ('SelectPerson', 'SelectMovingType', 'SelectDepartment', 'SelectBill') THEN 0 ELSE 1 END
FROM dbo.Operations
WHERE ProcedureName IN (
	'Arsenal.SelectEquipmentType',
	'Arsenal.InsertEquipmentType',
	--'Arsenal.UpdateEquipmentType',
	--'Arsenal.DeleteEquipmentType',
	'Arsenal.SelectMovingType',
	'Arsenal.SelectEquipment',
	'Arsenal.InsertEquipment',
	'Arsenal.UpdateEquipment',
	'Arsenal.DeleteEquipment',
	'Arsenal.SelectStructure',
	'Arsenal.SelectPart',
	'Arsenal.InsertPart',
	'Arsenal.UpdatePart',
	'Arsenal.DeletePart',
	'Arsenal.SelectMoving',
	'Arsenal.InsertMoving',
	'Arsenal.UpdateMoving',
	'Arsenal.DeleteMoving',
	'Office.SelectWorkPlace',
	'Office.InsertWorkPlace',
	'Office.UpdateWorkPlace',
	'Office.DeleteWorkPlace',
	'Office.SelectDepartment',
	--'Office.InsertDepartment',
	--'Office.UpdateDepartment',
	--'Office.DeleteDepartment',
	--'Office.SelectOrganisation',
	--'Office.InsertOrganisation',
	--'Office.UpdateOrganisation',
	--'Office.DeleteOrganisation',
	'Office.SelectPerson',
	'Arsenal.SelectVendor',
	'Office.SelectBill')

SELECT @ID_Job = ID
FROM [Arsenal].[Office].[Jobs]
WHERE Name = 'Operator'

IF @ID_Job IS NULL
BEGIN
	INSERT @OutID EXEC [Arsenal].[Office].InsertJob 'Operator', '�������� ����� ������������'
	SELECT @ID_Job = ID FROM @OutID
	DELETE @OutID

	UPDATE [Office].[Jobs]
	SET ID = @ID_JobD
	WHERE ID = @ID_Job
END

BEGIN
	INSERT @OutID EXEC [Arsenal].[Office].InsertPerson 'Operator1', @ID_Org, '������', '����', '��������', '19000101', NULL, NULL, 'test' -- has check for duplicates
	SELECT @ID_User = ID FROM @OutID
	DELETE @OutID
	IF @ID_User IS NOT NULL
		EXEC [Arsenal].[Office].InsertPersonJob @ID_User, @ID_JobD, '19000101', NULL -- has check for duplicates
END	 

DECLARE
	@Step int,
	@MaxStep int,
	@Managable bit

SELECT 
	@Step = 1, 
	@MaxStep = COUNT(ID)
FROM @JobOps

WHILE @Step <= @MaxStep
BEGIN
	SELECT 
		@ID_JobOp = ID,
		@Managable = Managable,
		@Step = @Step + 1
	FROM @JobOps
	WHERE Code = @Step
	EXEC [Arsenal].[Office].InsertJobOperation @ID_JobD, @ID_Jobop, '19000101', NULL, @Managable, 0  -- has check for duplicates
END