unit DetailsForm;

interface

uses
// Delphi units
  Windows, SysUtils, Variants, Classes, Controls, Forms, StdCtrls, DB, CommCtrl,
  DBCtrls, Dialogs, XPMan, Buttons, Grids, ExtCtrls, ComCtrls, DBGrids, ImgList,
  Graphics,
// Third Party
  sSkinProvider, sBitBtn, sPanel, sDBLookupComboBox, sDBNavigator,
  sGroupBox, sEdit, RXDBCtrl, acAlphaImageList, DBCtrlsEh, sMemo, sSpeedButton,
// Own units
  Common, ArsenalDB, ChildForms;

type

  NDeviceFilter = (dfAll);

  TEquipmentForm = class(TCommonChild)
  private
    ID_Equipment: TID;
    FStaticTypeFilter: AnsiString;
    ID_StaticType: AnsiString;
  protected
    btnImport, btnReport, btnMovingReport, btnBarcodeReport: TButtonUsed;
    dlgImportOpen: TOpenDialog;
    FilterPanel: TPanelUsed;
    FilterBtns: array[NDeviceFilter] of TButtonUsed;
    procedure ObtainData; override;
    procedure ApplyFilter(Sender: TObject);
    procedure OperationChange(Action: TButtonType; Value: Boolean); override;
    procedure EquipmentChanged(DataSet: TDataSet);
    procedure GridDblClick(Sender: TObject);
    procedure DoImport(Sender: TObject);
    procedure DoReport(Sender: TObject);
    procedure DoBarcodeReport(Sender: TObject);
    procedure DoMovingReport(Sender: TObject);
  public
    procedure SetupStaticType(const ID_Type: TID; const Value: AnsiString);
    procedure Start; override;
    procedure Stop(NeedsSave: Boolean = False); override;
    property StaticTypeFilter: AnsiString read FStaticTypeFilter;
  end;

  TDetailsForm = class(TBaseForm)
  published
    sPanel1: TsPanel;
    ButtonsPanel: TsPanel;
    gbCaptionEditor: TsGroupBox;
    PartsDBNav: TsDBNavigator;
    MovingsDBNav: TsDBNavigator;
    DetailsPanel: TsPanel;
    sPanel4: TsPanel;
    EquipmentTypeList: TsDBLookupComboBox;
    edtInvNumber: TsEdit;
    edtSettings: TsMemo;
    PartsGroupBox: TsGroupBox;
    PartsGrid: TDBGridUsed;
    MovingsGroupBox: TsGroupBox;
    MovingsGrid: TDBGridUsed;
    ControlPanel: TsPanel;
    btnOK: TsBitBtn;
    sSkinProvider1: TsSkinProvider;
    XPManifest1: TXPManifest;
    sBitBtn2: TsBitBtn;
    edtCaption: TsEdit;
    cbbVendor: TsDBLookupComboBox;
    cbbBill: TsDBLookupComboBox;
    edtVisualN: TsEdit;
    edtBarcode: TsEdit;
    edtPrice: TsEdit;
    edtLifeTime: TsEdit;
    pnlPart: TsPanel;
    cbbParts: TsDBLookupComboBox;
    bbnInstallPart: TsSpeedButton;
    procedure bbnInstallPartClick(Sender: TObject);
    procedure MovingsGridColEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    FMode: TJobMode;
    FID: TID;
    DSParts, DSMovings: TDataSet;
    SrcParts, SrcMovings: TDataSource;
    Btns: array[TSpecAction] of TButtonUsed;
    ID_BuyMovingType: TID;
  protected
    DateEditor: TDaterUsed;
    procedure MovingPost(DataSet: TDataSet);
    procedure DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure MovingsGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure MovingsGridColExit(Sender: TObject);
    procedure SetupButtons;
    procedure ApplyAction(Sender: TObject);
    procedure SetBtnEnabled(Btn: TSpecAction; DoEnable: Boolean);
    procedure FormResize(Sender: TObject); override;
    function CanShowDateEditor(WithBrowse: Boolean = True): Boolean;
    procedure ObtainData; override;
    procedure MovingsGridGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure RequeryParts;
    procedure RequeryAvailParts;
    procedure RequeryMovings;
  public
    function Open(AMode: TJobMode; AID: TID): TModalResult;
    property Mode: TJobMode read FMode;
    property ID: TID read FID;
  end;

implementation

{$R *.dfm}

uses
// Delphi
  DateUtils,
//Own
  MainForm, Reports;

const
  SDetails = '����������� �� ������������';
  SInstall = '���������';

  FilterText: array[NDeviceFilter] of string =
    ('��'{, '��� ��.'{, '�����', '�������'});

{$REGION ' TDetailsForm '}

procedure TDetailsForm.MovingPost(DataSet: TDataSet);
var
  BM: TBookmark;
  IsNew: Boolean;
begin
  {
  BM := DSMovings.GetBookmark;
  DSMovings.AfterPost := nil;
  DSMovings.First;
  IsNew := False;
  while not DSMovings.Eof do
  begin
    IsNew := SameID(DSMovings.FieldValues['ID'], ID_BuyMovingType) and
      SameID(VarToStrDef(DSMovings.FieldValues['ID_Accepter'], ID_NULL), ID_NULL);
    if IsNew then
      Break;
    DSMovings.Next;
  end;
//  SetBtnEnabled(saAcceptDevice, IsNew);
  GotoBookmark(DSMovings, BM);
  DSMovings.AfterPost := MovingPost;
  }
end;

procedure TDetailsForm.bbnInstallPartClick(Sender: TObject);
  function V(Name: string): Variant;
  begin
    Result := cbbParts.ListSource.DataSet.FieldValues[Name];
  end;
begin
  if not VarIsNull(cbbParts.KeyValue) then
  begin
    Manager.Update(xePart, [cbbParts.KeyValue, ID, V('ID_EquipmentType'), V('ID_Bill'),
      V('ID_Vendor'), V('Product'), V('VisualNumber'), V('Price'), V('Settings')]);
    RequeryParts;
    RequeryAvailParts;
  end;
end;

function TDetailsForm.CanShowDateEditor(WithBrowse: Boolean = True): Boolean;
var
  States: set of TDataSetState;
begin
  States := [dsInsert, dsEdit];
  if WithBrowse then
    States := States + [dsBrowse];
  Result := (DSMovings.State in States) and (MovingsGrid.SelectedField.DataType = ftDateTime);
end;

procedure TDetailsForm.MovingsGridColEnter(Sender: TObject);
var
  Rect: TRect;
begin
  if CanShowDateEditor then
  begin
    Rect := TCustomDrawGrid(MovingsGrid).CellRect(MovingsGrid.Col, MovingsGrid.Row);
    DateEditor.Left := Rect.Left + MovingsGrid.Left + 1;
    DateEditor.Top := Rect.Top + MovingsGrid.Top + 1;
    DateEditor.Width := Rect.Right - Rect.Left + 2;
//    DateEditor.Value := MovingsGrid.SelectedField.Value;// TernOP(MovingsGrid.SelectedField.IsNull, MovingsGrid.SelectedField.AsDateTime, Now);
    DateEditor.DataSource := SrcMovings;
    DateEditor.DataField := MovingsGrid.SelectedField.FieldName;
//    MovingsGrid.InplaceEditor := DateEditor;
    DateEditor.Show;
    DateEditor.BringToFront;
  end;
end;

procedure TDetailsForm.MovingsGridColExit(Sender: TObject);
begin
  if CanShowDateEditor(True)then
  begin
    DateEditor.DataSource := nil;
    DateEditor.Hide;
//    MovingsGrid.SelectedField.Value := DateEditor.Value;
  end;
end;

procedure TDetailsForm.MovingsGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin

end;

procedure TDetailsForm.MovingsGridGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
//  if not Highlight and Assigned(Field) and AnsiSameText(Field.FieldName, 'MovingType') then
//    Background := DataSet.FieldByName('Color').AsInteger;
end;

procedure TDetailsForm.FormCreate(Sender: TObject);
begin
  Caption := SDetails;
  gbCaptionEditor.Caption := Russian('Settings');
  PartsGroupBox.Caption := Russian('Parts');
  MovingsGroupBox.Caption := Russian('Movings');
  edtInvNumber.BoundLabel.Caption := Russian('InvNumber');
  edtVisualN.BoundLabel.Caption := Russian('VisualNumber');
  edtBarcode.BoundLabel.Caption := Russian('Barcode');
  edtLifeTime.BoundLabel.Caption := Russian('Lifetime');
  edtPrice.BoundLabel.Caption := Russian('Price');
  edtCaption.BoundLabel.Caption := Russian('Product');
  EquipmentTypeList.BoundLabel.Caption := Russian('Type');
  cbbVendor.BoundLabel.Caption := Russian('Vendor');
  cbbBill.BoundLabel.Caption := Russian('Bill');
  bbnInstallPart.Caption := SInstall;
  DateEditor := TDaterUsed.Create(Self);
  DateEditor.Parent := {MovingsGrid;//}MovingsGroupBox;
  DateEditor.Hide;
  DateEditor.Kind := dtkDateTimeEh;
  DateEditor.EditFormat := DateTimeFormat;
end;

procedure TDetailsForm.FormResize(Sender: TObject);
begin
  inherited;
  DateEditor.Hide;
end;

procedure TDetailsForm.ObtainData;
begin
  FDataSet := Manager.GetData(xeEquipment);
end;

function TDetailsForm.Open(AMode: TJobMode; AID: TID): TModalResult;
var
  CanInsert, CanUpdate: Boolean;
  DSWorkplaces, DSMovingTypes: TDataSet;
  ID_Stock: TID;
begin
  Result := mrNone;

  DSMovingTypes := Manager.GetData(xeMovingType);
  try
    DSMovingTypes.Locate('Name', 'Buy', [loCaseInsensitive]);
    ID_BuyMovingType := DSMovingTypes.FieldValues['ID'];
    if ID_BuyMovingType = ID_NULL then
      Abort;
  except
    ShowMessage('�� ������� "���������������" �������� !');
    Close;
    Exit;
  end;

  DSWorkplaces := Manager.GetData(xeWorkplace);
  try
    DSWorkplaces.Locate('Name', 'Stock', [loCaseInsensitive]);
    ID_Stock := DSWorkplaces.FieldValues['ID'];
    if ID_Stock = ID_NULL then
      Abort;
  except
    ShowMessage('�� ������� "���������" ������� ����� !');
    Close;
    Exit;
  end;

  Start;
  FID := AID;
  FMode := AMode;
  //edtInvNumber.Enabled := Mode <> jmEdit;

  ArsenalInstance.Switch(xeEquipment);
  DataSource := ArsenalInstance.DS;
  FDataSet := DataSource.DataSet;
//  ObtainData;

  Lookup(DataSet, EquipmentTypeList, xeEquipmentType, 'ID_EquipmentType', 'Caption', []);
  Lookup(DataSet, cbbVendor, xeVendor, 'ID_Vendor', 'Caption', []);
  Lookup(DataSet, cbbBill, xeBill, 'ID_Bill', 'Caption', []);

  edtSettings.Text := VarToStrDef(DataSet.FieldValues['Settings'], '');
  edtCaption.Text := VarToStrDef(DataSet.FieldValues['Product'], '');
  edtBarcode.Text := VarToStrDef(DataSet.FieldValues['Barcode'], '');
  edtInvNumber.Text := VarToStrDef(DataSet.FieldValues['InvNumber'], '');
  edtVisualN.Text := VarToStrDef(DataSet.FieldValues['VisualNumber'], '');
  edtPrice.Text := VarToStrDef(DataSet.FieldValues['Price'], '');
  edtLifeTime.Text := VarToStrDef(DataSet.FieldValues['LifeTime'], '');

  if Mode = jmAdd then
    Manager.Insert(xeMoving, [ID_Stock, ID_BuyMovingType, ID, ID_NULL,
      Manager.Portfolio[pdID], IncMinute(Now, -1)]);

  RequeryAvailParts;
  RequeryParts;
  RequeryMovings;

  MovingsGrid.OnColExit := MovingsGridColExit;
  //MovingsGrid.DefaultDrawing := False;
//  MovingsGrid.OnDrawColumnCell := MovingsGridDrawColumnCell;

  PartsDBNav.DataSource := SrcParts;
  MovingsDBNav.DataSource := SrcMovings;

  CanInsert := ArsenalInstance.EntityActionAccessible[xePart, daInsert];
  CanUpdate := ArsenalInstance.EntityActionAccessible[xePart, daUpdate];
  ShowDBNavButton(PartsDBNav, nbInsert, CanInsert);
  ShowDBNavButton(PartsDBNav, nbEdit, CanUpdate);
  ShowDBNavButton(PartsDBNav, nbPost, CanInsert or CanUpdate);
  ShowDBNavButton(PartsDBNav, nbDelete, ArsenalInstance.EntityActionAccessible[xePart, daDelete]);
  
  CanInsert := ArsenalInstance.EntityActionAccessible[xeMoving, daInsert];
  CanUpdate := ArsenalInstance.EntityActionAccessible[xeMoving, daUpdate];
  ShowDBNavButton(MovingsDBNav, nbInsert, CanInsert);
  ShowDBNavButton(MovingsDBNav, nbEdit, CanUpdate);
  ShowDBNavButton(MovingsDBNav, nbPost, CanInsert or CanUpdate);
  ShowDBNavButton(MovingsDBNav, nbDelete, ArsenalInstance.EntityActionAccessible[xeMoving, daDelete]);

  PartsDBNav.BeforeAction := DBNavBeforeAction;
  MovingsDBNav.BeforeAction := DBNavBeforeAction;
  PartsDBNav.Tag := Ord(xePart);
  MovingsDBNav.Tag := Ord(xeMoving);

  MovingsGrid.OnGetCellParams := MovingsGridGetCellParams;
  SetupGrid(PartsGrid);
  SetupGrid(MovingsGrid);
  SetupButtons;

//  DSMovings.AfterPost := MovingPost;
  Result := ShowModal;
//  DSMovings.AfterPost := nil;
end;

procedure TDetailsForm.RequeryAvailParts;
begin
  ArsenalInstance.Ally := 1;
  Lookup(DataSet, cbbParts, xePart, '', 'Caption', [WrongGUID], False);
  ArsenalInstance.Ally := 0;
end;

procedure TDetailsForm.RequeryMovings;
begin
  if Assigned(DSMovings) then
    DSMovings.DisableControls;
  DSMovings := Manager.GetData(xeMoving, [ID_NULL, ID_NULL, ID]);
  SrcMovings := ArsenalInstance.DS;
  ModifyDataSet(DSMovings, xeMoving);
  HideField(DSMovings, 'Equipment');
  HideField(DSMovings, 'Part');
  if ArsenalInstance.EntityPersonalized[xePerson] then
    case Mode of
      jmAdd: DSMovings.FieldByName('Person').Visible := True;
      jmEdit: DSMovings.FieldByName('Person').ReadOnly := True;
    end;
  MovingsGrid.DataSource := SrcMovings;
  DSMovings.EnableControls;
  ManageColumns(DSMovings, MovingsGrid);
  Resize;
end;

procedure TDetailsForm.RequeryParts;
begin
  if Assigned(DSParts) then
    DSParts.DisableControls;
  ArsenalInstance.Ally := 2;
  DSParts := Manager.GetData(xePart, [ID]);
  SrcParts := ArsenalInstance.DS;
  ModifyDataSet(DSParts, xePart);
  ArsenalInstance.Ally := 0;
  DSParts.FieldByName('Equipment').Visible := False;
  DSParts.FieldByName('Caption').Visible := False;
  DSParts.FieldByName('Installed').Visible := False;
  PartsGrid.DataSource := SrcParts;
  DSParts.EnableControls;
  ManageColumns(DSParts, PartsGrid);
  Resize;
end;

procedure TDetailsForm.ApplyAction(Sender: TObject);
var
  Btn: TButtonUsed absolute Sender;
  BtnAction: TSpecAction;
begin
  if not (Sender is TButtonUsed) then
    Exit;
  BtnAction := TSpecAction(Btn.Tag);
  Manager.SpecAction(BtnAction, [ID, Manager.Portfolio[pdID]], True);
  MovingPost(DSMovings);
  RequeryMovings;
  Resize;
end;

procedure TDetailsForm.DBNavBeforeAction(Sender: TObject; Button: TNavigateBtn);
var
  DBNav: TsDBNavigator absolute Sender;
  Entity: TEntity;
  DS: TDataSet;
  function V(Name: string): Variant;
  begin
    if (Name = 'ID_Person') and ArsenalInstance.EntityPersonalized[xePerson]
      and (Mode = jmAdd) then
      Result := Manager.Portfolio[pdID]
    else
      Result := DS.FieldValues[Name];
  end;
begin
  if not (Sender is TsDBNavigator) then
    Exit;
  if not (Button in [nbPost, nbDelete]) then
    Exit;
  DS := DBNav.DataSource.DataSet;
  Entity := TEntity(DBNav.Tag);
  if (Entity = xePart) and (Button = nbDelete) then
  begin
    Manager.Update(Entity, [V('ID'), ID_Null, V('ID_EquipmentType'), V('ID_Bill'),
      V('ID_Vendor'), V('Product'), V('VisualNumber'), V('Price'), V('Settings')]);
    RequeryAvailParts;
    RequeryParts;
  end;
  if Entity = xePart then
    if DS.State = dsInsert then
      Manager.Insert(Entity, [ID, V('ID_EquipmentType'), V('ID_Bill'),
        V('ID_Vendor'), V('Product'), V('VisualNumber'), V('Price'), V('Settings')])
    else if DS.State = dsEdit then
      Manager.Update(Entity, [V('ID'), ID, V('ID_EquipmentType'), V('ID_Bill'),
        V('ID_Vendor'), V('Product'), V('VisualNumber'), V('Price'), V('Settings')]);
  if Entity = xeMoving then
    if DS.State = dsInsert then
      Manager.Insert(Entity, [V('ID_Workplace'), V('ID_MovingType'),
        ID, ID_NULL, V('ID_Person'), V('Date'), V('Note')])
    else if DS.State = dsEdit then
      if Manager.Update(Entity, [V('ID'), V('ID_Workplace'), V('ID_MovingType'),
        ID, ID_NULL, V('ID_Person'), V('Date'), V('Note')]) <> 0
      then
        ShowMessage('������ ������� �������� ������������, ��������, ������ � ��������� �����');
  if Entity = xePart then
    RequeryParts;
  if Entity = xeMoving then
    RequeryMovings;
  Resize;
  Abort; // cancel standard dbnav action
end;

procedure TDetailsForm.btnOKClick(Sender: TObject);
begin
  if CondHint(edtCaption, '������� ������') or CondHint(edtPrice, '������� ����')
    or CondHint(edtVisualN, '������� ���������� �����')
    or CondHint('������� ���', VarIsNull(EquipmentTypeList.KeyValue))
  then
    Exit;
  ModalResult := mrOk;
end;

procedure TDetailsForm.SetBtnEnabled(Btn: TSpecAction; DoEnable: Boolean);
begin
  if Assigned(Btns[Btn]) then
    Btns[Btn].Enabled := DoEnable; 
end;

procedure TDetailsForm.SetupButtons;
const
  BtnCaptions: array[saNone..saNone] of string =
    ('�������');
var
  BtnAction: TSpecAction;
begin
  ButtonsPanel.Visible := False;
  for BtnAction := Low(BtnCaptions) to High(BtnCaptions) do
    if (SpecActionToEntity[BtnAction] = xeMoving) and ArsenalInstance.SpecActionAccessible[BtnAction] then
    begin
      ButtonsPanel.Visible := True;
      Btns[BtnAction] := TButtonUsed.Create(Self);
      Btns[BtnAction].Parent := ButtonsPanel;
      Btns[BtnAction].AlignWithMargins := True;
      Btns[BtnAction].Margin := 2;
      Btns[BtnAction].Left := Btns[BtnAction].Parent.Width;
      Btns[BtnAction].Align := alLeft;
      Btns[BtnAction].Width := 65;
      Btns[BtnAction].Tag := Ord(BtnAction);
      Btns[BtnAction].Caption := BtnCaptions[BtnAction];
//      Btns[TSpecAction].AllowAllUp := TSpecAction in [btFilter, btInsert, btUpdate];
//      Btns[TSpecAction].GroupIndex := Ord(Btns[TSpecAction].AllowAllUp) * (Ord(TSpecAction) + 1);
      Btns[BtnAction].OnClick := ApplyAction;
//      Btns[TSpecAction].Images := MainFormInstance.ButtonIcons;
//      Btns[TSpecAction].ImageIndex := Ord(TSpecAction);
    end;
end;

{$ENDREGION ' TDetailsForm '}

{$REGION ' TEquipmentForm '}

procedure TEquipmentForm.ApplyFilter(Sender: TObject);
var
  Btn: TButtonUsed absolute Sender;
  F: NDeviceFilter;
  // filter accumulator
  DF: set of NDeviceFilter;
  function FilterValue(AFilter: NDeviceFilter; AValue: Variant): Variant;
  begin
    Result := TernOP(AFilter in DF, AValue, ID_NULL);
    Exclude(DF, AFilter);
  end;
begin
  if not (Sender is TButtonUsed) then
    Exit;
  DF := [];
  for F := Low(NDeviceFilter) to High(NDeviceFilter) do
  if FilterBtns[F].Down then
    Include(DF, F); // accumulate all applied filters
  FDataSet := Manager.GetData(Entity,
  [
    ID_NULL, // State
    StaticTypeFilter, // Type
    ID_NULL // Device
//    FilterValue(dfMyPub, Manager.Portfolio[pdLastName]), // Publisher
//    FilterValue(dfMyAcc, Manager.Portfolio[pdLastName]) // Accepter
    {FilterValue(dfStateNew, ID_NewFlow), // ID_State
    FilterValue(dfShowDone, True)}
  ]);
  if DF <> [] then // if some of accumulated filters are not applied - error
    NotImplError;
  ModifyDataSet(DataSet, Entity);
  ManageColumns(DataSet, DBGrid1);
  Resize;
end;

procedure TEquipmentForm.DoBarcodeReport(Sender: TObject);
begin
  Reports.BarcodeReport;
end;

procedure TEquipmentForm.DoImport(Sender: TObject);
begin
  if dlgImportOpen.Execute then
  begin
  
  end;
end;

procedure TEquipmentForm.DoMovingReport(Sender: TObject);
begin
  if not SameID(ID_Equipment, ID_NULL) then
    MovingReport(ID_Equipment);
end;

procedure TEquipmentForm.DoReport(Sender: TObject);
begin
  if not SameID(ID_Equipment, ID_NULL) then
    EquipReport(ID_Equipment);
end;

procedure TEquipmentForm.EquipmentChanged(DataSet: TDataSet);
begin
  ArsenalInstance.Switch(xeEquipment);
  ID_Equipment := ArsenalInstance.CurrentID;
end;

procedure TEquipmentForm.GridDblClick(Sender: TObject);
begin
  if DBGrid1.ScreenToClient(Mouse.CursorPos).y > TGridUsed(DBGrid1).RowHeights[0] then
    IsUpdating := True;
end;

procedure TEquipmentForm.OperationChange(Action: TButtonType; Value: Boolean);
var
  Details: TDetailsForm;
  Ico: TIcon;
  Mode: TJobMode;
  d: Double;
  n: Integer;
begin
  inherited;
  SetBtnEnabled(btApply, IsUpdating or Inserting);
  SetBtnEnabled(btClear, IsUpdating or Inserting or Filtering);
  if Assigned(FilterPanel) then
    FilterPanel.Visible := Value and Filtering;
  if Filtering then
    Exit;
  if (Action = btFilter) and not Value and not IsUpdating then
  begin
    Requery;
    ManageColumns(DataSet, DBGrid1);
    Resize;
    Exit;
  end;
  if not Value then
    Exit;
  Details := TDetailsForm.Create(Application);
  begin
    Ico := TIcon.Create;
    MainFormInstance.TreeIcons.GetIcon(Ord(Entity), Ico);
    Details.Icon := Ico;
    FreeAndNil(Ico);
  end;
  if Inserting then
  begin
    ID_Equipment := Manager.Insert(xeEquipment, []);
    ManageColumns(DataSet, DBGrid1);
  end;
  Mode := TernOp(Inserting, jmAdd, TernOp(IsUpdating, jmEdit, jmView));
  if Details.Open(Mode, ID_Equipment) = mrOk then
  begin
    with Details do
      if Manager.Update(xeEquipment, [ID_Equipment, EquipmentTypeList.KeyValue,
        cbbBill.KeyValue, cbbVendor.KeyValue, edtCaption.Text,
        TernOp(edtInvNumber.Text <> '', edtInvNumber.Text, ID_NULL),
        TernOp(edtVisualN.Text <> '', edtVisualN.Text, ID_NULL),
        TernOp(TryStrToInt(edtBarcode.Text, n), edtBarcode.Text, ID_NULL),
        TernOp(TryStrToFloat(edtPrice.Text, d), edtPrice.Text, ID_NULL),
        TernOp(TryStrToInt(edtLifetime.Text, n), edtLifetime.Text, ID_NULL),
        edtSettings.Text]) <> 0
      then
        if Inserting then
        begin
          ShowMessage(Format(SFErrorIn, ['�������']) + #13#10 + SRepeatedNumber);
          Manager.Delete(xeEquipment, [ID_Equipment]);
        end;
  end
  else
    if Inserting then
      Manager.Delete(xeEquipment, [ID_Equipment]);
  ClearClick(nil);
  Details.Free;
  Requery;
  ManageColumns(DataSet, DBGrid1);
  Resize;
end;

procedure TEquipmentForm.ObtainData;
begin
  FDataset := Manager.GetData(Entity, [ID_NULL, StaticTypeFilter, ID_NULL, ID_NULL, ID_NULL, ID_NULL]);
end;

procedure TEquipmentForm.SetupStaticType(const ID_Type: TID; const Value: AnsiString);
begin
  ID_StaticType := ID_Type;
  FStaticTypeFilter := Value;
  Requery;
end;

procedure TEquipmentForm.Start;
var
//  BtnType: TButtonType;
  APanel: TPanelUsed;
  DF: NDeviceFilter;
//  DS: TDataSet;
begin
  inherited;
  DataSet.AfterScroll := EquipmentChanged;
  ManageColumns(DataSet, DBGrid1);
  DBGrid1.DataSource := DataSource;
  APanel := TPanelUsed.Create(Self);
  APanel.Parent := Panel1;
  APanel.Align := alBottom;
  Panel1.Height := 130;

  btnImport := TButtonUsed.Create(Self);
  btnImport.Parent := APanel;
  btnImport.Caption := '������';
  btnImport.Width := 90;
  btnImport.Align := alRight;
  btnImport.Images := MainFormInstance.ButtonIcons;
  btnImport.ImageIndex := 6;
  btnImport.OnClick := DoImport;
  
  btnMovingReport := TButtonUsed.Create(Self);
  btnMovingReport.Parent := APanel;
  btnMovingReport.Caption := ReportInfo[Ord(saMovingReport) - 1].Name;
  btnMovingReport.Width := 130;
  btnMovingReport.Align := alRight;
  btnMovingReport.Images := MainFormInstance.TreeIcons;
  btnMovingReport.ImageIndex := Ord(xeReport);
  btnMovingReport.OnClick := DoMovingReport;

  btnReport := TButtonUsed.Create(Self);
  btnReport.Parent := APanel;
  btnReport.Caption := ReportInfo[Ord(saEquipmentReport) - 1].Name;
  btnReport.Width := 130;
  btnReport.Align := alRight;
  btnReport.Images := MainFormInstance.TreeIcons;
  btnReport.ImageIndex := Ord(xeReport);
  btnReport.OnClick := DoReport;

  btnBarcodeReport := TButtonUsed.Create(Self);
  btnBarcodeReport.Parent := APanel;
  btnBarcodeReport.Caption := ReportInfo[Ord(saBarcodeReport) - 1].Name;
  btnBarcodeReport.Width := 130;
  btnBarcodeReport.Align := alRight;
  btnBarcodeReport.Images := MainFormInstance.TreeIcons;
  btnBarcodeReport.ImageIndex := Ord(xeReport);
  btnBarcodeReport.OnClick := DoBarcodeReport;

  dlgImportOpen := TOpenDialog.Create(Self);
  dlgImportOpen.Filter := '����� ������������� ����������|*.*';
  FilterPanel := TPanelUsed.Create(Self);
  FilterPanel.Parent := APanel;
  FilterPanel.Align := alClient;
  FilterPanel.Hide;
  for DF := Low(NDeviceFilter) to High(NDeviceFilter) do
  begin
    FilterBtns[DF] := TButtonUsed.Create(Self);
    FilterBtns[DF].Parent := FilterPanel;
    FilterBtns[DF].AlignWithMargins := True;
    FilterBtns[DF].Margin := 2;
    FilterBtns[DF].Left := FilterBtns[DF].Parent.Width;
    FilterBtns[DF].Align := alLeft;
    FilterBtns[DF].Width := 65;
    FilterBtns[DF].Caption := FilterText[DF];
    FilterBtns[DF].Tag := Ord(DF);
    FilterBtns[DF].OnClick := ApplyFilter;
    FilterBtns[DF].Images := MainFormInstance.FilterIcons;
    FilterBtns[DF].ImageIndex := Ord(DF);
    FilterBtns[DF].GroupIndex := 1;
    FilterBtns[DF].AllowAllUp := True;
  end;
//  SetupButtons;
  DBGrid1.OnDblClick := GridDblClick;
  ClearClick(nil);
  Resize;
end;

procedure TEquipmentForm.Stop(NeedsSave: Boolean = False);
begin
  inherited;
  DataSet.AfterScroll := nil;
end;

{$ENDREGION ' TEquipmentForm '}

end.

