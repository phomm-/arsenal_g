unit AboutBox;

interface

uses
// Delphi
  Windows, Classes, Forms, Controls, StdCtrls, ExtCtrls,
// ThirdParty
  sButton, sLabel, sPanel;

type
  TAboutBox = class(TForm)
    Panel1: TsPanel;
    OKButton: TsButton;
    ProgramIcon: TImage;
    ProductName: TsLabel;
    Version: TsLabel;
    Copyright: TsLabel;
    Comments: TsLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}

end.
 
