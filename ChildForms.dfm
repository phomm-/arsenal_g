object BaseChildForm: TBaseChildForm
  Left = 197
  Top = 117
  ClientHeight = 386
  ClientWidth = 623
  Color = clBtnFace
  Constraints.MinHeight = 424
  Constraints.MinWidth = 639
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Times New Roman'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 16
  object Splitter1: TsSplitter
    Left = 0
    Top = 365
    Width = 623
    Height = 2
    Cursor = crVSplit
    Align = alBottom
    SkinData.SkinSection = 'SPLITTER'
    ExplicitTop = 277
  end
  object DBGrid1: TRxDBGrid
    Left = 0
    Top = 86
    Width = 623
    Height = 279
    Align = alClient
    Color = clWhite
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Times New Roman'
    TitleFont.Style = []
    TitleButtons = True
  end
  object Status: TsStatusBar
    Left = 0
    Top = 367
    Width = 623
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SkinData.SkinSection = 'STATUSBAR'
  end
  object Panel1: TsPanel
    Left = 0
    Top = 0
    Width = 623
    Height = 86
    Align = alTop
    Constraints.MinHeight = 86
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object Panel2: TsPanel
      Left = 1
      Top = 1
      Width = 621
      Height = 42
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
    end
  end
  object XPManifest1: TXPManifest
    Left = 216
    Top = 40
  end
  object sSkinProvider1: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'Tahoma'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 232
    Top = 152
  end
end
