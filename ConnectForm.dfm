object ConnectForm: TConnectForm
  Left = 802
  Top = 315
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'FrmConnect'
  ClientHeight = 321
  ClientWidth = 358
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Times New Roman'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    358
    321)
  PixelsPerInch = 96
  TextHeight = 16
  object lblLoad: TLabel
    Left = 7
    Top = 301
    Width = 41
    Height = 16
    Caption = 'lblLoad'
    Visible = False
  end
  object BOk: TsBitBtn
    Left = 69
    Top = 270
    Width = 80
    Height = 26
    Anchors = [akTop]
    TabOrder = 0
    OnClick = BOkClick
    Kind = bkOK
    SkinData.SkinSection = 'BUTTON'
  end
  object BClose: TsBitBtn
    Left = 190
    Top = 270
    Width = 80
    Height = 26
    Anchors = [akTop]
    Cancel = True
    Caption = '&Close'
    ModalResult = 3
    TabOrder = 1
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
      F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
      000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
      338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
      45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
      3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
      F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
      000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
      338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
      4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
      8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
      333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
      0000}
    NumGlyphs = 2
    SkinData.SkinSection = 'BUTTON'
  end
  object chkShowPswd: TsCheckBox
    Left = 31
    Top = 243
    Width = 114
    Height = 22
    Caption = 'chkShowPswd'
    Anchors = [akTop]
    TabOrder = 2
    OnClick = chkShowPswdClick
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object chkTrusted: TsCheckBox
    Left = 190
    Top = 170
    Width = 93
    Height = 22
    Caption = 'chkTrusted'
    Anchors = [akTop]
    TabOrder = 3
    Visible = False
    OnClick = chkTrustedClick
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object chkShowFullAuth: TsCheckBox
    Left = 190
    Top = 242
    Width = 130
    Height = 22
    Caption = 'chkShowFullAuth'
    Anchors = [akTop]
    TabOrder = 4
    OnClick = chkShowFullAuthClick
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object pbLoad: TProgressBar
    Left = 62
    Top = 301
    Width = 273
    Height = 16
    TabOrder = 5
    Visible = False
  end
  object XPManifest1: TXPManifest
    Left = 120
    Top = 157
  end
end
