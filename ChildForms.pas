unit ChildForms;

interface

uses
// Delphi units
  Windows, Classes, SysUtils, Forms, Controls, StdCtrls, Dialogs, DB, Variants,
  Grids, XPMan, ComCtrls, DBGrids, Graphics, ExtCtrls, Contnrs,
// Third Party
  sPanel, RXDBCtrl, sStatusBar, sSplitter, sSkinProvider,
// Own units
  ArsenalDB, Common;

type

  TButtonType = (btFilter, btInsert, btUpdate, btApply, btClear, btDelete);

  TColumnManager = class(TObject)
  private
    FDataSet: TDataSet;
    FGrid: TDBGridUsed;
    FVisibleCols: Integer;
    FColWidths: array of Integer;
    function GetColWidths(Index: Integer): Integer;
  public
    property DataSet: TDataSet read FDataSet;
    property Grid: TDBGridUsed read FGrid;
    property ColWidths[Index: Integer]: Integer read GetColWidths;
    property VisibleCols: Integer read FVisibleCols;
  end;

  TBaseForm = class(TForm)
  private
    FColumnManagers: TObjectList;
    function GetManagers(Entity: TEntity): TColumnManager;
  protected
    FLookupField: string;
    /// <remarks>
    ///  do not touch, only for ancestors
    /// </remarks>
    FDataSet: TDataSet;
    DataSource: TDataSource;
    procedure ObtainData; virtual;
    property DataSet: TDataSet read FDataSet;
    procedure FormResize(Sender: TObject); virtual;
    procedure ManageColumns(ADataSet: TDataSet; AGrid: TDBGridUsed); virtual;
    procedure ResizeColumns(ADataSet: TDataSet); virtual;
    procedure ModifyDataSet(ADataSet: TDataSet; Entity: TEntity);
    procedure AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity); virtual;
    procedure SpecModifying(ADataSet: TDataSet; FldCnt: Integer;
      FieldName: string; IsLookup: Boolean = False); virtual;
    property Managers[Entity: TEntity]: TColumnManager read GetManagers; default;
    procedure Start; virtual;
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GridTitleClick(Sender: TObject; ACol: Longint; Field: TField);
    procedure GetBtnParams(Sender: TObject; Field: TField; AFont: TFont;
      var Background: TColor; var SortMarker: TSortMarker; IsDown: Boolean);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

  TBaseChildForm = class(TBaseForm)
    Panel2: TsPanel;
  private
    FFiltering: Boolean;
    FInserting: Boolean;
    FUpdating: Boolean;
    FEntity: TEntity;
    FOnClose2: TNotifyEvent;
  protected
    Btns: array[TButtonType] of TButtonUsed;
    Changed: Boolean;
    procedure SetBtnEnabled(Btn: TButtonType; DoEnable: Boolean);
    procedure SetBtnDown(Btn: TButtonType; DoDown: Boolean);
    //property Entity: TEntity read FEntity write SetEntity;
    procedure ObtainData; override;
    procedure SetEntity(const Value: TEntity);
    procedure AfterOpen(ADataSet: TDataSet); virtual;
    procedure FormResize(Sender: TObject); override;
    procedure SetFiltering(const Value: Boolean);
    procedure SetInserting(const Value: Boolean);
    procedure SetUpdating(const Value: Boolean);
    procedure FilterClick(Sender: TObject);
    procedure InsertClick(Sender: TObject);
    procedure UpdateClick(Sender: TObject);
    procedure ApplyClick(Sender: TObject);
    procedure DeleteClick(Sender: TObject);
    procedure DoInsert; virtual;
    procedure DoUpdate; virtual;
    procedure CommonClear; virtual;
    function IsInputValid: Boolean; virtual;
    procedure OperationChange(Action: TButtonType; Value: Boolean); virtual;
    procedure Prepare; virtual;
    procedure ClearFilter;
    procedure ClearClick(Sender: TObject);
    procedure ClearInsert;
    procedure SetupButtons;
    property Filtering: Boolean read FFiltering write SetFiltering;
    property Inserting: Boolean read FInserting write SetInserting;
    property IsUpdating: Boolean read FUpdating write SetUpdating;
  public
    procedure Start; override;
    procedure Stop(NeedsSave: Boolean = False); virtual;
    property Entity: TEntity read FEntity;
    property OnClose2: TNotifyEvent read FOnClose2 write FOnClose2;
  published
    Status: TsStatusBar;
    DBGrid1: TDBGridUsed;
    Panel1: TsPanel;
    Splitter1: TsSplitter;
    XPManifest1: TXPManifest;
    sSkinProvider1: TsSkinProvider;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Requery;
  end;

  TCommonChild = class(TBaseChildForm)
  private
    Combos: array of TComboUsed;
    Daters: array of TDaterUsed;
  protected
    procedure SwitchSGEditing(Value: Boolean);
    procedure ClearSG;
    procedure DoInsert; override;
    procedure InsertCmd; virtual;
    procedure DoUpdate; override;
    procedure UpdateCmd; virtual;
    procedure CommonClear; override;
    function IsInputValid: Boolean; override;
    procedure SGKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SetupFilter;
    procedure ComboCloseUp(Sender: TObject);
    procedure DaterCloseUp(Sender: TObject);
    procedure DaterClick(Sender: TObject);
    procedure SGDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure EntitleGrid;
    procedure OperationChange(Action: TButtonType; Value: Boolean); override;
    procedure FormResize(Sender: TObject); override;
    procedure SpecModifying(ADataSet: TDataSet; FldCnt: Integer;
      FieldName: string; IsLookup: Boolean = False); override;
  public
    SG: TGridUsed;
    procedure Start; override;
    procedure Stop(NeedsSave: Boolean = False); override;
  end;

  TBaseFormClass = class of TBaseChildForm;

  TNestedItem = record
    Name: string;
    FormClass: TBaseFormClass;
    Entity: TEntity;
  end;

  TItemsArray = array of TNestedItem;

  TFormMan = class(TObject)
  private
    FItems: TItemsArray;
    FForms: array[TEntity] of TBaseChildForm;
    function GetForm(Index: TEntity): TBaseChildForm;
  protected
  public
    constructor Create;
    property ChildForms[Index: TEntity]: TBaseChildForm read GetForm;
    function BuildForm(AOwner: TComponent; Entity: TEntity): TBaseChildForm;
    property Items: TItemsArray read FItems;
  end;

  TAbstractDecor = class(TObject)
    class procedure Populate(Ctl: TWinControl; FormMan: TFormMan); virtual;
  end;

  TTreeViewDecor = class(TAbstractDecor)
    class procedure Populate(Ctl: TWinControl; FormMan: TFormMan); override;
  end;

const
  HeaderRowNumber = 0;
  DataRowNumber = 1;
  MinWd = 90;
  MaxBtnWd = 90;
  BtnWd = 65;

implementation

uses
// Delphi units
  Math, ADODB,
// Own units
  MainForm, SpecialForms, DetailsForm, PartDetailsForm, StructureForm,
  InventarisationForm, Reports;

{$R *.dfm}

const
  BtnCaptions: array[TButtonType] of string =
  ('������', '��������', '��������', '���������', '�����', '�������');
  BtnTypeToDataAction: array[TButtonType] of set of TDataAction =
  ([daSelect], [daInsert], [daUpdate], [daInsert, daUpdate], [daSelect], [daDelete]);

  SDictionary = '�����������';
  SSystem = '���������';
  SReport = '������';
  SGroupDelim = '|';
  SASC = ' ASC';
  SDESC = ' DESC';
//  SortIgnoreField = 'Caption';

  SBadArg = 'Bad decorator argument';
  SWrongType = 'Wrong decorator type';

{ TColumnManager }

function TColumnManager.GetColWidths(Index: Integer): Integer;
begin
  Result := FColWidths[Index];
end;

{$REGION ' TFormMan '}

function TFormMan.BuildForm(AOwner: TComponent; Entity: TEntity): TBaseChildForm;
begin
  Result := nil;
  if Ord(Entity) >= Length(Items) then
    Result := Items[Ord(Entity) - 100].FormClass.Create(AOwner)
  else if Assigned(Items[Ord(Entity)].FormClass) then
    Result := Items[Ord(Entity)].FormClass.Create(AOwner)
  else
    Exit;
  if Ord(Entity) >= Length(Items) then
    Result.Caption := ReportInfo[Ord(Entity) - 100 - EntityCount].Name
  else
    Result.Caption := EntityName(Entity);
  Result.SetEntity(Entity);
end;

constructor TFormMan.Create;
var
  Ent: TEntity;
  i: Integer;
begin
  SetLength(FItems, EntityCount + ReportCount);
  for Ent := Low(TEntity) to High(TEntity) do
    if ArsenalInstance.EntityAccessible[Ent] and (Ent in WorkingEntities) then
    begin
      Items[Ord(Ent)].Entity := Ent;
      Items[Ord(Ent)].Name := EntityName(Ent);
      if UseDictionary and (Ent in DictionaryEntities {+ SystemEntities}) then
        Items[Ord(Ent)].Name := SDictionary + SGroupDelim + Items[Ord(Ent)].Name;
      if Ent in SystemEntities then
        Items[Ord(Ent)].Name := SSystem + SGroupDelim + Items[Ord(Ent)].Name;
      case Ent of
        xeEquipment: Items[Ord(Ent)].FormClass := TEquipmentForm;
        xePart: Items[Ord(Ent)].FormClass := TPartsForm;
        xeStructure: Items[Ord(Ent)].FormClass := TStructureForm;
        xeInventarisation: Items[Ord(Ent)].FormClass := TInventarisationForm;
        xeBill: Items[Ord(Ent)].FormClass := TBillsForm;
        xeOrganisation: Items[Ord(Ent)].FormClass := TOrganisationForm;
        xeDepartment: Items[Ord(Ent)].FormClass := TDepartmentForm;
        xeWorker: Items[Ord(Ent)].FormClass := TWorkersForm;
        xeWorkPlace: Items[Ord(Ent)].FormClass := TWorkPlacesForm;
      else
        Items[Ord(Ent)].FormClass := TCommonChild;
      end;
    end;
  for i := 0 to ReportCount - 1 do
  begin
    Items[EntityCount + i].Entity := xeReport;
    Items[EntityCount + i].Name := SReport + SGroupDelim + ReportInfo[i].Name;
    Items[EntityCount + i].FormClass := ReportInfo[i].Form;
  end;
end;

function TFormMan.GetForm(Index: TEntity): TBaseChildForm;
begin
  Result := FForms[Index];
end;

{$ENDREGION ' TFormMan '}

{$REGION ' Decors '}

{ TAbstractDecor }

class procedure TAbstractDecor.Populate(Ctl: TWinControl; FormMan: TFormMan);
begin
  Assert(FormMan <> nil, SBadArg);
end;

{ TTreeViewDecor }

class procedure TTreeViewDecor.Populate(Ctl: TWinControl; FormMan: TFormMan);
var
  i, j: Integer;
  sl: TStringList;
  tn: TTreeNode;
  Attach: TNodeAttachMode;
  TV: TTreeView;
begin
  inherited;
  tn := nil;
  sl := TStringList.Create;
  try
    Assert(Ctl is TTreeView, SWrongType);
    TV := Ctl as TTreeView;
    TV.Items.Clear;
    for I := 0 to High(FormMan.Items) do
      if FormMan.Items[i].Entity in ManagableEntities + [xeReport] then
      begin
        sl.Delimiter := SGroupDelim;
        sl.DelimitedText := StringReplace(FormMan.Items[i].Name, ' ', '/', [rfReplaceAll]);
        sl.Text := StringReplace(sl.Text, '/', ' ', [rfReplaceAll]);
        if sl.Count > 1 then
          Attach := naAddChild
        else
          Attach := naAdd;
        j := 0;
        if MatchSubText(sl[j], [SDictionary, SSystem, SReport]) and (sl.Count > 1) then
        begin
          tn := TV.Items.GetFirstNode;
          while Assigned(tn) and not MatchSubText(tn.Text, [sl[j]]) do
            tn := tn.GetNext;
          if not Assigned(tn) then
            tn := TV.Items.AddNode(nil, tn, sl[j],
              Pointer(Byte(TernOP(sl[j] = SSystem, xeOperation,
              TernOP(sl[j] = SReport, xeReport, xeCommon)))), naAdd);
          Inc(j);
        end;
        TV.Items.AddNode(nil, tn, sl[j], Pointer(Byte(Ternop(sl[0] = SReport,
          100 + i, FormMan.Items[i].Entity))), Attach);
        sl.Clear;
      end;
  except
    on EAssertionFailed do ;
  else
    raise;
  end;
  sl.Free;
end;

{$ENDREGION ' Decors '}

{$REGION ' TBaseChildForm '}

procedure TBaseChildForm.SetBtnDown(Btn: TButtonType; DoDown: Boolean);
begin
  if Assigned(Btns[Btn]) then
    Btns[Btn].Down := DoDown;
end;

procedure TBaseChildForm.SetBtnEnabled(Btn: TButtonType; DoEnable: Boolean);
begin
  if Assigned(Btns[Btn]) then
    Btns[Btn].Enabled := DoEnable;
end;

procedure TBaseChildForm.SetEntity(const Value: TEntity);
begin
  FEntity := Value;
  ObtainData;
  DataSource := ArsenalInstance.DS;
end;

procedure TBaseChildForm.Requery;
begin
  DataSet.DisableControls;
  ObtainData;
  ModifyDataSet(DataSet, Entity);
  DataSet.EnableControls;
  ManageColumns(DataSet, DBGrid1);
  Resize;
end;

procedure TBaseChildForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  NeedsSave: Boolean;
begin
  Action := caFree;
  NeedsSave := False;
  if Changed then
    case MessageDlg(Format(rsWishLeave, [Name]), mtConfirmation, mbYesNoCancel, 0) of
      mrYes:
        NeedsSave := True;
      mrCancel:
        Action := caNone;
    end;
  if Action = caFree then
    Stop(NeedsSave);
end;

procedure TBaseChildForm.Start;
begin
  inherited;
  DataSet.AfterOpen := AfterOpen;
end;

procedure TBaseChildForm.Stop(NeedsSave: Boolean = False);
begin
  Dataset.AfterOpen := nil;
  DataSet.Close;
  if NeedsSave then
    (Owner as TMainForm).FileSave1.Execute;
  if Assigned(OnClose2) then
    OnClose2(Self); 
end;

procedure TBaseChildForm.AfterOpen(ADataSet: TDataSet);
begin
  Status.Panels[1].Text := SRecCnt + IntToStr(ADataSet.RecordCount);
end;

procedure TBaseChildForm.ObtainData;
begin
  FDataSet := Manager.GetData(Entity);
end;

procedure TBaseChildForm.OperationChange(Action: TButtonType; Value: Boolean);
var
  DoEnable: Boolean;
begin
  Resize;
  DoEnable := not Inserting and (DataSet.RecordCount > 0);
  SetBtnEnabled(btFilter, not IsUpdating and DoEnable);
  SetBtnEnabled(btDelete, not IsUpdating and DoEnable);
  SetBtnEnabled(btUpdate, DoEnable);
  SetBtnEnabled(btInsert, not IsUpdating);
  SetBtnEnabled(btClear, Inserting or Filtering or IsUpdating);
end;

procedure TBaseChildForm.Prepare;
begin

end;

procedure TBaseChildForm.FormResize(Sender: TObject);
var
  I, BtnCount: Integer;
  bt: TButtonType;
begin
  inherited;
  for i := 0 to Status.Panels.Count - 1 do
    Status.Panels[i].Width := ClientWidth div Status.Panels.Count;
  BtnCount := 0;
  for BT := Low(TButtonType) to High(TButtonType) do
    if Assigned(Btns[BT]) then
      Inc(BtnCount);
  for BT := Low(TButtonType) to High(TButtonType) do
    if Assigned(Btns[BT]) then
      Btns[BT].Width := Min(MaxBtnWd, (Panel1.ClientWidth - BtnCount * 3 * Btns[bt].Margin) div BtnCount);
end;

procedure TBaseChildForm.SetFiltering(const Value: Boolean);
begin
  FFiltering := Value;
  if Value then
    ClearInsert
  else
    ClearFilter;
  SetBtnDown(btFilter, Value);
  OperationChange(btFilter, Value);
end;

procedure TBaseChildForm.SetInserting(const Value: Boolean);
begin
  FInserting := Value;
  if Value then
    ClearFilter;
  SetBtnDown(btInsert, Value);
  OperationChange(btInsert, Value);
end;

procedure TBaseChildForm.SetupButtons;
var
  BtnType: TButtonType;
  Actions: set of TDataAction;
  Action: TDataAction;
  procedure BtnClickSubscribe(ABtnType: TButtonType; Handler: TNotifyEvent);
  begin
    if Assigned(Btns[ABtnType]) then
      Btns[ABtnType].OnClick := Handler;
  end;
begin
  Actions := [];
  for Action := Low(TDataAction) to High(TDataAction) do
    if ArsenalInstance.EntityActionAccessible[Entity, Action] then
      Include(Actions, Action);
  for BtnType := Low(TButtonType) to High(TButtonType) do
  begin
    if Actions * BtnTypeToDataAction[BtnType] <> [] then
    begin
      Btns[BtnType] := TButtonUsed.Create(Self);
      Btns[BtnType].Parent := Panel2;
      Btns[BtnType].AlignWithMargins := True;
      Btns[BtnType].Margin := 2;
      Btns[BtnType].Left := Btns[BtnType].Parent.Width;
      Btns[BtnType].Align := alLeft;
      Btns[BtnType].Width := BtnWd;
      Btns[BtnType].Caption := BtnCaptions[BtnType];
      Btns[BtnType].AllowAllUp := BtnType in [btFilter, btInsert, btUpdate];
      Btns[BtnType].GroupIndex := Ord(Btns[BtnType].AllowAllUp) * (Ord(BtnType) + 1);
      Btns[BtnType].Images := MainFormInstance.ButtonIcons;
      Btns[BtnType].ImageIndex := Ord(BtnType);
    end;
  end;
  BtnClickSubscribe(btFilter, FilterClick);
  BtnClickSubscribe(btInsert, InsertClick);
  BtnClickSubscribe(btUpdate, UpdateClick);
  BtnClickSubscribe(btApply, ApplyClick);
  BtnClickSubscribe(btClear, ClearClick);
  BtnClickSubscribe(btDelete, DeleteClick);
end;

procedure TBaseChildForm.SetUpdating(const Value: Boolean);
begin
  FUpdating := Value;
  if Value then
    ClearFilter;
  SetBtnDown(btUpdate, Value);
  OperationChange(btUpdate, Value);
end;

procedure TBaseChildForm.FilterClick(Sender: TObject);
begin
  Filtering := not Filtering;
end;

procedure TBaseChildForm.InsertClick(Sender: TObject);
begin
  ApplyClick(nil);
  Inserting := not Inserting;
end;

procedure TBaseChildForm.UpdateClick(Sender: TObject);
begin
  ApplyClick(nil);
  IsUpdating := not IsUpdating;
end;

procedure TBaseChildForm.ApplyClick(Sender: TObject);
begin
  if (Inserting or IsUpdating) and not IsInputValid then
    Exit;
  Prepare;  
  if Inserting then
    DoInsert;
  if IsUpdating then
    DoUpdate;    
end;

function TBaseChildForm.IsInputValid: Boolean;
begin
  Result := True;
end;

procedure TBaseChildForm.ClearClick(Sender: TObject);
begin
  Filtering := False;
  Inserting := False;
  IsUpdating := False;
end;

procedure TBaseChildForm.ClearFilter;
begin
  if Filtering then
    Filtering := False;
  DataSet.Filter := '';
  DataSet.Filtered := False;
  //Requery;
  CommonClear;
end;

procedure TBaseChildForm.ClearInsert;
begin
  Inserting := False;
  CommonClear;
end;

procedure TBaseChildForm.CommonClear;
begin

end;

procedure TBaseChildForm.DeleteClick(Sender: TObject);
begin
  if not Confirm(SDel + ' ������ ?') then
    Exit;
  try
    Manager.Delete(Entity, [DataSet.FieldByName('ID').Value]);
  except
    ShowMessage(Format(SFErrorIn, ['��������']));
  end;
  OperationChange(btDelete, False);
  Requery;
//  if not DataSet.Active then
//    DataSet.Open;
//  DataSet.DisableControls;
//  ModifyDataSet(DataSet, Entity);
//  ManageColumns(DataSet, DBGrid1);
//  DataSet.EnableControls;
  Resize;
end;

procedure TBaseChildForm.DoInsert;
begin

end;

procedure TBaseChildForm.DoUpdate;
begin

end;

{$ENDREGION ' TBaseForm '}

{$REGION ' TCommonChild '}

procedure TCommonChild.ClearSG;
//var
//  I: Integer;
begin
  if SG.Rows[DataRowNumber].Text = '' then
  begin
    Inserting := False;
    IsUpdating := False;
    Filtering := False;
  end;
  SG.Rows[DataRowNumber].Clear;
  DBGrid1.SetFocus;
  SG.Invalidate;
  //SetupFilter;
end;

procedure TCommonChild.ComboCloseUp(Sender: TObject);
var
  Combo: TComboUsed absolute Sender;
begin
  Assert(Sender is TComboUsed);
  if VarIsNull(Combo.KeyValue) then
    SG.Cells[Combo.Tag, DataRowNumber] := ''
  else
    SG.Cells[Combo.Tag, DataRowNumber] := Combo.KeyValue;
  SetupFilter;
end;

procedure TCommonChild.CommonClear;
var
  I: Integer;
begin
  inherited;
  ClearSG;
  for I := 0 to High(Daters) do
    Daters[i].Text := '';//EditFormat := EmptyDateFormat;
  for I := 0 to High(Combos) do
    Combos[i].KeyValue := Null;
end;

procedure TCommonChild.DaterClick(Sender: TObject);
begin
  Assert(Sender is TDaterUsed);
  TDaterUsed(Sender).Text := '';//EditFormat := EmptyDateFormat;
end;

procedure TCommonChild.DaterCloseUp(Sender: TObject);
var
  Dater: TDaterUsed absolute Sender;
begin
  Assert(Sender is TDaterUsed);
  if VarIsNull(Dater.Value) then
    SG.Cells[Dater.Tag, DataRowNumber] := ''
  else
    SG.Cells[Dater.Tag, DataRowNumber] := FormatDateTime(DateFormat, Dater.Value);
  SetupFilter;
end;

procedure TCommonChild.InsertCmd;
begin
  Manager.Insert(Entity, [], SG.Rows[DataRowNumber]);
end;

procedure TCommonChild.DoInsert;
begin
  try
    InsertCmd;
  CommonClear; // ClearClick(nil);
  except
    ShowMessage(Format(SFErrorIn, ['�������']) + #13#10 + SCheckFields);
  end;
  if not DataSet.Active then
    DataSet.Open;
end;

procedure TCommonChild.DoUpdate;
begin
  try
    UpdateCmd;
    CommonClear; // ClearClick(nil);
  except
    ShowMessage(Format(SFErrorIn, ['����������']) + #13#10 + SCheckFields);
  end;
  if not DataSet.Active then
    DataSet.Open;
end;

procedure TCommonChild.UpdateCmd;
begin
  Manager.Update(Entity, [DataSet.FieldByName('ID').Value], SG.Rows[DataRowNumber]);
end;

procedure TCommonChild.FormResize(Sender: TObject);
var
  i, J: Integer;
  Wd: Integer;
  Rect: TRect;
  FS1, Fs2: TFormatSettings;
  ADate: TDateTime;
begin
  inherited;
  FS1.ShortDateFormat := ParseDateFormat1;
  FS1.DateSeparator := DSep1;
  FS2.ShortDateFormat := ParseDateFormat2;
  FS2.DateSeparator := DSep2;
  if Managers[Entity].VisibleCols = 0 then
    Wd := MinWd
  else
    Wd := Round(Managers[Entity].Grid.ClientWidth * 0.9) div Managers[Entity].VisibleCols;
  for I := 0 to DBGrid1.Columns.Count - 1 do
    if DBGrid1.Columns[i].Visible then
    begin
      if ArsenalInstance.EntityActionAccessible[Entity, daSelect] then
        SG.ColWidths[i] := Max(MinWd, Clamp(Managers[Entity].ColWidths[i], Wd, Wd));
      for J := 0 to High(Combos) do
        if Combos[J].Tag = i then
        begin
          Rect := SG.CellRect(I, DataRowNumber);
          Combos[J].Left := Rect.Left + 3;
          Combos[J].Top := Rect.Top + 3;
          Combos[J].Width := Rect.Right - Rect.Left;
          Combos[J].Visible := Inserting or Filtering or IsUpdating;
          Combos[J].BringToFront;
          if IsUpdating then
            Combos[J].KeyValue := SG.Cells[I, DataRowNumber];
          ComboCloseUp(Combos[J]);
        end;
      for J := 0 to High(Daters) do
        if Daters[J].Tag = i then
        begin
          Rect := SG.CellRect(I, DataRowNumber);
          Daters[J].Left := Rect.Left + 3;
          Daters[J].Top := Rect.Top + 2;
          Daters[J].Width := Rect.Right - Rect.Left;
          Daters[J].Visible := Inserting or Filtering or IsUpdating;
          Daters[J].BringToFront;
          if IsUpdating then
          begin
            Daters[J].Value := Null;
            if TryStrToDate(SG.Cells[I, DataRowNumber], ADate, FS1) or
              TryStrToDate(SG.Cells[I, DataRowNumber], ADate, FS2) then
              Daters[J].Value := ADate;
          end;
          DaterCloseUp(Daters[J]);
        end;
    end;
end;

function TCommonChild.IsInputValid: Boolean;
const
  CheckCol = 0;
begin
  Result := inherited IsInputValid;
  if Result and UsePascalStyleSysObjectNaming and not IsValidIdent(SG.Cells[CheckCol, DataRowNumber]) then
  begin
    Result := False;
    case Entity of
      xePerson: ShowMessage(Format(SFValueInvalid, ['������']));
      xeJob: ShowMessage(Format(SFValueInvalid, ['Name(���) ������']));
    else
      Result := True;
    end;
  end;
end;

procedure TCommonChild.EntitleGrid;
var
  I: Integer;
begin
  for I := 0 to DBGrid1.Columns.Count - 1 do
    SG.Cells[i, 0] := Russian(DBGrid1.Columns[i].Title.Caption);
  if Entity = xePerson then
    if Inserting then
      SG.Cells[SG.ColCount - 1, 0] := Russian('Password')
    else
      SG.Cells[SG.ColCount - 1, 0] := '';
end;

procedure TCommonChild.OperationChange(Action: TButtonType; Value: Boolean);
var
  I: Integer;
  FieldName: string;
begin
  inherited;
  EntitleGrid;
  SwitchSGEditing(Value);
  if Value then
    SG.ShowCaret;
  if (Action = btUpdate) and Value then
    for I := 0 to DBGrid1.Columns.Count - 1 do
    begin
      FieldName := 'ID_' + DBGrid1.Columns[i].FieldName;
      if not Assigned(DataSet.Fields.FindField(FieldName)) then
        FieldName := DBGrid1.Columns[i].FieldName;
      SG.Cells[i, DataRowNumber] := VarToStrDef(DataSet.FieldValues[FieldName], '');
    end;
end;

procedure TCommonChild.SetupFilter;
const
  andstr = ' and ';
var
  Filter: string;
  I, J: Integer;
begin
  Filter := '';
  if Filtering then
  begin
    DataSet.Filtered := True;
    J := 0;
    for I := 0 to DataSet.Fields.Count - 1 do
      with DataSet.Fields[i] do
        if Visible then
        begin
          if not (DataType in ftNonTextTypes) and (SG.Cells[J, DataRowNumber] <> '') then
//          [ftWideString, ftString, ftInteger, ftDateTime, ftFloat, ftCurrency]
            if (DataType in ftFixedSizeTypes) or (DataType in [ftGuid]) then
              Filter := Filter + FieldName + '=' + SG.Cells[J, DataRowNumber] + andstr
            else
              if Assigned(DataSet.FindField('ID_' + FieldName)) then
                Filter := Filter + 'ID_' + FieldName + '=' + SG.Cells[J, DataRowNumber] + andstr
              else  
                Filter := Filter + FieldName + ' LIKE ' +
                  QuotedStr(SG.Cells[J, DataRowNumber] + '%') + andstr;
          Inc(J);
        end;
    DataSet.Filter := Copy(Filter, 1, Length(Filter) - Length(andstr));
  end;
end;

procedure TCommonChild.SGDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Grid: TGridUsed absolute Sender;
begin
  Assert(Sender is TGridUsed);
  if ARow <> DataRowNumber then
    Exit;
  if (gdSelected in State) or not (IsUpdating or Inserting or Filtering) then
  begin
    Grid.Canvas.Brush.Color := clWhite;
    Grid.Canvas.Pen.Color := clWhite;
    Grid.Canvas.Rectangle(Rect);
  end;
end;

procedure TCommonChild.SGKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN, VK_TAB, VK_ESCAPE, VK_PRIOR..VK_DOWN: Exit;
  else
    SetupFilter;
  end;
end;

procedure TCommonChild.Stop(NeedsSave: Boolean = False);
begin
  inherited; // nothing except derived
end;

procedure TCommonChild.SwitchSGEditing(Value: Boolean);
begin
  if Value then
    SG.Options := SG.Options + [goEditing]
  else
    SG.Options := SG.Options - [goEditing];
end;

procedure TCommonChild.SpecModifying(ADataSet: TDataSet; FldCnt: Integer;
  FieldName: string; IsLookup: Boolean = False);
var
  Combo: TComboUsed;
  Dater: TDaterUsed;
begin
  inherited;
  if faHiddenCol in ADataSet.FieldDefList.FieldByName(FieldName).Attributes then
    Exit;
  if IsLookup then
  begin
    SetLength(Combos, Length(Combos) + 1);
    Combo := TComboUsed.Create(Self);
    Combos[High(Combos)] := Combo;
    Combo.Parent := Panel1;
    Combo.ListField := FLookupField;
    Combo.KeyField := 'ID';
    Combo.ListSource := ArsenalInstance.DS; // it is from lookupDataset from modify
    Combo.Hide;
    Combo.Tag := FldCnt;
    Combo.OnCloseUp := ComboCloseUp;
  end
  else
    if (Pos('Date', FieldName) <> 0) then
    begin
      SetLength(Daters, Length(Daters) + 1);
      Dater := TDaterUsed.Create(Self);
      Daters[High(Daters)] := Dater;
      Dater.Parent := Panel1;
      Dater.Hide;
      Dater.Tag := FldCnt;
      Dater.Font.Size := 9;
      Dater.Text := '';//EditFormat := EmptyDateFormat;
      Dater.OnDropDown := DaterClick;
      Dater.OnChange{OnCloseUp} := DaterCloseUp;
    end;
end;

procedure TCommonChild.Start;
var
  I, J: Integer;
begin
  inherited;
  SetLength(Combos, 0);
  SetLength(Daters, 0);

  ModifyDataSet(DataSet, Entity);

  ManageColumns(DataSet, DBGrid1);

  DBGrid1.DataSource := DataSource;

  SG := TGridUsed.Create(Self);
  SG.Parent := Panel1;
  SG.Align := alTop;
  SG.ColCount := Managers[Entity].VisibleCols;
  SG.DefaultRowHeight := 20;
  SG.RowCount := 2;
  SG.FixedCols := 0;
  SG.FixedRows := 1;
  SG.Height := SG.DefaultRowHeight * SG.RowCount + 6;
  SG.Options := SG.Options + [goTabs, goAlwaysShowEditor];
  J := 0;
  for i := 0 to DataSet.Fields.Count - 1 do
    if DataSet.Fields[i].Visible then
    begin
      SG.ColWidths[J] := Managers[Entity].ColWidths[i];
      Inc(J);
    end;
  SG.OnKeyUp := SGKeyUp;
  SG.OnDrawCell := SGDrawCell;
  SG.ScrollBars := ssNone;
  SetupButtons;

  EntitleGrid;
  Resize;
  ClearClick(nil);
end;

{$ENDREGION ' TCommonChild '}

{$REGION ' TBaseForm '}

procedure TBaseForm.AfterConstruction;
begin
  inherited;
  FColumnManagers := TObjectList.Create;
end;

procedure TBaseForm.BeforeDestruction;
begin
  inherited;
  OnResize := nil;
  FreeAndNil(FColumnManagers);
end;

procedure TBaseForm.FormResize(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to FColumnManagers.Count - 1 do
    ResizeColumns(TColumnManager(FColumnManagers[i]).DataSet);
end;

function TBaseForm.GetManagers(Entity: TEntity): TColumnManager;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to FColumnManagers.Count - 1 do
    if TColumnManager(FColumnManagers[i]).DataSet.Tag = Ord(Entity) then
    begin
      Result := TColumnManager(FColumnManagers[i]);
      Break;
    end;
end;

procedure TBaseForm.ManageColumns(ADataSet: TDataSet; AGrid: TDBGridUsed);
var
  I: Integer;
  ColManager: TColumnManager;
begin
  ColManager := Managers[TEntity(ADataSet.Tag)];
  if not Assigned(ColManager) then
  begin
    ColManager := TColumnManager.Create;
    ColManager.FDataSet := ADataSet;
    ColManager.FGrid := AGrid;
    FColumnManagers.Add(ColManager);
    AGrid.OnDrawColumnCell := DBGridDrawColumnCell;
    AGrid.OnGetBtnParams := GetBtnParams;
    AGrid.OnTitleBtnClick := GridTitleClick;
  end;

  with ColManager, ColManager.DataSet do
  begin
    for i := 0 to Fields.Count - 1 do
      with Fields[i] do
        if Visible then
          Visible :=
            not (((Pos('ID', FieldName) <> 0) and (DataType = ftGuid))
            or MatchSubText(FieldName, ['IsDeleted', 'TestType', 'Color'])
//            or AnsiSameText(SToBeHidden, FieldDefs.Find(FieldName).DisplayName)
//          or AnsiSameText('TestType', FieldName)
//          or (Pos('Color', FieldName) <> 0));
            );
    SetLength(FColWidths, Fields.Count);
    FVisibleCols := 0;
    for i := 0 to Fields.Count - 1 do
    begin
      if Fields[i].Visible then
        Inc(FVisibleCols);
      FColWidths[i] := Fields[i].DisplayWidth;
    end;
  end;
end;

procedure TBaseForm.ModifyDataSet(ADataSet: TDataSet; Entity: TEntity);
var
  i: Integer;
  Field: TStringField;
  FieldName: string;
  LookupEnt: TEntity;
  FldCnt: Integer;
  IsLookup: Boolean;
  BM: TBookMark;
begin
  BM := ADataSet.GetBookMark;
  ADataSet.Close;
  FldCnt := 0;
  ADataSet.Fields.Clear;
  if not ADataSet.FieldDefs.Updated then
    ADataSet.FieldDefs.Update;
  for i := 0 to ADataSet.FieldDefs.Count - 1 do
  begin
    if IsFieldHidden(ADataSet, ADataSet.FieldDefs[I].Name) then
      Continue;
    if ADataSet.FindField(ADataSet.FieldDefs[I].Name) = nil then
      ADataSet.FieldDefs[I].CreateField(ADataSet);
    IsLookup := (Pos('ID_', ADataSet.FieldDefs[i].Name) <> 0);
    if IsLookup then
    try
      FieldName := ADataSet.FieldDefs[i].Name;
      Delete(FieldName, 1, 3); // delete 'ID_'
      ADataSet.FieldDefs.Add(FieldName, ftWideString, 128);
      Field := TWideStringField.Create(nil);
      Field.Size := 128;
      Field.FieldName := FieldName;
      Field.FieldKind := fkLookup;
      Field.KeyFields := ADataSet.FieldDefs[i].Name;
      LookupEnt := TEntity(EnumValue(EntP, FieldName, 'xe'));
      if (Ord(LookupEnt) = -1) and (FieldName = 'CurrentWorkplace') then
        LookupEnt := xeWorkPlace;
      if FieldName = 'Buyer' then
      begin
        ArsenalInstance.Ally := 1;
        Field.LookupDataSet := Manager.GetData(xeOrganisation, [1]);
      end
      else if FieldName = 'Seller' then
      begin
        ArsenalInstance.Ally := 2;
        Field.LookupDataSet := Manager.GetData(xeOrganisation, [0, 'Seller']);
      end
      else
        Field.LookupDataSet := Manager.GetData(LookupEnt);
      FLookupField := 'Caption';
      if not Assigned(Field.LookupDataSet.FindField(FLookupField)) then
        FLookupField := 'Name';
      Field.LookupResultField := FLookupField;
      Field.DisplayWidth := MinWd + 1;
      Field.LookupKeyFields := 'ID';
      Field.DataSet := ADataSet;
    except
      raise;
    end;

    SpecModifying(ADataSet, FldCnt, ADataSet.Fielddefs[i].Name, IsLookup);
    // only after SpecModifying
    if (FieldName = 'Buyer') or (FieldName = 'Seller') then
      ArsenalInstance.Ally := 0;
    if not (AnsiSameText('ID', ADataSet.FieldDefs[i].Name) or
      AnsiSameText('IsDeleted', ADataSet.FieldDefs[i].Name) or
      (faHiddenCol in ADataSet.Fielddefs[i].Attributes))
    then
      Inc(FldCnt);
  end;
  ADataSet.Open;
  AfterModifyDataSet(ADataSet, Entity);
  GotoBookMark(ADataSet, BM);
end;

procedure TBaseForm.AfterModifyDataSet(ADataSet: TDataSet; Entity: TEntity);
begin
// nothing in base class
end;

procedure TBaseForm.ObtainData;
begin
// nothing in base class
end;

procedure TBaseForm.ResizeColumns(ADataSet: TDataSet);
const
  BigCoef = 0.4;
var
  I, Wd, k, SmallCols, MidCols, BigCols, ClWd, Clamped, MaxWd: Integer;
  ColManager: TColumnManager;
  Col: TColumn;
  DS: TDataSet;
  function IsMidCol(Field: TField): Boolean;
  begin
    Result := Field.DataType = ftDateTime;
  end;
begin
  ColManager := Managers[TEntity(ADataSet.Tag)];
  Assert(Assigned(ColManager), '');
  DS := ColManager.DataSet;
  with ColManager do
  begin
    SmallCols := 0;
    MidCols := 0;
    BigCols := 0;
    k := 0;
    ClWd := Grid.ClientWidth - 40;
    MaxWd := Round(ClWd * BigCoef);
    for I := 0 to DS.Fields.Count - 1 do
      if DS.Fields[i].Visible then
        if IsMidCol(DS.Fields[i]) then
          Inc(MidCols)
        else
        begin
          Inc(SmallCols, IfThen(ColWidths[i] < MinWd, 1));
          Inc(MidCols, IfThen(InRange(ColWidths[i], MinWd, ClWd), 1));
          Inc(BigCols, IfThen(ColWidths[i] > ClWd, 1));
        end;
    if (VisibleCols = 0) or (MidCols = 0) then
      Wd := MinWd
    else
      Wd := Max(Round((ClWd * (1 - BigCoef * BigCols) - SmallCols * MinWd) / MidCols), MinWd);
    for I := 0 to DS.Fields.Count - 1 do
      if DS.Fields[i].Visible then
      begin
        Col := Grid.Columns[k];
        Clamped := Clamp(ColWidths[i], MinWd, MaxWd);
        if (Clamped = ColWidths[i]) or IsMidCol(DS.Fields[i]) then
          Col.Width := Wd
        else
          Col.Width := Clamped;
        Col.Title.Caption := Russian(Col.FieldName);
        Inc(k);
      end;
  end;
end;

procedure TBaseForm.SpecModifying(ADataSet: TDataSet; FldCnt: Integer;
  FieldName: string; IsLookup: Boolean = False);
begin
  // nothing in base class
end;

procedure TBaseForm.Start;
begin
  OnResize := FormResize;
end;

procedure TBaseForm.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  DBGrid: TDBGridUsed absolute Sender;
  AText: string;
  IsTime: Boolean;
  Fld: TField;
begin
  if (Sender is TDBGridUsed) and Assigned(Column) and Assigned(Column.Field) then
  begin
    Fld := Column.Field;
    IsTime := (Fld.DataType = ftWideString) and MatchSubText(Fld.FieldName, ['time', 'duration']);
    if (Fld.DataType in [ftWideMemo, ftDate, ftDateTime, ftTime]) or IsTime then
      DBGrid.Canvas.FillRect(Rect)
    else
      Exit;
    if VarIsNull(Fld.Value) then
      Exit;
    if (Fld.DataType = ftWideMemo) then
      AText := Fld.AsString;
    if (Fld.DataType = ftDate) then
      AText := FormatDateTime(DateFormat, Fld.AsDateTime);
    if IsTime then
      AText := Copy(Fld.AsString, 1, 8);
    if (Fld.DataType = ftDateTime) then
      AText := FormatDateTime(DateTimeFormat, Fld.AsDateTime);
    DBGrid.Canvas.TextRect(Rect, Rect.Left, Rect.Top + 2, ' ' + AText);
  end;
end;

procedure TBaseForm.GridTitleClick(Sender: TObject; ACol: Longint; Field: TField);
var
  Grid: TDBGridUsed absolute Sender;
  DS: TADODataSet;
  FieldName: string;
begin
  if not (Sender is TDBGridUsed) then
    Exit;
//  if AnsiSameText(Field.FieldName, SortIgnoreField) then
//    Exit;
  DS := TADODataSet(Grid.DataSource.DataSet);
  if (Field.FieldKind = fkLookup) and Assigned(DS.FindField('ID_' + Field.FieldName)) then
    FieldName := 'ID_' + Field.FieldName
  else
    FieldName := Field.FieldName;
  if DS.Sort = FieldName + SASC then
    DS.Sort := FieldName + SDESC
  else
    DS.Sort := FieldName + SASC;
end;

procedure TBaseForm.GetBtnParams(Sender: TObject; Field: TField; AFont: TFont;
  var Background: TColor; var SortMarker: TSortMarker; IsDown: Boolean);
var
  Grid: TDBGridUsed absolute Sender;
  DS: TADODataSet;
begin
  if not (Sender is TDBGridUsed) then
    Exit;
  DS := TADODataSet(Grid.DataSource.DataSet);
  if Pos(Field.FieldName, DS.Sort) = 0 then
    Exit;
  SortMarker := TernOP(Pos(SASC, DS.Sort) <> 0, smDown, smNone);
  SortMarker := TernOP(Pos(SDESC, DS.Sort) <> 0, smUp, smNone);
end;

{$ENDREGION ' TBaseForm '}

end.
