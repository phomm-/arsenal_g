object DetailsForm: TDetailsForm
  Left = 0
  Top = 0
  Caption = 'Details'
  ClientHeight = 562
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 600
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DetailsPanel: TsPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 129
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sPanel4: TsPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 127
      Align = alClient
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      DesignSize = (
        782
        127)
      object EquipmentTypeList: TsDBLookupComboBox
        Left = 322
        Top = 13
        Width = 186
        Height = 21
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListField = 'Caption'
        ParentFont = False
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = 'Type'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
      end
      object edtInvNumber: TsEdit
        Left = 616
        Top = 13
        Width = 144
        Height = 21
        Anchors = [akTop, akRight]
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.Caption = 'InvNumber'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object edtCaption: TsEdit
        Left = 62
        Top = 13
        Width = 208
        Height = 21
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.Caption = 'Caption'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object cbbVendor: TsDBLookupComboBox
        Left = 322
        Top = 40
        Width = 186
        Height = 21
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListField = 'Caption'
        ParentFont = False
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = 'Vendor'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
      end
      object cbbBill: TsDBLookupComboBox
        Left = 322
        Top = 67
        Width = 186
        Height = 21
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListField = 'Caption'
        ParentFont = False
        TabOrder = 3
        BoundLabel.Active = True
        BoundLabel.Caption = 'Bill'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
      end
      object edtVisualN: TsEdit
        Left = 616
        Top = 40
        Width = 144
        Height = 21
        Anchors = [akTop, akRight]
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.Caption = 'VisualN'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object edtBarcode: TsEdit
        Left = 616
        Top = 67
        Width = 144
        Height = 21
        Anchors = [akTop, akRight]
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.Caption = 'Barcode'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object edtPrice: TsEdit
        Left = 110
        Top = 40
        Width = 112
        Height = 21
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.Caption = 'Price'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object gbCaptionEditor: TsGroupBox
        Left = 1
        Top = 88
        Width = 780
        Height = 38
        Align = alBottom
        Caption = 'Settings'
        TabOrder = 8
        SkinData.SkinSection = 'GROUPBOX'
        object edtSettings: TsMemo
          Left = 2
          Top = 15
          Width = 776
          Height = 21
          Align = alClient
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'Tahoma'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.SkinSection = 'EDIT'
        end
      end
      object edtLifeTime: TsEdit
        Left = 110
        Top = 67
        Width = 112
        Height = 21
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.Caption = 'Lifetime'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = 2171169
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
    end
  end
  object PartsGroupBox: TsGroupBox
    Left = 0
    Top = 129
    Width = 784
    Height = 169
    Align = alClient
    Caption = 'Parts'
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object PartsGrid: TRxDBGrid
      Left = 2
      Top = 41
      Width = 780
      Height = 81
      Align = alClient
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleButtons = True
    end
    object PartsDBNav: TsDBNavigator
      Left = 2
      Top = 122
      Width = 780
      Height = 45
      Align = alBottom
      FullRepaint = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      SkinData.SkinSection = 'TOOLBAR'
      Images = MainForm.DBNavBtns
      VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
    end
    object pnlPart: TsPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 26
      Align = alTop
      TabOrder = 2
      SkinData.SkinSection = 'PANEL'
      object bbnInstallPart: TsSpeedButton
        Left = 667
        Top = 1
        Width = 112
        Height = 24
        Align = alRight
        Caption = 'Install'
        OnClick = bbnInstallPartClick
        SkinData.SkinSection = 'SPEEDBUTTON'
        ImageIndex = 7
        Images = MainForm.ButtonIcons
        ExplicitLeft = 668
      end
      object cbbParts: TsDBLookupComboBox
        Left = 1
        Top = 1
        Width = 666
        Height = 21
        Align = alClient
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = 'Type'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
      end
    end
  end
  object MovingsGroupBox: TsGroupBox
    Left = 0
    Top = 298
    Width = 784
    Height = 216
    Align = alBottom
    Caption = 'Movings'
    TabOrder = 2
    SkinData.SkinSection = 'GROUPBOX'
    object sPanel1: TsPanel
      Left = 2
      Top = 120
      Width = 780
      Height = 94
      Align = alBottom
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object ButtonsPanel: TsPanel
        Left = 1
        Top = 46
        Width = 778
        Height = 47
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object MovingsDBNav: TsDBNavigator
        Left = 1
        Top = 1
        Width = 778
        Height = 45
        Align = alTop
        FullRepaint = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        SkinData.SkinSection = 'TOOLBAR'
        Images = MainForm.DBNavBtns
        VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
      end
    end
    object MovingsGrid: TRxDBGrid
      Left = 2
      Top = 15
      Width = 780
      Height = 105
      Align = alClient
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnColEnter = MovingsGridColEnter
      TitleButtons = True
    end
  end
  object ControlPanel: TsPanel
    Left = 0
    Top = 514
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      784
      48)
    object btnOK: TsBitBtn
      Left = 333
      Top = 14
      Width = 88
      Height = 25
      Anchors = [akBottom]
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = btnOKClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn2: TsBitBtn
      Left = 686
      Top = 14
      Width = 88
      Height = 25
      Anchors = [akRight]
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      Kind = bkAbort
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sSkinProvider1: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'Tahoma'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 136
    Top = 520
  end
  object XPManifest1: TXPManifest
    Left = 64
    Top = 520
  end
end
